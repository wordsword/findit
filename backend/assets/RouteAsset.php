<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class RouteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        '\backend\assets\AppAsset'
    ];
    public $css = [

    ];
    public $js = [
        'js/route.js',
    ];
}
