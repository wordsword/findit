<?php

namespace app\assets;

use yii\web\AssetBundle;

class MaterializeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/materialize.css?v=0.2',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'yii\widgets\PjaxAsset',
        '\macgyer\yii2materializecss\assets\MaterializeAsset',
    ];
    public $js = [];
}

?>