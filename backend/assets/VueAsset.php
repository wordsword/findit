<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class VueAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'vuejs/order.js',
    ];
}