<?php 

namespace backend\assets;

use yii\web\AssetBundle;

class Import2Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        // 'app\assets\MaterializeAsset'
    ];
    public $css = [
       'css/import.css',
    ];
    public $js = [
        // 'js/document-messages.js',
        'js/compressor.js',
        'js/importv2.js?v=0.1',
    ];
}
 ?>