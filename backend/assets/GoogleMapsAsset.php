<?php

namespace backend\assets;

use yii\web\AssetBundle;

class GoogleMapsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'https://maps.googleapis.com/maps/api/js?libraries=places',
        'js/jquery.geocomplete.min.js',
        'js/FullScreenControl.js',
        'js/CircleControl.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
