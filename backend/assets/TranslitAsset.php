<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class TranslitAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/jquery.liTranslit.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}