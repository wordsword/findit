<?php 

namespace backend\assets;

use yii\web\AssetBundle;

class InitialDebtsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'app\assets\MaterializeAsset'
    ];
    public $css = [
        'css/document/return.css',
    ];
    public $js = [
        'js/document-messages.js',
        'js/document/initial-debts.js',
    ];
}
 ?>