<?php 

namespace app\assets;

use yii\web\AssetBundle;

class FundMovementAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        // 'app\assets\MaterializeAsset'
    ];
    public $css = [
        'css/document/fund-movement.css',
    ];
    public $js = [
        'js/document-messages.js',
        'js/document/fund-movement.js',
    ];
}
 ?>