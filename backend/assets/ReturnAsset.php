<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class ReturnAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/document/products.css',
        'css/document/categories.css',
        'css/document/return.css'

        // 'css/label_test.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'app\assets\MaterializeAsset',
        '\backend\assets\Select2MaterializeAsset'
    ];
    public $js = [
        // 'materialize/materialize.min.js',
        'js/document-messages.js',
        'js/document/return.js'
    ];
}
