<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class UpdateOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/document/update-order.css?v=0.1',
        'css/materialize.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\widgets\PjaxAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'newerton\fancybox3\FancyBoxAsset',
        '\backend\assets\Select2MaterializeAsset',
        'app\assets\MaterializeAsset',
        '\backend\assets\DeliveryAsset'
    ];

    public $js = [
        'js/document/update-order-return.js?v=0.6',
        'js/document/update-order.js'
    ];
}
