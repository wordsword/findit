<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class UpdateReturnAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'materialize/materialize.min.css',
        // 'css/label_test.css',
        'css/document/products.css',
        'css/document/categories.css',
        'css/document/return.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'app\assets\MaterializeAsset',
        '\backend\assets\Select2MaterializeAsset',
        '\backend\assets\InputmaskAsset'
    ];
    public $js = [
        // 'materialize/materialize.min.js',
        'js/document-messages.js',
        'js/document/update-order-return.js?v=0.1',
        'js/document/update-return.js'
    ];
}
