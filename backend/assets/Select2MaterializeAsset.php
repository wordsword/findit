<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class Select2MaterializeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/document/select2.css',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\widgets\PjaxAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'kartik\select2\Select2Asset',
        'macgyer\yii2materializecss\assets\MaterializeAsset',
    ];
    public $js = [];
}
