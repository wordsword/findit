<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class PaymentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/document/payment.css'
        // 'css/label_test.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'app\assets\MaterializeAsset',
        '\backend\assets\Select2MaterializeAsset'
    ];
    public $js = [
        'js/document-messages.js',
        'js/document/payment.js'
    ];
}