<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'input-master/css/intlTelInput.min.css',
        'css/nanoscroller.css',
        'css/uagent.css?v=0.5',
        // 'css/document/status-bar.css?v=0.2',
        'css/main.css?v=0.38',
//        'css/theme.css?v=0.1',
//        'css/site.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'newerton\fancybox3\FancyBoxAsset',
        'kartik\grid\GridViewAsset'
    ];
    public $js = [
        'input-master/js/intlTelInput-jquery.min.js',
        'js/jquery.bootstrap-growl.min.js',
        'js/messages.js?v=0.1',
        'js/vue.min.js',
        'js/jquery.cookie.js',
        'js/jquery.nanoscroller.js',
        'js/hammer.min.js',
        'js/jquery.hammer.js',
        'js/jquery.ba-throttle-debounce.min.js',
        // 'js/document.js',
        'js/document-messages.js',
        // 'js/document/update-order.js',
        // 'js/label_test.js',
        'js/images.js',
        'js/main.js?v=0.38',
    ];
}