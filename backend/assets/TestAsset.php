<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class TestAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/test.css',
    ];
    public $depends = [
        '\backend\assets\AppAsset'
    ];
    public $js = [
        'js/test.js'
    ];
}
