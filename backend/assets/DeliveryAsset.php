<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DeliveryAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/i18n/uk.js',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/i18n/ru.js',
        'js/delivery/delivery.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\widgets\PjaxAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\jui\JuiAsset',
        'kartik\select2\Select2Asset',
    ];

}