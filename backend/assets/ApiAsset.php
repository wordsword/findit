<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 */
class ApiAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/api.css',
    ];
    public $depends = [
        '\backend\assets\AppAsset'
    ];
    public $js = [
        'js/api.js?v=0.1'
    ];
}
