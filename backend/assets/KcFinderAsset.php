<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class KcFinderAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/kcFinder.js',
    ];
}