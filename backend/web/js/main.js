$(document).ready(function () {

    var prev_partner_id = null;
    $(document).on('select2:opening', '#partner-select, #checkout-user-id', function (event) {
        prev_partner_id = $(this).val();
        $(this).val(null);
    })

    $(document).on('select2:closing', '#partner-select, #checkout-user-id', function (event) {
        if ((prev_partner_id != null && prev_partner_id != undefined) && ($(this).val() == null || $(this).val() == undefined || $(this).val() == '')) {
            $(this).val(prev_partner_id).trigger('change')
        }
    })

    $(document).on('select2:selecting', '#partner-select, #checkout-user-id', function (event) {
        target = $(event.params.args.originalEvent.target)

        pin = target.is('.pin-partner') || target.parents('.pin-partner').length != 0;

        if (pin) {
            event.preventDefault();
            event.stopPropagation();
        }
        // event.params.prevented = true;
        // console.log(event)
    })

    $(document).on('click', '.pin-partner', function (e) {
        // e.preventDefault();
        // e.stopPropagation();

        select_id = '#' + $(this).closest('ul').attr('id').replace('select2-', '').replace('-results', '');

        // console.log(select_id)

        id = $(this).closest('.partner-item').attr('data-id');
        is_pined = $(this).attr('data-pined') == 1;

        option = $(select_id + ' option[value="' + id + '"]');
        li = $(this).closest('li');


        if (is_pined) {
            partners[id].pined = 0;
            $(this).attr('data-pined', 0);
            $(this).css('color', 'transparent');

            last_pined = $(this).closest('ul').find('.pin-partner[data-pined="1"]').last();

            // console.log(last_pined)

            if (last_pined.length > 0) {
                last_pined_li = last_pined.closest('li');
                last_pined_id = last_pined_li.find('.partner-item').attr('data-id')

                // console.log(last_pined_li)

                last_pined_li.after(li);
                $(select_id + ' option[value="' + last_pined_id + '"]').after(option);
            }
        } else {
            partners[id].pined = 1;
            $(this).attr('data-pined', 1);
            $(this).css('color', 'black');

            $(select_id).prepend(option)
            $(this).closest('ul').prepend(li)

            // console.log(prev_partner_id)
            $(select_id).val(prev_partner_id).trigger('change')
        }

        $.get({
            url: '/admin/user-management/partner/set-pined',
            data: {
                id: id,
                pined: is_pined ? 0 : 1
            }
        })


        // alert('pined')
    })

    $(document).on('click', '.filter-fix', function (e) {
        e.preventDefault();
        state = $(this).attr('data-state');
        // alert('fix-filter');
        if (state != -1)
            $("#is_post-filter").val(state);
        else
            $("#is_post-filter").val('');

        gridID = '#' + $(this).parents('.grid-view').attr('id');
        $(gridID).yiiGridView('applyFilter');
    })
    var sorterActive;

    if ($('.filter-sum-input').length) {

        function showRangerSum(filter) {

            var parent = filter.closest('.ranger-sum-field');
            var inputResult = parent.find('.filter-sum-input');
            var rangerSum = parent.find('.ranger-sum');
            var rangerAction = rangerSum.find('#ranger-action');
            var dataMin = Number(rangerAction.attr('data-min'));
            var dataMax = Number(rangerAction.attr('data-max'));
            var inputMin = rangerSum.find('.sum-input-min');
            var inputMax = rangerSum.find('.sum-input-max');
            var inputMinVal = Number(inputMin.val()).toFixed(2);
            var inputMaxVal = Number(inputMax.val()).toFixed(2);

            if (parent.hasClass('active')) {
                parent.removeClass('active');
                return;
            }

            parent.addClass('active');

            var values = [
                inputMinVal,
                inputMaxVal
            ];

            rangerAction.slider({
                range: true,
                min: dataMin,
                max: dataMax,
                values: values,
                slide: function (event, ui) {
                    inputMin.val(Number(ui.values[0]).toFixed(2));
                    inputMax.val(Number(ui.values[1]).toFixed(2));
                }
            });

            function inputWrite(e) {
                e = e || window.event;
                var input = $(e.target);
                var value = input.val();
                if (value < dataMin) {
                    input.val(dataMin)
                }
                if (value > dataMax) {
                    Number(input.val(dataMax)).toFixed(2)
                }
                if (isNaN(value)) {
                    Number(input.val(dataMin)).toFixed(2)
                }
                rangerAction.slider('values', [
                    inputMin.val(),
                    inputMax.val()
                ]);
                setValue();
            };

            function applyAction() {
                setValue();
                $(inputResult).trigger('change');
            }

            function setValue() {
                var value = Number(inputMin.val()).toFixed(2) + ' - ' + Number(inputMax.val()).toFixed(2);
                $(inputResult).val(value);
            }

            function closeRanger(e) {
                e = e || window.event;
                var target = $(e.target).closest(parent).length;
                if (!target) {
                    hideRanger();
                }
                ;
            }

            function hideRanger() {
                parent.removeClass('active');
                $(rangerSum).off('input', '.sum-input', inputWrite);
                $(rangerSum).off('click', '.applyBtn', applyAction);
                $(document).off('click', closeRanger);
            };

            $(rangerSum).on('input', '.sum-input', inputWrite);
            $(rangerSum).on('click', '.applyBtn', applyAction);
            $(document).on('click', closeRanger);
        };

        $(document).on('click', '.filter-sum-input', function (e) {
            showRangerSum($(this))
        });


    }
    $(document).on('click', '.cancelBtn', function (e) {
        // window.location.href = window.location.origin + window.location.pathname;
        if ($(this).closest('.ranger-sum-field').length > 0) {
            action = $('#ranger-action');
            min = action.attr('data-min');
            max = action.attr('data-max');

            console.log(min)
            console.log(max)

            $(this).closest('td').find('.sum-input-min').val(min)
            $(this).closest('td').find('.sum-input-max').val(max)
        }
        $(this).closest('td').find('input').first().val('');
        $(this).closest('.grid-view').yiiGridView('applyFilter');
    });



    $(document).on('click', '.radio-item', function () {
        $(this).closest('.kv-grid-container').find('td[data-col-seq=' + $(this).closest('td').data('col-seq') + '] .radio-item').removeClass('checked').find('input').val('0');
        $(this).addClass('checked').find('input').val('1');
        $(this).find('input').trigger('change');
    });

    $(document).on('click', '.checkbox-item', function () {
        $(this).toggleClass('checked');
        $(this).find('input').val(Number($(this).hasClass('checked')));
        $(this).find('input').trigger('change');
    });

    /*$(document).on('click','.save-btn',function (event) {
        var data = $(this).closest("form").serialize() + "&save=1";
        if (typeof($(this).attr('data-action')) != 'undefined') {
            data += '&stay=1';
        }
        $.pjax.reload({
            type: "POST",
            container: "#pjax-container",
            scrollTo: false,
            timeout: 3000,
            data: data
        });
    });*/

    $(document).on('click', '.get-product-batches', function (e) {
        e.preventDefault();

        product = $(this).attr('data-product');
        title = $(this).attr('data-title');
        var msg = $(this).attr('data-msg');
        // _content = '';
        $.ajax({
            url: '/admin/main/products/batches',
            data: {
                id: product
            },
            method: 'GET',
            success: function (re) {
                _movs = $(re);
                _movs.find('.date-range-filter').daterangepicker({
                    opens: "center",
                    buttonClasses: "btn btn-md",
                    locale: {
                        format: 'DD.MM.YYYY',
                        separator: " - ",
                        applyLabel: "Применить",
                        cancelLabel: "Отмена",
                        fromLabel: "От",
                        toLabel: "До",
                        customRangeLabel: "Свой",
                        daysOfWeek: [
                            "Вс",
                            "Пн",
                            "Вт",
                            "Ср",
                            "Чт",
                            "Пт",
                            "Сб"
                        ],
                        monthNames: [
                            "Январь",
                            "Февраль",
                            "Март",
                            "Апрель",
                            "Май",
                            "Июнь",
                            "Июль",
                            "Август",
                            "Сентябрь",
                            "Октябрь",
                            "Ноябрь",
                            "Декабрь"
                        ],
                        firstDay: 1
                    },
                    ranges: {
                        'Cегодня': [moment(), moment()],
                        'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                        'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                        'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                        'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                });
                _movs.find('.date-range-filter').on('apply.daterangepicker', function (ev, picker) {
                    range = $(this).val();
                    $.ajax({
                        url: '/admin/main/products/batches',
                        data: {
                            id: product,
                            date_range: range
                        },
                        method: 'GET',
                        success: function (resp) {
                            _movs.find('.product-batch-list').replaceWith($(resp).find('.product-batch-list'));
                        }
                    });
                });
                skDetailInfo(title, msg, _movs);
                // skAlert(_movs, title)
            }
        });
    });

    $(document).on('click', '.get-product-store-movements', function (e) {
        e.preventDefault();

        product = $(this).attr('data-product');
        title = $(this).attr('data-title');
        var msg = $(this).attr('data-msg');

        // _content = '';
        $.ajax({
            url: '/admin/main/products/movements',
            data: {
                product_id: product
            },
            method: 'GET',
            success: function (re) {
                _movs = $(re);
                _movs.find('.date-range-filter').daterangepicker({
                    opens: "center",
                    buttonClasses: "btn btn-md",
                    locale: {
                        format: 'DD.MM.YYYY',
                        separator: " - ",
                        applyLabel: "Применить",
                        cancelLabel: "Отмена",
                        fromLabel: "От",
                        toLabel: "До",
                        customRangeLabel: "Свой",
                        daysOfWeek: [
                            "Вс",
                            "Пн",
                            "Вт",
                            "Ср",
                            "Чт",
                            "Пт",
                            "Сб"
                        ],
                        monthNames: [
                            "Январь",
                            "Февраль",
                            "Март",
                            "Апрель",
                            "Май",
                            "Июнь",
                            "Июль",
                            "Август",
                            "Сентябрь",
                            "Октябрь",
                            "Ноябрь",
                            "Декабрь"
                        ],
                        firstDay: 1
                    },
                    ranges: {
                        'Cегодня': [moment(), moment()],
                        'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                        'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                        'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                        'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                });

                setScroll = function () {
                    _movs.find('.table-wrap').on('scroll', function () {
                        console.log('scroll');
                        if ($(this)[0].scrollHeight - $(this).scrollTop() <= $(this).outerHeight()) {


                            product = _movs.find('.store-filter').attr('data-product');
                            store = _movs.find('.store-filter').prop('value');
                            // alert(3);
                            leftover = $(this).find('tbody tr').last().find('.leftover').text().split(',').join('');

                            range = _movs.find('.date-range-filter').val();

                            record_count = _movs.find('tbody tr').length;
                            total_count = parseInt(_movs.find('tbody').attr('data-total-count'));
                            current_page = parseInt(_movs.find('tbody').attr('data-page'));

                            // page = Math.ceil(record_count / 100);
                            if (record_count < total_count) {
                                current_page = current_page + 1;
                                _movs.find('tbody').attr('data-page', current_page);
                                $.ajax({
                                    url: '/admin/main/products/movements',
                                    data: {
                                        product_id: product,
                                        store_id: store,
                                        date_range: range,
                                        page: current_page,
                                        leftover: leftover
                                    },
                                    method: 'GET',
                                    success: function (resp) {
                                        if ($(resp).find('.empty-movements').length == 0)
                                            _movs.find('tbody').append($(resp).find('tbody tr'));
                                        _movs.find('.sent-info').empty();
                                    }
                                });
                            }
                        }
                    });
                };



                _movs.find('.date-range-filter').on('apply.daterangepicker', function (ev, picker) {
                    product = _movs.find('.store-filter').attr('data-product');
                    store = _movs.find('.store-filter').prop('value');
                    range = $(this).val();
                    $.ajax({
                        url: '/admin/main/products/movements',
                        data: {
                            product_id: product,
                            store_id: store,
                            date_range: range
                        },
                        method: 'GET',
                        success: function (resp) {
                            _movs.find('.products-movement-list').replaceWith($(resp).find('.products-movement-list'));
                            setTimeout(setScroll, 50);
                        }
                    });
                });
                _movs.find('.store-filter').change(function (e) {
                    product = $(this).attr('data-product');
                    store = $(this).prop('value');
                    range = _movs.find('.date-range-filter').val();
                    $.ajax({
                        url: '/admin/main/products/movements',
                        data: {
                            product_id: product,
                            store_id: store,
                            date_range: range
                        },
                        method: 'GET',
                        success: function (resp) {
                            _movs.find('.products-movement-list').replaceWith($(resp).find('.products-movement-list'));
                            setTimeout(setScroll, 50);
                        }
                    });
                });


                setTimeout(setScroll, 50);

                console.log();
                skDetailInfo(title, msg, _movs);
                // skAlert(_movs, title)
            },
            error: showFailMessage
        });
    });

    $(document).on('click', '.get-storage-movements', function (e) {
        e.preventDefault();

        id = $(this).attr('data-id');
        title = $(this).attr('data-title');
        type = $(this).attr('data-type');
        // _content = '';
        $.ajax({
            url: '/admin/main/documents/storage-movements',
            data: {
                id: id,
                type: type
            },
            method: 'GET',
            success: function (re) {
                _movs = $(re);
                _movs.find('.date-range-filter').daterangepicker({
                    locale: {
                        format: 'DD.MM.YYYY'
                    },
                    separator: '-',
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                });
                _movs.find('.date-range-filter').on('apply.daterangepicker', function (ev, picker) {
                    product = _movs.find('.store-filter').attr('data-product');
                    store = _movs.find('.store-filter').prop('value');
                    range = $(this).val();
                    $.ajax({
                        url: '/admin/main/documents/storage-movements',
                        data: {
                            id: id,
                            type: type,
                            date_range: range
                        },
                        method: 'GET',
                        success: function (resp) {
                            _movs.find('.storage-movement-list').replaceWith($(resp).find('.storage-movement-list'));
                        }
                    });
                });
                skAlert(_movs, title)
            }
        });
    });

    $(document).on('click', '.get-user-settlements', function (e) {
        e.preventDefault();

        id = $(this).attr('data-id');
        title = $(this).attr('data-title');
        msg = $(this).attr('data-msg');
        // _content = '';
        $.ajax({
            url: '/admin/user-management/partner/partner-settlements',
            data: {
                id: id
            },
            method: 'GET',
            success: function (re) {
                _settls = $(re);
                _settls.find('.date-range-filter').daterangepicker({
                    locale: {
                        format: 'DD.MM.YYYY'
                    },
                    separator: '-',
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                });

                setScroll = function () {
                    _settls.find('.table-wrap').on('scroll', function () {
                        if ($(this)[0].scrollHeight - $(this).scrollTop() <= $(this).outerHeight()) {
                            // alert(3);
                            leftover = $(this).find('tbody tr').last().find('.leftover').text().split(',').join('');

                            range = _settls.find('.date-range-filter').val();

                            record_count = _settls.find('tbody tr').length;
                            total_count = parseInt(_settls.find('tbody').attr('data-total-count'));
                            current_page = parseInt(_settls.find('tbody').attr('data-page'));

                            // page = Math.ceil(record_count / 100);
                            if (record_count < total_count) {
                                current_page = current_page + 1;
                                _settls.find('tbody').attr('data-page', current_page);
                                $.ajax({
                                    url: '/admin/user-management/partner/partner-settlements',
                                    data: {
                                        id: id,
                                        date_range: range,
                                        page: current_page,
                                        leftover: leftover
                                    },
                                    method: 'GET',
                                    success: function (resp) {
                                        if ($(resp).find('.empty-settls').length == 0)
                                            _settls.find('tbody').append($(resp).find('tbody tr'));
                                        _settls.find('.sent-info').empty();
                                    }
                                });
                            }
                        }
                    });
                }

                _settls.find('.date-range-filter').on('apply.daterangepicker', function (ev, picker) {
                    range = $(this).val();
                    $.ajax({
                        url: '/admin/user-management/partner/partner-settlements',
                        data: {
                            id: id,
                            date_range: range
                        },
                        method: 'GET',
                        success: function (resp) {
                            _settls.find('.user-settlements-list').replaceWith($(resp).find('.user-settlements-list'));
                            _settls.find('.sent-info').empty();
                            setTimeout(setScroll, 50);
                        }
                    });
                });

                _settls.find('.show-partner-emails').on('click', function (e) {
                    // alert('open')
                    item_count = $(this).parent().find('.send-email').length;

                    $(this).parent().find('.dropdown-menu').css('top', (-100 * item_count) + '%')

                    // alert(item_count);
                })

                _settls.find('.send-email').click(function () {
                    range = _settls.find('.date-range-filter').val();
                    email = $(this).attr('data-email');
                    $.ajax({
                        url: '/admin/user-management/partner/partner-settlements',
                        data: {
                            id: id,
                            date_range: range,
                            send: true,
                            email: email
                        },
                        method: 'GET',
                        success: function (resp) {
                            resp = JSON.parse(resp);

                            skAlert("E-mail: " + email, resp.text, null, "uagent-popup")

                            // _settls.find('.sent-info').text(resp.text);

                            // if (resp.success)
                            //     _settls.find('.sent-info').css('color', 'green');
                            // else
                            //     _settls.find('.sent-info').css('color', 'red');

                        }
                    });
                });
                setTimeout(setScroll, 50);
                skDetailInfo(title, msg, _settls);
            }
        });
    });

    $('[data-toggle=tooltip]').tooltip({ trigger: 'hover' });

    onResize();
    if ($('#treeview').length > 0) {
        initDragAndDrop();
    }

    $(document).on('pjax:beforeSend', function (event) {
        $(event.target).find('.kv-grid-container').addClass('loading');
    });

    $(document).on('pjax:beforeReplace', function () {
        sorterActive = $('.grid-view .sorter').hasClass('active');
    });

    $(document).on('pjax:success', function () {
        initDragAndDrop();
        initSortable();
        toggleDragAndDrop(sorterActive);
    });

    $(document).on('pjax:error', function (e) {
        e.preventDefault();
    });

    function refreshGrid(pjaxID, gridID) {
        $.pjax.reload({ container: pjaxID });
        hideLoading(gridID);
    }

    function checkDefLanguage($this) {
        if ($this.parents('.grid-view').attr('id') == 'language-grid' && $this.parents('tr').hasClass('default')) {
            $th = $this.parents('.grid-view').find('.action-column-header');
            skAlert($th.data('error'), $th.data('error-title'), null, 'alert-danger');
            return false;
        }
        if ($this.parents('.grid-view').attr('id') == 'page-grid' && $this.parents('tr').hasClass('default')) {
            $th = $this.parents('.grid-view').find('.action-column-header');
            skAlert($th.data('error'), $th.data('error-title'), null, 'alert-danger');
            return false;
        }
        return true;
    }

    $(document).on('click', '.grid-view a.licenses', function (e) {
        e.preventDefault();
        var $this = $(this);

        $table = $this.attr('data-table');
        $primaryKey = $this.attr('data-primary-key')

        $.get({
            url: $this.attr('href'),
            success: function (re) {
                $dialog = $(re);
                skAlert($dialog, $this.attr('data-dialog-title'), function () {
                    $data = $dialog.find(':checkbox').serialize();
                    $.post({
                        url: '/admin/main/documents/set-licenses?table_name=' + $table + '&primaryKey=' + $primaryKey,
                        data: $data,
                        success: function (data) {
                            refreshGrid(
                                '#' + $this.parents('[id^=pjax-container]').attr('id'),
                                '#' + $this.parents('.grid-view').attr('id')
                            );
                            refreshTree('#' + $('#treeview').parent().attr('id'));
                        },
                        error: showFailMessage
                    })
                }, null);
            }
        })
    });

    $(document).on('click', '.grid-view a.uagent-user-licenses', function (e) {
        e.preventDefault();
        var _this = $(this);

        $primaryKey = _this.attr('data-primary-key')

        $.get({
            url: _this.attr('href'),
            success: function (re) {
                $dialog = $(re);
                skAlert($dialog, _this.attr('data-dialog-title'), function () {
                    $data = $dialog.find(':checkbox').serialize();
                    $.post({
                        url: '/admin/uagent/user/set-licenses?primaryKey=' + $primaryKey,
                        data: $data,
                        success: function (data) {
                            refreshGrid(
                                '#' + _this.parents('[id^=pjax-container]').attr('id'),
                                '#' + _this.parents('.grid-view').attr('id')
                            );
                        },
                        error: showFailMessage
                    })
                }, null);
            }
        })
    });

    $(document).on('click', '.grid-view a.user-licenses', function (e) {
        e.preventDefault();
        var _this = $(this);

        $primaryKey = _this.attr('data-primary-key')

        $.get({
            url: _this.attr('href'),
            success: function (re) {
                $dialog = $(re);
                skAlert($dialog, _this.attr('data-dialog-title'), function () {
                    $data = $dialog.find(':checkbox').serialize();
                    $.post({
                        url: '/admin/user-management/user/set-licenses?primaryKey=' + $primaryKey,
                        data: $data,
                        success: function (data) {
                            refreshGrid(
                                '#' + _this.parents('[id^=pjax-container]').attr('id'),
                                '#' + _this.parents('.grid-view').attr('id')
                            );
                        },
                        error: showFailMessage
                    })
                }, null);
            }
        })
    });

    $(document).on('click', '.grid-view a.tracked-licenses', function (e) {
        e.preventDefault();
        var _this = $(this);

        if (_this.attr('data-has-export-groups') !== undefined) {
            dialogAlert(_this.attr('data-error-msg'), _this.attr('data-error-title'));
        } else {
            $license_number = _this.attr('data-license-number')

            $.get({
                url: _this.attr('href'),
                success: function (re) {
                    $dialog = $(re);
                    skAlert($dialog, _this.attr('data-dialog-title'), function () {
                        $data = $dialog.find(':checkbox').serialize();
                        $.post({
                            url: '/admin/uagent/licenses/set-licenses?license=' + $license_number,
                            data: $data,
                            success: function (data) {
                                refreshGrid(
                                    '#' + _this.parents('[id^=pjax-container]').attr('id'),
                                    '#' + _this.parents('.grid-view').attr('id')
                                );
                            },
                            error: showFailMessage
                        })
                    }, null);
                }
            })
        }
    });

    $(document).on('click', '.grid-view a.delete', function (e) {
        e.preventDefault();
        var $this = $(this);
        if (checkDefLanguage($this)) {
            skConfirmAction($this.data('confirm-msg'), function (result) {
                $.post({
                    url: $this.attr('href'),
                    data: {},
                    success: function (data) {
                        refreshGrid(
                            '#' + $this.parents('[id^=pjax-container]').attr('id'),
                            '#' + $this.parents('.grid-view').attr('id')
                        );
                        refreshTree('#' + $('#treeview').parent().attr('id'));
                    },
                    error: showFailMessage
                });
            }, null, $this.data('msg'));
        }
    });

    $(document).on('click', '.order-pay-btn', function (e) {
        e.preventDefault();

        $dialog = $('.payment-form.hidden').clone();

        $dialog.removeClass('hidden');
        $dialog.find('.id').prop('value', parseInt($(this).attr('data-id')));

        $dialog.find('.summ').change(function (e) {
            if (isNaN(parseFloat($(this).val())) || parseFloat($(this).val()) < 0)
                $(this).val($(this).attr('data-prev'));

            $(this).attr('data-prev', $(this).val());
        });
        skAction($(this).attr('data-title'), $dialog, function () {
            $form = $dialog.find('form');
            if (parseFloat($form.find('.summ').val()) > 0) {
                $.post({
                    url: $form.attr('action'),
                    data: $form.serialize()
                });
            }
            $.fancybox.close();
        }, function () {
            $.fancybox.close();
        }, function () {
            $dialog.find('select').formSelect();
        }
        );
    });

    $(document).on('click', '#treeview a.delete, #treeview a.btnvis', function (e) {
        e.preventDefault();
        var $this = $(this);
        skConfirmAction($this.data('confirm-msg'), function () {
            $.post({
                url: $this.attr('href'),
                data: {},
                success: function (data) {
                    var $data = $('<div/>').html(data);
                    if ($data.find('#success').length > 0) {
                        refreshTree('#' + $('#treeview').parent().attr('id'));
                    }
                },
                error: showFailMessage
            });
        }, null, $this.data('msg'));
    });

    $(document).on('click', '.grid-view a.btnvis', function (e) {
        e.preventDefault();
        var $this = $(this);
        if (checkDefLanguage($this)) {
            skConfirmAction($this.data('confirm-msg'), function (result) {
                $.post($this.attr('href'), {},
                    function (data) {
                        refreshGrid(
                            '#' + $this.parents('[id^=pjax-container]').attr('id'),
                            '#' + $this.parents('.grid-view').attr('id')
                        );
                    }
                );
            }, null, $this.data('msg'));
        }
    });

    $(document).on('change', '.grid-view .checkbox-wrap input[type="checkbox"]', function () {
        if ($('.grid-view .checkbox-wrap input:checked').length > 0) {
            $('.del-all').removeClass('disabled');
            $('.fix-all').removeClass('disabled');
            $('.unfix-all').removeClass('disabled');
        } else {
            $('.del-all').addClass('disabled');
            $('.fix-all').addClass('disabled');
            $('.unfix-all').addClass('disabled');
        }
    });

    $(document).on('click', '.del-all', function () {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');
        if (keys.length > 0) {
            var $this = $(this);
            $msg = $this.data('confirm-msg').replace('{n}', keys.length);
            skConfirm($msg, function () {
                $.ajax({
                    type: "POST",
                    url: $this.data('href'),
                    data: { keys: keys },
                    success: function (data) {
                        showLoading('#' + $this.parents('.grid-view').attr('id'));
                        $.get(location.href, {}, function (data) {
                            refreshGrid(
                                '#' + $this.parents('[id^=pjax-container]').attr('id'),
                                '#' + $this.parents('.grid-view').attr('id')
                            );
                            if ($('#treeview').length > 0) {
                                refreshTree('#' + $('#treeview').parent().attr('id'));
                            }
                        });
                    }
                });
            });
        }
    });

    $(document).on('click', '#update-translate', function (e) {
        e.preventDefault();
        var $this = $(this);
        showLoading('#' + $this.parents('.grid-view').attr('id'));
        $.ajax({
            type: "POST",
            url: $this.attr('href'),
            success: function (data) {
                refreshGrid(
                    '#' + $this.parents('[id^=pjax-container]').attr('id'),
                    '#' + $this.parents('.grid-view').attr('id')
                );
            }
        });
    });

    $(document).on('click', '.uncheck-all', function () {
        $('#treeview').find('.chtrigger')
            .removeClass('checked')
            .removeClass('fa-check-square-o')
            .addClass('fa-square-o');
        $('#treeview').find('input').removeClass('checked');
        $('.grid-view').yiiGridView('applyFilter');
    });

    $(".nano").nanoScroller();

    $('#toggle-sidebar').click(function () {
        $("#sidebar-left").toggleClass('open closed');
        $('.navbar-brand-wrap').toggleClass('open');
        $(".wrap").toggleClass('wide');
        $(".breadcrumbs-wrap").toggleClass('wide');
        $('body').css('min-height', $('#sidebar-left').height());
        if ($(window).width() > 768) {
            if ($('.navbar-brand-wrap').hasClass('open')) {
                // alert(1);
                setTimeout(function () {
                    $('#sidebar-left .dropdown.active .dropdown-toggle').trigger('click');
                }, 100);
            }
            $.cookie('toggle-sidebar', $('.navbar-brand-wrap').hasClass('open'), {
                expires: 365,
                path: '/'
            });
        } else {
            $.cookie('toggle-sidebar', false, {
                expires: 365,
                path: '/'
            });
        }
    });

    function myPanHandler(ev) {
        if (ev.type == 'panleft' && $("#sidebar-left").hasClass('open')
            || ev.type == 'panright' && !$("#sidebar-left").hasClass('open')) {
            $('#toggle-sidebar').trigger('click');
        }
    }

    $('#sidebar-left').hammer({
        threshold: 30
    }).bind("panright panleft", $.debounce(200, myPanHandler));

    if ($(window).width() > 768) {
        if ($("#sidebar-left").hasClass('closed')) {
            $('body').css('min-height', $('#sidebar-left').height());
        }
    }

    $('#sidebar-left .dropdown').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(100);
    });
    $('#sidebar-left .dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
    });

    if ($("#sidebar-left").hasClass('closed')) {
        $('#sidebar-left .dropdown.active').removeClass('open');
    }

    function onResize() {
        var toolbarWidth,
            headingWidth,
            titleWidth,
            blockWidth,
            innerWidth;
        if ($(window).width() <= 768) {
            $("#sidebar-left").addClass('closed').removeClass('open');
            $('.navbar-brand-wrap').removeClass('open');
            $(".wrap").addClass('wide');
            $(".breadcrumbs-wrap").addClass('wide');
            $.cookie('toggle-sidebar', false, {
                expires: 365,
                path: '/'
            });
        }
        $('body').css('min-height', 100);
        if (!$('aside').hasClass('open')) {
            $('body').css('min-height', $('#sidebar-left').height());
        }
        $('.panel').each(function () {
            headingWidth = $(this).find('.panel-heading').width();
            titleWidth = $(this).find('.panel-title').width();
            if ($(this).find('.toolbar').length > 0) {
                toolbarWidth = $(this).find('.toolbar span').width();
            } else {
                toolbarWidth = $(this).find('.btn-toolbar span').width();
            }
            if ((titleWidth + toolbarWidth + 50) > headingWidth) {
                $(this).find('.btn-toolbar').addClass('wide');
                $(this).find('.toolbar').addClass('wide');
            } else {
                $(this).find('.btn-toolbar').removeClass('wide');
                $(this).find('.toolbar').removeClass('wide');
            }
        });
        $('.category-wrap').each(function () {
            blockWidth = $(this).width();
            innerWidth = $(this).find('.chtrigger').width();
            innerWidth += $(this).find('.link-text span').width();
            innerWidth += $(this).find('.action-buttons').width();
            if ($(this).find('.action-buttons .counter').is(':hidden')) {
                innerWidth += 25;
            }
            innerWidth += $(this).find('.extrigger').width();

            if (innerWidth > blockWidth) {
                $(this).addClass('wide');
            } else {
                $(this).removeClass('wide');
            }
        });
    }

    $(window).resize($.debounce(350, onResize));

    function initSortable() {
        // if ($(document).find('.profile-form-wrapper').length > 0 || $(document).find('.grid-view .sorter').length <= 0) {
        //     return;
        // }
        if ($(document).find('.profile-form-wrapper').length > 0) {
            return;
        }
        if ($(".grid-view").length > 0) {
            var widthArray = {};
            $(".grid-view tbody").sortable({
                axis: "y",
                placeholder: "ui-state-highlight",
                create: function () {
                    $(".grid-view").each(function () {
                        var tmpArr = [];
                        $(this).find('tbody tr:first td').each(function (index) {
                            tmpArr[index] = $(this).width();
                        });
                        widthArray[$(this).attr('id')] = tmpArr;
                    });
                },
                start: function (event, ui) {
                    var id = $(ui.item).closest('.grid-view').attr('id');
                    $(ui.item).closest('.grid-view').find('tbody tr').each(function (index) {
                        $(this).find('td').each(function (index) {
                            $(this).width(widthArray[id][index]);
                        });
                    });
                },
                stop: function (event, ui) {
                    var next = null;
                    var prev = null;
                    var curr = $(ui.item).data('key');
                    if ($(ui.item).next().length > 0) {
                        next = $(ui.item).next().data('key');
                    } else if ($(ui.item).prev().length > 0) {
                        prev = $(ui.item).prev().data('key');
                    }
                    onGridSort(curr, prev, next);
                }
            });
            $(".grid-view tbody").find(".grid-view tbody").sortable('destroy');
            // $(".grid-view tbody").disableSelection();
            $(".grid-view tbody.ui-sortable").sortable("disable");
        }
    }

    initSortable();

    function initDragAndDrop() {
        if ($('.profile-form-wrapper').length > 0) {
            return;
        }
        $('.grid-view tbody tr').draggable({
            cursor: 'default',
            helper: function () {
                return '<div class="drag-icon-wrapper"><i class="drag-icon fa fa-th-list"></i></div>';
            },
            cursorAt: { right: 0, bottom: 0 },
            start: function () {
                var keys = $('.grid-view').yiiGridView('getSelectedRows');
                if (keys.length == 0) {
                    $(this).addClass('start-drag');
                }
            },
            stop: function () {
                var keys = $('.grid-view').yiiGridView('getSelectedRows');
                if (keys.length == 0) {
                    $(this).removeClass('start-drag');
                }
            }
        });
        $('.grid-view tbody tr').draggable("disable");
        $('#treeview li div').droppable({
            hoverClass: 'droppable-hover',
            drop: function (event, ui) {
                var draggable = ui.draggable;
                var recid = $(this).parent().attr('recid');
                var keys = $('.grid-view').yiiGridView('getSelectedRows');
                if (keys.length == 0) {
                    keys[0] = draggable.attr('data-key');
                }
                if (draggable.parents('#treeview').length > 0) {
                    return;
                }
                $.post($('input[name="changeCategory"]').val(),
                    {
                        category: recid,
                        keys: keys
                    },
                    function (data) {
                        refreshGrid('#' + $('.grid-view').parents('[id^=pjax-container]').attr('id'));
                        refreshTree('#' + $('#treeview').parent().attr('id'), true);
                    }
                );
            }
        });
        $('#treeview li div').droppable("disable");
    }

    function toggleDragAndDrop(active) {
        if ($('.profile-form-wrapper').length > 0) {
            return;
        }
        if (active) {
            $('.grid-view .sorter').addClass('active');
            $('.grid-view tbody tr').draggable('enable');
            $('#treeview li div').droppable('enable');
        } else {
            $('.grid-view tbody tr').draggable('disable');
            $('#treeview li div').droppable('disable');
        }
    }

    $(document).on('click', '.grid-view .sorter', function () {
        $(this).toggleClass('active');
        toggleDragAndDrop($(this).hasClass('active'));
    });
    // if ($(".grid-view tbody").length > 0) {
    //     initGridSorting();
    //     $(document).on('click', '.sortable', function () {
    //         $(this).toggleClass('active');
    //         if ($(this).hasClass('active')) {
    //             $(this).parents('.grid-view').find('tbody').sortable('enable');
    //         } else {
    //             $(this).parents('.grid-view').find('tbody').sortable('disable');
    //         }
    //     });
    // }

    if ($(".grid-view").length > 0) {
        $(document).on('click', '.sortable', function () {
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).parents('.grid-view').find('tbody.ui-sortable').sortable('enable');
            } else {
                $(this).parents('.grid-view').find('tbody.ui-sortable').sortable('disable');
            }
        });
    }

    var local_country = null;

    local_country = "ua";

    function phoneMask(selector) {
        $(document).find(selector).each(function () {
            if ($(this).length > 0) {

                var elem = $(this);
                elem.intlTelInput({
                    allowExtensions: true,
                    defaultCountry: (local_country !== null) ? local_country : "ua",
                    initialCountry: (local_country !== null) ? local_country : "ua",
                    preferredCountries: ["ua", "ru", "by", 'pl', 'md', 'tr'],
                    utilsScript: "/input-master/js/utils.min.js",
                    separateDialCode: true
                });

                if (local_country !== null) {
                    if (local_country.toUpperCase() === 'UA') {
                        setTimeout(function () {
                            elem.attr('placeholder', '67 123 4567');
                        });
                    }
                }

                elem.on("countrychange", function (e, countryData) {
                    var countryCode = elem.intlTelInput('getSelectedCountryData')['iso2'];
                    if (countryCode.toUpperCase() === 'UA') {
                        setTimeout(function () {
                            elem.attr('placeholder', '67 123 4567');
                        });
                    }
                });

                elem.parents('form').on('submit', function () {
                    var dialCode = elem.intlTelInput('getSelectedCountryData')['iso2'];
                    elem.parents('form').find('#country').val(dialCode);
                });
            }
        });
    }

    $(document).on('click', '.save-btn', function (event) {
        event.preventDefault();
        var data = $(this).closest("form").serialize() + "&save=1";
        if (typeof ($(this).attr('data-action')) != 'undefined') {
            data += '&stay=1';
        }
        var dialCode = $(document).find('#user-phone').intlTelInput('getSelectedCountryData')['iso2'];
        $(this).closest("form").find('#country').val(dialCode);
        $.pjax.reload({
            type: "POST",
            container: "#pjax-container",
            scrollTo: false,
            timeout: 20000,
            data: data
        });
    });

    phoneMask('#user-phone, #loginform-user_phone');

    $(document).on('pjax:success', function (event, data, status, xhr, options) {
        phoneMask('#user-phone, #loginform-user_phone');
    });

    $(document).on('click', '.header-btn-icon', function () {
        $(this).toggleClass('active');
    });


    $(document).on('click', '.sort-results__dropdown-item a', function (e) {
        e.preventDefault();
        var link = $(this);
        var container = link.parent('.sort-results__dropdown-item').attr('data-pjax-id');
        $.pjax({
            container: container,
            url: link.attr('href'),
            push: true,
            type: "GET",
            target: link.parent('.sort-results__dropdown-item')
        });
    });

    $(document).on('pjax:success', function (event, data, status, xhr, options) {
        if ($(options.target).hasClass('sort-results__dropdown-item')) {
            var target = $(options.target).attr('data-target')
            var sorter = $(document).find('#' + target + '.hidden').clone();
            sorter.removeClass('hidden');
            $(document).find('#' + target + ':not(hidden)').replaceWith(sorter);
        }
    });


    $(document).on('click', '.filter-action', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.filters-sidebar').toggleClass('active');
    });

    $(document).on('click', '.close-filters-sidebar', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.filters-sidebar').removeClass('active');
    });

    $('#documentSettings').on('show.bs.collapse', function () {
        $(this).closest('.document-option-row').addClass('show-settings');
    });

    $("#documentSettings").on('hide.bs.collapse', function () {
        $(this).closest('.document-option-row').removeClass('show-settings');
    });


    /*$(".action-column .dropdown").on("show.bs.dropdown", function (event) {

        if($(window).width()>767) {
            var $this = $(event.relatedTarget),
                parent_tr = $this.closest('tr'),
                drop_menu = $this.siblings('.dropdown-menu'),
                drop_menu_height = drop_menu.height();

            if (parent_tr.is(":last-child")) {
                drop_menu.css({
                    'top': 'auto',
                    'bottom': '100%',
                });
            } else {

                var after_height = 0,
                    row_height = parent_tr.height(),
                    need_height = drop_menu_height;

                parent_tr.nextAll('tr').each(function () {
                    after_height += $(this).height();
                });

                if (after_height < need_height) {
                    drop_menu.css({
                        'top': 'auto',
                        'bottom': '100%',
                    });
                }
            }
        }
    });*/


    $(document).on('click', '.fix-all, .unfix-all', function (e) {
        e.preventDefault();
        _href = $(this).attr('href');
        $this = $(this);

        $msg = $this.data('confirm-msg');
        $count = $('.fix-check:checked').length;

        $msg = $msg.replace('{n}', $count);

        skConfirm($msg, function () {
            $.post({
                url: _href,
                data: $('.fix-check').serialize(),
                success: function (re) {
                    refreshGrid(
                        '#' + $this.parents('[id^=pjax-container]').attr('id'),
                        '#' + $this.parents('.grid-view').attr('id')
                    );
                }
            });
        });
    });



    // example use


});

// function initGridSorting() {
//     $(".grid-view tbody").sortable({
//         placeholder: "ui-state-highlight",
//         start: function (event, ui) {
//             var widthArray = [];
//             $(ui.item).parent().find('tr').each(function () {
//                 if ($(this).data('key') !== $(ui.item).data('key')) {
//                     $(this).find('td').each(function (index, element) {
//                         widthArray[index] = $(this).width();
//                     });
//                     return false;
//                 }
//             });
//             $(ui.item).find('td').each(function (index, element) {
//                 $(this).width(widthArray[index]);
//             });
//         },
//         stop: function (event, ui) {
//             var next = null;
//             var prev = null;
//             var curr = $(ui.item).data('key');
//             if ($(ui.item).next().length > 0) {
//                 next = $(ui.item).next().data('key');
//             } else if ($(ui.item).prev().length > 0) {
//                 prev = $(ui.item).prev().data('key');
//             }
//             onGridSort(curr, prev, next);
//         }
//     });
//     $(".grid-view tbody").disableSelection();
//     $(".grid-view tbody").sortable("disable");
// }

function showLoading(selector) {
    $(selector).addClass('loading');
}

function hideLoading(selector) {
    $(selector).removeClass('loading');
}

function refreshTree(treeID, onlyCounters) {
    var params = location.search;
    var url = location.href;
    if (params === '') {
        url += '?_pjax=' + encodeURIComponent(treeID);
    } else {
        url += '&_pjax=' + encodeURIComponent(treeID);
    }
    // Эмулируем PJAX запрос
    $.ajax({
        type: 'GET',
        url: url,
        headers: {
            "X-PJAX": true,
            "X-PJAX-Container": treeID
        }
    }).done(function (data) {
        showLoading(treeID);
        var $data = $('<div/>').html(data);
        if (onlyCounters == true) {
            $.each($('#treeview .counter'), function (index, element) {
                $(this).replaceWith($data.find('.counter[recid=' + $(this).attr('recid') + ']'));
            });
        } else {
            $('#treeview').replaceWith($data.find('#treeview'));
            $('#treeview').skTreeView('refresh');
        }
        hideLoading(treeID);
    });
}


$("#open_pass").click(function (event) {
    event.preventDefault();
    $("#open_pass").css("display", 'none');
    $(".pass_set").css('overflow', 'visible');
});


function checkCurr(input) {
    if (window.event) {
        if (event.keyCode == 37 || event.keyCode == 39) return;
    }
    input.value = input.value.replace(/[^\d]/g, '');
};
// Функція відображення помилки отриманої від сервера
// ==================================================

function showFailMessage(jqXHR, exception) {

    // Our error logic here
    console.log(jqXHR);

    var msg = '';
    if (jqXHR.status === 0) {
        msg = http_errors['Not connect. Verify Network.'];
    } else if (jqXHR.status == 301 || jqXHR.status == 302) {
        return;
    } else if (jqXHR.status == 404) {
        msg = http_errors['Requested page not found.'];
    } else if (jqXHR.status == 403) {
        msg = http_errors['Forbidden'];
    } else if (jqXHR.status == 500) {
        msg = http_errors['Internal Server Error.'];
    } else if (jqXHR.status == 400) {
        msg = http_errors['Bad Request.'];
    } else if (exception === 'parsererror') {
        msg = http_errors['Requested JSON parse failed.'];
    } else if (exception === 'timeout') {
        msg = http_errors['Time out error.'];
    } else if (exception === 'abort') {
        msg = http_errors['Ajax request aborted.'];
    }
    else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }

    $.fancybox.close();

    skAlert(jqXHR.responseText, msg, null, 'uagent-popup');

}