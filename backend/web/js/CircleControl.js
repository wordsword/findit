/// <reference path="../typings/google.maps.d.ts" />
function googleMapButton(text, className) {
    "use strict";
    var controlDiv = document.createElement("div");
    controlDiv.className = className;
    controlDiv.index = 2;
    controlDiv.style.padding = "10px";
    // set CSS for the control border.
    var controlUi = document.createElement("div");
    controlUi.style.backgroundColor = "rgb(255, 255, 255)";
    controlUi.style.borderBottomLeftRadius = '2px';
    controlUi.style.borderBottomRightRadius = '2px';
    controlUi.style.borderTopLeftRadius = '2px';
    controlUi.style.borderTopRightRadius = '2px';
    controlUi.style.color = "#565656";
    controlUi.style.cursor = "pointer";
    controlUi.style.textAlign = "center";
    controlUi.style.boxShadow = "rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px";
    controlDiv.appendChild(controlUi);
    // set CSS for the control interior.
    var controlText = document.createElement("div");
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "11px";
    controlText.style.paddingTop = "8px";
    controlText.style.paddingBottom = "8px";
    controlText.style.paddingLeft = "8px";
    controlText.style.paddingRight = "8px";
    controlText.innerHTML = text;
    controlUi.appendChild(controlText);
    $(controlUi).on("mouseenter", function () {
        controlUi.style.backgroundColor = "rgb(235, 235, 235)";
        controlUi.style.color = "#000";
    });
    $(controlUi).on("mouseleave", function () {
        controlUi.style.backgroundColor = "rgb(255, 255, 255)";
        controlUi.style.color = "#565656";
    });
    return controlDiv;
}
function CircleControl(map, rInput, marker, showLabel, hideLabel) {
    "use strict";
    if (showLabel === void 0) { showLabel = null; }
    if (hideLabel === void 0) { hideLabel = null; }
    if (showLabel == null) {
        showLabel = "Показать радиус";
    }
    if (hideLabel == null) {
        hideLabel = "Cкрыть радиус";
    }
    var controlDiv = googleMapButton(showLabel, "circleControl");
    var isShowing = false;
    var circle = new google.maps.Circle({
        map: map,
        radius: Number(rInput.val()),
        fillColor:"#ffeff3",
        fillOpacity:0.4, 
        strokeColor:'#9c4358',
        strokeOpacity: 0.4,
        strokeWeight: 2,
        editable: true,
        visible: false
    });
    circle.bindTo('center', marker, 'position');
    controlDiv.circle = circle;
    google.maps.event.addListener(circle, "radius_changed", function () {
        if (Math.round(circle.getRadius()) > 50000) {
            circle.setRadius(50000);
        }
        if( Math.round(circle.getRadius()) < 100) {
            circle.setRadius(100);
        }
        rInput.val(Math.round(circle.getRadius()));
        rInput.parent().find('span input').val(Math.round(circle.getRadius()));
    });
    
    controlDiv.showCircle = function () {
        circle.setVisible(true);
        jQuery(controlDiv).find("div div").html(hideLabel);
        isShowing = true;
    };
    controlDiv.hideCircle = function () {
        circle.setVisible(false);
        jQuery(controlDiv).find("div div").html(showLabel);
        isShowing = false;
    };
    // setup the click event listener
    google.maps.event.addDomListener(controlDiv, "click", function () {
        if (!isShowing) {
            controlDiv.showCircle();
        }
        else {
            controlDiv.hideCircle();
        }
    });
    return controlDiv;
}
