dialogConfirm = function (message, title, ok, cancel, closeOnOk) {
    var event;
    var $confirm = $('.confirm-form-wrap').clone();
    $confirm.removeClass('hidden');
    if (closeOnOk === undefined) {
        closeOnOk = true;
    }
    $confirm.find('.content').html(message);
    if (title != null)
        $confirm.find('.form-title').html(title);
    $.fancybox.open($confirm, {
        baseTpl:
        '<div class="fancybox-container" role="dialog">' +
        '<div class="fancybox-bg"></div>' +
        '<div class="fancybox-inner">' +
        '<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' +
        '<div class="fancybox-toolbar">{{buttons}}</div>' +
        '<div class="fancybox-navigation">{{arrows}}</div>' +
        '<div class="fancybox-stage"></div>' +
        '<div class="fancybox-caption"></div>' +
        "</div>" +
        "</div>",
        beforeClose: cancel
    });
    $('.confirmOk').on('click', function (e) {
        event = 'btnClick';
        if (closeOnOk) {
            $.fancybox.close();
        }
        !ok || ok();
    });
    $('.confirmCancel').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.close();
        !cancel || cancel();
    });
};

dialogAlert = function (message, title, msgType, containerClass, footerClass, buttonClass, eventOnClose = $.noop) {
    // $.fancybox.open('<div class="message"><h2>Hello!</h2><p>You are awesome!</p></div>');
    var event;
    var $alert = $('.alert-form-wrap').clone();
    $alert.removeClass('hidden');
    $alert.find('.message').html(message);
    $alert.find('.form-title').html(title);
    if (msgType !== undefined) {
        $alert.find('.form-title').addClass(msgType);
    }
    if (containerClass !== undefined) {
        $alert.addClass(containerClass);
    }

    if (footerClass !== undefined) {
        $alert.find('.buttons-wrap').addClass(footerClass);
    }

    if (buttonClass !== undefined) {
        $alert.find('.alertOk').removeClass('btn btn-sm btn-primary');
        $alert.find('.alertOk').removeClass(buttonClass);
    }

    $alert.find('.alertOk').remove();

    $.fancybox.open($alert, {
        beforeClose: eventOnClose
    });
    // $alert.find('.alertOk').remove();
};


dialogAction = function (actionTitle, actionBlockHtml, ok, cancel, afterShow, closeOnOk) {
    var event;
    var $action = $('.action-form-wrap').clone();
    $action.removeClass('hidden');
    $action.find('.form-title').text(actionTitle);
    $action.find('.action-block').html(actionBlockHtml);
    if (afterShow === undefined) {
        afterShow = function () {
        }
    }
    if (closeOnOk === undefined) {
        closeOnOk = true;
    }
    $.fancybox.open($action);
    $('.actionOk').on('click', function (e) {
        event = 'btnClick';
        if (closeOnOk) {
            $.fancybox.close();
        }
        !ok || ok();
    });
    $('.actionCancel').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.close();
        !cancel || cancel();
    });
};

dialogCalculator = function (message, title, msgType, afterShow) {
    var $calculator = $('.calculator-wrap').clone();
    $calculator.removeClass('hidden');
    $calculator.find('.calculator-title').html(title);
    $calculator.find('.calculator-container').html(message);
    if (msgType !== undefined) {
        $calculator.addClass(msgType);
    }

    $.fancybox.open({
        src: $calculator,
        type: 'html',
        touch: false,
        buttons: false,
        afterShow: afterShow,
    });
    setTimeout(() => {
        $calculator.find('.add-product-input').focus();
    }, 50);

};