$(document).ready(function(){

    $(".grid-collapse").click(function(e){
        e.preventDefault();
        if($(this).hasClass("active"))
        {
            $(this).closest(".is-bs3").find(".kv-grid-container").first().css("display", "none");
            $(this).find(".fa-caret-down").toggleClass("fa-caret-right").toggleClass("fa-caret-down");
        }
        else
        {
            $(this).closest(".is-bs3").find(".kv-grid-container").first().css("display", "block");
            $(this).find(".fa-caret-right").toggleClass("fa-caret-right").toggleClass("fa-caret-down");

        }

        $(this).toggleClass("active");
    });

    $(".dragg").draggable({
    	"revert":"invalid",
    	"containment": "document",
    	"cursor": "move"
    });

    $(".dragg").draggable("disable");

    $(".sortable-table").sortable({
    	placeholder: "ui-state-highlight",
    	stop: function(e, ui){
    		e.stopPropagation();
    		curr = $(ui.item).attr("data-id");
    		next = $(ui.item).next().attr("data-id");

    		onGridSort(curr, next);
    	}


    });

    $(".sortable-table").sortable("disable");

    $(".group-table").droppable({
      drop: function( e, ui ) {
    	e.stopPropagation();
        group = $(this).attr("data-group");
        id = $(ui.draggable).attr("data-id");

        orig_group = $(ui.draggable).closest(".group-table").attr("data-group");

        // console.log(group);
        // console.log(orig_group);
        // console.log(id);

        $(ui.draggable).css("top", "");
        $(ui.draggable).css("left", "");

        if(group !== orig_group)
        {
        	droppable = this;
        	$.get({
        		url: "/admin/user-management/permission/change-group",
        		data:{
        			'id': id,
        			'group': group
        		},
        		success: function(re){
        			re = JSON.parse(re);
        			// console.log(re);
        			if(re.success == true)
                    {
        				$(droppable).find("tbody").first().append(ui.draggable);
                        gr_quant = parseInt($("[data-group = '"+group+"']").find(".gr-quant").text())+1;
                        orig_gr_quant = parseInt($("[data-group = '"+orig_group+"']").find(".gr-quant").text())-1;

                        $("[data-group = '"+group+"']").find(".gr-quant").text(gr_quant);
                        $("[data-group = '"+orig_group+"']").find(".gr-quant").text(orig_gr_quant);
                    }

        		}
        	});
        }
      }
    });

    $(".group-container").sortable({
        items: ".group-table:not(.unsorted)",
        placeholder: "ui-state-highlight",
        stop: function(e, ui){
            curr = $(ui.item).attr("data-group");
            next = $(ui.item).next().attr("data-group");

            $(ui.item).find(".grid-collapse").first().trigger('click');

            onGroupGridSort(curr, next);
        }
    });
    $(".group-container").sortable("disable");

    $(".change-group").click(function(e){
    	e.preventDefault();	
    	e.stopPropagation();

    	$(this).parent().find(".sort-group.active").trigger("click");
        $(".sort-groups.active").trigger("click");

    	if($(this).hasClass("active"))
    		$(this).closest(".group-table").find(".dragg").draggable("disable");
    	else
    		$(this).closest(".group-table").find(".dragg").draggable("enable");

    	$(this).toggleClass("active");
    });

    $(".sort-group").click(function(e){
    	e.preventDefault();
    	e.stopPropagation();

    	$(this).parent().find(".change-group.active").trigger("click");
        $(".sort-groups.active").trigger("click");

    	if($(this).hasClass("active"))
    		$(this).closest(".group-table").find(".sortable-table").sortable("disable");
    	else
    		$(this).closest(".group-table").find(".sortable-table").sortable("enable");

    	$(this).toggleClass("active");
    });

    $(".btn-group a").click(function(e){e.stopPropagation();});

    $(".sort-groups").click(function(e){
        e.preventDefault();
        e.stopPropagation();

        $(".change-group.active").trigger("click");
        $(".sort-group.active").trigger("click");

        if($(this).hasClass("active"))
            $(".group-container").sortable("disable");
        else
            $(".group-container").sortable("enable");

        $(this).toggleClass("active");
    });
});

function onGroupGridSort(curr, next)
{
    var sendData = {
        curr: curr,
        next: next
    };
    $.get({
        url: "/admin/user-management/permission/sort-groups",
        data: sendData
    });
}

function onGridSort(curr,next){
    var sendData = {
        sortable: {
            curr: curr,
            next: next
        }
    };
    $.post({
        url: "/admin/user-management/permission/sort",
        data: sendData
    });
};