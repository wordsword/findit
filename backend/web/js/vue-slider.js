var app = new Vue({
    el: '#slide',

    data: {
        "items": [],
        "messages": [],
        "fontFamilies": [],
        "textPosition": [],
        "textFontStyle": [],
        'animations': [],
        "imageHeight": 0,
        "imageWidth": 0,
        "imageSize": 0
    },
    methods: {
        fetchData:
            function () {
                this.items = base_property['items'];
                this.messages = base_property['messages'];
                this.fontFamilies = base_property['fontFamily'];
                this.textPosition = base_property['textPosition'];
                this.textFontStyle = base_property['fonStyle'];
                this.animations = base_property['animations']

                this.imageHeight = base_property['sizes']['height'];
                this.imageWidth = base_property['sizes']['width'];
                this.imageSize = base_property['sizes']['size'];
            },
        getImageSizeInBytes: function (imgURL) {
            var request = new XMLHttpRequest();
            request.open("HEAD", imgURL, false);
            request.send(null);
            var headerText = request.getAllResponseHeaders();
            var re = /Content\-Length\s*:\s*(\d+)/i;
            re.exec(headerText);
            return parseInt(RegExp.$1);
        },
        convertHex: function (color, opacity) {
            color = color.replace('#', '')
            let r = parseInt(color.substring(0, 2), 16)
            let g = parseInt(color.substring(2, 4), 16)
            let b = parseInt(color.substring(4, 6), 16)
            let result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
            return result;
        },
        openFile: function () {
            var vm = this;
            window.KCFinder = {
                callBack:
                    function (url) {
                        vm.items.image = url;
                        window.KCFinder = null;
                        var img = new Image();
                        img.src = url;
                        img.onload = function () {
                            vm.imageWidth = this.width;
                            vm.imageHeight = this.height;
                            vm.imageSize = vm.getImageSizeInBytes(url) / 1000;
                        }
                    }
            };

            window.open('/kcfinder/browse.php?type=images&lang="' + base_property['lang'] + '"&dir=images/slides',
                'kcfinder_textbox', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=800, height=600'
            )
        },
        getShadow: function (dx, dy, radius, color, opacity) {
            return dx + "px " + dy + "px " + radius + "px " + this.convertHex(color, opacity);
        },

        slidePlay: function (event) {
            var elemets = $(document).find('.animation-block');

            elemets.each(function () {
                var el = $(this);
                var anim_name = el.attr('data-name');
                if (event.target.value !== 'none') {
                    $(this).removeClass('animated ' + anim_name);
                    el.addClass('animated ' + anim_name);

                    el.on("animationend", function () {
                        $(this).removeClass('animated ' + anim_name);
                    });
                }
            });
        },
        onChange: function (event) {
            if (event.target.value !== 'none') {
                var tr = $(event.target).attr('data-target');
                var el = $(document).find('.main-slider-' + tr);
                el.addClass('animated ' + event.target.value);
                el.on("animationend", function () {
                    $(this).removeClass('animated ' + event.target.value);
                });
            }
        }
    },

    computed: {
        baseFamily: function () {
            var vm = this;
            var f = vm.fontFamilies.filter(function (t) {
                return t.value == vm.items.title_fontfamily
            });
            return f[0].family;
        },
        baseDescrFamily: function () {
            var vm = this;
            var f = vm.fontFamilies.filter(function (t) {
                return t.value == vm.items.descr_fontfamily
            });
            return f[0].family;
        },
        baseTitleStyle: function () {
            var vm = this;
            var s = vm.textFontStyle.filter(function (t) {
                return t.id == vm.items.title_fontstyle
            });
            return s[0].value;
        },
        baseDescrStyle: function () {
            var vm = this;
            var s = vm.textFontStyle.filter(function (t) {
                return t.id == vm.items.descr_fontstyle
            });
            return s[0].value;
        },
        baseTextPosition: function () {
            var vm = this;
            var p = vm.textPosition.filter(function (t) {
                return t.id == vm.items.position
            });
            return p[0].class;
        },
        imageInfo: function () {
            return (this.items.image.length > 0 && +this.imageHeight > 0 && +this.imageWidth > 0 && +this.imageSize > 0) ? true : false
        },
        overlayGradient: function () {
            return "background: linear-gradient(" + this.items.overlay_radius + "deg, " + this.convertHex(this.items.overlay_color, this.items.overlay_opacity) + " " + this.items.overlay_width + "%, transparent); ";
        },
    },

    created: function () {
        this.fetchData();
    },
    mounted: function () {
        // Прелоадер при ініціалізації Vue
        $(document).find('.slide-update, .slide-create').removeClass('loading');
    },
});