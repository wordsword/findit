skConfirm = function (message, ok, cancel) {
    var event;
    var $confirm = $('.confirm-form-wrap').clone();
    $confirm.find('.content').text(message);

    $.fancybox.open({
        src: $confirm.find('.confirm-form'),
        type: 'html',
        touch: false,
        buttons: false,
        afterClose: function (instance, current) {
            if (event === undefined) {
                !cancel || cancel();
            }
        }
    });

    $('.confirmOk').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.getInstance().close();
        !ok || ok();
    });
    $('.confirmCancel').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.getInstance().close();
        !cancel || cancel();
    });
};


skConfirmAction = function (title, ok, cancel, message, close_modal) {
    var event;
    var $confirm = $('.confirm-form-wrap').clone();
    $confirm.find('.message').html(message);
    if(title === undefined){
        $confirm.find('.content').addClass('hidden');
    }else{
        $confirm.find('.content').html(title);
    }
    $.fancybox.open({
        // minWidth: 400,
        openSpeed: 100,
        padding: 0,
        closeSpeed: 100,
        src: $confirm.find('.confirm-form'),
        type: 'html',
        touch: false,
        buttons: false,
        afterClose: function () {
            if (event === undefined) {
                !cancel || cancel();
            }
        }
    });
    $('.confirmOk').on('click', function (e) {
        event = 'btnClick';
        if (close_modal === undefined) {
            $.fancybox.getInstance().close();
        }
        !ok || ok();
    });
    $('.confirmCancel').on('click', function (e) {
        event = 'btnClick';

        $.fancybox.getInstance().close();
        !cancel || cancel();
    });
};

skAlert = function (message, title, ok, msgType) {
    var event;
    var $alert = $('.alert-form-wrap').clone();
    $alert.find('.message').html(message);
    $alert.find('.form-title').html(title);
    if (msgType !== undefined) {
        $alert.find('.form-title').addClass(msgType);
    }
    $.fancybox.open({
        src: $alert.find('.alert-form'),
        type: 'html',
        touch: false,
        buttons: false,
        afterClose: function (instance, current) {
            if (event === undefined) {
                !ok || ok();
            }
        }
    });

    $('.alertOk').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.getInstance().close();
        !ok || ok();
    });
};

skAction = function (actionTitle, actionBlockHtml, ok, cancel, afterShow) {
    var event;
    var $action = $('.action-form-wrap').clone();
    $action.find('.form-title').text(actionTitle);
    $action.find('.action-block').html(actionBlockHtml);
    if (afterShow === undefined) {
        afterShow = function () {
        }
    }

    $.fancybox.open({
        src: $action.find('.action-form'),
        type: 'html',
        touch: false,
        buttons: false,
        smallBtn: false,
        toolbar: false,
        infobar: false,
        afterShow: afterShow,
        afterClose: function (instance, current) {
            if (event === undefined) {
                !cancel || cancel();
            }
        }
    });

    $('.actionOk').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.getInstance().close();
        !ok || ok();
    });
    $('.actionCancel').on('click', function (e) {
        event = 'btnClick';
        $.fancybox.getInstance().close();
        !cancel || cancel();
    });
};

skDetailInfo = function (title, message, content) {
    var $alert = $('.custom-alert-form-wrap').clone();
    $alert.find('.title').html(title);
    $alert.find('.message').html(message);
    $alert.find('.custom-modal-body').html(content);
    $.fancybox.open({
        src: $alert.find('.custom-alert-form'),
        type: 'html',
        touch: false,
        buttons: false,
        afterClose: function (instance, current) {
            $(document).find('.date-range-filter').datepicker('destroy');
            $(document).find('.daterangepicker').remove();
        }
    });
};