$("#opt_imgs").click(function(e){
    e.preventDefault();
    // console.log(document.location.href);   

    $("#opt_log").empty();
    $("#proct").empty();
    $("#srinkd").empty();
    $('#w0').progressbar("value", 0);
    $('#w0').find('.progress-label').text( "Loading..." );
    $(".log-panel").removeClass('hidden');

    var size = $("#min_opt_sz").val();
    
    $.get({
        url: "/admin/main/settings/getimgs"
    }).done(function(r){
        r = JSON.parse(r);
        // console.log(r);

        var total = r.length;

        proc(size, 0, total, r, 0, 0);
        
        // $("#opt_log").append("<p>Done!</p>");
    });
});


function proc(size, cur, total, arr, srk, all)
{
    if(cur<total)
    {
        item = arr[cur];
        $.post({
            url: "/admin/main/settings/processimg",
            data: {
                img: item,
                size: size
            },
            success: function(re){

                        re = JSON.parse(re);

                        all++;
                        if(re.processed)
                            srk++;

                        $("#opt_log").append(re.log);
                        
                        $("#proct").text("Processed: "+all+";");
                        $("#srinkd").text("Shrinked: "+srk+";");
                        $('#w0').progressbar("value", Math.round((all/total)*100));
                        window.scrollTo(0,document.body.scrollHeight);

                        proc(size, cur+1, total, arr, srk, all);
                    },
            error: function(){


                        all++;

                        $("#opt_log").append("<p>Failed to process "+item+"</p>");
                        
                        $("#proct").text("Processed: "+all+";");
                        $("#srinkd").text("Shrinked: "+srk+";");
                        $('#w0').progressbar("value", Math.round((all/total)*100));
                        window.scrollTo(0,document.body.scrollHeight);$("#proct").text("Processed: "+all+";");
                        $('#w0').progressbar("value", Math.round((all/total)*100));
                        window.scrollTo(0,document.body.scrollHeight);

                        proc(size, cur+1, total, arr, srk, all);
                    }

        });
    }
}