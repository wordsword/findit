<?php
use app\widgets\treeViewCategory\TreeViewAsset;
use yii\helpers\Url;
TreeViewAsset::register($this);
$this->registerJs($this->context->js, yii\web\View::POS_READY, __CLASS__.'treeView');
echo yii\helpers\Html::tag('div',$data,['id'=>'treeview']);

