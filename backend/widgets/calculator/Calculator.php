<?php

namespace backend\widgets\calculator;

use common\components\SkLangHelper;
use yii\helpers\Html;
use Yii;

class Calculator extends \yii\bootstrap\Widget
{
    public $layout = null;
    public $layoutClass = null;
    public $registerJs = true;

    public function init()
    {
    }

    public function run()
    {
        switch ($this->layout) {
            case "inverse-button":
                return $this->render('calculator', ['class' => $this->layoutClass, 'js' => $this->registerJs]);
                break;
            case "plus-minus-button":
                return $this->render('calculator_v2', ['class' => $this->layoutClass, 'js' => $this->registerJs]);
                break;
            case "select-button":
                return $this->render('calculator_v3', ['class' => $this->layoutClass, 'js' => $this->registerJs]);
                break;
        }
    }
}