$(document).ready(function () {
    var use_dot = false
    var clear = true;
    $(document).on('click', '.dot a', function (e) {
        e.preventDefault();
        _item_summ = $(this).closest('table').find('.add-product-input').val();

        if (_item_summ.indexOf('.') == -1)
            use_dot = true;

        $(this).closest('table').find('.add-product-input').prop('value', _item_summ);
    });

    $(document).on('click', '.number a', function (e) {
        e.preventDefault();
        _item_summ = $(this).closest('table').find('.add-product-input').val();

        
        
        
        if (clear) {
            _item_summ = '0';
            clear = false;
        }
        use_minus = $(this).closest('table').find('.plus-minus').length > 0 && _item_summ == 0;
        
        _fixed = _item_summ.indexOf('.');

        if (_fixed == -1)
            _fixed = 0;
        else {
            _fixed = _item_summ.length - _fixed - 1;
        }

        if (_fixed >= 2)
            return;

        if (_item_summ.length == 1 && parseFloat(_item_summ) == 0 && !use_dot)
            _item_summ = $(this).text();
        else if (use_dot)
            _item_summ += '.' + $(this).text();
        else
            _item_summ += '' + $(this).text();

        use_dot = false;

        if(use_minus && _item_summ > 0)
            _item_summ = -_item_summ;

        $(this).closest('table').find('.add-product-input').prop('value', _item_summ);
    });

    $(document).on('click', '.clear a', function (e) {
        e.preventDefault();
        use_dot = false;
        $(this).closest('table').find('.add-product-input').prop('value', '0');
    });

    $(document).on('click', '.back a', function (e) {
        e.preventDefault();
        _item_summ = $(this).closest('table').find('.add-product-input').val();

        if (clear) {
            _item_summ = '0';
            clear = false;
        }

        _item_summ = _item_summ.substring(0, _item_summ.length - 1);

        if (_item_summ[_item_summ.length - 1] == '.')
            _item_summ = _item_summ.substring(0, _item_summ.length - 1);

        if (_item_summ.length == 0)
            _item_summ = '0';

        use_dot = false;

        $(this).closest('table').find('.add-product-input').prop('value', _item_summ);
    });

    $(document).on('click', '.plus a', function (e) {
        e.preventDefault();
        _item_summ = parseFloat($(this).closest('table').find('.add-product-input').val());
        _item_summStr = _item_summ.toString();

        if (clear) {
            _item_summStr = '0';
            _item_summ = 0;
            clear = false;
        }

        _fixed = _item_summStr.indexOf('.');


        if (_fixed == -1)
            _fixed = 0;
        else {
            _fixed = _item_summStr.length - _fixed - 1;
        }


        _item_summ += 1;

        use_dot = false;
        $(this).closest('table').find('.add-product-input').prop('value', _item_summ.toFixed(_fixed));
    });


    $(document).on('click', '.minus a', function (e) {
        e.preventDefault();
        _item_summStr = $(this).closest('table').find('.add-product-input').val();
        _item_summ = parseFloat(_item_summStr);

        if (clear) {
            _item_summStr = '0';
            _item_summ = 0;
            clear = false;
        }

        _item_summ -= 1;
        if (_item_summ < 0)
            return;

        _fixed = _item_summStr.indexOf('.');

        if (_fixed == -1)
            _fixed = 0;
        else {
            _fixed = _item_summStr.length - _fixed - 1;
        }


        use_dot = false;
        $(this).closest('table').find('.add-product-input').prop('value', _item_summ.toFixed(_fixed));
    });


    $(document).on('click', '.plus-minus a', function (e) {
        e.preventDefault();
        _item_summStr = $(this).closest('table').find('.add-product-input').val();
        _item_summ = parseFloat(_item_summStr);

        clear = false;

        _item_summ *= -1;

        _fixed = _item_summStr.indexOf('.');

        if (_fixed == -1)
            _fixed = 0;
        else {
            _fixed = _item_summStr.length - _fixed - 1;
        }

        use_dot = false;
        $(this).closest('table').find('.add-product-input').prop('value', _item_summ.toFixed(_fixed));
    });


    $(document).on('click', '.OK a', function (e) {
        clear = true;
    });
// alert('af');
    $(document).on('keypress', '.calculator-dialog .add-product-input', function(e) {
        if (e.which == 13) {
            $(this).trigger('change');
            setTimeout(() => {
                $(this).closest('.calculator-dialog').find('.OK a').trigger('click');    
            }, 50);
            return;
        }
    })

    $(document).on('focus', '.calculator-dialog .add-product-input', function () {
        $(this).select();
    });
});