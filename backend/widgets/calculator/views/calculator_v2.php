<?php

if ($js) {
    \app\widgets\calculator\CalculatorAsset::register($this);
} else {
    \app\widgets\calculator\CalculatorV3Asset::register($this);
}
?>


<style>
    .add-product-input {
        appearance: none;
        -moz-appearance: textfield;
        -webkit-appearance: none;
    }

    .add-product-input::-webkit-outer-spin-button,
    .add-product-input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>

<div class="calculator-dialog hidden <?= ($class) ? $class : '' ?>">
    <table>
        <tr>
            <td colspan="3" class="input">
                <input class="add-product-input" value="0" min="0" type="number" style="appearance: none;" autocomplete="off"></td>
            <td class="back">
                <a class="waves-effect"><span class="glyphicon glyphicon-arrow-left"></span></a> 
            </td>
        </tr>
        <tr>
            <td class="number"><a class="waves-effect"><span>7</span></a></td>
            <td class="number"><a class="waves-effect"><span>8</span></a></td>
            <td class="number"><a class="waves-effect"><span>9</span></a></td>
            <td class="clear"><a class="waves-effect"><span>C</span></a></td>
        </tr>
        <tr>
            <td class="number"><a class="waves-effect"><span>4</span></a></td>
            <td class="number"><a class="waves-effect"><span>5</span></a></td>
            <td class="number"><a class="waves-effect"><span>6</span></a></td>
            <td class="plus"><a class="waves-effect"><span>+</span>1</a></td>
        </tr>
        <tr>
            <td class="number"><a class="waves-effect"><span>1</span></a></td>
            <td class="number"><a class="waves-effect"><span>2</span></a></td>
            <td class="number"><a class="waves-effect"><span>3</span></a></td>
            <td class="minus"><a class="waves-effect"><span>-</span>1</a></td>
        </tr>
        <tr>
            <td class="dot"><a class="waves-effect"><span>.</span></a></td>
            <td class="number"><a class="waves-effect"><span>0</span></a></td>
            <td colspan="2" class="OK"><a class="waves-effect"><span>OK</span></a></td>
        </tr>
    </table>
</div>