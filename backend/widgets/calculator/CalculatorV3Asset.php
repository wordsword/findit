<?php

namespace app\widgets\calculator;


use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class CalculatorV3Asset extends AssetBundle
{
    public $sourcePath = '@app/widgets/calculator/assets';
    public $css = [
        'calculator.css',
    ];
    public $js = [];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        '\app\assets\MaterializeAsset'
    ];
}