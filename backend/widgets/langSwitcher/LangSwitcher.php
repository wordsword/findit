<?php

namespace backend\widgets\langSwitcher;
use common\components\SkLangHelper;
use yii\helpers\Html;
use Yii;

class LangSwitcher extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        
        $langs = SkLangHelper::suffixList();
        $links = [];    
        $active = [];
        foreach ($langs as $urlSuffix => $lang) {
            if($lang['lang_id'] == Yii::$app->language) {
                $active = [
                    'label'=>Html::img(Html::encode($lang['image']),['alt'=>Html::encode($lang['lang_name'])]),
                    'url'=>$urlSuffix.Yii::$app->getRequest()->getUrl(),
                    'linkOptions'=>[
                        'class'=>'deflang'
                    ]
                ];
            } else {
                $links[] = [
                    'label'=>Html::img(Html::encode($lang['image']),['alt'=> ' ']).' '.Html::encode($lang['lang_name']), /* Html::encode($lang['lang_name']) */
                    'url'=>$urlSuffix.Yii::$app->getRequest()->getUrl(),
                ];
            }
        }
        if (count($links) > 0) {
            $active['items'] = $links;
        }
        return $this->render('langSwitcher', [
            'links' => [$active],
        ]);
    }
}