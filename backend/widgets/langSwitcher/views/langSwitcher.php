<?php

use yii\bootstrap\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right','id'=>'lang-switcher'],
    'items' => $links,
    'encodeLabels'=>false
]);

