<?php

namespace app\widgets\menuTreeView;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use rmrevin\yii\fontawesome\FA;

class TreeView extends \yii\bootstrap\Widget
{
    public $data;
    public $js;
    public $inputName;
    public $useCheckBox = true;
    public $checkboxType = 'checkbox'; //radio
    public $onNodeChecked;
    public $onNodeUnchecked;
    public $onNodeCollapsed;
    public $onNodeExpanded;
    public $onRelocate;
    public $collapsedClass = 'nestedSortable-collapsed';
    public $expandedClass = 'nestedSortable-expanded';
    public $branchClass = 'nestedSortable-branch';
    public $leafClass = 'nestedSortable-leaf';
    public $checkedClass = 'fa fa-check-square-o';
    public $uncheckedClass = 'fa fa-square-o';
    public $checkedRadioClass = 'fa fa-check-square-o';
    public $uncheckedRadioClass = 'fa fa-square-o';
    public $disableSorting = false;
    public $startCollapsed = true;
    public $openSelectedBranch = false;
    public $view = 'treeView';
    public $viewParams = [];
    public $maxLevels = 0;

    public function init()
    {
    }

    private function _getDataRecursive($data, $options = [])
    {
        unset($uncategorizedItem);
        $tree = Html::beginTag('ol', $options);
        foreach ($data as $key => $item) {
            if (!isset($item['options'])) {
                $item['options'] = [];
            }
            if ($item['recID'] == 0) {
                $uncategorizedItem = $item;
                continue;
            }
            $itemOptions = $item['options'];
            $itemOptions['class'] .= $item['checked'] ? 'checked' : '';
            $itemOptions['recID'] = $item['recID'];
            $itemOptions['visible'] = $item['visible'];
            $tree .= Html::beginTag('li', $itemOptions);
            $style = "";
            if ($item['img']) {
                $style = "height:auto;";
            }
            $tree .= Html::beginTag('div', ['class' => 'category-wrap', 'title' => $item['text'], 'style' => $style]);
            if($this->collapsedClass != 'nestedSortable-expanded')
                $tree .= '<i class="extrigger"></i>';
            $tree .= Html::hiddenInput(
                $this->inputName . '[' . $item['recID'] . ']',
                $item['recID'],
                ['class' => $item['checked'] ? 'checked' : '']
            );
            if (!$item['emptyCheckbox']) {
                $tree .= Html::beginTag('a', ['href' => '#', 'class' => 'category']);
            }
            if ($this->useCheckBox && !$item['emptyCheckbox'] && ($this->checkboxType == 'checkbox' || $this->checkboxType == 'radio')) {

                if ($this->checkboxType == 'checkbox') {
                    $checkClass = $this->checkedClass;
                    $uncheckClass = $this->uncheckedClass;
                } else {
                    $checkClass = $this->checkedRadioClass;
                    $uncheckClass = $this->uncheckedRadioClass;
                }

                $tree .= Html::tag('i', '', [
                    'class' => 'chtrigger ' . ($item['checked'] ? $checkClass : $uncheckClass),
                ]);
            }

            $item['text'] = Html::tag('span', $item['text'])    ;
            if ($item['image']) {
                $tree .= Html::img($item['image'], ['style' => 'max-height:40px;']);
            };

            $tree .= Html::tag('span', $item['text'], ['class' => 'link-text']);
            if (!$item['emptyCheckbox']) {
                $tree .= Html::endTag('a');
            }
            
            $tree .= Html::beginTag('span', ['class' => 'action-buttons']);
            if(isset($item['roles'])){
                $tree .= Html::beginTag('span', ['style' => [
                    'display' => 'inline-block',
                    'line-height' => '42px',
                    // 'height' => '42px',
                    'top' => '1px'
                ]]).
                    $item['roles'].
                Html::endTag('span');
            }

            if (isset($item['itemCount'])) {
                $class = $item['hasChilds'] ? 'label label-primary' : 'label label-info';
                $tree .= Html::tag('span', '<label class="' . $class . ' counter" recid="' . $item['recID'] . '">' . $item['itemCount'] . '</label>');
            }
            if (isset($item['visLink']) || isset($item['updateLink']) || isset($item['delLink'])) {
                $tree .= Html::tag('span', '', ['class' => 'action-divider']);
            }
            if (isset($item['visLink'])) {
                $vis = $item['visible'] ? '' : '-slash';
                $tree .= Html::a('<i class="fa fa-eye' . $vis . '"></i>', $item['visLink'], [
                    'title' => Yii::t('app', 'Visibility'),
                    'aria-label' => Yii::t('app', 'Visibility'),
                    'data-confirm-msg' => Yii::t('app', 'Are you sure you want to change visibility of this item?'),
                    'class' => 'btnvis',
                    'data-pjax' => '0',
                ]);
            }
            if (isset($item['updateLink'])) {
                $tree .= Html::a('<i class="fa fa-pencil"></i>', $item['updateLink'], [
                    'title' => Yii::t('app', 'Update'),
                    'aria-label' => Yii::t('app', 'Update'),
                    'data-pjax' => '0',
                    'class' => 'updateLink'
                ]);
            }
            if (isset($item['delLink'])) {
                $tree .= Html::a('<i class="fa fa-trash"></i>', $item['delLink'], [
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm-msg' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'class' => 'delete',
                    //'data-method' => 'post',
                    'data-pjax' => '0',
                ]);
            }

            $tree .= Html::endTag('span');
            $tree .= Html::endTag('div');
            if ($item['childs']) {
                $tree .= $this->_getDataRecursive($item['childs']);//['class'=>'dropdown']
            }
            $tree .= Html::endTag('li');
        }
        $tree .= Html::endTag('ol');
        if (isset($uncategorizedItem)) {
            $tree .= Html::beginTag('ol', ['class' => 'uncategorized-wrap']);
            $tree .= Html::beginTag('li', ['recID' => $uncategorizedItem['recID']]);
            $tree .= Html::beginTag('div', ['class' => 'category-wrap']);
            $tree .= '<i class="extrigger"></i>';
            $tree .= Html::hiddenInput(
                $this->inputName . '[' . $uncategorizedItem['recID'] . ']',
                $uncategorizedItem['recID'],
                ['class' => $uncategorizedItem['checked'] ? 'checked' : '']
            );
            $tree .= Html::beginTag('a', ['href' => '#', 'class' => 'category']);
            if ($this->useCheckBox && !$uncategorizedItem['emptyCheckbox'] && ($this->checkboxType == 'checkbox' || $this->checkboxType == 'radio')) {

                if ($this->checkboxType == 'checkbox') {
                    $checkClass = $this->checkedClass;
                    $uncheckClass = $this->uncheckedClass;
                } else {
                    $checkClass = $this->checkedRadioClass;
                    $uncheckClass = $this->uncheckedRadioClass;
                }

                $tree .= Html::tag('i', '', [
                    'class' => 'chtrigger ' . ($uncategorizedItem['checked'] ? $checkClass : $uncheckClass),
                ]);
            }
            $uncategorizedItem['text'] = Html::tag('span', $uncategorizedItem['text']);
            $tree .= Html::tag('span', $uncategorizedItem['text'], ['class' => 'link-text']);
            $tree .= Html::endTag('a');
            $tree .= Html::beginTag('span', ['class' => 'action-buttons']);
            if (isset($item['itemCount'])) {
                $tree .= Html::tag('span', '<label class="label label-info counter"  recid="0">' . $item['itemCount'] . '</label>');
            }
            $tree .= Html::endTag('span');
            $tree .= Html::endTag('div');
            $tree .= Html::endTag('li');
            $tree .= Html::endTag('ol');
        }
        return $tree;
    }

    public function run()
    {

        $nestedOptions = [
            'forcePlaceholderSize' => true,
            'handle' => 'div',
            'helper' => 'clone',
            'items' => 'li',
            'opacity' => .6,
            'placeholder' => 'placeholder',
            'revert' => 250,
            'tolerance' => 'pointer',
            'toleranceElement' => '> div',
            'disableNestingClass' => 'disabled-node',
            'isTree' => true,
            'startCollapsed' => $this->startCollapsed,
            'expandedClass' => $this->expandedClass,
            'collapsedClass' => $this->collapsedClass,
            'branchClass' => $this->branchClass,
            'leafClass' => $this->leafClass,
            'relocate' => $this->onRelocate,
            'maxLevels' => $this->maxLevels
        ];

        $options = [
            'onNodeChecked' => $this->onNodeChecked,
            'onNodeUnchecked' => $this->onNodeUnchecked,
            'onNodeCollapsed' => $this->onNodeCollapsed,
            'onNodeExpanded' => $this->onNodeExpanded,
            'expandedClass' => $this->expandedClass,
            'collapsedClass' => $this->collapsedClass,
            'branchClass' => $this->branchClass,
            'leafClass' => $this->leafClass,
            'checkboxType' => $this->checkboxType,
            'checkedIcon' => $this->checkedClass,
            'uncheckedIcon' => $this->uncheckedClass,
            'checkedRadioIcon' => $this->checkedRadioClass,
            'uncheckedRadioIcon' => $this->uncheckedRadioClass,
            'disableSorting' => $this->disableSorting,
            'startCollapsed' => $this->startCollapsed,
            'openSelectedBranch' => $this->openSelectedBranch,
            'nestedOptions' => $nestedOptions
        ];

        $this->js = "$('.treeview').skTreeView(" . \yii\helpers\Json::encode($options) . ");";
        if (empty($this->onRelocate)) {
            $this->onRelocate = new \yii\web\JsExpression("function (undefined, object, list) {}");
        }
        if (!$this->disableSorting) {
            $this->js .= "$('.treeview-sortable').nestedSortable(" . \yii\helpers\Json::encode($nestedOptions) . ");
                    $('.treeview-sortable').nestedSortable('disable');
                    $(document).on('click','.toggle-sorter',function(){
                        $(this).toggleClass('active btn-info btn-success');
                        if ($(this).hasClass('active')) {
                            $('.treeview-sortable').nestedSortable('enable');
                        } else {
                            $('.treeview-sortable').nestedSortable('disable');
                        }
                    })";
        }
        return $this->render($this->view, ArrayHelper::merge($this->viewParams, [
            'data' => $this->_getDataRecursive($this->data, ['class' => 'treeview-sortable']),
        ]));
    }
}