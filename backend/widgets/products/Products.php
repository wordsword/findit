<?php

namespace backend\widgets\products;

use common\components\SkLangHelper;
use yii\helpers\Html;
use Yii;

class Products extends \yii\bootstrap\Widget
{
    public $containerOptions = [];
    public $productsDataProvider = null;
    public $productsLayout = null;
    public $productsLayoutType = null;
    public $productsPages = null;
    public $productsCount = null;
    public $defaultPrice = null;
    public $modelName = null;

    public $categoriesDataProvider = null;
    public $categoriesViewType = 'tree';
    public $useCategories = true;
    public $usePromotions = true;

    public function init()
    {
    }

    public function run()
    {
        return $this->render('layout', [
            'containerOptions' => $this->containerOptions,
            'productsDataProvider' => $this->productsDataProvider,
            'productsLayout'=> $this->productsLayout,
            'productsLayoutType'=> $this->productsLayoutType,
            'productsPages'=> $this->productsPages,
            'productsCount'=> $this->productsCount,
            'defaultPrice'=> $this->defaultPrice,
            'categoriesDataProvider'=>$this->categoriesDataProvider,
            'categoriesViewType'=>$this->categoriesViewType,
            'useCategories'=>$this->useCategories,
            'modelName'=>$this->modelName,
            'usePromotions'=>$this->usePromotions,
        ]);
    }
}