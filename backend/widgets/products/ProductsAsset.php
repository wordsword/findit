<?php

namespace app\widgets\products;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class ProductsAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/products/assets';
    public $css = [
        'product-list.css',
    ];
    public $js = [
//        'calculator.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        '\app\assets\MaterializeAsset'
    ];
}