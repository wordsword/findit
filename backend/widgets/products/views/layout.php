<?php

use yii\db\Query;
use yii\web\View;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use app\modules\main\models\Store;
use app\modules\main\models\Product;
use app\widgets\treeViewDocument\TreeView;

\app\widgets\products\ProductsAsset::register($this);

$layout_arr = [
    '_image_list' => Yii::t('app', 'List with images'),
    '_list' => Yii::t('app', 'List'),
    '_tile' => Yii::t('app', 'Tiles'),
];

?>

<div class="product-list <?= (isset($containerOptions['class'])) ? $containerOptions['class'] : '' ?>">

    <? if ($useCategories) : ?>
        <div class="categories">
            <div class="product-list-header document-option-row">
                <!-- <div class="menu-block"> -->
                <ul class="category-list header-category-list">
                    <li data-id="0" class="breadcrumbs-category waves-effect waves-light"><?= Yii::t('app', 'All products') ?></li>
                </ul>
                <!-- </div> -->
                <div class="document-option-button-block" style="margin-left: auto;">
                    <span class="document-option-button bgc waves-effect waves-light close-category">
                        <svg>
                            <use xlink:href="/backend/web/img/icons.svg#arrow-right">
                            </use>
                        </svg>
                    </span>
                </div>
            </div>
            <? if ($categoriesViewType == 'tree') : ?>
                <div class="category-treeView">
                    <?= TreeView::widget([
                        'data' => $categoriesDataProvider,
                        'openSelectedBranch' => true,
                    ]) ?>
                </div>
            <? elseif ($categoriesViewType == 'list') : ?>
                <div class="category-listBlock">
                    <?
                    $category_list = [];
                    $storeID = Store::getCurrentStoreId();
                    $sql = new Query();
                    $sql->select('t.id, t.parent_id, t.visible, tlang.title');
                    $sql->from(' {{%category}} t');
                    $sql->leftJoin('{{%category_lang}} tlang', 'tlang.owner_id=t.id AND tlang.language=:language', ['language' => Yii::$app->language]);
                    $sql->where(['t.object' => Product::className(), 'is_deleted' => 0]);
                    $sql->orderBy('t.sort_order');
                    // $sql->indexBy('t.id');
                    $list = $sql->all();

                    // ${exit();}

                    foreach ($list as $item) {
                        $category_list[$item['parent_id']][] = $item;
                    }

                    foreach ($category_list as $key => $list) : ?>
                        <ul data-parent="<?= $key ?>" class="category-listView <?= ($key == 0) ? 'active' : '' ?>">
                            <li class="category-item all-current" data-id="<?= $key ?>" data-parent="<?= $key ?>">
                                <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                    <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                                </svg>
                                <span class="link-text"><?= Yii::t('app', 'All products') ?></span>
                                <span class="category-count"><?= \app\modules\main\models\Category::getItemsCount($key, Product::className()) ?></span>
                            </li>
                            <? foreach ($list as $item) :
                                $count = \app\modules\main\models\Category::getItemsCount($item['id'], Product::className());
                            ?>
                                <? if (isset($category_list[$item['id']]) && is_array($category_list[$item['id']])) { ?>
                                    <li class="category-item has-child" data-id="<?= $item['id'] ?>" data-parent="<?= $item['parent_id'] ?>">
                                        <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                            <use xlink:href="/backend/web/img/icons.svg#new-folder"></use>
                                        </svg>
                                        <span class="link-text"><?= $item['title'] ?></span>
                                        <span class="category-count"><?= $count ?></span>
                                    </li>
                                <? } else { ?>
                                    <li class="category-item" data-id="<?= $item['id'] ?>" data-parent="<?= $item['parent_id'] ?>">
                                        <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                            <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                                        </svg>
                                        <span class="link-text"><?= $item['title'] ?></span>
                                        <span class="category-count"><?= $count ?></span>
                                    </li>
                                <? } ?>
                            <? endforeach; ?>
                            <? $it_count = (new Query)->from(Product::tableName())->where(['parent_id' => $key, 'is_deleted' => 0, 'visible' => 1])->count() ?>
                            <? if ($it_count) { ?>
                                <li class="category-item only-current" data-id="<?= $key ?>" data-parent="<?= $key ?>">
                                    <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                        <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                                    </svg>
                                    <span class="link-text">...</span>
                                    <span class="category-count"><?= $it_count ?></span>
                                </li>
                            <? } ?>
                        </ul>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
    <? endif; ?>
    <div class="products">
        <div class="product-list-header document-option-row">
            <div class="search">
                <div class="form-group input-field">
                    <i class="material-icons prefix active">search</i>
                    <input type="text" class="form-control" id="product-search" autocomplete="off" placeholder="<?= Yii::t('app', 'Search') ?>">
                    <a href="#" class="close-search-block" title="<?= Yii::t('app', 'Close') ?>"></a>
                </div>
            </div>
            <div class="menu-block">
                <div class="document-option-button-block ">
                    <span class="document-option-button bgc waves-effect waves-light category-button">
                        <svg>
                            <use xlink:href="/backend/web/img/icons.svg#arrow-right">
                        </svg>
                    </span>
                </div>
                <ul class="category-list header-category-list">
                    <li data-id="0" class="breadcrumbs-category waves-effect waves-light"><?= Yii::t('app', 'All products') ?></li>
                </ul>
                <div class="document-option-button-block">
                    <span class="document-option-button waves-effect waves-light search-button">
                        <svg>
                            <use xlink:href="/backend/web/img/icons.svg#search">
                        </svg>
                    </span>
                </div>
                <div class="document-option-button-block layout-select dropdown">
                    <?= Html::hiddenInput('state_12', $productsLayoutType, ['id' => 'product-layout-select']) ?>
                    <a class='document-option-button waves-effect waves-light dropdown-toggle' id="layout-select-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <svg>
                            <use xlink:href='/backend/web/img/icons.svg#<?= $productsLayoutType ?>'></use>
                        </svg>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="layout-select-menu">
                        <li class="form-group-material">
                            <label class="no-margin">
                                <input type="checkbox" class="filled-in rigth-position" id="in-stock-only" value="0" autocomplete="off">
                                <span><?= Yii::t('app', 'By availability') ?></span>
                            </label>
                        </li>
                        <li class="add-all-products hidden" style="cursor: pointer" data-msg="<?=Yii::t('app', 'Add {n} items?')?>">
                            <?= Yii::t('app', 'Add all products') ?>
                        </li>

                        <ul class="product-layout-item-list">
                            <? foreach ($layout_arr as $key => $layout) : ?>
                                <li class="<?= ($key == $productsLayoutType) ? 'active' : '' ?>">
                                    <a href="<?= $key ?>" class='product-layout-item'>
                                        <svg>
                                            <use xlink:href='/backend/web/img/icons.svg#<?= $key ?>'></use>
                                        </svg>
                                        <span class='title'><?= Html::encode($layout) ?><span>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
        <div class="products-list" enable-scroll="true">
            <?= $this->render($productsLayout, [
                'productsDataProvider' => $productsDataProvider,
                'layout' => $productsLayoutType,
                'pages' => $productsPages,
                'count' => $productsCount,
                'defPrice' => $defaultPrice,
                'modelName' => $modelName,
                'usePromotions' => $usePromotions,
            ]) ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="total-bar document-total-row"></div>
</div>