<?php

namespace backend\widgets\category;

use common\components\SkLangHelper;
use yii\helpers\Html;
use Yii;

class Categories extends \yii\bootstrap\Widget
{
    public $containerOptions = [];

    public $categoriesDataProvider = null;
    public $categoriesViewType = 'tree';
    public $modelName = '';

    public function init()
    {
    }

    public function run()
    {
        return $this->render('layout', [
            'containerOptions' => $this->containerOptions,
            'categoriesDataProvider'=>$this->categoriesDataProvider,
            'categoriesViewType'=>$this->categoriesViewType,
            'modelName'=>$this->modelName,
        ]);
    }
}