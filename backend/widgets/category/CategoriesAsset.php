<?php

namespace app\widgets\category;

use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class CategoriesAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/category/assets';
    public $css = [
        'category-list.css',
    ];
    public $js = [
//        'calculator.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\widgets\PjaxAsset',
        'newerton\fancybox3\FancyBoxAsset',
        '\app\assets\MaterializeAsset'
    ];
}