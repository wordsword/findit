<?php

use yii\db\Query;
use yii\web\View;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use app\modules\main\models\Store;
use app\modules\main\models\Product;
use app\widgets\treeViewDocument\TreeView;

\app\widgets\category\CategoriesAsset::register($this);

?>

<div class="product-list <?= (isset($containerOptions['class'])) ? $containerOptions['class'] : '' ?>">

    <div class="categories">
        <div class="product-list-header document-option-row">
            <!-- <div class="menu-block"> -->
            <ul class="category-list header-category-list">
                <li data-id="0"
                    class="breadcrumbs-category waves-effect waves-light"><?= Yii::t('app', 'All products') ?></li>
            </ul>
            <!-- </div> -->
            <div class="document-option-button-block" style="margin-left: auto;">

            </div>
        </div>
        <? if ($categoriesViewType == 'tree') : ?>
            <div class="category-treeView">
                <?= TreeView::widget([
                    'data' => $categoriesDataProvider,
                    'openSelectedBranch' => true,
                ]) ?>
            </div>
        <? elseif ($categoriesViewType == 'list') : ?>
            <div class="category-listBlock">
                <?
                $category_list = [];
                $storeID = Store::getCurrentStoreId();
                $sql = new Query();
                $sql->select('t.id, t.parent_id, t.visible, tlang.title');
                $sql->from(' {{%category}} t');
                $sql->leftJoin('{{%category_lang}} tlang', 'tlang.owner_id=t.id AND tlang.language=:language', ['language' => Yii::$app->language]);
                $sql->where(['t.object' => Product::className()]);
                $sql->andWhere(['t.is_deleted' => 0]);
                $sql->orderBy('t.sort_order');
                // $sql->indexBy('t.id');
                $list = $sql->all();

                // ${exit();}

                foreach ($list as $item) {
                    $category_list[$item['parent_id']][] = $item;
                }

                foreach ($category_list as $key => $list) : ?>
                    <ul data-parent="<?= $key ?>" class="category-listView <?= ($key == 0) ? 'active' : '' ?>">
                        <li class="category-item all-current" data-id="<?= $key ?>" data-parent="<?= $key ?>"
                            data-category-only="1" data-model-name="<?= $modelName ?>">
                            <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                            </svg>
                            <span class="link-text"><?= Yii::t('app', 'All categories') ?></span>
                            <span class="category-count"><?= \app\modules\main\models\Category::getItemsCount($key, Product::className()) ?></span>
                        </li>
                        <? foreach ($list as $item) :
                            $count = \app\modules\main\models\Category::getItemsCount($item['id'], Product::className());
                            ?>
                            <? if (isset($category_list[$item['id']]) && is_array($category_list[$item['id']])) { ?>
                            <li class="category-item has-child" data-id="<?= $item['id'] ?>"
                                data-parent="<?= $item['parent_id'] ?>" data-category-only="1" data-model-name="<?= $modelName ?>">
                                <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                    <use xlink:href="/backend/web/img/icons.svg#new-folder"></use>
                                </svg>
                                <span class="link-text"><?= $item['title'] ?></span>
                                <span class="category-count"><?= $count ?></span>
                            </li>
                        <? } else { ?>
                            <li class="category-item" data-id="<?= $item['id'] ?>"
                                data-parent="<?= $item['parent_id'] ?>" data-category-only="1" data-model-name="<?= $modelName ?>">
                                <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                    <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                                </svg>
                                <span class="link-text"><?= $item['title'] ?></span>
                                <span class="category-count"><?= $count ?></span>
                            </li>
                        <? } ?>
                        <? endforeach; ?>
                        <? $it_count = (new Query)->from(Product::tableName())->where(['parent_id' => $key, 'is_deleted' => 0, 'visible' => 1])->count() ?>
                        <? if ($it_count) { ?>
                            <li class="category-item only-current" data-id="<?= $key ?>" data-parent="<?= $key ?>"
                                data-category-only="1" data-model-name="<?= $modelName ?>">
                                <svg class="category-icon" style="display: inline-block; vertical-align: bottom;">
                                    <use xlink:href="/backend/web/img/icons.svg#folder"></use>
                                </svg>
                                <span class="link-text"><?= Yii::t('app', 'Current category') ?></span>
                                <span class="category-count"><?= $it_count ?></span>
                            </li>
                        <? } ?>
                    </ul>
                <? endforeach; ?>
            </div>
        <? endif; ?>
    </div>
    <div class="products">
        <div class="product-list-header document-option-row">
        </div>
        <div class="products-list" enable-scroll="true">
            <div id="products-list" class="list-view"></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="total-bar document-total-row"></div>
</div>