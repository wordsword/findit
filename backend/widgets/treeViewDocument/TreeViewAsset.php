<?php
namespace app\widgets\treeViewDocument;


use yii\web\AssetBundle;

/**
 * Custom styles
 *
 * @author eXeCUT
 */
class TreeViewAsset extends AssetBundle {
    public $sourcePath = '@app/widgets/treeViewDocument/assets';
    public $css = [
        'treeView.css',
    ];
    public $js = [
        'jquery.ui.touch-punch.min.js',
        'jquery.mjs.nestedSortable.js',
        'jquery.ba-throttle-debounce.min.js',
        'treeView.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}