<?php

namespace app\widgets\treeViewDocument;

use yii\helpers\Html;
use Yii;
use rmrevin\yii\fontawesome\FA;

class TreeView extends \yii\bootstrap\Widget
{
    public $data;
    public $js;
    public $inputName;
    public $useCheckBox = true;
    public $checkboxType = 'checkbox'; //radio
    public $onNodeChecked;
    public $onNodeUnchecked;
    public $onNodeCollapsed;
    public $onNodeExpanded;
    public $onRelocate;
    public $collapsedClass = 'nestedSortable-collapsed';
    public $expandedClass = 'nestedSortable-expanded';
    public $branchClass = 'nestedSortable-branch';
    public $leafClass = 'nestedSortable-leaf';
    public $checkedClass = 'fa fa-check-square-o';
    public $uncheckedClass = 'fa fa-square-o';
    public $checkedRadioClass = 'fa fa-check-square-o';
    public $uncheckedRadioClass = 'fa fa-square-o';
    public $disableSorting = false;
    public $startCollapsed = true;
    public $openSelectedBranch = false;
    public $maxLevels = 0;
    public $allCategory = true;

    public function init()
    {
    }

    private function _getDataRecursive($data, $options = [], $all = false)
    {
        $tree = Html::beginTag('ol', $options);
        if ($all) {
            $tree .= Html::beginTag('li', [
                'class' => 'nestedSortable-leaf'
            ]);
            $tree .= Html::beginTag('div', [
                'class' => 'category-wrap category-item active',
                'title' => Yii::t('app', 'All categories'),
                'data-id' => null]);
            $text = Html::tag('span', Yii::t('app', 'All categories'));
            $tree .= Html::tag('span', $text, ['class' => 'link-text']);
            $tree .= Html::endTag('div');
            $tree .= Html::endTag('li');
        }
        foreach ($data as $key => $item) {
            $itemOptions = $item['options'];
            $itemOptions['recID'] = $item['recID'];
            $itemOptions['visible'] = $item['visible'];
            // $itemOptions['class'] .= ' category-item';
            // $itemOptions['data-id'] = $item['recID'];
            $itemOptions['class'] .= $item['checked'] ? 'checked' : '';
            $tree .= Html::beginTag('li', $itemOptions);
            $tree .= Html::beginTag('div', ['class' => 'category-wrap category-item waves-effect waves-light btn' . (($item['checked'] ? ' active' : '')), 'title' => $item['text'], 'data-id' => $item['recID']]);
            $tree .= '<i class="extrigger"></i>';
            $tree .= Html::tag('span', $item['text'], ['class' => 'link-text']);
            $tree .= Html::endTag('div');
            if ($item['childs']) {
                $tree .= $this->_getDataRecursive($item['childs']);//['class'=>'dropdown']
            }
            $tree .= Html::endTag('li');
        }
        $tree .= Html::endTag('ol');

        return $tree;
    }

    public function run()
    {

        $nestedOptions = [
            'forcePlaceholderSize' => true,
            'handle' => 'div',
            'helper' => 'clone',
            'items' => 'li',
            'opacity' => .6,
            'maxLevels' => $this->maxLevels,
            'placeholder' => 'placeholder',
            'revert' => 250,
            'tolerance' => 'pointer',
            'toleranceElement' => '> div',
            'disableNestingClass' => 'disabled-node',
            'isTree' => true,
            'startCollapsed' => $this->startCollapsed,
            'expandedClass' => $this->expandedClass,
            'collapsedClass' => $this->collapsedClass,
            'branchClass' => $this->branchClass,
            'leafClass' => $this->leafClass,
            'relocate' => $this->onRelocate
        ];

        $options = [
            'onNodeChecked' => $this->onNodeChecked,
            'onNodeUnchecked' => $this->onNodeUnchecked,
            'onNodeCollapsed' => $this->onNodeCollapsed,
            'onNodeExpanded' => $this->onNodeExpanded,
            'expandedClass' => $this->expandedClass,
            'collapsedClass' => $this->collapsedClass,
            'branchClass' => $this->branchClass,
            'leafClass' => $this->leafClass,
            'checkboxType' => $this->checkboxType,
            'checkedIcon' => $this->checkedClass,
            'uncheckedIcon' => $this->uncheckedClass,
            'checkedRadioIcon' => $this->checkedRadioClass,
            'uncheckedRadioIcon' => $this->uncheckedRadioClass,
            'disableSorting' => $this->disableSorting,
            'startCollapsed' => $this->startCollapsed,
            'openSelectedBranch' => $this->openSelectedBranch,
            'nestedOptions' => $nestedOptions
        ];

        $this->js = "$('#treeview').skTreeView(" . \yii\helpers\Json::encode($options) . ");";
        if (empty($this->onRelocate)) {
            $this->onRelocate = new \yii\web\JsExpression("function (undefined, object, list) {}");
        }
        if (!$this->disableSorting) {
            $this->js .= "$('.treeview-sortable').nestedSortable(" . \yii\helpers\Json::encode($nestedOptions) . ");
                    $('.treeview-sortable').nestedSortable('disable');
                    $(document).on('click','.toggle-sorter',function(){
                        $(this).toggleClass('active btn-info btn-success');
                        if ($(this).hasClass('active')) {
                            $('.treeview-sortable').nestedSortable('enable');
                        } else {
                            $('.treeview-sortable').nestedSortable('disable');
                        }
                    })";
        }
        return $this->render('treeView', [
            'data' => $this->_getDataRecursive($this->data, ['class' => 'treeview-sortable'], $this->allCategory),
        ]);
    }
}