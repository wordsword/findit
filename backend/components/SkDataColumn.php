<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 14.06.2016
 * Time: 10:51
 */

namespace app\components;


use kartik\grid\DataColumn;
use yii\helpers\Html;

class SkDataColumn extends DataColumn
{
    public $data = [];

    public function renderFilterCell()
    {
        $content = $this->renderFilterCellContent();
        if (mb_strpos($content,'form-control') !== false) {
            Html::addCssClass($this->filterOptions, 'filter-cell');
        }
        return Html::tag('td', $content, $this->filterOptions);

    }

}