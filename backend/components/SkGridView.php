<?php

namespace app\components;


use kartik\grid\GridView;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA as FAv;
use Yii;
use yii\bootstrap\Html;
use kartik\base\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

class SkGridView extends GridView
{

    const FILTER_DATE_RANGE = 'app\components\SkDateRangePicker';

    public $collapse = true;
    public $sizer = true;
    public $sizerOptions = [
        'pageVar' => 'per-page',
        'sizeVariants' => null
    ];

    public $dataColumnClass = 'app\components\SkDataColumn';

    public $resizableColumns = false;

    const TYPE_DEFAULT = 'panel panel-default';

    public $panelTemplate = <<< HTML
<div class="{prefix}{type}">
    {panelHeading}
    {panelBefore}
    {items}
    {panelAfter}
    {panelFooter}
</div>
HTML;

    public function run()
    {
        $this->initSizer();
        $this->initToggleData();
        $this->initExport();
        if ($this->export !== false && isset($this->exportConfig[self::PDF])) {
            Config::checkDependency(
                'mpdf\Pdf',
                'yii2-mpdf',
                "for PDF export functionality. To include PDF export, follow the install steps below. If you do not " .
                "need PDF export functionality, do not include 'PDF' as a format in the 'export' property. You can " .
                "otherwise set 'export' to 'false' to disable all export functionality"
            );
        }
        $this->initHeader();
        $this->initBootstrapStyle();
        $this->containerOptions['id'] = $this->options['id'] . '-container';
        if (isset($_COOKIE[$this->containerOptions['id']])) {
            Html::addCssStyle($this->containerOptions, ['display' => 'none']);
        }
        Html::addCssClass($this->containerOptions, 'kv-grid-container');
        $this->registerAssets();
        $this->initPanel();
        $this->initLayout();
        $this->beginPjax();
        parent::run();
        $this->endPjax();
    }

    public function initSizer()
    {
        if (!$this->sizer || (is_array($this->sizerOptions['sizeVariants']) && sizeof($this->sizerOptions['sizeVariants']) == 0)) {
            return '';
        }

        if (empty($this->sizerOptions['sizeVariants'])) {
            $this->sizerOptions['sizeVariants'] = [25 => 25, 50 => 50, 100 => 100, 'all' => Yii::t('app', 'All')];
        }

        $pageSize = Yii::$app->request->queryParams[$this->sizerOptions['pageVar']];

        if (isset($pageSize) && key_exists($pageSize, $this->sizerOptions['sizeVariants'])) {
            if ($pageSize == 'all') {
                $pagination = false;
            } else {
                $pagination = [
                    'pageSize' => $pageSize,
                ];
            }
        } else {
            $pagination = [
                'pageSize' => key($this->sizerOptions['sizeVariants'])
            ];
        }

        $this->dataProvider->setPagination($pagination);
        $this->dataProvider->refresh();
    }

    protected static function initCss(&$options, $css)
    {
        if (!isset($options['class'])) {
            $options['class'] = $css;
        }
    }

    protected function initPanel()
    {
        if (!$this->bootstrap || !is_array($this->panel) || empty($this->panel)) {
            return;
        }
        $type = ArrayHelper::getValue($this->panel, 'type', 'default');
        $heading = ArrayHelper::getValue($this->panel, 'heading', '');
        $footer = ArrayHelper::getValue($this->panel, 'footer', '');
        $before = ArrayHelper::getValue($this->panel, 'before', '');
        $after = ArrayHelper::getValue($this->panel, 'after', '');
        $headingOptions = ArrayHelper::getValue($this->panel, 'headingOptions', []);
        $footerOptions = ArrayHelper::getValue($this->panel, 'footerOptions', []);
        $beforeOptions = ArrayHelper::getValue($this->panel, 'beforeOptions', []);
        $afterOptions = ArrayHelper::getValue($this->panel, 'afterOptions', []);
        $panelHeading = '';
        $panelBefore = '';
        $panelAfter = '';
        $panelFooter = '';

        if ($heading !== false) {
            static::initCss($headingOptions, 'panel-heading');
            $content = strtr($this->panelHeadingTemplate, ['{heading}' => $heading]);
            $panelHeading = Html::tag('div', $content, $headingOptions);
        }
        if ($footer !== false) {
            static::initCss($footerOptions, 'panel-footer');
            $content = strtr($this->panelFooterTemplate, ['{footer}' => $footer]);
            if (isset($_COOKIE[$this->containerOptions['id']])) {
                Html::addCssStyle($footerOptions, ['display' => 'none']);
            }
            $panelFooter = Html::tag('div', $content, $footerOptions);
        }
        if ($before !== false) {
            static::initCss($beforeOptions, 'kv-panel-before');
            $content = strtr($this->panelBeforeTemplate, ['{before}' => $before]);
            $panelBefore = Html::tag('div', $content, $beforeOptions);
        }
        if ($after !== false) {
            static::initCss($afterOptions, 'kv-panel-after');
            $content = strtr($this->panelAfterTemplate, ['{after}' => $after]);
            $panelAfter = Html::tag('div', $content, $afterOptions);
        }
        $this->layout = strtr(
            $this->panelTemplate,
            [
                '{panelHeading}' => $panelHeading,
                '{prefix}' => $this->panelPrefix,
                '{type}' => $type,
                '{panelFooter}' => $panelFooter,
                '{panelBefore}' => $panelBefore,
                '{panelAfter}' => $panelAfter
            ]
        );
    }

    protected function initLayout()
    {
        Html::addCssClass($this->filterRowOptions, 'skip-export');
        if ($this->resizableColumns && $this->persistResize) {
            $key = empty($this->resizeStorageKey) ? Yii::$app->user->id : $this->resizeStorageKey;
            $gridId = empty($this->options['id']) ? $this->getId() : $this->options['id'];
            $this->containerOptions['data-resizable-columns-id'] = (empty($key) ? "kv-{$gridId}" : "kv-{$key}-{$gridId}");
        }
        $export = $this->renderExport();
        $toggleData = $this->renderToggleData();
        $sizer = $this->renderSizer();
        $collapse = $this->renderCollapse();
        $toolbar = strtr(
            $this->renderToolbar(),
            [
                '{export}' => $export,
                '{toggleData}' => $toggleData,
                '{sizer}' => $sizer,
                '{collapse}' => $collapse
            ]
        );
        $replace = ['{toolbar}' => $toolbar];
        if (strpos($this->layout, '{export}') > 0) {
            $replace['{export}'] = $export;
        }
        if (strpos($this->layout, '{toggleData}') > 0) {
            $replace['{toggleData}'] = $toggleData;
        }
        if (strpos($this->layout, '{sizer}') > 0) {
            $replace['{sizer}'] = $sizer;
        }
        if (strpos($this->layout, '{collapse}') > 0) {
            $replace['{collapse}'] = $collapse;
        }
        $this->layout = strtr($this->layout, $replace);
        $this->layout = str_replace('{items}', Html::tag('div', '{items}', $this->containerOptions), $this->layout);
        if (is_array($this->replaceTags) && !empty($this->replaceTags)) {
            foreach ($this->replaceTags as $key => $value) {
                if ($value instanceof \Closure) {
                    $value = call_user_func($value, $this);
                }
                $this->layout = str_replace($key, $value, $this->layout);
            }
        }
    }

    public function renderCollapse()
    {
        if (!$this->collapse) {
            return '';
        }
        $js = <<<JS
        $(document).on('click', '.kv-grid-toolbar .toggle-collapse', function () {
            $(this).toggleClass('active');
            $(this).find('i').toggleClass('fa-compress fa-expand');
            $(this).parents('.panel').find('.kv-grid-container').slideToggle();
            $(this).parents('.panel').find('.panel-footer').slideToggle();
            if ($(this).hasClass('active')) {
                if ($(this).parents('.panel').find('.kv-grid-container').length > 0) {
                    $.cookie($(this).parents('.panel').find('.kv-grid-container').attr('id'), 'true', {
                        expires: 365,
                        path: '/'
                    });
                }
            } else {
                $.removeCookie($(this).parents('.panel').find('.kv-grid-container').attr('id'), { path: '/' });
            }
        });
JS;
        $view = $this->getView();
        $view->registerJs($js);
        if (isset($_COOKIE[$this->containerOptions['id']])) {
            $btn = Html::button(FAv::icon('expand'), [
                'type' => 'button',
                'title' => Yii::t('app', 'Collapse'),
                'class' => 'btn btn-danger toggle-collapse active',
                'data-toggle' => 'tooltip',
                'data-original-title' => Yii::t('app', 'Collapse'),
            ]);
        } else {
            $btn = Html::button(FAv::icon('compress'), [
                'type' => 'button',
                'title' => Yii::t('app', 'Collapse'),
                'class' => 'btn btn-danger toggle-collapse',
                'data-toggle' => 'tooltip',
                'data-original-title' => Yii::t('app', 'Collapse'),
            ]);
        }
        return $btn;
    }

    public function renderSizer()
    {
        if (!$this->sizer || (is_array($this->sizerOptions['sizeVariants']) && sizeof($this->sizerOptions['sizeVariants']) == 0)) {
            return '';
        }

        $js = <<<JS
        $(document).on('select2:select', '.toolbar-sizer', function () {
            var url = $(this).find('option:selected').attr('data-url');
            var id = '#' + $(this).closest('[id^="pjax-container"]').attr('id');
            $.pjax.reload(id, {
                url: url,
                timeout: 5000,
                push: true
            });
        });
JS;

        $view = $this->getView();
        $view->registerJs($js);

        $options = [];
        foreach ($this->sizerOptions['sizeVariants'] as $size => $value) {
            $options[$size] = ['data-url' => Url::current([$this->sizerOptions['pageVar'] => $size])];
        }


        $sizer = Select2::widget([
            'name' => $this->sizerOptions['pageVar'],
            'data' => $this->sizerOptions['sizeVariants'],
            'theme' => Select2::THEME_BOOTSTRAP,
            'value' => !empty($this->dataProvider->getPagination()->pageSize) ? $this->dataProvider->getPagination()->pageSize : 'all',
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => false
            ],
            'options' => [
                'class' => 'toolbar-sizer',
                'options' => $options
            ]
        ]);
        return $sizer;
    }

    protected function renderToolbar()
    {
        if (empty($this->toolbar) || (!is_string($this->toolbar) && !is_array($this->toolbar))) {
            return '';
        }
        if (is_string($this->toolbar)) {
            return $this->toolbar;
        }
        $toolbar = '';
        foreach ($this->toolbar as $item) {
            if (is_array($item)) {
                $content = Html::beginTag('span');
                $content .= ArrayHelper::getValue($item, 'content', '');
                $content .= Html::endTag('span');
                $options = ArrayHelper::getValue($item, 'options', []);
                static::initCss($options, 'btn-group');
                $toolbar .= Html::tag('div', $content, $options);
            } else {
                $toolbar .= "\n{$item}";
            }
        }
        return $toolbar;
    }

    protected function beginPjax()
    {
        if (!$this->pjax) {
            return;
        }
        $view = $this->getView();
        if (empty($this->pjaxSettings['options']['id'])) {
            $this->pjaxSettings['options']['id'] = $this->options['id'] . '-pjax';
        }
        $container = 'jQuery("#' . $this->pjaxSettings['options']['id'] . '")';
        $js = $container;
        if (ArrayHelper::getvalue($this->pjaxSettings, 'neverTimeout', true)) {
            $js .= ".on('pjax:timeout', function(e){e.preventDefault()})";
        }
        $loadingCss = ArrayHelper::getvalue($this->pjaxSettings, 'loadingCssClass', 'kv-grid-loading');
        $postPjaxJs = "setTimeout({$this->_gridClientFunc}, 2500);";
        if ($loadingCss !== false) {
            $grid = 'jQuery("#' . $this->containerOptions['id'] . '")';
            if ($loadingCss === true) {
                $loadingCss = 'kv-grid-loading';
            }
            $js .= ".on('pjax:send', function(){{$grid}.addClass('{$loadingCss}')})";
            $postPjaxJs .= "{$grid}.removeClass('{$loadingCss}');";
        }
        if (!empty($postPjaxJs)) {
            $event = 'pjax:complete.' . hash('crc32', $postPjaxJs);
            $js .= ".off('{$event}').on('{$event}', function(){{$postPjaxJs}})";
        }
        if ($js != $container) {
            $view->registerJs("{$js};");
        }
        if (!isset($this->pjaxSettings['options']['enablePushState'])) {
            $this->pjaxSettings['options']['enablePushState'] = true;
        }
        Pjax::begin($this->pjaxSettings['options']);
        echo ArrayHelper::getValue($this->pjaxSettings, 'beforeGrid', '');
    }

    protected function endPjax()
    {
        echo ArrayHelper::getValue($this->pjaxSettings, 'afterGrid', '');
        if (!$this->pjax) {
            return;
        }
        Pjax::end();
    }







}