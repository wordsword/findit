<?php

namespace app\components;


use kartik\widgets\Select2;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\SkLinkSorter;
use yii\widgets\ListView;

class SkListView extends ListView
{
    /**
     * @var array the configuration for the sizer section.
     */
    public $sizer = [
        'sizes' => [12, 24, 32],
        'selectAction' => 'function(){}'
    ];

    public function init()
    {
        if ($this->dataProvider === null) {
            throw new InvalidConfigException('The "dataProvider" property must be set.');
        }

        $pagination = $this->dataProvider->getPagination();

        if ($pagination !== false && is_array($this->sizer) && is_array($this->sizer['sizes'])) {
            $pagination->defaultPageSize = $this->sizer['sizes'][0];
//            if (empty($pagination->pageSize) || !in_array($pagination->pageSize, $this->sizer['sizes'])) {
            /*if ($pagination->pageSize && !in_array($pagination->pageSize, $this->sizer['sizes'])) {
                $pagination->setPageSize($this->sizer['sizes'][0]);
            }*/
        }

        if ($this->emptyText === null) {
            $this->emptyText = Yii::t('yii', 'No results found.');
        }
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    public function renderSection($name)
    {
        switch ($name) {
            case '{summary}':
                return $this->renderSummary();
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            case '{sorter}':
                return $this->renderSorter();
            case '{sizer}':
                return $this->renderSizer();
            default:
                return false;
        }
    }

    public function renderSorter()
    {
        $sort = $this->dataProvider->getSort();
        if ($sort === false || empty($sort->attributes) || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class SkLinkSorter */
        $sorter = $this->sorter;
        $class = ArrayHelper::remove($sorter, 'class', SkLinkSorter::className());
        $sorter['sort'] = $sort;
        $sorter['view'] = $this->getView();

        ob_start();
        ob_implicit_flush(false);
        echo Html::beginTag('div', ['class' => 'sorter-wrap']);
        echo Html::beginTag('div', ['class' => 'sorter-select']);
        echo Html::tag('span', Yii::t('app', 'Sort by'), ['class' => 'sorter-title']);
        echo $class::widget($sorter);
        echo Html::endTag('div');
        echo Html::endTag('div');
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * Renders the sorter.
     * @return string the rendering result
     */
    public function renderSizer()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || !is_array($this->sizer) || !is_array($this->sizer['sizes'])) {
            return '';
        }
        $list = [];
        $sizes = $this->sizer['sizes'];
        foreach ($sizes as $size) {
            $list[$pagination->createUrl(0, $size)] = $size;
        }
        ob_start();
        ob_implicit_flush(false);
        echo Html::beginTag('div', ['class' => 'sizer-wrap']);
        echo Html::beginTag('div', ['class' => 'sizer-select']);
        echo Html::tag('span', Yii::t('app', 'Show'), ['class' => 'sizer-title']);
        echo Select2::widget([
            'id' => 'page-sizer',
            'name' => $pagination->pageSizeParam,
            'data' => $list,
            'value' => $pagination->createUrl($pagination->page, $pagination->pageSize),
            'theme' => Select2::THEME_BOOTSTRAP,
            'hideSearch' => true,
            'pluginOptions' => [
                'allowClear' => false,
            ],
            'pluginEvents' => [
                "select2:select" => $this->sizer['selectAction'],
            ]
        ]);
        echo Html::endTag('div');
        echo Html::endTag('div');
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}
