<?php

namespace app\components;

interface CategoryUI {
    
    public static function getBreadcrumbsLink();

    public static function getRedirectLink();

    public static function getAdminCtrlName();

    public static function getCategoryFiedls();
    
}