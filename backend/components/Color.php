<?php

namespace backend\components;

use yii\base\Model;

class Color extends Model
{
    public $red = 0;
    public $green = 0;
    public $blue = 0;
    public $alpha = 1;

    public static function getRandomColor(){
        return new self([
            'red' => mt_rand( 0, 255 ),
            'green' => mt_rand( 0, 255 ),
            'blue' => mt_rand( 0, 255 ),
        ]);
    }

    public function toRGBA(){
        return 'rgba(' . $this->red . ',' . $this->green . ',' . $this->blue . ',' . $this->alpha . ')';
    }

    public function toRGB(){
        return 'rgba(' . $this->red . ',' . $this->green . ',' . $this->blue . ')';
    }

    public function toHash(){
        return '#' . str_pad( dechex( $this->red ), 2, '0', STR_PAD_LEFT) . str_pad( dechex( $this->green ), 2, '0', STR_PAD_LEFT) . str_pad( dechex( $this->blue ), 2, '0', STR_PAD_LEFT);
    }
    // public function __construct()
}