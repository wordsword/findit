<?php

namespace backend\components;

use Yii;
use yii\db\Query;
use yii\helpers\Json;
use dosamigos\transliterator\TransliteratorHelper;

class DocumentLogger
{
    public static function writeLog($model, $reason)
    {
        $table = $model->tableName();
        $table = str_replace(['{', '}', '%'], '', $table);
        $time = time();

        $attributes = $model->attributes;
        $old_attributes = $model->oldAttributes;

        $id = $model->id ? $model->id : SkDbHelper::getAutoIncrement($table);

        $attrs = [];
        foreach ($attributes as $attribute => $value) {
            $attrs[] = [
                'attribute' => $attribute,
                'value' => $model->$attribute,
                'changed' => $old_attributes[$attribute] != $model->$attribute ? 1 : 0
            ];
        }

        Yii::$app->db->createCommand()->insert('shop_document_log', [
            'document_id' => $id,
            'created_at' => $time,
            'reason' => $reason,
            'user_id' => Yii::$app->user->identity->id ?? 0,
            'table' => $table,
            'columns' => Json::encode($attrs)
        ])->execute();
    }

    public static function writeLogArr($table, $id, $attributes, $reason)
    {
        $table = str_replace(['{', '}', '%'], '', $table);
        $time = time();

        $record = (new Query)->from($table)->where(['id' => $id])->one();

        $changed_attrs = array_keys($attributes);
        $attrs = [];

        foreach ($attributes as $attribute => $value) {
            $attrs[] = [
                'attribute' => $attribute,
                'value' => $value,
                'changed' => 1
            ];
        }

        if ($record != false)
            foreach ($record as $attribute => $value) {
                if (!in_array($attribute, $changed_attrs))
                    $attrs[] = [
                        'attribute' => $attribute,
                        'value' => $value,
                        'changed' => 0
                    ];
            }

        Yii::$app->db->createCommand()->insert('shop_document_log', [
            'document_id' => $id,
            'created_at' => $time,
            'reason' => $reason,
            'user_id' => Yii::$app->user->identity->id ?? 0,
            'table' => $table,
            'columns' => Json::encode($attrs)
        ])->execute();
    }
}
