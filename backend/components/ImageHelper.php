<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 25.06.2016
 * Time: 14:07
 */

namespace backend\components;


// use backend\modules\shop\models\Boutique;
// use backend\modules\shop\models\BoutiqueImage;
use backend\modules\shop\models\ProductImage;

// use backend\modules\shop\models\ProductOriginalImage;
// use backend\modules\shop\models\ProductThumbs;
use backend\modules\shop\Module;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

class ImageHelper
{
    const DS = DIRECTORY_SEPARATOR;

    public static function checkDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    public static function removeImage($article, $file)
    {
        $files = [
            Yii::getAlias('@frontend/web/uploads/products') . self::DS . $article . self::DS . $file,
        ];
        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    public static function removeUnusedImages($article)
    {
        $dirs = [
            Yii::getAlias('@frontend/web/uploads/products') . self::DS . $article . self::DS,
        ];

        $queryImg = new Query();
        $queryImg->from('{{%shop_product_images}}');
        $queryImg->select('path');
        $list = $queryImg->column();

        foreach ($dirs as $dir) {
            if (is_dir($dir)) {
                $files = scandir($dir);
                foreach ($files as $file) {
                    if (!is_dir($file) && $file !== '.' && $file !== '..' && !in_array($file, $list)) {
                        unlink($dir . $file);
                    }
                }
            }
        }
    }


    public static function removeAllImages($article)
    {

        $dirs = [
            Yii::getAlias('@frontend/web/uploads/products') . self::DS . $article . self::DS,
        ];

        $queryImg = new Query();
        $queryImg->from('{{%shop_product_images}}');
        $queryImg->select('path');
        $queryImg->where(['product_id' => $article]);
        $list = $queryImg->column();

        foreach ($dirs as $dir) {
            if (is_dir($dir)) {
                $files = scandir($dir);
                foreach ($files as $file) {
                    if (!is_dir($file) && $file !== '.' && $file !== '..' && in_array($file, $list)) {
                        unlink($dir . $file);
                    }
                }
            }
        }
        Yii::$app->db->createCommand()->delete('{{%shop_product_images}}', ['product_id' => $article])->execute();
    }
}