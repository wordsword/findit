<?php

namespace backend\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
use common\components\SkLangHelper;


class AliasesBehavior extends Behavior
{

    public $aliasAttribute = 'alias';
    public $model;


    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterSave($event)
    {
        $object = $event->sender;
        if (empty($this->model))
            $this->model = $object->className();
        if ($this->model == 'app\modules\main\models\Category') {
            $linkObject = $object->object;
            $useAlias = false;
            if (in_array($this->aliasAttribute, $linkObject::getCategoryFiedls())) {
                $useAlias = true;
            }
        } else {
            $useAlias = true;
        }
        if ($useAlias && (isset($event->changedAttributes[$this->aliasAttribute]) || $event->name = 'afterInsert')) {
            $sql  = 'SELECT * FROM {{%aliases}} ';
            $sql .= 'WHERE model=:model ';
            $sql .= 'AND primaryKey=:primaryKey ';
            $sql .= 'AND alias=:alias ';
            $alias = Yii::$app->db->createCommand($sql)->bindValues([
                ':model' => $this->model,
                ':primaryKey' => $object->primaryKey,
                ':alias' => SkLangHelper::getLangFromUrl($object->path),
            ])->queryOne();

            // echo '<pre>';
            // // print_r($alias);
            // echo '</pre>';

            // return;
            if (empty($alias)) {

                $result = Yii::$app->db->createCommand()->insert('{{%aliases}}', [
                    'alias' => SkLangHelper::getLangFromUrl($object->path),
                    'controller' => $object->ctrlName,
                    'model' => $this->model,
                    'primaryKey' => $object->primaryKey,
                    'redirectID' => 0,
                    'updated_at' => new Expression('NOW()')
                ])->execute();

                if ($result) {
                    $aliasID = Yii::$app->db->lastInsertID;
                } else {
                    $object->addError('alias', Yii::t('app', 'Alias is already taken'));
                    // $model->addError('alias')
                }
            } else {
                $aliasID = $alias['id'];
                if ($alias['redirectID'] != 0) {
                    $result = Yii::$app->db->createCommand()->update('{{%aliases}}', [
                        'redirectID' => 0,
                        'updated_at' => new Expression('NOW()'),
                    ], [
                        'id' => $aliasID,
                        'model' =>  str_replace("\\", '\\\\', $this->model)
                    ])->execute();
                    if (!$result) {
                        $object->addError('alias', Yii::t('app', 'Alias is already taken'));
                        // $model->addError('alias')
                    }
                }
            }


            Yii::$app->db->createCommand()->update($object->tableName(), ['alias' => $object->alias], ['id' => $object->primaryKey]);
            Yii::$app->db->createCommand()->update('{{%aliases}}', [
                'redirectID' => $aliasID,
                'updated_at' => new Expression('NOW()'),
            ], 'id!=:id AND primaryKey=:primaryKey AND model=:model', [
                'id' => $aliasID,
                'primaryKey' => $object->primaryKey,
                'model' =>  str_replace("\\", '\\\\', $this->model)
            ])->execute();
        }
    }

    public function afterDelete($event)
    {
        $object = $event->sender;
        if ($this->model == 'Category') {
            $linkObject = $object->object;
            $useAlias = false;
            if (in_array($this->aliasAttribute, $linkObject::getCategoryFiedls())) {
                $useAlias = true;
            }
        } else {
            $useAlias = true;
        }
        if ($useAlias) {
            Yii::$app->db->createCommand()->delete('{{%aliases}}', [
                'primaryKey' => $object->primaryKey,
                'model' => $this->model
            ])->execute();
        }
    }
}
