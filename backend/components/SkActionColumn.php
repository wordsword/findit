<?php

namespace app\components;


use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use rmrevin\yii\fontawesome\FA;
use Yii;

class SkActionColumn extends ActionColumn
{

    protected $_isDropdown = true;
    public $dropdown = true;
    public $dropdownMenu = [];
    public $dropdownButton = [
        'caret' => false,
        'class' => 'simple-button-button',
        'label' => '<i class="fa fa-ellipsis-v"></i>',
    ];

    public function init()
    {
        $this->initColumnSettings([
            'hiddenFromExport' => true,
            'mergeHeader' => false,
            'hAlign' => GridView::ALIGN_CENTER,
            'vAlign' => GridView::ALIGN_MIDDLE,
            'width' => '50px',
            'header' => false
        ]);
        /** @noinspection PhpUndefinedFieldInspection */
        $this->_isDropdown = ($this->grid->bootstrap && $this->dropdown);
        if (!isset($this->header)) {
            $this->header = Yii::t('kvgrid', 'Actions');
        }
        $this->parseFormat();
        $this->parseVisibility();
        parent::init();
        $this->initDefaultButtons();
        $this->setPageRows();
    }

}