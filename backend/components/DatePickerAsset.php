<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 16.12.2015
 * Time: 5:26
 */

namespace backend\components;

use Yii;


class DatePickerAsset extends \kartik\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath('@vendor/kartik-v/yii2-widget-datepicker/assets');
        $this->setupAssets('css', ['css/bootstrap-datepicker3', 'css/datepicker-kv']);
        $this->setupAssets('js', ['js/bootstrap-datepicker', 'js/datepicker-kv', 'js/locales/bootstrap-datepicker.'.Yii::$app->language.'.min']);
        parent::init();
    }
}