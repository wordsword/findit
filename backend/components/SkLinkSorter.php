<?php

namespace app\components;


use yii\helpers\Html;
use yii\widgets\LinkSorter;

class SkLinkSorter extends LinkSorter
{

    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $links = [];
        foreach ($attributes as $name) {
            $link = $this->sort->link($name, $this->linkOptions);
            if (is_array($link)){
                foreach ($link as $linkItem) {
                    $links[] = $linkItem;
                }
            } else {
                $links[] = $link;
            }
        }
        return $this->sort->getCurrentOrderLink($this->linkOptions).Html::ul($links, array_merge($this->options, ['encode' => false]));
    }

}