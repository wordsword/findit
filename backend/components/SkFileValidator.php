<?php
namespace app\components;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\validators\FileValidator;

class SkFileValidator extends FileValidator
{
    public $badExtensions = 'bat,exe,cmd,sh,php,pl,cgi,386,dll,com,torrent,js,app,jar,pif,vb,vbscript,wsf,asp,cer,csr,jsp,drv,sys,ade,adp,bas,chm,cpl,crt,csh,fxp,hlp,hta,inf,ins,isp,jse,htaccess,htpasswd,ksh,lnk,mdb,mde,mdt,mdw,msc,msi,msp,mst,ops,pcd,prg,reg,scr,sct,shb,shs,url,vbe,vbs,wsc,wsf,wsh,cgi,pl,py,pyc,pyo,phtml,php,php3,php4,php5,php6,php7,pcgi,pcgi3,pcgi4,pcgi5,pchi6,pchi7,inc,bin';
    
    public $badExtensionMsg;


    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = Yii::t('yii', 'File upload failed.');
        }
        if ($this->uploadRequired === null) {
            $this->uploadRequired = Yii::t('yii', 'Please upload a file.');
        }
        if ($this->tooMany === null) {
            $this->tooMany = Yii::t('yii', 'You can upload at most {limit, number} {limit, plural, one{file} other{files}}.');
        }
        if ($this->wrongExtension === null) {
            $this->wrongExtension = Yii::t('yii', 'Only files with these extensions are allowed: {extensions}.');
        }
        if ($this->badExtensionMsg === null) {
            $this->badExtensionMsg = Yii::t('app', 'The extension of the file "{file}" is denied to be uploaded.');
        }
        if ($this->tooBig === null) {
            $this->tooBig = Yii::t('yii', 'The file "{file}" is too big. Its size cannot exceed {limit, number} {limit, plural, one{byte} other{bytes}}.');
        }
        if ($this->tooSmall === null) {
            $this->tooSmall = Yii::t('yii', 'The file "{file}" is too small. Its size cannot be smaller than {limit, number} {limit, plural, one{byte} other{bytes}}.');
        }
        if (!is_array($this->badExtensions)) {
            $this->badExtensions = preg_split('/[\s,]+/', strtolower($this->badExtensions), -1, PREG_SPLIT_NO_EMPTY);
        } else {
            $this->badExtensions = array_map('strtolower', $this->badExtensions);
        }
        if (!is_array($this->extensions)) {
            $this->extensions = preg_split('/[\s,]+/', strtolower($this->extensions), -1, PREG_SPLIT_NO_EMPTY);
        } else {
            $this->extensions = array_map('strtolower', $this->extensions);
        }
        if ($this->wrongMimeType === null) {
            $this->wrongMimeType = Yii::t('yii', 'Only files with these MIME types are allowed: {mimeTypes}.');
        }
        if (!is_array($this->mimeTypes)) {
            $this->mimeTypes = preg_split('/[\s,]+/', strtolower($this->mimeTypes), -1, PREG_SPLIT_NO_EMPTY);
        } else {
            $this->mimeTypes = array_map('strtolower', $this->mimeTypes);
        }
    }
    
    protected function validateValue($file)
    {
        if (!$file instanceof UploadedFile || $file->error == UPLOAD_ERR_NO_FILE) {
            return [$this->uploadRequired, []];
        }

        switch ($file->error) {
            case UPLOAD_ERR_OK:
                if ($this->maxSize !== null && $file->size > $this->maxSize) {
                    return [$this->tooBig, ['file' => $file->name, 'limit' => $this->getSizeLimit()]];
                } elseif ($this->minSize !== null && $file->size < $this->minSize) {
                    return [$this->tooSmall, ['file' => $file->name, 'limit' => $this->minSize]];
                } elseif (!empty($this->extensions) && !$this->validateExtension($file)) {
                    return [$this->wrongExtension, ['file' => $file->name, 'extensions' => implode(', ', $this->extensions)]];
                } elseif (!empty($this->badExtensions) && !$this->validateBadExtension($file)) {
                    return [$this->badExtensionMsg, ['file' => $file->name, 'extensions' => implode(', ', $this->badExtensions)]];
                } elseif (!empty($this->mimeTypes) &&  !in_array(FileHelper::getMimeType($file->tempName), $this->mimeTypes, false)) {
                    return [$this->wrongMimeType, ['file' => $file->name, 'mimeTypes' => implode(', ', $this->mimeTypes)]];
                } else {
                    return null;
                }
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                return [$this->tooBig, ['file' => $file->name, 'limit' => $this->getSizeLimit()]];
            case UPLOAD_ERR_PARTIAL:
                Yii::warning('File was only partially uploaded: ' . $file->name, __METHOD__);
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                Yii::warning('Missing the temporary folder to store the uploaded file: ' . $file->name, __METHOD__);
                break;
            case UPLOAD_ERR_CANT_WRITE:
                Yii::warning('Failed to write the uploaded file to disk: ' . $file->name, __METHOD__);
                break;
            case UPLOAD_ERR_EXTENSION:
                Yii::warning('File upload was stopped by some PHP extension: ' . $file->name, __METHOD__);
                break;
            default:
                break;
        }

        return [$this->message, []];
    }
    
    /**
     * Checks if given uploaded file have correct type (extension) according current validator settings.
     * @param UploadedFile $file
     * @return boolean
     */
    protected function validateBadExtension($file)
    {
        $extension = mb_strtolower($file->extension, 'utf-8');

        if (in_array($extension, $this->badExtensions, true)) {
            return false;
        }
        return true;
    }
}
