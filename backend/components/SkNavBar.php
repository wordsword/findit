<?php

namespace backend\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\NavBar;

class SkNavBar extends NavBar
{
    public $beforeBrand = false;
    public $afterBrand = false;
    public $brandWrapOptions = ['class'=>'navbar-brand-wrap open'];

    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        $this->clientOptions = false;
        if (empty($this->options['class'])) {
            Html::addCssClass($this->options, ['navbar', 'navbar-default']);
        } else {
            Html::addCssClass($this->options, ['widget' => 'navbar']);
        }
        if (empty($this->options['role'])) {
            $this->options['role'] = 'navigation';
        }
        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'nav');
        echo Html::beginTag($tag, $options);
        if ($this->renderInnerContainer) {
            if (!isset($this->innerContainerOptions['class'])) {
                Html::addCssClass($this->innerContainerOptions, 'container');
            }
            echo Html::beginTag('div', $this->innerContainerOptions);
        }
        echo Html::beginTag('div', ['class' => 'navbar-header']);
        if (!isset($this->containerOptions['id'])) {
            $this->containerOptions['id'] = "{$this->options['id']}";//-collapse
        }
        //echo $this->renderToggleButton();
        if ($this->brandLabel !== false) {
            echo Html::beginTag('div',$this->brandWrapOptions);
                Html::addCssClass($this->brandOptions, ['widget' => 'navbar-brand']);
                if ($this->beforeBrand !== false) {
                    echo $this->beforeBrand;
                }
                echo Html::a($this->brandLabel, $this->brandUrl === false ? Yii::$app->homeUrl : $this->brandUrl, $this->brandOptions);
                if ($this->afterBrand !== false) {
                    echo $this->afterBrand;
                }
            echo Html::endTag('div');
        }
        echo Html::endTag('div');
        //Html::addCssClass($this->containerOptions, ['collapse' => 'collapse', 'widget' => 'navbar-collapse']);
        $options = $this->containerOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        echo Html::beginTag($tag, $options);
    }
}