<?php

namespace backend\components;

use yii\base\Object;


function mb_ord($string) {
	if (extension_loaded('mbstring') === true) {
		mb_language('Neutral');
		mb_internal_encoding('UTF-8');
		mb_detect_order(array('UTF-8', 'ISO-8859-15', 'ISO-8859-1', 'ASCII'));
		$result = unpack('N', mb_convert_encoding($string, 'UCS-4BE', 'UTF-8'));
		if (is_array($result) === true) return $result[1];
	}
	return ord($string);
}


class Compressor
{
//     String.prototype.compress = function (asArray) {
//     "use strict";
//     // Build the dictionary.
//     asArray = (asArray === true);
//     var i,
//         dictionary = {},
//         uncompressed = this,
//         c,
//         wc,
//         w = "",
//         result = [],
//         ASCII = '',
//         dictSize = 256;
//     for (i = 0; i < 256; i += 1) {
//         dictionary[String.fromCharCode(i)] = i;
//     }

//     for (i = 0; i < uncompressed.length; i += 1) {
//         c = uncompressed.charAt(i);
//         wc = w + c;
//         //Do not use dictionary[wc] because javascript arrays
//         //will return values for array['pop'], array['push'] etc
//         // if (dictionary[wc]) {
//         if (dictionary.hasOwnProperty(wc)) {
//             w = wc;
//         } else {
//             result.push(dictionary[w]);
//             ASCII += String.fromCharCode(dictionary[w]);
//             // Add wc to the dictionary.
//             dictionary[wc] = dictSize++;
//             w = String(c);
//         }
//     }

//     // Output the code for w.
//     if (w !== "") {
//         result.push(dictionary[w]);
//         ASCII += String.fromCharCode(dictionary[w]);
//     }
//     return asArray ? result : ASCII;
// };

    public static function decompress(string $binary) {
        $dictionary_count = 256;
        $bits = 8;
        $codes = array();
        $rest = 0;
        $rest_length = 0;
        
        mb_internal_encoding("UTF-8"); 
        for ($i = 0; $i < mb_strlen($binary); $i++ ) {$codes[] = mb_ord(mb_substr($binary, $i, 1)); }
            
        // decompression
        $dictionary = range("\0", "\xFF");
        $return = "";
        foreach ($codes as $i => $code) {
            $element = $dictionary[$code];
            if (!isset($element)) $element = $word . $word[0];
            $return .= $element;
            if ($i) $dictionary[] = $word . $element[0];			
            $word = $element;
        }
        return $return;
    }
}