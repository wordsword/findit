<?php

namespace app\components;

interface MenuUI {
    
    public function getMenuObjectType();

    public function getCtrlName();
    
    public function getUrl();
    
}