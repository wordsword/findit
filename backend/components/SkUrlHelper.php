<?php

namespace backend\components;

use dosamigos\transliterator\TransliteratorHelper;

class SkUrlHelper
{
    public static function TranslitUrl(string $str){
        $str = TransliteratorHelper::process($str, '', 'en');
        $str = str_replace(' ', '-', $str);
        $str = preg_replace("/[^A-Za-z0-9 ]/", '-', $str);
        $str = trim($str, '-');
        return strtolower($str);
    }

}
?>