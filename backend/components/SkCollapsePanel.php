<?php
namespace app\components;

use rmrevin\yii\fontawesome\FA;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Html;

class SkCollapsePanel extends Widget
{

    public $panelOptions;
    public $panelType = 'default';
    public $heading;
    public $toolbar;
    public $collapse = true;
    public $toolbarClass = 'toolbar';
    public $contentOptions;

    public $panelHeadingTemplate = <<<HTML
<div class="panel-heading">
    <h3 class="panel-title">
        {heading}
    </h3>
</div>
HTML;
    public $panelToolbarTemplate = <<<HTML
<div class="panel-body {toolbarClass}">
    <span>
        {toolbar}
    </span>
</div>
HTML;

    public function init()
    {
        ob_start();
        ob_implicit_flush(false);
    }

    public function run()
    {
        $content = ob_get_clean();
        $this->renderPanel($content);
    }

    protected function renderPanel($content)
    {
        if (!isset($this->contentOptions['id'])) {
            $this->contentOptions['id'] = $this->getId();
        }
        $panelID = 'panel-'.$this->contentOptions['id'];
        Html::addCssClass($this->panelOptions,['panel','panel-'.$this->panelType,$panelID]);
        echo Html::beginTag('div',$this->panelOptions);
        echo strtr($this->panelHeadingTemplate,[
            '{heading}'=>$this->heading
        ]);
        $collapse = $this->renderCollapse();
        $toolbar = $this->renderToolbar();
        if (strpos($toolbar, '{collapse}') !== false) {
            $toolbar = strtr($toolbar,[
                '{collapse}'=>$collapse
            ]);
        }
        $this->panelToolbarTemplate = strtr($this->panelToolbarTemplate,[
            '{toolbarClass}' => $this->toolbarClass
        ]);
        echo strtr($this->panelToolbarTemplate,[
            '{toolbar}' => $toolbar
        ]);
        echo Html::tag('div','',['class'=>'clearfix']);
        Html::addCssClass($this->contentOptions,'collapsed');
        if (isset($_COOKIE[$this->contentOptions['id']])) {
            Html::addCssStyle($this->contentOptions,['display'=>'none']);
        }
        echo Html::beginTag('div',$this->contentOptions);
        echo $content;
        echo Html::endTag('div');
        echo Html::endTag('div');

    }

    public function renderCollapse()
    {
        if (!$this->collapse) {
            return '';
        }
        $js = <<<JS
        $(document).on('click', '.{$this->toolbarClass} .toggle-collapse', function () {
            $(this).toggleClass('active');
            $(this).find('i').toggleClass('fa-compress fa-expand');
            $('.category-wrap').each(function(){
                $(this).removeClass('wide');
            });
            $(this).parents('.panel').find('.collapsed').slideToggle('100',function(){
                $('.category-wrap').each(function(){
                    blockWidth = $(this).width();
                    innerWidth = $(this).find('.chtrigger').width();
                    innerWidth += $(this).find('.link-text span').width();
                    innerWidth += $(this).find('.action-buttons').width();
                    if ($(this).find('.action-buttons .counter').is(':hidden')) {
                        innerWidth += 25;
                    }
                    innerWidth += $(this).find('.extrigger').width();
                    if (innerWidth > blockWidth) {
                        $(this).addClass('wide');
                    } else {
                        $(this).removeClass('wide');
                    }
                });
            });
            if ($(this).hasClass('active')) {
                if ($(this).parents('.panel').find('.collapsed').length > 0) {
                    $.cookie($(this).parents('.panel').find('.collapsed').attr('id'), 'true', {
                        expires: 365,
                        path: '/'
                    });
                }
                $(this).trigger('skColapsedPanel:close', [null, $(this).closest('.panel').find('.collapsed').attr('id')]);
            } else {
                $.removeCookie($(this).parents('.panel').find('.collapsed').attr('id'), { path: '/' });
                $(this).trigger('skColapsedPanel:open', [null, $(this).closest('.panel').find('.collapsed').attr('id')]);
            }
        });
JS;
        $view = $this->getView();
        $view->registerJs($js);
        if (isset($_COOKIE[$this->contentOptions['id']])) {
            $btn = Html::button(FA::icon('expand'), [
                'type' => 'button',
                'title' => Yii::t('app', 'Collapse'),
                'class' => 'btn btn-danger toggle-collapse active',
                'data-toggle' => 'tooltip',
                'data-original-title' => Yii::t('app', 'Collapse'),
            ]);
        } else {
            $btn = Html::button(FA::icon('compress'), [
                'type' => 'button',
                'title' => Yii::t('app', 'Collapse'),
                'class' => 'btn btn-danger toggle-collapse',
                'data-toggle' => 'tooltip',
                'data-original-title' => Yii::t('app', 'Collapse'),
            ]);
        }
        return $btn;
    }

    protected function renderToolbar()
    {
        if (empty($this->toolbar) || (!is_string($this->toolbar) && !is_array($this->toolbar))) {
            return '';
        }
        if (is_string($this->toolbar)) {
            return $this->toolbar;
        }
        $toolbar = '';
        foreach ($this->toolbar as $item) {
            $toolbar .= $item;
        }
        return $toolbar;
    }

}