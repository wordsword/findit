<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 17.08.2016
 * Time: 9:32
 */

namespace app\components;

use ReflectionClass;
use yii\base\DynamicModel;

class SkDynamicModel extends DynamicModel
{
    private $_formName;

    public function formName()
    {
        if (empty($this->_formName)) {
            $reflector = new ReflectionClass($this);
            $this->_formName = $reflector->getShortName();
        }
        return $this->_formName;
    }

    public function setFormName($value) {
        $this->_formName = $value;
    }

    private $_attributeLabels = [];

    public function attributeLabels()
    {
        return $this->_attributeLabels;
    }

    public function setAttributeLabels($attributeLabels)
    {
        $this->_attributeLabels = $attributeLabels;
    }
}