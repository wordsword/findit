<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 14.02.2016
 * Time: 2:28
 */

namespace backend\components;

use common\components\SkLangHelper;
use Yii;
use yii\validators\Validator;

class SkAliasValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $sql  = "SELECT COUNT(*) FROM aliases ";
        $sql .= "WHERE alias=:alias AND primaryKey!=:primaryKey AND model!=:model ";
        $count = Yii::$app->db->createCommand($sql)->bindValues([
            ':alias'=>SkLangHelper::getLangFromUrl($model->path),
            ':model'=>str_replace("\\",'\\\\',$model->className()),
            ':primaryKey'=> $model->isNewRecord ? 0 : $model->primaryKey,
        ])->queryScalar();
        if ($count > 0) {
            $this->addError($model, $attribute, Yii::t('app','This alias has already been taken.'));
        }
    }
}