<?php

namespace backend\components;

use Yii;
use yii\db\Query;
use yii\db\Exception;
use dosamigos\transliterator\TransliteratorHelper;

class SkDbHelper
{
    public static function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    public static function getDbName()
    {
        return self::getDsnAttribute('dbname', Yii::$app->db->dsn);
    }

    public static function getHost()
    {
        return self::getDsnAttribute('host', Yii::$app->db->dsn);
    }

    public static function getAutoIncrement($table) {
        return (new Query)
            ->from('INFORMATION_SCHEMA.TABLES')
            ->select('AUTO_INCREMENT')
            ->where([
                'TABLE_SCHEMA' => self::getDbName(),
                'TABLE_NAME' => $table
            ])
            ->scalar();
    }

    public static function checkTable($table) {
        return (new Query)
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where([
                'TABLE_SCHEMA' => self::getDbName(),
                'TABLE_NAME' => $table
            ])
            ->count() > 0;
    }


    public static function nativeQuery($sql, $type = 'all')
    {
        $dsn = Yii::$app->db->dsn;

        $host = self::getDsnAttribute('host', $dsn);
        $dbName = self::getDsnAttribute('dbname', $dsn);
        $username = Yii::$app->db->username;
        $password = Yii::$app->db->password;

        $link = mysqli_connect($host, $username, $password, $dbName);

        $result = mysqli_query($link, $sql);

        $res = null;
        if ($result !== false) {
            switch ($type) {
                case 'execute': {
                        $res = true;
                        break;
                    }
                case 'one': {
                        if (mysqli_num_rows($result) !== 0)
                            $res = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        break;
                    }
                case 'scalar': {
                        if (mysqli_num_rows($result) !== 0)
                            $res = array_shift(mysqli_fetch_array($result));
                        break;
                    }
                case 'all':
                default: {
                        if (mysqli_num_rows($result) !== 0)
                            $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
                        else
                            $res = [];
                        break;
                    }
            }

            if ($result !== true)
                mysqli_free_result($result);
        } else {
            throw new Exception(mysqli_error($link));
        }

        mysqli_close($link);
        return $res;
    }
}
