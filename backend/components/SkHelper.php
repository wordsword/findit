<?php

namespace backend\components;

use Yii;

class SkHelper
{
    
    public static function getActiveTab($currTabID,$defTabID,$sessionKey) {
        if (!isset(Yii::$app->session[$sessionKey]) && $currTabID === $defTabID) {
            return true;
        } elseif (isset(Yii::$app->session[$sessionKey]) && $currTabID === Yii::$app->session[$sessionKey]) {
            return true;
        } else {
            return false;
        }
    }

    public static function varDump($value,$end=false) {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
        if ($end)
            Yii::$app->end();
    }
}

