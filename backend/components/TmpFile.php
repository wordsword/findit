<?php

namespace backend\components;

class TmpFile
{
    public $filename;

    public function __construct()
    {
        $this->filename = tempnam(sys_get_temp_dir(), 'sql');
    }

    public function __destruct()
    {
        @unlink($this->filename);
    }

    public function __toString()
    {
        return $this->filename;
    }
}