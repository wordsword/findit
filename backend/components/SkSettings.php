<?php
namespace backend\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class SkSettings extends Component {

    private $_attributes;

    public function init()
    {
        $request = Yii::$app->db->createCommand("SELECT * FROM {{%settings}}")->queryAll();
        $this->_attributes = ArrayHelper::map($request,'key','value');
    }

    public function getValue($key)
    {
        if (isset($this->_attributes[$key])) {
            return $this->_attributes[$key];
        } else {
            return null;
        }
    }

}