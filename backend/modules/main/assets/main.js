$(document).ready(function () {
    $(document).on('click', '#product-tabs a', function () {
        $.cookie('active-product-tab', $(this).attr('href'), {
            expires: 365,
            path: '/'
        });
    });
    $(document).on('click', '#boutique-tabs a', function () {
        $.cookie('active-boutique-tab', $(this).attr('href'), {
            expires: 365,
            path: '/'
        });
    });

    $(document).on('click', '#settings-tabs a', function () {
        $.cookie('active-settings-tab', $(this).attr('href'), {
            expires: 365,
            path: '/'
        });
    });

    $(document).on('pjax:success', function (e, data, status, xhr, options) {
        var target = $(e.target);
        if (typeof (options.data) == 'string') {
            var query = URI.parseQuery(options.data);
            if ((target.attr('id') == 'option-grid') && ('deleteOption' in query)) {
                updateStockGrid();
                ubdateBarGrid();
            }
        }
        if (target.attr('id') == 'stock-grid') {
            $('.thumbs-grid-wrapper').replaceWith($(data).find('.thumbs-grid-wrapper'));
            $('#products-images').replaceWith($(data).find('#products-images'));
            $('#barcode-grid').replaceWith($(data).find('#barcode-grid'));
        }


        $('[data-krajee-select2]').each(function () {
            var settings = $(this).attr('data-krajee-select2'),
                id = $(this).attr('id');
            settings = window[settings];
            if (typeof (settings) == 'undefined') {
                settings = window['defS2Settings'];
            }
            $(this).select2(settings);
            $.when($(this).select2(settings)).done(initS2Loading(id, $(this).attr('data-s2-options')));
        });
        $('.kv-grid-container').each(function () {
            if ($(this).find('.radio-item').length > 0 && $(this).find('.radio-item.checked').length == 0) {
                $(this).find('.radio-item:first').trigger('click');
            }
        });

        if (target.attr('id') == 'opt-grid') {
            $('.promo-opt-count').text(target.find('.summary').find('span').text());
        }
    });

    function updateStockGrid() {
        var data = $('#stock-grid').closest("form").serialize() + "&save=0";
        var containerID = '#stock-grid';
        $.pjax({
            url: location.href,
            type: "POST",
            container: containerID,
            fragment: containerID,
            scrollTo: false,
            timeout: 3000,
            data: data
        });
    }

    function updateBarGrid() {
        var data = $('#barcode-grid').closest("form").serialize() + "&save=0";
        var containerID = '#barcode-grid';
        $.pjax({
            url: location.href,
            type: "POST",
            container: containerID,
            fragment: containerID,
            scrollTo: false,
            timeout: 3000,
            data: data
        });
    }

    $(document).on('change', '.product-option', function (event) {
        updateStockGrid();
    });
    $(document).on('change', '.product-feature, .product-thumbs-option', function (event) {
        var target = $(event.target);
        var data = target.closest("form").serialize() + "&save=0";
        var containerID = '#' + $(this).closest('.grid-view').attr('id');
        $.pjax({
            url: location.href,
            type: "POST",
            container: containerID,
            fragment: containerID,
            scrollTo: false,
            timeout: 15000,
            data: data
        });
    });
    $(document).on('change', '.price-currency', function (event) {
        var target = $(event.target);
        var data = target.closest("form").serialize() + "&save=0";
        var containerID = '#stock-grid';
        $.pjax({
            url: location.href,
            type: "POST",
            container: containerID,
            fragment: containerID,
            scrollTo: false,
            timeout: 15000,
            data: data
        });
    });

    $(document).on('change', '.product-feature-lang', function (event) {
        var target = $(event.target);
        var lang = target.find("option:selected").val();
        $(this).closest("tr").find("[data-language]").addClass("hidden");
        $(this).closest("tr").find("[data-language=" + lang + "]").removeClass("hidden");
    });
    $(document).on('change', '.product-feature-lang-all', function (event) {
        var target = $(event.target);
        var lang = target.find("option:selected").val();
        $('.product-feature-lang').each(function () {
            $(this).val(lang);
            $(this).trigger('change');
        });
    });
    $(document).on('click', '#add-product-option, #add-product-feature, #add-product-stock, #add-product-pack, #add-product-barcode, #add-product-supplier-code, .copy-stock-field', function (e) {
        e.preventDefault();
        var data;
        if ($(this).hasClass('copy-stock-field')) {
            console.log($(this).closest('tr').attr('data-key'));
            data = $(this).closest("form").serialize() + "&save=0&" + $(this).attr('data-action') + "=1" + "&data-key=" + $(this).closest('tr').attr('data-key');
        } else {
            data = $(this).closest("form").serialize() + "&save=0&" + $(this).attr('data-action') + "=1";
        }
        var containerID = '#' + $(this).closest('.grid-view').attr('id');
        $.pjax({
            url: location.href,
            type: "POST",
            container: containerID,
            fragment: containerID,
            scrollTo: false,
            timeout: 3000,
            data: data
        });
    });
    $(document).on('click', '.del-attr', function (e) {
        e.preventDefault();
        var $this = $(this);
        var form = $this.closest("form");
        var containerID = '#' + $(this).closest('.grid-view').attr('id');
        skConfirmAction($this.data('confirm-msg'), function (result) {
            $this.closest('tr').remove();
            var data = form.serialize() + "&save=0&" + $this.attr('data-action') + "=1";
            console.log(containerID);
            $.pjax({
                url: location.href,
                type: "POST",
                container: containerID,
                fragment: containerID,
                scrollTo: false,
                timeout: 3000,
                data: data
            });
        }, null, $this.data('msg'));
    });
    $(document).on('change', '.checkbox-wrap input[type="checkbox"]', function () {
        if ($(this).closest('.grid-view').find('.checkbox-wrap input:checked').length > 0) {
            $(this).closest('.grid-view').find('.del-all-attr').removeClass('disabled');
        } else {
            $(this).closest('.grid-view').find('.del-all-attr').addClass('disabled');
        }
    });
    $(document).on('click', '.del-all-attr', function () {
        var keys = $(this).closest('.grid-view').yiiGridView('getSelectedRows');
        var containerID = '#' + $(this).closest('.grid-view').attr('id');
        if (keys.length > 0) {
            var $this = $(this);
            var form = $this.closest("form");
            count = keys.length;
            skConfirm($this.data('confirm-msg').replace('{n}', count), function () {
                $.each(keys, function (index, key) {
                    $this.closest('.grid-view').find('tr[data-key=' + key + ']').remove();
                });
                var data = form.serialize() + "&save=0&" + $this.attr('data-action') + "=1";
                $.pjax({
                    url: location.href,
                    type: "POST",
                    container: containerID,
                    fragment: containerID,
                    scrollTo: false,
                    timeout: 3000,
                    data: data
                });
            });
        }
    });
    var lockAction = false;
    $(document).on('keydown', '#feature-grid .form-control.value', function () {
        if (!lockAction) {
            lockAction = true;
            $(this).closest('td').find('[data-krajee-select2]').select2("val", "");
            $(this).closest('td').find('.form-control.value').addClass('edited');
            lockAction = false;
        }
    });
    $(document).on('change', '#feature-grid .attribute-list-id', function (event) {
        if (!lockAction) {
            lockAction = true;
            var value = $(this).find('option:selected').text();
            if ($(this).find('option:selected').val() != '') {
                $(this).closest('td').find('.form-control.value').removeClass('edited').val(value);
            } else {
                $(this).closest('td').find('.form-control.value').removeClass('edited').val('');
            }
            lockAction = false;
        }
    });
    $(document).on('change', '.base-price', function () {
        $(this).closest('tr').find('.discount-price').val($(this).val());
    });
    $(document).on('keydown', '.base-price, .discount-price', function (event) {
        if (event.keyCode == 13)
            $(this).blur();
    });

    function getCoords(elem) { // кроме IE8-
        var box = elem.getBoundingClientRect();
        return {
            top: Math.round(box.top + pageYOffset),
            left: Math.round(box.left + pageXOffset)
        };
    }

    $("body").click(function (e) {
        var target = $(e.target);
        if (!target.hasClass('stock-price-input-form') && target.closest('.stock-price-input-form').length == 0) {
            $('.stock-price-input-form').addClass('hidden');
        }
    });
    $(document).on('click', '.stock-price', function () {
        var position = getCoords(this);
        var gridPosition = getCoords($('#stock-grid').get(0));
        $('.stock-price-input-form').find('.base-price').val($(this).find('input.base-price').val());
        $('.stock-price-input-form').find('.discount-price').val($(this).find('input.discount-price').val());
        $('.stock-price-input-form').attr('data-key', $(this).attr('data-key'));
        $('.stock-price-input-form').removeClass('hidden');
        $('.stock-price-input-form').css({
            'top': (position.top - gridPosition.top) + 'px',
            'left': (position.left - gridPosition.left + 10) + 'px',
        });

    });
    $(document).on('click', '.stock-price-input-form .cancel', function () {
        $('.stock-price-input-form').addClass('hidden');
    });
    $(document).on('click', '.stock-price-input-form .save', function () {
        var basePrice = $('.stock-price-input-form .base-price').val();
        var discountPrice = $('.stock-price-input-form .discount-price').val();
        $('.stock-price-input-form').addClass('hidden');
        var block = $('.stock-price[data-key=' + $('.stock-price-input-form').attr('data-key') + ']');
        block.find('span.base-price').text(basePrice);
        block.find('span.discount-price').text(discountPrice);
        block.find('input.base-price').val(basePrice);
        block.find('input.discount-price').val(discountPrice);

    });
    $(document).on('change', '.stock-price-input-form .base-price', function () {
        $('.stock-price-input-form').find('.discount-price').val($(this).val());
    });
    $(document).on('change', '[name="ProductStore[quantity][]"]', function () {
        $total = 0;
        $('[name="ProductStore[quantity][]"]').each(function () {
            $total += Number($(this).val());
        });
        $('.total-quantity').text($total);
    });

    $(document).on('click', '.upload-image', function () {
        if (typeof ($(this).attr('data-key')) != 'undefined')
            $('#product-images-upload').trigger('click');
        else
            skAlert($skMsgList.emptyProductMsg.msg, $skMsgList.emptyProductMsg.title, null, 'alert-warning');
    });

    function initializeImagesUpload() {
        var $this = $('.product-images-upload, .boutique-images-upload');
        if ($this.length == 0)
            return;
        var queryParams = '';
        var options = {
            uploadUrl: $this.attr('uploadUrl'),
            uploadExtraData: {"model": $this.attr('model')},
            showCaption: false,
            showPreview: false,
            showRemove: false,
            showUpload: false,
            showCancel: false,
            showUploadedThumbs: false,
            removeFromPreviewOnError: false,
            language: $this.attr('language'),
            allowedFileTypes: ['image'],
            layoutTemplates: {
                main2: '{remove} {browse}',
                btnBrowse: '<div tabindex="500" class="btn btn-inverse btn-file btn-upload-images" {status}>' + $this.attr('btnBrowse') + '</div>'
            },
            elErrorContainer: '#' + $('.product-images, .boutique-images').find('.images-errors').attr('id'),
            msgErrorClass: 'alert alert-block alert-danger'
        };
        var fileinput = $('#' + $this.attr('id'));
        fileinput.fileinput(options).on('filebatchselected', function (event, files) {
            queryParams = '';
            fileinput.fileinput('upload');
            $('#products-images .kv-grid-container').addClass('loading');
            $('#boutique-images .kv-grid-container').addClass('loading');
        }).on('fileuploaderror', function (event, data) {
            var $errors = $('.product-images').find('.images-errors').clone();
            $errors.removeAttr('style');
            $('#products-images .kv-grid-container').removeClass('loading');
            $('#boutique-images .kv-grid-container').removeClass('loading');
            skAlert($errors.html(), $this.attr('errorTitle'), function () {
                $('#' + $this.attr('id')).fileinput('clear');
            }, 'alert-danger');
        }).on('fileuploaded', function (event, data) {
            if (typeof (data.response.queryParams) != 'undefined' && data.response.queryParams.length > 0) {
                queryParams += '&' + data.response.queryParams;
            }
        }).on('filebatchuploadcomplete', function () {
            var pjaxData = $this.closest('form').serialize() + "&save=0&" + $this.attr('data-action') + "=1";
            pjaxData += queryParams;
            var containerID = '#products-images';
            if ($this.hasClass('boutique-images-upload')) {
                containerID = '#boutique-images';
            }
            $.pjax({
                url: location.href,
                type: "POST",
                container: containerID,
                fragment: containerID,
                scrollTo: false,
                timeout: 3000,
                data: pjaxData
            });
        });
    }

    initializeImagesUpload();

    var senderImage;

    function initializeImageItemUpload() {
        var $this = $('.product-image-item-upload');
        if ($this.length == 0)
            return;
        var options = {
            uploadUrl: $this.attr('uploadUrl'),
            uploadExtraData: function () {
                return {
                    "model": senderImage.attr('data-model'),
                    "key": senderImage.attr('data-key'),
                    "type": senderImage.attr('data-type')
                }
            },
            showCaption: false,
            showPreview: false,
            showRemove: false,
            showUpload: false,
            showCancel: false,
            showUploadedThumbs: false,
            removeFromPreviewOnError: false,
            language: $this.attr('language'),
            allowedFileTypes: ['image'],
            layoutTemplates: {
                main2: '{remove} {browse}',
                btnBrowse: '<div tabindex="500" class="btn btn-inverse btn-file btn-upload-images" {status}>' + $this.attr('btnBrowse') + '</div>'
            },
            elErrorContainer: '#' + $('.product-images').find('.images-errors').attr('id'),
            msgErrorClass: 'alert alert-block alert-danger'
        };
        var fileinput = $('#' + $this.attr('id'));
        fileinput.fileinput(options).on('filebatchselected', function (event, files) {
            var container = '#products-images';
            if (senderImage.hasClass('thumbs-wrap')) {
                container = '#thumbs-grid';
            }
            $(container).find('.kv-grid-container').addClass('loading');
            fileinput.fileinput('upload');
        }).on('fileuploaderror', function (event, data) {
            var $errors = $('.product-images').find('.images-errors').clone();
            $errors.removeAttr('style');
            var container = '#products-images';
            if (senderImage.hasClass('thumbs-wrap')) {
                container = '#thumbs-grid';
            }
            $(container).find('.kv-grid-container').removeClass('loading');
            skAlert($errors.html(), $this.attr('errorTitle'), function () {
                $('#' + $this.attr('id')).fileinput('clear');
            }, 'alert-danger');
        }).on('fileuploaded', function (event, data) {
            if (typeof (data.response.filename) != 'undefined' && data.response.filename.length > 0) {
                senderImage.closest('td').find('input.path').val(data.response.filename)
            }
            var container = '#products-images';
            if (senderImage.hasClass('thumbs-wrap')) {
                container = '#thumbs-grid';
            }
            var pjaxData = $this.closest('form').serialize() + "&save=0&" + $this.attr('data-action') + "=1";
            $.pjax({
                url: location.href,
                type: "POST",
                container: container,
                fragment: container,
                scrollTo: false,
                timeout: 3000,
                data: pjaxData
            });
        });
    }

    initializeImageItemUpload();

    $(document).on('click', '.image-wrap, .thumbs-wrap', function (e) {
        senderImage = $(this);
        if (typeof ($(this).attr('data-model')) != 'undefined') {
            $('.product-image-item-upload, .boutique-image-item-upload').attr('data-type', $(this).attr('data-type'));
            $('.product-image-item-upload, .boutique-image-item-upload').attr('data-key', $(this).attr('data-key'));
            $('.product-image-item-upload, .boutique-image-item-upload').trigger('click');
        } else
            skAlert($skMsgList.emptyProductMsg.msg, $skMsgList.emptyProductMsg.title, null, 'alert-warning');
    });

    $(document).on('click', '.product-card #parent_id', function (event) {
        var target = $(this);
        var category_container = $(document).find('.product-card .custom-modal-category');
        var old_category = $(document).find('.custom-modal-category #treeview div.category-item.active');

        var fancy = $.fancybox.open({
            src: category_container,
            type: 'inline',
            touch: false,
            buttons: false,
        });

        $('.actionOk').on('click', function (e) {
            event = 'btnClick';
            fancy.close();
            var category_input = $(document).find('input#product-parent_id');
            var active_category = $(document).find('.custom-modal-category #treeview div.category-item.active');

            if (active_category.length) {
                var current_id = active_category.attr('data-id');
                category_input.val(current_id);
                target.val(categories[current_id]);
            } else {
                category_input.val('');
                target.val('');
            }
        });

        $('.actionCancel').on('click', function (e) {
            event = 'btnClick';
            fancy.close();
            if (old_category.length) {
                var old_id = old_category.attr('data-id');
                $(document).find('.custom-modal-category #treeview div.category-item').removeClass('active');
                $(document).find('.custom-modal-category #treeview div.category-item[data-id="' + old_id + '"]').addClass('active');
            } else {
                $(document).find('.custom-modal-category #treeview div.category-item').removeClass('active');
            }
        });
    });

    $(document).on('click', '.custom-modal #treeview div.category-item', function (e) {
        if ($(e.target).hasClass('extrigger')) {
            return false;
        }
        var $this = $(this);
        var current_id = $(this).attr('data-id');

        $(document).find('.custom-modal #treeview div.category-item[data-id!="' + current_id + '"].active').removeClass('active');
        $this.toggleClass('active');
    });
});