(function ($) {
    // функция вызова jQuery-плагина
    $.fn.NovaPoshtaApi = function (options) {

        // опции
        var city = null;
        var department = null;

        var config = $.extend({}, {
            city: null,
            department: null
        }, options);

        // Функція отримання списку міст
        // ===================================

        function getCity() {
            var city_settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://api.novaposhta.ua/v2.0/json/",
                "method": "POST",
                "headers": {
                    "content-type": "text/plain",
                    "cache-control": "no-cache",
                },
                "data": JSON.stringify({
                    "modelName": "Address",
                    "calledMethod": "getCities",
                    "apiKey": delivery.options.apiKey,
                })
            };

            $.ajax(city_settings).done(function (response) {
                if (!response.success && !response.data.length) {
                    return false;
                }

                var data = response.data;
                var value = city.val();

                data.forEach(function (item, i, arr) {
                    // delivery.options.field - повертає назву ключа відповідно до вибраної мови на сайті
                    // Description
                    // DescriptionRu

                    var selected = false;

                    if (value !== null && value !== undefined) {
                        if (value.length && (value === item[delivery.options.field])) {
                            selected = true;
                        }
                    }

                    var newOption = new Option(item[delivery.options.field], item[delivery.options.field], false, selected);
                    city.append(newOption);
                });

                city.attr({
                    'disabled': false,
                    'readonly': false
                });

            });
        }

        // ===================================

        // Функція отримання списку відділень
        // ===================================

        function getWarehouse(city_id) {

            department.closest('.form-group').removeClass('has-error');
            department.siblings('.help-block').text('');

            var warehouses_settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://api.novaposhta.ua/v2.0/json/",
                "method": "POST",
                "headers": {
                    "content-type": "text/plain",
                    "cache-control": "no-cache",
                },
                "data": JSON.stringify({
                    "apiKey": delivery.options.apiKey,
                    "modelName": "AddressGeneral",
                    "calledMethod": "getWarehouses",
                    "methodProperties": {
                        "CityName": city_id,
                    }
                })
            };

            department.attr('readOnly', true);

            $.ajax(warehouses_settings).done(function (response) {

                if (!response.success && !response.data.length) {
                    return false;
                }

                var data = response.data;
                var value = department.val();

                department.find('option').remove();
                data.forEach(function (item, i, arr) {

                    var selected = false;

                    if (value !== null && value !== undefined) {
                        if (value.length && (value === item[delivery.options.field])) {
                            selected = true;
                        }
                    }

                    var newOption = new Option(item[delivery.options.field], item[delivery.options.field], false, selected);
                    department.append(newOption);
                });
            });

            department.attr({
                'disabled': false,
                'readonly': false
            });
        }

        // ===================================

        function main(e) {
            city = $(document).find(config.city_select);
            department = $(document).find(config.department_select);

            getCity();

            city.on('change', function () {
                var value = city.val();
                getWarehouse(value);
            });

            if (city.val() !== null && city.val() !== undefined) {
                var value = city.val();
                if (value.length) {
                    getWarehouse(value);
                }
            }
        }

        // Основна функція
        main(this);

        return this;
    };
})(jQuery);