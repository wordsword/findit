<?php

namespace app\modules\main\controllers;

use Yii;
use backend\modules\main\models\Language;
use backend\modules\main\models\SourceMessage;
use common\components\SkLangHelper;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SourceMessageController implements the CRUD actions for Language model.
 */
class LanguageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'ghost-access'=> [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Creates a new Language model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Language();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/main/dictionary']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/main/dictionary']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Language model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (!$model->deflang)
            $model->delete();
        if (!Yii::$app->request->isAjax)
            return $this->redirect(Yii::$app->user->getReturnUrl(['/main/dictionary']));
    }

    public function actionDeleteall()
    {
        if (isset($_POST['keys']) && is_array($_POST['keys'])) {
            $model = Language::find()->where(['deflang' => 1])->one();
            if (in_array($model->primaryKey, $_POST['keys'])) {
                unset($_POST['keys'][array_search($model->primaryKey, $_POST['keys'])]);
            }
            return Language::deleteAll(['in', 'id', $_POST['keys']]);
        }
    }

    public function actionVisible($id)
    {
        $model = $this->findModel($id);
        if (!$model->deflang) {
            $model->visible = abs(1 - $model->visible);
            $model->save();
        }
        if (!Yii::$app->request->isAjax)
            return $this->redirect(['/main/dictionary']);
    }

    public function actionTranslate($lang_from = 'src', $lang_to = null)
    {
        set_time_limit (3600);




        if(Yii::$app->request->post('Translate'))
        {
          if(empty($lang_to))
            throw new \yii\web\BadRequestHttpException();

          if($lang_from == 'src')
          {
            $messages = Yii::$app->db->createCommand("SELECT id, message FROM source_message WHERE source_message.id NOT IN (SELECT id FROM message WHERE language = :lang AND translation IS NOT NULL AND TRIM(translation) != '')", [':lang' => $lang_to])->queryAll();
            $lang_from = 'en';
          }
          else
            $messages = Yii::$app->db->createCommand("SELECT id, translation message FROM message WHERE message.id NOT IN (SELECT id FROM message WHERE language = :dest_lang AND translation IS NOT NULL AND TRIM(translation) != '') AND language = :src_lang", [':dest_lang' => $lang_to, ':src_lang' => $lang_from])->queryAll();


            $client = new Client();        
            $response = $client->createRequest()
                ->setMethod('GET')
                ->setUrl('http://pubproxy.com/api/proxy?')
                ->setData([
                  'format' => 'json',
                  'country' => 'RU'
                ]);

            $response = $response->send();
            $proxy = $response->data['data'][0]['ipPort'];
            // echo $proxy;

            Yii::info($proxy, 'proxy');



          while(count($messages) > 0)
          {
              foreach($messages as $key => $message)
              {

                  $success = true;
                  while($success)
                  {
                      $client = new Client();
                      $response = $client->createRequest()
                          ->setMethod('GET')
                          ->setUrl('https://translate.yandex.net/api/v1.5/tr.json/translate')
                          ->setData([
                            'text' => $message['message'],
                            'key' => 'trnsl.1.1.20190207T093615Z.0b384b978e94a260.f5254720822b125626428e11cf495e9a8f5a250c',
                            'lang' => $lang_from.'-'.$lang_to
                          ])
                          ->setOptions([
                              'proxy' => $proxy, // use a Proxy
                              'timeout' => 60, // set timeout to 60 seconds for the case server is not responding
                          ]);
                      try{
                          $response = $response->send();
                      } catch(Exception $ex) {
                        Yii::error($ex->message, "translation");
                        Yii::error("\n\n\n", "translation");
                        $success = true;
                        continue;
                        // die;
                      }

                      Yii::info($message['message'].' - '.$response->data['text'][0],'translation');
                      Yii::info("\n\n\n", "translation");

                        if(intval(Yii::$app->db->createCommand("SELECT COUNT(*) FROM message WHERE language = :lang AND id=:id", [':id' => $message['id'], ':lang' => $lang_to])->queryScalar()) == 0)
                        {
                          Yii::$app->db->createCommand("INSERT INTO message (id, language, translation) VALUES (:id, :lang, :trans)", [
                            ':id' => $message['id'],
                            ':lang' => $lang_to,
                            ':trans' => $response->data['text'][0]
                          ])->execute();
                        }
                        else
                        {
                          Yii::$app->db->createCommand("UPDATE message SET translation = :trans WHERE id = :id AND language = :lang", [
                            ':id' => $message['id'],
                            ':lang' => $lang_to,
                            ':trans' => $response->data['text'][0]
                          ])->execute();
                        }
                        // 
                        $success = false;
                        unset($messages[$key]);
                  }
                    // echo '<pre>';
                    // print_r();

                    // echo '</pre>';
                

              }
          }
          return $this->redirect(['/main/dictionary']);
        }

        $langs = Language::find()->select('lang_id')->column();

        return $this->render('translate', compact('langs'));
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Language the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Language::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
