<?php

namespace app\modules\main\controllers;

use app\modules\main\models\Document;
use app\modules\main\models\Slide;
use app\modules\main\models\DocumentSearch;
use app\modules\main\models\ReviewSearch;
use app\modules\main\models\ParseForm;
use backend\modules\main\models\UserMessageSearch;
use common\modules\user\models\search\UserSearch;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use Yii;
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\modules\main\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }


}
