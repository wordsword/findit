<?php

namespace app\modules\main\controllers;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use backend\components\TmpFile;
use common\models\Settings;
use yii2tech\csvgrid\CsvGrid;
use yii\data\ArrayDataProvider;
use yii\imagine\Image;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * SettingsController implements the CRUD actions for ShopSettings model.
 */
class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

    private function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    /**
     * Lists all ShopSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Settings::getModel();

        $userModel = \common\modules\user\models\Settings::getModel();

        if (
            $model->load(Yii::$app->request->post()) && $model->validate()
            && $userModel->load(Yii::$app->request->post()) && $userModel->validate()
        ) {
            Yii::$app->session->setFlash('saveSuccess', Yii::t('app', 'Saved.'));

            Settings::saveModel($model);
            \common\modules\user\models\Settings::saveModel($userModel);
        }

        return $this->render('view', [
            'model' => $model,
            'userModel' => $userModel,
        ]);
    }

    public function actionGetDb()
    {
        if (!Yii::$app->user->isSuperadmin)
            throw new \yii\web\ForbiddenHttpException();

        $file = new TmpFile();

        exec('mysqldump --host=' . $this->getDsnAttribute('host', Yii::$app->db->dsn) . ' --user=' . Yii::$app->db->username . ' --password=' . Yii::$app->db->password . ' ' . $this->getDsnAttribute('dbname', Yii::$app->db->dsn) . ' --skip-add-locks > ' . $file);

        return Yii::$app->response->sendFile($file, 'landing.sql');

    }

    public function actionOptImages()
    {
        return $this->render('images');
    }


    public function actionGetimgs()
    {
        $directory = $_SERVER['DOCUMENT_ROOT'] . "/frontend/web/uploads/images/";

        $cdir = $this->rglob($directory . "*.{jpg,png,jpeg,JPG,PNG,JPEG}", GLOB_BRACE);

        return json_encode($cdir);
    }

    public function actionProcessimg()
    {
        if (Yii::$app->request->isPost) {
            $proc = false;
            $img = $_POST['img'];
            $size = $_POST['size'];

            $image = Image::getImagine()->open($img);
            $i_size = $image->getSize();

            $res = "<p>Processed $img. Initial size: (" . $i_size->getHeight() . ", " . $i_size->getWidth() . ")";


            if ($i_size->getWidth() > $size || $i_size->getHeight() > $size) {

                $proc = true;

                $image->thumbnail(new Box($size, $size))->save($img);

                $i_s = $image->thumbnail(new Box($size, $size))->getSize();

                $res .= " Shrinked to (" . $i_s->getHeight() . ", " . $i_s->getWidth() . ").</p>";
            } else
                $res .= "</p>";

            return json_encode([
                'log' => $res,
                'processed' => $proc
            ]);
        } else {
            throw new \yii\web\BadRequestHttpException();
        }
    }

    public function actionDownloadSummary($reason)
    {
        $summary = $_GET['summary'];

        // print_r($summary);

        // die;
        if (empty($summary))
            throw new BadRequestHttpException();

        $empty = $summary['empty'];
        $duplicates = $summary['duplicates'];


        $sheets = [];

        if (!empty($empty)) {
            $sheets['Users with empty ' . $reason] = [
                'class' => 'codemix\excelexport\ExcelSheet',
                'data' => $empty,
                'titles' => [
                    'ID',
                    'Username'
                ]
            ];
        }

        if (!empty($duplicates)) {
            $sheets['Users with duplicate ' . $reason . 's'] = [
                'class' => 'codemix\excelexport\ExcelSheet',
                'data' => $duplicates,
                'titles' => [
                    'ID',
                    'Username',
                    ucfirst($reason)
                ]
            ];
        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => $sheets
        ]);
        $file->send('report.xlsx');

        // if(!empty($empty))
        // {
        //     $exporter = new CsvGrid([
        //         'dataProvider' => new ArrayDataProvider([
        //             'allModels' => $empty,
        //             'pagination' => false
        //         ]),
        //         'columns' => [
        //             [
        //                 'attribute' => 'id',
        //             ],
        //             [
        //                 'attribute' => 'username'
        //             ],
        //         ],
        //         'csvFileConfig' => [
        //             'cellDelimiter' => "\t",
        //             'rowDelimiter' => "\n",
        //             'enclosure' => '',
        //         ],
        //     ]);
        //     $exporter->export()->send('users_with_empty_'.$reason.'.csv');
        // }        
    }


    function rglob($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->rglob($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }
}


