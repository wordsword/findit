<?php

namespace app\modules\main\controllers;

use Yii;
use app\modules\main\models\AdminMenu;
use common\modules\user\models\AdminMenuRole;
use common\modules\user\models\Role;
// use app\modules\main\models\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class AdminMenuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'sort' => ['post'],
                ],
            ],
            'ghost-access'=> [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

    public function beforeAction($action){
        if(!Yii::$app->user->isSuperadmin && $action->id != 'index' && $action->id != 'asign-role')
            throw new \yii\web\ForbiddenHttpException;

        return parent::beforeAction($action);
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
        $menu = AdminMenu::getTreeViewDataTest();

        $roles = Role::find()->select('title')->column();
        
        return $this->render('index', [
            'menu' => $menu,
            'roles' => $roles
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminMenu();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Sorting an existing Menu models.
     */
    public function actionSort()
    {
        if (Yii::$app->request->isAjax) {
            $list = Yii::$app->request->getBodyParams()['list'];
            if (!empty($list)) {
                foreach ($list as $key => $val) {
                    $menu = AdminMenu::findOne($val['recid']);
                    if ($menu) {
                        $menu->sort_order = \yii\helpers\Html::encode($key);
                        $menu->parent_id = \yii\helpers\Html::encode($val['parent']);
                        $menu->save();
                    }
                }
            }
            return \yii\helpers\Json::encode(['status'=>200]);
        } else {
            throw new \yii\web\BadRequestHttpException(Yii::t('app','Bad Request'));
        }
        
    }
    
    
    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
        } else {
            return '<div id="success"></div>';
        }
    }

    private function arrToStr($arr)
    {
        return "(".implode(",", $arr).")";
    }

    

    public function actionAsignRole()
    {
        if(!Yii::$app->request->isPost)
            throw new \yii\web\BadRequestException();

        $data = [];

        parse_str($_POST['body'], $data);

        $role_menu = AdminMenuRole::find()->where(['admin_menu_id' => $data['item_id'], 'role_id' => $data['role_id']])->one();

        if(!$role_menu && isset($data['value']))
        {
            $item = AdminMenu::findOne($data['item_id']);

            if($item->url != "" && $item->url!="/")
            {
                $parsed = explode('/', $item->url);

                foreach ($parsed as $key => $value) {
                    if(empty($value))
                        unset($parsed[$key]);
                }
                // print_r($parsed);
                $parsed = array_values($parsed);

                if(count($parsed) > 1)
                {
                    if(count($parsed) == 2)
                        $parsed[2] = 'index';

                    $route = implode('/', ["", $parsed[0], $parsed[1], $parsed[2]]);

                    $ids = Yii::$app->db->createCommand("SELECT id FROM permission WHERE url LIKE '%".$route."%'")->queryColumn();
                    // print_r($ids);
                    // exit;
                    foreach ($ids as $id) {
                        $pr = new \common\modules\user\models\RolePermission([
                            'permission_id' => $id,
                            'role_id' => $data['role_id']
                        ]);

                        $pr->save();
                    }

                }
            }
            $role_menu = new AdminMenuRole([
                'role_id' => $data['role_id'],
                'admin_menu_id' => $data['item_id']
            ]);

            $role_menu->save();

            return "allowed";
        }
        elseif ($role_menu && !isset($data['value'])) {
            $item = AdminMenu::findOne($data['item_id']);

            if($item->url != "" && $item->url!="/")
            {
                $ids = Yii::$app->db->createCommand("SELECT id FROM permission WHERE url LIKE '%" . $item['url'] . "%'")->queryColumn();
                // print_r($ids);
//                Yii::$app->db->createCommand('DELETE FROM role_permission WHERE permission_id IN ' . $this->arrToStr($ids) . ' AND role_id = :role', [':role' => $data['role_id']])->execute();
                if(count($ids) > 0)
                    Yii::$app->db->createCommand("DELETE FROM role_permission WHERE permission_id IN " . $this->arrToStr($ids) . " AND role_id = :role", [':role' => $data['role_id']])->execute();

            }

            $role_menu->delete();

            return "disallowed";
        }
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminMenu::find()->where(['id'=>$id])->multilingual()->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
        }
    }
}
