<?php

namespace app\modules\main\controllers;

use backend\modules\main\models\Language;
use backend\modules\main\models\LanguageSearch;
use Yii;
use backend\modules\main\models\SourceMessage;
use backend\modules\main\models\SourceMessageSearch;
use backend\modules\main\models\Message;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SourceMessageController implements the CRUD actions for SourceMessage model.
 */
class DictionaryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'ghost-access' => [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all SourceMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
//        SourceMessage::importTranslations();

        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchLangModel = new LanguageSearch();
        $langDataProvider = $searchLangModel->search(Yii::$app->request->queryParams);

        $sortable = Yii::$app->request->post('sortable');
        if (isset($sortable) && is_array($sortable) && !empty($sortable['curr'])) {
            $field = Language::findOne($sortable['curr']);
            if (!empty($field)) {
                $nextField = Language::findOne($sortable['next']);
                if (!empty($nextField)) {
                    $field->sort_order = $nextField->sort_order;
                } else {
                    $prevField = Language::findOne($sortable['prev']);
                    if (!empty($prevField)) {
                        $field->sort_order = $prevField->sort_order + 1;
                    }
                }
                if (!empty($nextField) || !empty($prevField)) {
                    $field->save();
                }
            }
        }

        if (Yii::$app->request->post('hasEditable')) {
            $msgId = Yii::$app->request->post('editableKey');
            $attr = explode('_', Yii::$app->request->post('editableAttribute'));
            $lang = $attr[1];
            $message = Message::findOne(['id' => $msgId, 'language' => $lang]);
            if (!$message) {
                $message = new Message;
                $message->id = $msgId;
                $message->language = $lang;
            }
            $posted = current($_POST['SourceMessage']);
            $message->translation = HtmlPurifier::process($posted[Yii::$app->request->post('editableAttribute')]);
            $message->save();
            return Json::encode(['output' => '', 'message' => '']);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchLangModel' => $searchLangModel,
            'langDataProvider' => $langDataProvider,
        ]);
    }

    /**
     * Creates a new SourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SourceMessage();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach (\common\components\SkLangHelper::getLangs() as $key => $lang) {
                $message = new Message;
                $message->id = $model->id;
                $message->language = $key;
                $message->translation = Yii::$app->request->post()['SourceMessage']['message_' . $key];
                $message->save();
            }
            return $this->redirect(['update', 'id' => $model->primaryKey]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach (\common\components\SkLangHelper::getLangs() as $key => $lang) {
                $message = Message::findOne(['id' => $id, 'language' => $key]);
                if (!$message) {
                    $message = new Message;
                    $message->id = $model->id;
                    $message->language = $key;
                }
                $message->translation = Yii::$app->request->post()['SourceMessage']['message_' . $key];
                $message->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionFindTranslate()
    {
        if (SourceMessage::importTranslations()) {
            return true;
        }
    }

    /**
     * Deletes an existing SourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
    }

    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $query = SourceMessage::find();
        $query->from(['source_message t']);
        $tCols = ['t.*'];
        foreach (\common\components\SkLangHelper::getLangs() as $key => $lang) {
            $tCols[] = 't' . $key . '.translation as message_' . $key;
            $query->leftJoin('message t' . $key, 't' . $key . '.id=t.id AND t' . $key . '.language="' . $key . '"');
        }
        $query->select($tCols);
        $query->andWhere(['t.id' => $id]);
        if (($model = $query->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
