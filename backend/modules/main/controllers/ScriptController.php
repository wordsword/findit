<?php

namespace app\modules\main\controllers;

use \Yii;
use common\models\Script;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * 
 */
class ScriptController extends Controller
{

	public function behaviors()
    {
        return [
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
            'ghost-access'=> [
                'class' => 'common\modules\user\components\GhostAccessControl',
            ],
        ];
    }

	public function actionIndex()
	{
		Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
		$dataProvider = new ActiveDataProvider([
			'query' => Script::find()
		]);

		return $this->render('index', compact('dataProvider'));
	}


	public function actionCreate()
	{
		$model = new Script();

		if($model->load(Yii::$app->request->post()) && $model->save())
		{
			return $this->redirect(['index']);
		}

		return $this->render('create', compact('model'));
	}


	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if($model->load(Yii::$app->request->post()) )
		{
			if($model->save())
				return $this->redirect(['index']);
			else
				print_r($model->getErrors);
		}

		return $this->render('update', compact('model'));
	}


	public function actionEnabled($id)
	{
		$model = $this->findModel($id);
		$model->enabled = 1 - $model->enabled;
		$model->save(false);

		return $this->redirect(['index']);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
	}

	private function findModel($id)
	{
		$model = Script::findOne($id);

		if($model)
			return $model;
		else
			throw new BadRequestHttpException();
	}
}