<?php

use common\components\SkLangHelper;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */

?>

<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'Accounting settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <?
        echo $form->field($model, 'max_return_days')->textInput(['type' => 'number', 'min' => 0, 'class' => 'form-control browser-default']);
        echo $form->field($model, 'stockControl')->widget(Select2::classname(), [
                'data' => [
                    'noControl' => Yii::t('app', 'Disabled'),
                    'strictControl' => Yii::t('app', 'Strict control'),
                ],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ]
        )->label(Yii::t('app', 'Stock control'));

        // echo $form->field($model, 'strict_model_attribute_control')->checkBox();
        // echo $form->field($model, 'use_product_models')->checkBox();

        echo $form->field($model, 'strict_model_attribute_control', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('strict_model_attribute_control'), ['style' => 'display:inline-block;']);
        echo $form->field($model, 'use_product_models', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('use_product_models'), ['style' => 'display:inline-block;']);

        ?>
    </div>
</div>
