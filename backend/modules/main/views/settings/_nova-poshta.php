<?php

use common\components\SkLangHelper;
use kartik\widgets\SwitchInput;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */

?>
<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'Nova poshta settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <? echo $form->field($model, 'delivery_api_key')->textInput(['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter api key') . '...', 'class' => 'form-control browser-default']); ?>
    </div>
</div>