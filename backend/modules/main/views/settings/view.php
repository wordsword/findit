<?php

use common\components\SkLangHelper;
use kartik\form\ActiveForm;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\components\SkDynamicModel */

\app\modules\main\ShopAsset::register($this);

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;

if (isset($_COOKIE['active-settings-tab'])) {
    $tab = $_COOKIE['active-settings-tab'];
    $activeTab = strtr($tab, ['#settings-tabs-tab' => '']);
}

Pjax::begin([
    'id' => 'pjax-container',
    'formSelector' => '#settings-form',
    'timeout' => 3000,
]);
$form = ActiveForm::begin([
    'id' => 'settings-form',
    'action' => SkLangHelper::addLangToUrl(Url::toRoute('')),
    'enableClientValidation' => false,
    'fieldConfig' => [
        'errorOptions' => [
            'encode' => false,
            'class' => 'help-block'
        ],
        'options' => ['class' => 'form-group form-group-material'],
    ],
]);

?>
    <div class="custom-page-header column-template">
        <div class="action-line"><?= Html::tag('span', Yii::t('app', 'Editing')) ?></div>
        <div class="info-line">
            <div class="left-box"><?= Yii::t('app', 'System parameters') ?></div>
            <div class="right-box">
                <div class="action">
                    <?php
                    echo Html::button(FA::icon('save'), [
                        'title' => Yii::t('app', 'Save'),
                        'class' => 'btn btn-primary save-btn',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
if (Yii::$app->session->hasFlash('saveSuccess')) {
    echo Alert::widget([
        'body' => Yii::$app->session->getFlash('saveSuccess'),
        'closeButton' => [],
        'options' => [
            'class' => 'alert alert-success'
        ],
    ]);
}
?>
<?= Html::decode($form->errorSummary($model)) ?>
    <div class="row product-card">
        <?= Tabs::widget([
            'id' => 'settings-tabs',
            'items' => [
                [
                    'label' => Yii::t('app', 'General'),
                    'content' => $this->render('_update', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 0 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Catalogue'),
                    'content' => $this->render('_catalogue', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 1 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'SMS'),
                    'content' => $this->render('_sms', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 2 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Images'),
                    'content' => $this->render('_images', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 3 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Authorization'),
                    'content' => $this->render('_auth', [
                        'model' => $model,
                        'userModel' => $userModel,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 4 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Accounting'),
                    'content' => $this->render('_account', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 5 ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Nova Poshta'),
                    'content' => $this->render('_nova-poshta', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'active' => $activeTab == 6 ? true : false,
                ]
            ],
            'navType' => 'nav-pills',
            'options' => [
                'class' => 'nav-stacked col-sm-3'
            ],
            'itemOptions' => [
                'class' => 'col-sm-9 settings-tab'
            ],
            'encodeLabels' => false
        ]) ?>
    </div>
<?php
ActiveForm::end();
Pjax::end();
