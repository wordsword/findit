<?php

use common\components\SkLangHelper;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */

?>
<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'General settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'form' => $form,
            'model' => $model,
        ]) ?>
    </div>
</div>