<?php

use common\components\SkLangHelper;
use kartik\widgets\SwitchInput;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */

?>
<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'Authorization settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <? /**
         * Вибір основного типу реєстрації - телефон/email
         */ ?>

        <? echo $form->field($model, 'required_input', [
            'template' => "{label}\n{input}",
        ])->radioList(
            [
                'phone' => Yii::t('app', 'Phone'),
                'email' => Yii::t('app', 'E-mail')
            ],
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return
                        '<div><label>' . Html::radio($name, $checked, ['value' => $value]) . '<span>' . $label . '<span></label></div>';
                },
            ]
        ); ?>

        <? /**
         * Базові налаштування реєстрації
         */ ?>
        <h6><?= Yii::t('app', 'Registration settings') ?></h6>

        <?= $form->field($userModel, 'enableRegistration', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('enableRegistration'), ['style' => 'display:inline-block;']); ?>

        <?= $form->field($userModel, 'activateAfterRegistration', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('activateAfterRegistration'), ['style' => 'display:inline-block;']); ?>

        <? /*= $form->field($userModel, 'phoneConfirmationRequired', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('phoneConfirmationRequired'), ['style' => 'display:inline-block;']); ?>

        <?= $form->field($userModel, 'loginAfterRegistration', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('loginAfterRegistration'), ['style' => 'display:inline-block;']); ?>

        <?= $form->field($userModel, 'emailConfirmationRequired', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('emailConfirmationRequired'), ['style' => 'display:inline-block;']); */ ?>

        <?= $form->field($userModel, 'accountConfirmationRequired', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($userModel->getAttributeLabel('accountConfirmationRequired'), ['style' => 'display:inline-block;']); ?>

    </div>
</div>