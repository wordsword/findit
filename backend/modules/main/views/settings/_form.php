<?php

use common\components\SkLangHelper;
use common\models\Settings;
use dosamigos\ckeditor\CKEditor;
// use yii\bootstrap\Html;
use kartik\widgets\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\\SkDynamicModel */
/* @var $form yii\widgets\ActiveForm */

function folderSize($dir)
{
    $size = 0;
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $file) {

        //     echo "<pre>";
        //     var_dump($file->getBaseName());
        //     echo "</pre>";
        // exit;
        if ($file->getBaseName() != '.' && $file->getBaseName() != '..')
            $size += $file->getSize();
    }
    return $size;
}


function format_size($size)
{
    $units = explode(' ', 'B KB MB GB TB PB');

    $mod = 1024;

    for ($i = 0; $size > $mod; $i++) {
        $size /= $mod;
    }

    $endIndex = strpos($size, ".") + 3;

    return substr($size, 0, $endIndex) . ' ' . $units[$i];
}

?>

<div class="general-settings-form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-block">
                <?php
                echo $form->field($model, 'companyName')->textInput(['class' => 'form-control browser-default']);
                echo $form->field($model, 'email')->textInput(['class' => 'form-control browser-default']);
                echo $form->field($model, 'phone_number')->textInput(['class' => 'form-control browser-default']);
                echo $form->field($model, 'complife_partner_key')->textInput(['class' => 'form-control browser-default']);
                ?>
            </div>
            <? if (Yii::$app->user->isSuperadmin) { ?>
                <div class="form-block">
                    <label><?= Yii::t('app', 'Project size') ?></label>
                    <input type="text" class="form-control browser-default" disabled
                           value="<?= format_size(folderSize($_SERVER['DOCUMENT_ROOT'])) ?>">
                </div>
            <? } ?>
        </div>
    </div>
</div>