<?php

use common\components\SkLangHelper;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */

?>

<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'Image settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <?
        echo $form->field($model, 'shrink_on_upload', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('shrink_on_upload'), ['style' => 'display:inline-block;']);

        echo $form->field($model, 'max_images_size')->textInput(['type' => 'number', 'class' => 'form-control browser-default']);
        ?>
    </div>
</div>