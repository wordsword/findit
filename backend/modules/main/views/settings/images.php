<?php 
use yii\jui\ProgressBar;
$this->title = Yii::t('app', 'Optimize images');
 ?>
 <style>
  .ui-progressbar {
    position: relative;
  }
  .progress-label {
    position: absolute;
    left: 50%;
    top: 4px;
    font-weight: bold;
    text-shadow: 1px 1px 0 #fff;
  }
  .panel{
  	padding: 10px;
  }
</style>

<div class="panel panel-deffault log-panel hidden">
   
	<div id="opt_log" style="font-size: 0.85em;">
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
		<p>asdf</p>
	</div>
	<p id="proct"></p>
	<p id="srinkd"></p>
	<?php 
		ProgressBar::begin([
		    // 'clientOptions' => ['value' => 75],
		    'clientEvents' => [
		    	      'change'=> "function() {
	    	      			        $(this).find('.progress-label').text( $('#w0').progressbar( \"value\" ) + \"%\" );
	    	      			      }",
			           'complete' => "function() {
		           						$(this).find('.progress-label').text( \"Complete!\" );
		           					  }"
		    ]
		]);

		echo '<div class="progress-label">Loading...</div>';

		ProgressBar::end();
	 ?>
</div>


 <div class="panel panel-default">
    <div>
        <p>
            <?=Yii::t('app','Minimal size of image side').': '?>
            <input type="number" id="min_opt_sz" value=1000 min=100 max=2000>
        </p>
        <button id="opt_imgs"><?=Yii::t('app','Optimize now')?></button>
    </div>
 </div>