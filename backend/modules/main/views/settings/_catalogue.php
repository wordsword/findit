<?php

use common\components\SkLangHelper;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Settings */
app\assets\MaterializeAsset::register($this);

//$js = <<<JS
//function () {
//  $(document).on('change', '#generalsettings-shop_status', function() {
//      console.log('asas');
//    });
//}
//JS;

$js = new \yii\web\JsExpression(<<<JS
 $(document).on('change', 'input#generalsettings-shop_status', function(e) {
     if(this.checked) {
        $('.shop-property').slideDown();
    }else{
         $('.shop-property').slideUp();
    }
   });
JS
);
$this->registerJs($js, \yii\web\View::POS_READY);
?>
<div class="shop-settings-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= '<strong>' . Yii::t('app', 'Catalogue settings') . '</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <?
        echo $form->field($model, 'shop_status', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('shop_status'), ['style' => 'display:inline-block;']);

        // Налаштування магазину
        echo Html::beginTag('div', ['class' => 'shop-property', 'style' => 'display:' . (($model->shop_status) ? ' block' : ' none')]);
        echo $form->field($model, 'show_price_types', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('show_price_types'), ['style' => 'display:inline-block;']);
        echo $form->field($model, 'use_product_card', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
            'onText' => '',
            'offText' => '',
            'checked' => false,
            'uncheck' => 0
        ])->label($model->getAttributeLabel('use_product_card', ['template' => "{input}\n{label}\n{hint}\n{error}"]), ['style' => 'display:inline-block;']);

        echo $form->field($model, 'display_leftovers')->widget(Select2::classname(), [
                'data' => [
                    'noView' => Yii::t('app', "Do not display"),
                    'numberView' => Yii::t('app', 'Display as number'),
                    'gradeView' => Yii::t('app', 'Display as marker')
                ],
                'hideSearch' => true,
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'placeholder' => '',
                    'allowClear' => false
                ],
            ]
        );
        echo Html::endTag('div'); ?>
    </div>
</div>
