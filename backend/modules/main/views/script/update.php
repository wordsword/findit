<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Price */

$this->title = Yii::t('app', 'Update script:').' '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Scripts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="price-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Update script:').' <strong>'.Html::encode($model->title).'</strong>' ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
