<?php

use app\components\SkCollapsePanel;
use app\components\SkGridView;
use app\modules\main\models\Product;
use app\modules\main\models\Stock;
use app\widgets\treeView\TreeView;
use common\models\Script;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;

\yii\jui\JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\main\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Scripts');
$this->params['breadcrumbs'][] = $this->title;
$gridID = 'script-grid';
// $reflection = new ReflectionClass($searchModel);
// $class = $reflection->getShortName();

$this->registerJs($js, \yii\web\View::POS_END);

$PHT = <<< HTML
    <h3 class="panel-title">
        {$this->title}
        <span class="summary">
            <span class="label label-success">{$dataProvider->getTotalCount()}</span>
        </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;

?>

<div class="stock-index">
    
    <div class="col-lg-12">
        <?= SkGridView::widget([
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'class' => ($model['enabled'] ? 'published' : 'not-published'),
                    'style' => 'text-align:left;'
                ];
            },
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterUrl' => $url,
            'filterSelector' => '#treeview input.checked',
            'panelHeadingTemplate' => $PHT,
            'panelFooterTemplate' => $PFT,
            'panelBeforeTemplate' => $PBT,
            'toolbar' => [
                ['content' =>
                    Html::a(FA::icon('plus'), Url::toRoute(['/main/script/create']), ['title' => Yii::t('app', 'Create'), 'class' => 'btn btn-success']).
                    '{collapse}'
                ],
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container-stock',
                ]
            ],
            'pager' => [
                'class' => 'app\components\SkPager',
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => true,
            'panel' => [
                'type' => SkGridView::TYPE_DEFAULT,
                'heading' => $this->title,
                'footer' => true,
                'after' => false,
                'beforeOptions' => ['style' => 'padding: 0;'],
            ],
            'persistResize' => false,
            'filterPosition' => SkGridView::FILTER_POS_BODY,
            'columns' => [
                'title',
                [
                    'attribute' => 'position',
                    'header' => Yii::t('app', 'Position'),
                    'content' => function($model){
                        return Script::$positions[$model['position']];
                    }
                ],
                [
                   'class' => 'yii\grid\ActionColumn',
                   'contentOptions' => [
                       'class' => 'action-column',
                       'style' => 'text-align:center; min-width: 95px !important;'
                   ],
                   'template' => '{visible}{update}{delete}',
                   'buttons' => [
                       'visible' => function ($url, $model, $key) {
                           return Html::a(
                               FA::icon('power-off'),
                               Url::toRoute(['/main/script/enabled', 'id' => Html::encode($model['id'])]),
                               [
                                   'class' => 'btnvis',
                                   'title' => $model['enabled'] ? Yii::t('app', 'Disable') : Yii::t('app', 'Enable'),
                                   'aria-label' => $model['enabled'] ? Yii::t('app', 'Disable') : Yii::t('app', 'Enable'),
                                    'data-confirm-msg' => $model['enabled'] ? Yii::t('app', 'Are you sure you want to disable this script?')
                                        : Yii::t('app', 'Are you sure you want to enable this script?'),
                                    'data-pjax' => 0,
                                    'style' => 'width:30px; height:30px;'
                                ]
                            );
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a(
                                FA::icon('pencil'),
                                $url,
                                [
                                    'title' => Yii::t('app', 'Update'),
                                    'data-pjax' => 0,
                                    'style' => 'width:30px; height:30px;'
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a(
                                FA::icon('trash'),
                                $url,
                                [
                                    'class' => 'delete',
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-pjax' => 0,
                                    'style' => 'width:30px; height:30px;',
                                    'data-confirm-msg' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                ]
                            );
                        },
                    ]
                ]
            ]
        ]); ?>
    </div>
</div>
</div>

