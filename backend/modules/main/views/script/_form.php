<?php

use app\modules\main\Module;
use app\modules\main\models\Price;
use common\components\SkLangHelper;
use common\models\Script;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\main\models\Script */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="script-form">
    <?/*php Pjax::begin([
        'id' => 'pjax-container',
        'formSelector' => '#script-form',
        'timeout' => 3000,
        'clientOptions' => [

        ]
    ]);*/ ?>
    <?php $form = ActiveForm::begin([
        'id' => 'script-form',
        // 'action' => SkLangHelper::addLangToUrl(Url::toRoute([''])),
        'enableClientValidation' => false,
    ]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'title')->label(Yii::t('app', 'Title'))?>
            <?= $form->field($model, 'position')->dropdownList(Script::$positions)->label(Yii::t('app', 'Position'))?>
            <?= $form->field($model, 'script')->textArea(['rows' => 15])->label(Yii::t('app', 'Script'))?>
            <?= $form->field($model, 'enabled')->checkBox(['label' => false])->label(Yii::t('app', 'Enabled'))?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php // Pjax::end(); ?>
</div>
