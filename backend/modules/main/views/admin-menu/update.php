<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\Menu */

$this->title = Yii::t('app', 'Update admin panel menu item') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="menu-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Update admin panel menu item:').' <strong>'.Html::encode($model->title).'</strong>' ?></h3>  
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
