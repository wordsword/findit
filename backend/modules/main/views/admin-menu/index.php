<?php

use yii\helpers\Html;
use app\modules\main\models\Menu;
use app\widgets\menuTreeView\TreeView;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\main\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Admin panel menu');
$this->params['breadcrumbs'][] = $this->title;
$url = \yii\helpers\Url::toRoute(['/main/admin-menu/sort']);
$onRelocate = new \yii\web\JsExpression(<<<JS
function (undefined, object, list) {
    $.post('$url',{list:list},function(data){

    },'json');
}
JS
);


$role_check = <<<JS
$(".role-check").change(function(e){
    role = $(this).attr('data-role');
    val = $(this).prop("checked");
    sendData = $(this).closest('form').serialize();
    $.post({
        url:"/admin/main/admin-menu/asign-role",
        data: {
            body: sendData
        }
    });
    treeItem = $(this).closest('li');
    if(treeItem.hasClass("nestedSortable-branch"))
    {
        leafs = treeItem.find(".nestedSortable-leaf");
        leafs.each(function(i, it){
            check = $(it).find("[data-role = \""+role+"\"]");

            check.prop("checked", val);
            check.trigger('change');
        });
    }
});
JS;

$this->registerJs($role_check);
?>
<style type="text/css">
.roles
{
    width: 100%;
    text-align: right;
    height: 1.2em;
    padding-right: 85px;
    margin-top: 5px;
}
.roles h5
{
    display: inline-block;
    /*float: right;*/
    width: 120px;
    text-align: center;
    margin: 3px;
    font-weight: 900;
}

#main-menu-wrap
{
    margin-top: 0px;
}
</style>
<div class="menu-index">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" id="menu-container">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('app', 'Admin panel menu') ?></h3>
                </div>
                <div class="panel-body toolbar">
                    <span> 
                        <? if(Yii::$app->user->isSuperadmin) echo Html::a(FA::icon('plus'), ['create'], ['title' => Yii::t('app', 'Create'), 'class' => 'btn btn-success']) ?>
                        <? if(Yii::$app->user->isSuperadmin) echo Html::button(FA::icon('sort'), ['class' => 'btn btn-info toggle-sorter', 'title' => Yii::t('app', 'Sort')]) ?>
                    </span>
                </div>
                <div class="clearfix"></div>
                <div class="roles">
                    <?php 
                        foreach ($roles as $role) {
                            echo Html::tag('h5', $role);                        }
                     ?>
                </div>
                <div id="main-menu-wrap">
                    <?= TreeView::widget([
                        'data' => $menu,
                        'useCheckBox' => false,
                        'onRelocate' => $onRelocate,
                        'view' => '@backend/modules/main/views/admin-menu/_treeview',
                        'maxLevels' => 2,
                        // 'collapsedClass' => 'nestedSortable-expanded',
                        // 'startCollapsed' => false
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
