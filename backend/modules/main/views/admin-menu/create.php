<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\Menu */

$this->title = Yii::t('app', 'Create admin panel menu item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin panel menu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $this->title; ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
