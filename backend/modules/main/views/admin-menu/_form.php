<?php
/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\Menu */
/* @var $form yii\widgets\ActiveForm */

use kartik\widgets\ColorInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\SkLangHelper;
use yii\bootstrap\Tabs;
use app\modules\main\models\Menu;
use yii\helpers\Url;
use app\widgets\treeView\TreeView;

$icon = <<<JS
$("#adminmenu-icon").focusout(function(e){
    ic = $(this).val();
    $("#icon-test").attr('class', "fa fa-"+ic);
});
$("#adminmenu-icon").trigger('focusout');
JS;

$this->registerJs($icon);

?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(['action'=> SkLangHelper::addLangToUrl(Url::to(''))]);?>
    <div class="row">
        <div class="col-sm-12">
            <?php
            $langTabs = [];
            foreach (SkLangHelper::getLangs() as $lang) {
                unset($suffix);
                if (!$lang['deflang']) {
                    $suffix = '_'.Html::encode($lang['lang_id']);
                }
                $content  = $form->field($model, 'title'.$suffix)->label(Yii::t('app','Title'))->textInput();
                $langTabs[] = [
                    'label' => Html::img(Html::encode($lang['image']),['alt'=>Html::encode($lang['lang_name']),'data-toggle'=>'tooltip','title'=> Html::encode($lang['lang_name'])]),
                    'content' => $content,
                    'options' => ['langid' => Html::encode($lang['lang_id'])],
                ];
            }
            echo Tabs::widget([
                'items' => $langTabs,
                'encodeLabels'=>false
            ]);
            ?>
            
            
            <?= $form->field($model, 'icon')->label(Yii::t('app', 'Image').'(<i id="icon-test" class="fa fa-<?=$model->icon?>"></i>)')?>

            <?= $form->field($model, 'url')?>

            <?= $form->field($model, 'counter')->dropdownList([
                'none' => 'none',
                'messages' => Yii::t('app', 'User messages'),
                'calls' => Yii::t('app', 'Call requests'),
                'users' => Yii::t('app', 'Users'),
                'subscribe' => Yii::t('app', 'Subscriptions'),
                'orders' => Yii::t('app', 'Orders'),
                'reviews' => Yii::t('app', 'Reviews'),
                'requests' => Yii::t('app', 'Request'),
            ])?>

            <?= $form->field($model, 'owner')->checkBox(['label' => false])->label(Yii::t('app', 'Grant access to owner'))?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
