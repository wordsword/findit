<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\SourceMessage */

$this->title = Yii::t('app', 'Update translation:').' '.$model->message;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Localization'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->message;
?>
<div class="source-message-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?= Yii::t('app', 'Update translation:').' <strong>'.Html::encode($model->message).'</strong>' ?>
        </h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
            'translation' => $translation
        ]) ?>
    </div>
</div>
