
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">
    <div class="row">
        <div class="col-sm-12">
            <?php $form = ActiveForm::begin(); ?>
          
            <?php echo $form->field($model, 'message')->textarea(['rows' => 2, 'class' => 'form-control']); ?>
            <?php echo $form->field($model, 'category')->textInput() ?>


            <?php foreach (\common\components\SkLangHelper::getLangs() as $key => $lang) :
                echo $form->field($model, 'message_' . $key)->textarea(['rows' => 2, 'class' => 'form-control lang_' . $key]);
            endforeach; ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm'
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
