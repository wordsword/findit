<?php

use app\components\SkGridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\main\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\app\modules\main\ShopAsset::register($this);
\yii\jui\JuiAsset::register($this);

$this->title = Yii::t('app', 'Localization');
$this->params['breadcrumbs'][] = $this->title;

$url = Url::toRoute(['/main/dictionary']);
$gridID = 'language-grid';
$gridID2 = 'dictionary-grid';


$title1 = Yii::t('app', 'Languages');
$title2 = Yii::t('app', 'Dictionary');

$PHT1 = <<< HTML
    <h3 class="panel-title">
        {$title1}
            <span class="summary">
                <span class="label label-success">{$langDataProvider->getTotalCount()}</span>
            </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PHT2 = <<< HTML
    <h3 class="panel-title">
        {$title2}
            <span class="summary">
                <span class="label label-success">{$dataProvider->getTotalCount()}</span>
            </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT2 = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;
$onGridSort = new \yii\web\JsExpression(<<<JS
function onGridSort(curr,prev,next){
    var sendData = {
        sortable: {
            curr: curr,
            prev: prev,
            next: next
        }
    };
    $.post(location.href,sendData,function(data){

    });
};
JS
);

$this->registerJs($onGridSort, \yii\web\View::POS_END);
?>
<div class="source-message-index">
    <div class="row">
        <div class="col-lg-12">
            <!-- Language grid -->
            <?= SkGridView::widget([
                'id' => $gridID,
                'dataProvider' => $langDataProvider,
                'filterModel' => $searchLangModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    $options = $model->visible ? 'published ' : 'not-published ';
                    $options .= $model->deflang ? 'default' : '';
                    return [
                        'class' => $options,
                        'style' => 'text-align:left;'
                    ];
                },
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterUrl' => $url,
                'panelHeadingTemplate' => $PHT1,
                'panelBeforeTemplate' => $PBT,
                'toolbar' => [
                    ['content' =>
                        Html::a(FA::icon('plus'), Url::toRoute('/main/language/create'), [
                            'title' => Yii::t('app', 'Create'),
                            'class' => 'btn btn-success',
                            'id' => 'add-language',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Create'),
                        ]) .
                        Html::button(FA::icon('sort'), [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Sort'),
                            'class' => 'btn sortable',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Sort'),
                        ]) .
                        Html::button(FA::icon('trash'), [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Delete all'),
                            'class' => 'btn btn-danger disabled del-all',
                            'data-href' => Url::toRoute('/main/language/deleteall'),
                            'data-confirm-msg' => Yii::t('app', '{n} records will be deleted'),
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Delete all'),
                        ]) .
                        '{collapse}'
                    ],
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container-language',
                    ]
                ],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => true,
                'panel' => [
                    'type' => SkGridView::TYPE_DEFAULT,
                    'heading' => Yii::t('app', 'Languages'),
                    'footer' => false,
                    'after' => false,
                    'beforeOptions' => ['style' => 'padding: 0;'],
                ],
                'persistResize' => false,
                'filterPosition' => SkGridView::FILTER_POS_BODY,
                'columns' => [
                    [
                        'class' => '\kartik\grid\CheckboxColumn',
                        'rowSelectedClass' => SkGridView::TYPE_INFO,
                        'contentOptions' => ['class' => 'checkbox-wrap'],
                        'headerOptions' => ['class' => 'checkbox-wrap'],
                        'width' => '34px',
                    ],
                    'image' => [
                        'attribute' => 'image',
                        'content' => function ($data) {
                            return !empty($data->image) ? Html::img(Html::encode($data->image), ['style' => 'border: 1px solid #aaa; width: 22px;']) : '';
                        },
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
                    [
                        'attribute' => 'deflang',
                        'content' => function ($data) {
                            return $data->deflang ? FA::icon('check') : '';
                        },
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
                    'lang_id',
                    'lang_name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'class' => 'action-column',
                            'style' => 'text-align:center; padding:0;'
                        ],
                        'headerOptions' => [
                            'class' => 'action-column-header',
                            'data-error' => Yii::t('app', "Action denied for default language!"),
                            'data-error-title' => Yii::t('app', 'Warning'),
                            'style' => 'width:85px;'
                        ],
                        'template' => '{visible}{translate}<br>{update}{delete}',
                        'buttons' => [
                            'visible' => function ($url, $model, $key) {
                                return Html::a(
                                    $model->visible ? FA::icon('eye') : FA::icon('eye-slash'),
                                    Url::toRoute(['/main/language/visible', 'id' => Html::encode($model->primaryKey)]),
                                    [
                                        'class' => 'btnvis',
                                        'title' => $model->visible ? Yii::t('app', 'Hide') : Yii::t('app', 'Show'),
                                        'aria-label' => $model->visible ? Yii::t('app', 'Hide') : Yii::t('app', 'Show'),
                                        'data-confirm-msg' => $model->visible ? Yii::t('app', 'Are you sure you want to hide this item?')
                                            : Yii::t('app', 'Are you sure you want to show this item?'),
                                        'data-pjax' => 0
                                    ]
                                );
                            },
                            'translate' => function ($url, $model, $key) {
                                return Html::a(
                                    FA::icon('language'),
                                    Url::toRoute(['/main/language/translate', 'lang_to' => Html::encode($model->lang_id)]),
                                    [
                                        'title' => Yii::t('app', 'Translate'),
                                        'data-pjax' => 0
                                    ]
                                );
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a(
                                    FA::icon('pencil'),
                                    Url::toRoute(['/main/language/update', 'id' => Html::encode($model->primaryKey)]),
                                    [
                                        'title' => Yii::t('app', 'Update'),
                                        'data-pjax' => 0
                                    ]
                                );
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a(
                                    FA::icon('trash'),
                                    Url::toRoute(['/main/language/delete', 'id' => Html::encode($model->primaryKey)]),
                                    [
                                        'class' => 'delete',
                                        'title' => Yii::t('app', 'Delete'),
                                        'aria-label' => Yii::t('app', 'Delete'),
                                        'data-pjax' => 0,
                                        'data-confirm-msg' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    ]
                                );
                            },
                        ]
                    ]
                ],
            ]); ?>

            <!-- Dictionary grid -->
            <?= SkGridView::widget([
                'id' => $gridID2,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => ['style' => 'text-align:left;'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterUrl' => $url,
                'panelHeadingTemplate' => $PHT2,
                'panelFooterTemplate' => $PFT2,
                'panelBeforeTemplate' => $PBT,
                'toolbar' => [
                    ['content' =>
                        Html::a(FA::icon('refresh'), Url::toRoute('/main/dictionary/find-translate'), [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'btn btn-success',
                            'id' => 'update-translate',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Update'),
                        ]) .
                        Html::a(FA::icon('plus'), Url::toRoute('/main/dictionary/create'), [
                            'title' => Yii::t('app', 'Create'),
                            'class' => 'btn btn-success',
                            'id' => 'add-message',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Create'),
                            'data-pjax' => 0
                        ]) .
                        '{sizer}' .
                        '{collapse}'
                    ],
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container-dictionary',
                    ]
                ],
                'pager' => [
                    'class' => 'app\components\SkPager',
                ],
                'sizer' => true,
                'sizerOptions' => [
                    'pageVar' => 'per-page',
                    'sizeVariants' => [25 => 25, 50 => 50, 100 => 100, 200 => 200, 500 => 500],
                ],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => true,
                'panel' => [
                    'type' => SkGridView::TYPE_DEFAULT,
                    'heading' => Yii::t('app', 'Dictionary'),
                    'footer' => true,
                    'after' => false,
                    'beforeOptions' => ['style' => 'padding: 0;'],
                ],
                'persistResize' => false,
                'filterPosition' => SkGridView::FILTER_POS_BODY,
                'columns' => ArrayHelper::merge(
                    \backend\modules\main\models\SourceMessageSearch::getMessageColumns(),
                    [[
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'class' => 'action-column',
                            'style' => 'text-align:center; padding:0;'
                        ],
                        'headerOptions' => [
                            'class' => 'action-column-header',
                            'style' => 'width:42px;'
                        ],
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a(
                                    FA::icon('pencil'),
                                    $url,
                                    [
                                        'title' => Yii::t('app', 'Update'),
                                        'data-pjax' => 0
                                    ]
                                );
                            },
                        ]
                    ]]
                )
            ]); ?>
        </div>
    </div>
</div>