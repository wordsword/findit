<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="calculator-wrap hidden">
    <div class="calculator-title"></div>
    <div class="calculator-container"></div>
</div>