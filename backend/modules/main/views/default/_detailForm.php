<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="custom-alert-form-wrap hidden">
    <div class="custom-alert-form custom-modal custom-modal-lg">
        <div class="custom-modal-header">
            <div class="title-header">
                <span class="title"></span>
                <div class="message"></div>
            </div>
        </div>
        <div class="custom-modal-body log_table_block"></div>
    </div>
</div>