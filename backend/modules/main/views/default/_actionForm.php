<?php
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<?/*<div class="action-form-wrap hidden">
    <div class="action-form">
        <h1 class="form-title"></h1>
        <div class="form-inner">
            <div class="action-block"></div>
        </div>
        <div class="form-group buttons-wrap">
            <?= Html::button(Yii::t('app', 'OK'),['class' =>'actionOk btn btn-sm btn-primary']) ?>
            <?= Html::button(Yii::t('app', 'Cancel'),['class' =>'actionCancel btn btn-sm btn-default']) ?>
        </div>
    </div>
</div>*/?>

<div class="action-form-wrap hidden">
    <div class="action-form custom-modal custom-modal-xs">
        <div class="custom-modal-header">
            <div class="title-header">
                <span class="title"><?= Yii::t('app','Confirm action')?></span>
                <div class="message"></div>
            </div>
        </div>
        <div class="custom-modal-body">
            <div class="content">

            </div>
            <div class="modal-body-buttons">
                <?= Html::button(Yii::t('app', 'OK'),['class' =>'actionOk simple-button']) ?>
                <?= Html::button(Yii::t('app', 'Cancel'),['class' =>'actionCancel hollow-button']) ?>
            </div>
        </div>
    </div>
</div>
