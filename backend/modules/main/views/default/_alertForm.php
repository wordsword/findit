<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="alert-form-wrap hidden">
    <div class="alert-form custom-modal">

        <div class="custom-modal-header">
            <div class="title-header">
                <span class="title form-title"></span>
            </div>
        </div>


        <div class="custom-modal-body">
            <div class="content">
                <div class="message"></div>
            </div>
            <div class="modal-body-buttons">
                <?= Html::button(Yii::t('app', 'OK'), ['class' => 'alertOk simple-button']) ?>
            </div>
        </div>

    </div>
</div>