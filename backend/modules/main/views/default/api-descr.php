<div class="panel panel-default">
		<h2>Товары</h2>
		<div class="method">
			<div class="head">
				<div class="meth post">
					POST
				</div>
				<span class="link">
					/api/slides
				</span>
				<span class="desc">
					Создать/Обновить товар
				</span>
			</div>
			<div class="method-desc">
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
							<td>Тип</td>
							<td>Значение по умолчанию</td>
							<td>Обязателен</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>aboffice_id</td>
							<td>ID товара в программе АБофис</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>title</td>
							<td>Название</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>language</td>
							<td>Язык названия</td>
							<td>string</td>
							<td>ru</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>price</td>
							<td>Цена</td>
							<td>double</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>sizes</td>
							<td>Размеры</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>visible</td>
							<td>Видимость на сайте</td>
							<td>integer</td>
							<td>0</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>aboffice_parent_id</td>
							<td>ID категории в программе АБофис</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>image</td>
							<td>Ссылка на изображение</td>
							<td>string</td>
							<td>-</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>color</td>
							<td>Цвет</td>
							<td>string</td>
							<td>-</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>in_pack</td>
							<td>Количество товара в упаковке</td>
							<td>integer</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>label_id</td>
							<td>ID метки на сайте</td>
							<td>integer</td>
							<td>0</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>in_stock</td>
							<td>Количество товара на складе</td>
							<td>integer</td>
							<td>0</td>
							<td>Нет</td>
						</tr>
					</tbody>
				</table>
				<hr>
				Формат тела запроса: <b>application/json</b><br>
				Принимаемое значение: <b>Массив товаров</b><br>
				<hr>
				Пример тела запроса:
				<br>
				<pre>
[
	{
		"aboffice_id": "12",
	   	"title": "Тестовое платье",
	   	"price": "12",
	   	"sizes": "50-64	",
	   	"color": "червоний",
	   	"in_pack": 4,
	  	"aboffice_parent_id": "12",
	  	"visible": 1,
	  	"in_stock": 2
	},
	{
		"aboffice_id": "13",
	  	"title": "Тестовая кофта",
	   	"price": "23",
	   	"sizes": "40-54",
	   	"color": "синій",
	   	"in_pack": 6,
	  	"aboffice_parent_id": "10",
	  	"visible": 1,
	  	"in_stock": 3
  	}
]
				</pre>
			</div>
		</div>
		<div class="method">
			<div class="head">
				<div class="meth get">
					GET
				</div>
				<span class="link">
					/api/slides
				</span>
				<span class="desc">
					Получить список товаров
				</span>
			</div>
			<div class="method-desc">
				<b>GET-парметры:</b>
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>per-page</td>
							<td>Количество товаров на странице</td>
						</tr>
						<tr>
							<td>page</td>
							<td>Номер страницы</td>
						</tr>
						<tr>
							<td>aboffice_id</td>
							<td>ID товара в программе АБофис</td>
						</tr>
						<tr>
							<td>title</td>
							<td>Название</td>
						</tr>
						<tr>
							<td>price</td>
							<td>Цена</td>
						</tr>
						<tr>
							<td>sizes</td>
							<td>Размеры</td>
						</tr>
						<tr>
							<td>visible</td>
							<td>Видимость на сайте</td>
						</tr>
						<tr>
							<td>aboffice_parent_id</td>
							<td>ID категории в программе АБофис</td>
						</tr>
						<tr>
							<td>color</td>
							<td>Цвет</td>
						</tr>
						<tr>
							<td>label_id</td>
							<td>ID метки на сайте</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<b>Пример запроса:</b>
				<pre>
http://polla.complife.ua/api/slides?page=2&per-page=10&color=синій
				</pre>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<h2>Категории</h2>
		<div class="method">
			<div class="head">
				<div class="meth post">
					POST
				</div>
				<span class="link">
					/api/categories
				</span>
				<span class="desc">
					Создать/Обновить категорию
				</span>
			</div>
			<div class="method-desc">
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
							<td>Тип</td>
							<td>Значение по умолчанию</td>
							<td>Обязателен</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>aboffice_id</td>
							<td>ID категории в программе АБофис</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>title</td>
							<td>Название</td>
							<td>string</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>language</td>
							<td>Язык названия</td>
							<td>string</td>
							<td>ru</td>
							<td>Нет</td>
						</tr>
						<tr>
							<td>visible</td>
							<td>Видимость на сайте</td>
							<td>integer</td>
							<td>0</td>
							<td>Нет</td>
						</tr>
					</tbody>
				</table>
				<hr>
				Формат тела запроса: <b>application/json</b><br>
				Принимаемое значение: <b>Массив категорий</b><br>
				<hr>
				Пример тела запроса:
				<br>
				<pre>
[
	{
		"aboffice_id": "001", 
		"title": "Категория 11",
		"visible": 1
	}
]
				</pre>
			</div>
		</div>
		<div class="method">
			<div class="head">
				<div class="meth get">
					GET
				</div>
				<span class="link">
					/api/categories
				</span>
				<span class="desc">
					Получить список категорий
				</span>
			</div>
			<div class="method-desc">
				<b>GET-парметры:</b>
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>aboffice_id</td>
							<td>ID категории в программе АБофис</td>
						</tr>

						<tr>
							<td>visible</td>
							<td>Видимость на сайте</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<b>Пример запроса:</b>
				<pre>
http://polla.complife.ua/api/сategories?visible=1
				</pre>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<h2>Заказы</h2>
		<div class="method">
			<div class="head">
				<div class="meth get">
					GET
				</div>
				<span class="link">
					/api/documents
				</span>
				<span class="desc">
					Получить список заказов
				</span>
			</div>
			<div class="method-desc">
				<b>GET-парметры:</b>
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>user_id</td>
							<td>ID пользователя</td>
						</tr>
						<tr>
							<td>state_id</td>
							<td>ID состояния</td>
						</tr>
						<tr>
							<td>payment_state</td>
							<td>состояние оплаты</td>
						</tr>
						<tr>
							<td>created_from</td>
							<td>Дата от()</td>
						</tr>
						<tr>
							<td>created_to</td>
							<td>Дата до()</td>
						</tr>
						<tr>
							<td>name</td>
							<td>Имя заказщика</td>
						</tr>
						<tr>
							<td>emai</td>
							<td>Email заказщика</td>
						</tr>
						<tr>
							<td>phone</td>
							<td>телефон заказщика</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<b>Пример запроса:</b>
				<pre>
http://polla.complife.ua/api/documents?created_from=20-09-2018&created_to=30-10-2018
				</pre>
			</div>
		</div>
		<div class="method">
			<div class="head">
				<div class="meth post">
					POST
				</div>
				<span class="link">
					/api/check
				</span>
				<span class="desc">
					Изменить состояние заказов
				</span>
			</div>
			<div class="method-desc">
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
							<td>Тип</td>
							<td>Значение по умолчанию</td>
							<td>Обязателен</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>state_id</td>
							<td>ID состояния заказа</td>
							<td>integer</td>
							<td>-</td>
							<td>Да</td>
						</tr>
						<tr>
							<td>orders</td>
							<td>Массив ID заказов</td>
							<td>array</td>
							<td>-</td>
							<td>Да</td>
						</tr>
					</tbody>
				</table>
				<hr>
				Формат тела запроса: <b>application/json</b><br>
				Принимаемое значение: <b>ID состояния + список закзов</b><br>
				<hr>
				Пример тела запроса:
				<br>
				<pre>
{ 
	"state_id": 2,
	"orders":["12","13","14"]
}
				</pre>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<h2>Метки</h2>
		<div class="method">
			<div class="head">
				<div class="meth get">
					GET
				</div>
				<span class="link">
					/api/labels
				</span>
				<span class="desc">
					Получить список меток
				</span>
			</div>
			<div class="method-desc">
				<b>Пример запроса:</b>
				<pre>
http://polla.complife.ua/api/labels
				</pre>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<h2>Пользователи</h2>
		<div class="method">
			<div class="head">
				<div class="meth get">
					GET
				</div>
				<span class="link">
					/api/users
				</span>
				<span class="desc">
					Получить список пользователей
				</span>
			</div>
			<div class="method-desc">
				<b>GET-парметры:</b>
				<table>
					<thead>
						<tr>
							<td>Название</td>
							<td>Описание</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>per_page</td>
							<td>Количество пользователей на странице</td>
						</tr>
						<tr>
							<td>page</td>
							<td>Номер страницы</td>
						</tr>
						<tr>
							<td>id</td>
							<td>ID пользователя на сайте</td>
						</tr>
						<tr>
							<td>status</td>
							<td>статус пользователя</td>
						</tr>
						<tr>
							<td>admin</td>
							<td>Является ли пользователь администратором</td>
						</tr>
						<tr>
							<td>username</td>
							<td>Имя пользователя</td>
						</tr>
						<tr>
							<td>email</td>
							<td>E-mail</td>
						</tr>
						<tr>
							<td>phone</td>
							<td>Номер телефона</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<b>Пример запроса:</b>
				<pre>
http://polla.complife.ua/api/users?username=admin
				</pre>
			</div>
		</div>
	</div>