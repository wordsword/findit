<?php
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<?/*<div class="confirm-form-wrap hidden">
    <div class="confirm-form">
        <h1 class="form-title"><?= \rmrevin\yii\fontawesome\FA::icon('question').Yii::t('app','Confirm action')?></h1>
        <div class="form-inner">
            <p class="message"></p>
        </div>
        <div class="form-group buttons-wrap">
            <?= Html::button(Yii::t('app', 'OK'),['class' =>'confirmOk btn btn-sm btn-primary']) ?>
            <?= Html::button(Yii::t('app', 'Cancel'),['class' =>'confirmCancel btn btn-sm btn-default']) ?>
        </div>
    </div>
</div>*/?>

<div class="confirm-form-wrap hidden">
    <div class="confirm-form custom-modal">

        <div class="custom-modal-header">
            <div class="title-header">
                <span class="title"><?= Yii::t('app','Confirm action')?></span>
                <div class="message"></div>
            </div>
        </div>

        <div class="custom-modal-body">
            <div class="content"></div>
            <div class="modal-body-buttons">
                <?= Html::button(Yii::t('app', 'OK'),['class' =>'confirmOk simple-button']) ?>
                <?= Html::button(Yii::t('app', 'Cancel'),['class' =>'confirmCancel hollow-button']) ?>
            </div>
        </div>

    </div>
</div>