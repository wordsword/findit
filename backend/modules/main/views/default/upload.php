<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use common\components\SkLangHelper;
use backend\assets\KcFinderAsset;

KcFinderAsset::register($this);

$this->title = Yii::t('app', 'Admin panel');
$this->params['breadcrumbs'][] = $this->title;


$basePath = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
Yii::$app->session->set('KCfinder', [
    'uploadURL' => Yii::$app->request->hostInfo.'/uploads',
    'uploadDir' => $basePath,
    'disabled' => false,
]);

Pjax::begin([
    'id' => 'pjax-container',
    'formSelector' => '#slide-form',
    'timeout' => 3000,
    'clientOptions' => [
        'scrollTop' => false
    ]
]);
?>

    <div class="upload-form">

        <?php $form = ActiveForm::begin([
            'id' => 'upload-form',
            'action' => SkLangHelper::addLangToUrl(Url::toRoute('')),
            'enableClientScript' => false,
        ]); ?>
            <div class="">
                <?= $form->field($model, 'XMLfile')->textInput([
                    'maxlength' => true,
                    'onclick' => "openKCFinder(this," . yii\helpers\Json::encode(Yii::$app->language) . ",'xml','xml')"
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Open'), ['class' => 'btn btn-sm btn-primary']) ?>
                </div>
            </div>
            <?php if(is_array($content)) { ?>
                <?= Html::submitButton(Yii::t('app', 'Parse'), ['class' => 'btn btn-sm btn-primary', 'name' => 'parse']) ?>
                <pre style="height: 500px;">
                    <?php 
                        print_r($content);
                     ?>
                </pre>
            <?php } ?>
        <?php ActiveForm::end(); ?>

    </div>

<?php

Pjax::end();
 ?>