<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\SourceMessage */

$this->title = Yii::t('app', 'Create language');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dictionary'), 'url' => ['/main/dictionary']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-create panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $this->title; ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
