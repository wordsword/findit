<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\SourceMessage */

$this->title = Yii::t('app', 'Update language:').' '.$model->lang_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dictionary'), 'url' => ['/main/dictionary']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-update panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?= Yii::t('app', 'Update language:').' <strong>'.Html::encode($model->lang_name).'</strong>' ?>
        </h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
