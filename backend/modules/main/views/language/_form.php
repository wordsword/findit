<?php

use backend\modules\main\models\Language;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

\backend\assets\KcFinderAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\modules\main\models\Language */
/* @var $form yii\widgets\ActiveForm */

$basePath = Yii::getAlias('@frontend').DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;
Yii::$app->session->set('KCfinder', [
    'uploadURL'=>'/uploads',
    'uploadDir'=>$basePath,
    'disabled'=>false,
]);

?>

<div class="source-message-form">
    <div class="row">
        <div class="col-sm-12">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'deflang')->checkbox() ?>
            <?= $form->field($model, 'lang_id')->textInput()->hint(Yii::t('app','Use ISO 639-1 Language Codes')) ?>
            <?= $form->field($model, 'lang_name')->textInput() ?>
            <?= $form->field($model, 'image')->textInput([
                'maxlength' => true,
                'onclick'=>"openKCFinder(this,".yii\helpers\Json::encode(Yii::$app->language).",'langs')"
            ]) ?>
            <?= $form->field($model, 'sort_order')->hint(Yii::t('app','Item will be displayed after selected item in list'))
                ->widget(Select2::classname(), [
                    'data' => Language::getSortData($model->primaryKey),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ]
            ) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm'
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
