<?php 

use yii\helpers\Html;
$js = <<<JS
$(document).on('click', '#translate', function(e){
	e.preventDefault();
	_from = $('#from').prop('value');
	_to = $('#to').prop('value');



	console.log(_from);
	console.log(_to);

	if(_from == "")
	{
		$('#from').parent().addClass('has-error');
		return;
	}

	if(_to == "")
	{
		$('#to').parent().addClass('has-error');
		return;
	}

	
	_href = window.location.href.split('?')[0];
	_href += "?lang_from=" + _from + '&lang_to=' + _to; 

	$.post({
		url: _href,
		data: {
			Translate: 1
		},
		error: function(er){
			console.log(er);
			if(er.status == 500)
				alert("Translation error. Try once more.");
		}
	});
});

$(document).on('focusin', 'select', function(){
	$(this).parent().removeClass('has-error');
});

JS;

$this->registerJs($js);
?>	
<div class="panel panel-default">
	<div class="panel-body">
		<div class="form-group">
			<label for="#from"><?=Yii::t('app', 'Source language')?></label>
			<select class="form-control" id="from" autocomplete="off">
				<?php 
				echo Html::tag('option', Yii::t('app', 'Source message'), [
					'value' => 'src',
					'selected' => $_GET['lang_from'] == 'src'
				]);
				foreach ($langs as $lang) {
					echo Html::tag('option', $lang, [
						'value' => $lang,
						'selected' => $_GET['lang_from'] == $lang
					]);
				}
				?>
			</select>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<label for="to"><?=Yii::t('app', 'Destination language')?></label>
			<select class="form-control" id="to" autocomplete="off">
				<?php
				foreach ($langs as $lang) {
					echo Html::tag('option', $lang, [
						'value' => $lang,
						'selected' => $_GET['lang_to'] == $lang
					]);
				}
				?>
			</select>
			<div class="help-block"></div>
		</div>
		<button class="btn btn-defalult btn-primary" id="translate">
			<?=Yii::t('app', 'Translate')?>
		</button>
	</div>
</div>

<!-- <pre>
	<?//=print_r($langs)?>
</pre> -->