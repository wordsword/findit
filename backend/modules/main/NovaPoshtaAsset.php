<?php

namespace backend\modules\main;

use yii\web\AssetBundle;

class NovaPoshtaAsset extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset',
        'kartik\select2\Select2Asset'
    ];

    public $js = [
        'nova-poshta.js'
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }
}
