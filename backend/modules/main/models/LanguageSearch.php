<?php

namespace backend\modules\main\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\main\models\Language;

/**
 * LanguageSearch represents the model behind the search form about `backend\modules\main\models\Language`.
 */
class LanguageSearch extends Language
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        
        return [
            [['lang_id','lang_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Language::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort_order'=>SORT_ASC]
            ],
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['like', 'lang_name', $this->lang_name]);
        $query->andFilterWhere(['like', 'lang_id', $this->lang_id]);

        return $dataProvider;
    }
}
