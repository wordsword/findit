<?php

namespace app\modules\main\models;

use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\caching\DbDependency;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use rmrevin\yii\fontawesome\FA;
use app\modules\shop\models\Review;
use app\modules\shop\models\Request;
use common\modules\user\models\Role;
use common\modules\user\models\User;
use app\modules\main\models\Document;
use app\modules\main\models\RequestCall;
use backend\modules\main\models\UserMessage;
use common\modules\user\models\AdminMenuRole;

function folderSize($dir)
{
    $size = 0;
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $file) {

    //     echo "<pre>";
    //     var_dump($file->getBaseName());
    //     echo "</pre>";
    // exit;
    if($file->getBaseName() != '.' && $file->getBaseName() != '..')
        $size += $file->getSize();
    }
    return $size;
}


function format_size($size)
{
    $units = explode(' ', 'B KB MB GB TB PB');

    $mod = 1024;

    for ($i = 0; $size > $mod; $i++) {
        $size /= $mod;
    }

    $endIndex = strpos($size, ".") + 3;

    return substr($size, 0, $endIndex) . ' ' . $units[$i];
}
class AdminMenu extends \common\models\AdminMenu {
    
    public static function getTreeViewData() {
        return self::_getTreeViewDataRecursive(0);
    }
    
    private static function _getTreeViewDataRecursive($parent) {
        $roles = Role::find()->all();

        $items = [];
        $query = self::find()
            ->where(['parent_id'=>$parent])
            ->orderBy(['sort_order'=>SORT_ASC,'id'=>SORT_DESC]);
        $menus = $query->all();
        foreach ($menus as $key => $menu) {
            $childs = self::_getTreeViewDataRecursive($menu->primaryKey);
            $roleCheck = "";

            foreach ($roles as $role) {
                $roleCheck .= 
                Html::beginTag('form', ['style' => [
                    'margin' => '0 3px',
                    'display' => 'inline-block',
                    'width' => '120px',
                    'text-align' => 'center'
                ]]).
                    Html::hiddenInput('role_id', $role->id).
                    Html::hiddenInput('item_id', $menu->id).
                    Html::checkbox(
                        'value',
                        $role->canSee($menu->id),
                        [
                            'class' => 'role-check',
                            'data-role' => $role->id,
                        ]
                    ).
                Html::endTag('form');
            }
            $items[$key] = [
                'text' => Html::encode($menu->title),
                'roles' => $roleCheck,
                'recID' => Html::encode($menu->primaryKey),
                'updateLink' => \yii\helpers\Url::toRoute(['/main/admin-menu/update','id'=>Html::encode($menu->primaryKey)]),
                'delLink' => \yii\helpers\Url::toRoute(['/main/admin-menu/delete','id'=>Html::encode($menu->primaryKey)]),
            ];
            if (count($childs) > 0) {
                $items[$key]['childs'] = $childs;
            }
        }
        return $items;
        
    }

    public static function getTreeViewDataTest()
    {
        $roles = Role::find()->select('id')->asArray()->indexBy('id')->all();
        foreach ($roles as $key => $role) {
            $roles[$key]['menu_role'] = AdminMenuRole::find()->select('admin_menu_id')->where(['role_id' => $role['id']])->asArray()->column();
        }

        $menu = [];
        $menu_base = [];

        $query = new Query();
        $query->select(['menu.*', 'menu_lang.title']);
        $query->from(self::tableName() . ' menu');
        $query->leftJoin(self::langTableName() . ' menu_lang', 'menu_lang.owner_id=menu.id AND menu_lang.language=:language', ['language' => Yii::$app->language]);
        $items = $query->orderBy(['menu.sort_order' => SORT_ASC])->indexBy('id')->all();

        if (!empty($items)) {
            foreach ($items as $id => &$node) {
                $roleCheck = "";
                foreach ($roles as $role) {
                    $roleCheck .=
                        Html::beginTag('form', ['style' => [
                            'margin' => '0 3px',
                            'display' => 'inline-block',
                            'width' => '120px',
                            'text-align' => 'center'
                        ]]) .
                        Html::hiddenInput('role_id', $role['id']) .
                        Html::hiddenInput('item_id', $node['id']) .
                        Html::checkbox(
                            'value',
                            (in_array($node['id'], $role['menu_role'])) ? 1 : 0,
                            [
                                'class' => 'role-check',
                                'data-role' => $role['id'],
                            ]
                        ) .
                        Html::endTag('form');
                }
                $node['roles'] = $roleCheck;
                if (!$node['parent_id']) {
                    $menu[$id] = &$node;
                } else {
                    $items[$node['parent_id']]['items'][$node['id']] = &$node;
                }
            }

            foreach ($menu as $id => $item) {
                $menu_base[$id] = [
                    'text' => Html::encode($item['title']),
                    'title' => Html::encode($item['title']),
                    'roles' => $item['roles'],
                    'recID' => Html::encode($item['id']),
                    // 'updateLink' => \yii\helpers\Url::toRoute(['/main/admin-menu/update', 'id' => Html::encode($item['id'])]),
                ];
                
                if(Yii::$app->user->isSuperAdmin){
                    $menu_base[$id]['updateLink'] = \yii\helpers\Url::toRoute(['/main/admin-menu/update', 'id' => Html::encode($item['id'])]);
                    $menu_base[$id]['delLink'] = \yii\helpers\Url::toRoute(['/main/admin-menu/delete', 'id' => Html::encode($item['id'])]);
                }

                if (isset($item['items'])) {
                    foreach ($item['items'] as $key => $sub_menu) {
                        $menu_base[$id]['childs'][$key] = [
                            'text' => Html::encode($sub_menu['title']),
                            'title' => Html::encode($sub_menu['title']),
                            'roles' => $sub_menu['roles'],
                            'recID' => Html::encode($sub_menu['id']),
                            // 'updateLink' => \yii\helpers\Url::toRoute(['/main/admin-menu/update', 'id' => Html::encode($sub_menu['id'])]),
                            // 'delLink' => \yii\helpers\Url::toRoute(['/main/admin-menu/delete', 'id' => Html::encode($sub_menu['id'])]),
                        ];
                        if(Yii::$app->user->isSuperAdmin){
                            $menu_base[$id]['childs'][$key]['updateLink'] = \yii\helpers\Url::toRoute(['/main/admin-menu/update', 'id' => Html::encode($sub_menu['id'])]);
                            $menu_base[$id]['childs'][$key]['delLink'] = \yii\helpers\Url::toRoute(['/main/admin-menu/delete', 'id' => Html::encode($sub_menu['id'])]);
                        }
                    }
                }
            }
        }
        return $menu_base;
    }

    public function getHasChildren()
    {
        return self::find()->where(['parent_id' => $this->id])->count() > 0;
    }

    public static function getMenu()
    {
        $items = [];

        $menus = self::find()->orderBy(['sort_order' => SORT_ASC])->indexBy('id')->multilingual()->all();


        $counters = [];
       
        // $counters['users'] = User::getCountForToday();

        $counters['orders'] = Document::getNewOrdersCount();

        foreach ($menus as $id => $menu) {
            if(!User::canSee($id))
                unset($menus[$id]);
        }


        foreach ($menus as $id => $menu) {
            if(User::canSee($id) && ($menu->parent_id == 0 || (in_array($menu->parent_id, array_keys($menus)))))
            {
                $items[$id]['label'] = FA::icon($menu->icon).Html::tag('span', $menu->title);
                $items[$id]['url'] = [($menu->hasChildren ? '#' : $menu->url)];

                if(!empty($menu->counter) && in_array($menu->counter, array_keys($counters)))
                {
                    $items[$id]['label'] .= Html::tag('span', $counters[$menu->counter], ['class' => 'badge']);
                    $items[$id]['counter'] += $counters[$menu->counter];
                }

                if($menu->parent_id != 0)
                {
                    $items[$menu->parent_id]['items'][] = $items[$id];
                    unset($items[$id]);
                }
            }
        }

        foreach ($items as &$item) {
            if(!empty($item['items']))
            {
                $counter = 0;
                foreach ($item['items'] as $subItem) {
                    if(!empty($subItem['counter']))
                    {
                        $counter += intval($subItem['counter']);
                    }
                }
                if($counter != 0)
                    $item['label'] .= Html::tag('span', $counter, ['class' => 'badge red new']);
            }
        }




        $items[] = ['label' => FA::icon('money') . '<span>' . Yii::t('app', 'SMS Balance') . ': ' . number_format(Yii::$app->smsClient->balance, 2) . '</span>', 'class' => 'sms-balance'];

        return $items;
    }


    public static function getMenuTest()
    {
        $menu = [];
        $menu_base = [];

        $query = new Query();
        $query->select(['menu.*', 'menu_lang.title']);
        $query->from(self::tableName() . ' menu');
        $query->leftJoin(self::langTableName() . ' menu_lang', 'menu_lang.owner_id=menu.id AND menu_lang.language=:language', ['language' => Yii::$app->language]);
        if (!Yii::$app->user->isSuperadmin) {
            $query->innerJoin('{{admin_menu_role}} rp', 'rp.admin_menu_id = menu.id AND rp.role_id=(SELECT role_id FROM user WHERE id = :user)', ['user' => Yii::$app->user->identity->id]);
        }

        if(!Yii::$app->user->isSuperAdmin)
            $query->andWhere(['owner' => 1]);

        $items = $query->orderBy(['menu.sort_order' => SORT_ASC])->indexBy('id')->all();

        if (!empty($items)) {
            foreach ($items as $id => &$node) {
                if (!empty($node['counter'])) {
                    switch ($node['counter']) {
                        
                        // case 'users':
                        //     $node['counter_count'] = User::getCountForToday();
                        //     break;
                        case 'orders':
                            $node['counter_count'] = Document::getNewOrdersCount();
                            break;
                        default:
                            $node['counter_count'] = 0;
                            break;
                    }
                }

                // $item['options']['active'] = ;

                if (!$node['parent_id']) {
                    $menu[$id] = &$node;
                } else {
                    if (isset($node['counter_count'])) {
                        $items[$node['parent_id']]['counter_count'] += $node['counter_count'];
                    }
                    $items[$node['parent_id']]['items'][$node['id']] = &$node;
                }
            }

            foreach ($menu as $id => $item) {
                
                $item['options'] = [];
                if(!empty($item['items']))
                {
                    foreach ($item['items'] as $key => $child) {
                        // echo Url::toRoute($child['url']).' '.$_SERVER['REQUEST_URI'].'<br>';
                        if($_SERVER['REQUEST_URI'] == Url::toRoute($child['url']))
                        {
                            $item['active'] = true;
                            $item['items'][$key]['active'] = true;
                        }
                            // Html::addCssClass($item['options'], 'active');
                    }
                } else {
                    if(!empty($item['url']))
                    {
                        if($_SERVER['REQUEST_URI'] == Url::toRoute($item['url']))
                        {
                            $item['active'] = true;
                        }
                    } else continue;
                }

                // var_dump($_SERVER['REQUEST_URI'] == Url::to($item['url']) && empty($item['items']));
                // echo Url::to([$item['url']]).'-'.$_SERVER['REQUEST_URI'].'-'.'<br>';
                // continue;

                $menu_base[$id]['label'] = FA::icon($item['icon']) . Html::tag('span', $item['title']);
                $menu_base[$id]['active'] = $item['active'];
                $menu_base[$id]['url'] = [(isset($item['items']) ? '#' : Url::to($item['url']))];
                if ($item['counter_count'] > 0) {
                    $menu_base[$id]['label'] .= Html::tag('span', $item['counter_count'], ['class' => 'badge bage-info']);
                }

                if (isset($item['items'])) {
                    foreach ($item['items'] as $key => $sub_menu) {
                        $menu_base[$id]['items'][$key]['label'] = FA::icon($sub_menu['icon']) . Html::tag('span', $sub_menu['title']);
                        $menu_base[$id]['items'][$key]['url'] =  Url::toRoute($sub_menu['url']);
                        $menu_base[$id]['items'][$key]['active'] = $sub_menu['active'];
                        if ($sub_menu['counter_count'] > 0) {
                            $menu_base[$id]['items'][$key]['label'] .= Html::tag('span', $sub_menu['counter_count'], ['class' => 'badge bage-info']);
                        }
                    }
                }
            }
            if (Yii::$app->user->isSuperadmin) {
                $menu_base[] = ['label' => FA::icon('money') . '<span>' . Yii::t('app', 'SMS Balance') . ': ' . number_format(Yii::$app->smsClient->balance, 2) . '</span>', 'options' => ['class' => 'sms-balance']];
                // $menu_base[] = ['label' => FA::icon('database') . '<span>' . Yii::t('app', 'Size') . ': ' . format_size(folderSize($_SERVER['DOCUMENT_ROOT'])) . '</span>', 'options' => ['class' => 'sms-balance']];
            }
        }
        return $menu_base;
    }
    
}
