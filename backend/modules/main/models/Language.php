<?php

namespace backend\modules\main\models;

use Yii;
use yii\db\Query;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property string $lang_id
 * @property string $lang_name
 * @property string $image
 * @property integer $sort_order
 * @property integer $visible
 * @property integer $deflang
 * @property string $created_at
 * @property string $updated_at
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'lang_name', 'image'], 'required'],
            [['sort_order', 'visible', 'deflang'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lang_id'], 'string', 'max' => 6],
            ['deflang','defLangValidate'],
            [['lang_name', 'image'], 'string', 'max' => 250],
            [['lang_id'], 'unique']
        ];
    }

    public function defLangValidate($attribute, $params) {
        $id = $this->primaryKey;
        if (empty($id))
            $id = 0;
        $deflang = self::find()->andWhere(['deflang'=>1])->andWhere(['!=','id',$id])->count();
        if ($deflang == 0 && $this->deflang == 0) {
            $this->addError($attribute, Yii::t('app','Should be at least one language as default!'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang_id' => Yii::t('app', 'Language ID'),
            'lang_name' => Yii::t('app', 'Language title'),
            'image' => Yii::t('app', 'Image'),
            'sort_order' => Yii::t('app', 'Sort order'),
            'visible' => Yii::t('app', 'Visibility'),
            'deflang' => Yii::t('app', 'Default language'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    public function behaviors()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $oldPos = $this->oldAttributes['sort_order'];
            if ($this->sort_order != $oldPos) {
                if (!$insert) {
                    $sql = "UPDATE " . self::tableName() . " ";
                    $sql .= "SET sort_order = sort_order - 1 ";
                    $sql .= "WHERE sort_order >= :oldPos ";
                    $sql .= "AND id!=:id ";
                    Yii::$app->db->createCommand($sql)->bindValues([
                        'oldPos' => $oldPos,
                        'id' => $this->primaryKey
                    ])->execute();
                    if ($this->sort_order > 1 && $oldPos < $this->sort_order)
                        $this->sort_order = $this->sort_order - 1;
                }
                $sql = "UPDATE " . self::tableName() . " ";
                $sql .= "SET sort_order = sort_order + 1 ";
                $sql .= "WHERE sort_order >= :curPos ";
                Yii::$app->db->createCommand($sql)->bindValues([
                    'curPos' => $this->sort_order,
                ])->execute();
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        $sql  = "UPDATE ".self::tableName()." ";
        $sql .= "SET sort_order =(select @a:= @a + 1 from (select @a:= 0) s) ";
        $sql .= "ORDER BY sort_order";
        Yii::$app->db->createCommand($sql)->execute();
        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->deflang) {
            Yii::$app->db->createCommand()->update(self::tableName(),['deflang'=>0],['!=','id',$this->primaryKey])->execute();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public static function getSortData($id) {
        if (empty($id)) {
            $id = 0;
        }
        $fields = [1 => '- '.Yii::t('app','First place').' -'];
        $sql  = "SELECT sort_order, lang_name ";
        $sql .= "FROM ".self::tableName()." ";
        $sql .= "WHERE id!=:id ";
        $sql .= "ORDER BY sort_order ";
        $tmpFields = Yii::$app->db->createCommand($sql)
            ->bindValues([
                'id' => $id,
            ])->queryAll();
        foreach ($tmpFields as $field) {
            $fields[Html::encode($field['sort_order'])+1] = Html::encode($field['lang_name']);
        }
        return $fields;
    }

    private static $list = null;

    public static function getList(){
        if(self::$list == null)
            self::$list = (new Query)->from('languages')->select('lang_id')->column();;

        return self::$list;
    }
}
