<?php

namespace backend\modules\main\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{
    private static $_columns;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source_message}}';
    }
    
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(
            parent::attributes(), 
            self::getLangColumns()
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            ['message','unique'],
            [['category'], 'string', 'max' => 32],
            [['category'],'default','value' => 'app'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        foreach (\common\components\SkLangHelper::getLangs() as $key => $lang){
            $langs['message_'.$key] = Yii::t('app', 'Translation').' '.$key;
        }
        return array_merge([
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app', 'System message'),
        ], $langs);
    }

    protected static function getLangColumns() {
        if (empty(self::$_columns)) {
            $columns = [];
            foreach (\common\components\SkLangHelper::getLangs() as $lang) {
                $columns[] = 'message_'.$lang['lang_id'];
            }
            self::$_columns = $columns;
        }
        return self::$_columns;
    }

    private static function getListFiles($folder,&$msg){
        $fp=opendir($folder);
        while($cv_file=readdir($fp)) {
            $ext = pathinfo($cv_file,PATHINFO_EXTENSION);
            if(is_file($folder."/".$cv_file) && $ext == 'php') {
                $file=$folder."/".$cv_file;
                $content = file_get_contents($file);
                $result = preg_match_all('/Yii::t\([\'|"]app[\'|"],\s*[\'|"]([\w\d\s\-\,\.:\?\!\(\)]+)[\'|"]\)/',$content, $matches);
                if ($result && count($matches) > 0) {
                    foreach ($matches[1] as $match)
                        $msg[] = $match;
                }
            }elseif($cv_file!="." && $cv_file!=".." && is_dir($folder."/".$cv_file)){
                self::getListFiles($folder."/".$cv_file,$msg);
            }
        }
        closedir($fp);
    }

    public static function importTranslations() {

        $msg=[];
        self::getListFiles(Yii::getAlias('@backend'),$msg);
        foreach ($msg as $value) {
            $dicMsg = Yii::$app->db->createCommand("SELECT COUNT(*) FROM {{%source_message}} WHERE message=:val")
                ->bindParam('val',$value)->queryScalar();
            if ($dicMsg == 0) {
                Yii::$app->db->createCommand()->insert('{{%source_message}}',['category'=>'app','message'=>$value])->execute();
            }
        }
        $msg=[];
        self::getListFiles(Yii::getAlias('@frontend'),$msg);
        foreach ($msg as $value) {
            $dicMsg = Yii::$app->db->createCommand("SELECT COUNT(*) FROM {{%source_message}} WHERE message=:val")
                ->bindParam('val',$value)->queryScalar();
            if ($dicMsg == 0) {
                Yii::$app->db->createCommand()->insert('{{%source_message}}',['category'=>'app','message'=>$value])->execute();
            }
        }
        self::getListFiles(Yii::getAlias('@common'),$msg);
        foreach ($msg as $value) {
            $dicMsg = Yii::$app->db->createCommand("SELECT COUNT(*) FROM {{%source_message}} WHERE message=:val")
                ->bindParam('val',$value)->queryScalar();
            if ($dicMsg == 0) {
                Yii::$app->db->createCommand()->insert('{{%source_message}}',['category'=>'app','message'=>$value])->execute();
            }
        }
        return 1;
    }
}
