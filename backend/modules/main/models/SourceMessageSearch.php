<?php

namespace backend\modules\main\models;

use kartik\grid\EditableColumn;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\main\models\SourceMessage;

/**
 * SourceMessageSearch represents the model behind the search form about `backend\modules\main\models\SourceMessage`.
 */
class SourceMessageSearch extends SourceMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [array_merge(['category', 'message'], self::getLangColumns()), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $query = SourceMessage::find()->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['message' => SORT_ASC]
            ],
            'pagination' => [
                'defaultPageSize' => 25,
                'pageSizeLimit' => [1, 500]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->from(['source_message t']);
        $tCols = ['t.message', 't.id'];
        foreach (\common\components\SkLangHelper::getLangs() as $key => $lang) {
            $tCols[] = 't' . $key . '.translation as message_' . $key;
            $query->leftJoin('message t' . $key, 't' . $key . '.id=t.id AND t' . $key . '.language="' . $key . '"');
            $property = 'message_' . $key;
            $query->andFilterWhere(['like', 't' . $key . '.translation', $this->$property]);
        }
        $query->andFilterWhere(['like', 'message', $this->message]);
        $query->select($tCols);

        return $dataProvider;
    }

    public static function getMessageColumns()
    {
        $columns[] = 'message:ntext';
        foreach (parent::getLangColumns() as $column) {
            $columns[] = [
                'class' => '\kartik\grid\EditableColumn',
                'attribute' => $column,
            ];
        }
//        $columns[] = [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{update}',
//        ];
        return $columns;
    }
}
