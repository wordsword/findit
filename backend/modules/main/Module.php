<?php

namespace app\modules\main;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\main\controllers';
    
    public $allowedExtensions = ['jpeg', 'jpg', 'png'];
    public $objects = [];

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    


    public static function getVisibibilityStates()
    {
        return [
            '0' => Yii::t('app', 'Hidden'),
            '1' => Yii::t('app', 'Visible'),
        ];
    }
}
