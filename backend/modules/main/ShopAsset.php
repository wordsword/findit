<?php

namespace app\modules\main;

use yii\web\AssetBundle;

class ShopAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/main/assets';
    public $css = [
        'style.css?v=0.4',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\components\FancyBoxAsset',
        'backend\assets\UriAsset'
    ];
    public $js = [
        'main.js?v=0.10'
    ];

    public function init()
    {
        // $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }
}
