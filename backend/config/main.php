<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
//    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'debug'],
    'modules' => [
        'datecontrol' =>  [
            'class' => '\kartik\datecontrol\Module'
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*']
        ],
        'formatter' => [
            'class' => ' yii\i18n\Formatter',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'user-management' => [
            'class' => 'common\modules\user\Module',
            'maxAttempts' => 5,
            // Here you can set your handler to change layout for any controller or action
            // Tip: you can use this event in any module
            'on beforeAction' => function (yii\base\ActionEvent $event) {
                if ($event->action->uniqueId == 'user-management/auth/login') {
                    $event->action->controller->layout = 'loginLayout.php';
                }
            },
        ],
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
    ],
    'components' => [
        //        'cache' => [
        //            'class' => 'yii\caching\FileCache',
        //        ],
        //        'db' => [
        //            // Кеширования схемы
        //            'enableSchemaCache' => true,
        //
        //            // Продолжительность кеширования схемы.
        //            'schemaCacheDuration' => 3600,
        //
        //            // Название компонента кеша, используемого для хранения информации о схеме
        //            'schemaCache' => 'cache',
        //        ],
        'urlManager' => [
            'class' => 'common\components\SkUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'main/default/index',
                '<_a:error>' => 'main/default/<_a>',
                '<_a:(login|logout)>' => '/user-management/auth/<_a>',

                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<param:[\w\-]+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>' => '<_m>/admin/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
            ]
        ],
        'urlManagerFrontend' => [
            'class' => 'common\components\SkUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '',
            'rules' => [
                '<path:.*>' => 'main/router/index',
            ]
        ],
        'user' => [
            'class' => 'common\modules\user\components\UserConfig',
            // Comment this if you don't want to record user logins
//            'on afterLogin' => function($event) {
//                \common\modules\user\models\UserVisitLog::newVisitor($event->identity->id);
//            }
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget', //в файл
                    'categories' => ['proxy', 'translation'], //категория логов
                    'logFile' => '@runtime/logs/translate.log', //куда сохранять
                    'logVars' => [] //не добавлять в лог глобальные переменные ($_SERVER, $_SESSION...)
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'request' => [
            'class' => 'common\components\SkRequest',
            'baseUrl' => '/admin'
        ],
    ],
    'params' => $params,
];