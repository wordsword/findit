<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_123435_add_info_tbl extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%notes}}', [
            'id'         => Schema::TYPE_PK,
            'owner_id'   => Schema::TYPE_INTEGER,
            'class'      => Schema::TYPE_STRING.'(255)',
            'user_id'    => Schema::TYPE_INTEGER,
            'text'       => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%notes}}');
    }
}
