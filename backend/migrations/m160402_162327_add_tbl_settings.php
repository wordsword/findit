<?php

use yii\db\Schema;
use yii\db\Migration;

class m160402_162327_add_tbl_settings extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%settings}}', [
            'id'         => Schema::TYPE_PK,
            'key'        => Schema::TYPE_STRING,
            'value'      => Schema::TYPE_STRING,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
