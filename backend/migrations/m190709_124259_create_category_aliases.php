<?php

namespace backend\migrations;

use yii\db\Migration;
use app\modules\main\models\Category;

/**
 * Class m190709_124259_create_category_aliases
 */
class m190709_124259_create_category_aliases extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('aliases', [
            'model' => "app\\modules\\main\\models\\Category"
        ]);


        $categories = Category::find()->all();

        foreach($categories as $cat){
            // $tmp = $cat->alias;
            // $cat->alias = "";
            // $cat->save();
            // $cat->alias  = $tmp;
            $cat->load($cat->attributes, '');
            $cat->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190709_124259_create_category_aliases cannot be reverted.\n";

        // return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190709_124259_create_category_aliases cannot be reverted.\n";

        return false;
    }
    */
}
