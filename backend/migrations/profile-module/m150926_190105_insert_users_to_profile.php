<?php

use yii\db\Schema;
use yii\db\Migration;

class m150926_190105_insert_users_to_profile extends Migration
{
    public function up()
    {
        $rows = Yii::$app->db->createCommand("SELECT id FROM {{%user}}")->queryColumn();
        $insertRows = [];
        foreach ($rows as $row) {
            $insertRows[] = [$row]; 
        }
        if (count($rows) > 0) {
            $this->batchInsert('{{%profile}}', ['user_id'], $insertRows);
        }
    }

    public function down()
    {
        $this->delete('{{%profile}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
