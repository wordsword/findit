<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_222605_profile_field_group_table_alter_parent_field extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%profile_field_group}}', 'parent_id', Schema::TYPE_INTEGER.' NOT NULL DEFAULT "0"');
    }

    public function down()
    {
        echo "m151001_222605_profile_field_group_table_alter_parent_field cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
