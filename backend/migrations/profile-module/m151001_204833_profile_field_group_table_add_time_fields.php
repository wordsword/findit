<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_204833_profile_field_group_table_add_time_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%profile_field_group}}', 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_field_group}}', 'updated_at', Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%profile_field_group}}', 'created_at');
        $this->dropColumn('{{%profile_field_group}}', 'updated_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
