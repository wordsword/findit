<?php

use yii\db\Schema;
use yii\db\Migration;

class m150926_163547_create_profile_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%profile}}', [
                'user_id' => Schema::TYPE_PK
        ], $tableOptions);
        
        $this->addForeignKey('profile_user', '{{%profile}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('profile_user', '{{%profile}}');
        $this->dropTable('{{%profile}}');
    }

}
