<?php

use yii\db\Schema;
use yii\db\Migration;

class m150926_222530_create_profile_field_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%profile_field}}', [
            'id' => Schema::TYPE_PK,
            'fieldname' => Schema::TYPE_STRING.'(50)',
            'field_type' => Schema::TYPE_STRING.'(50)',
            'field_size' => Schema::TYPE_SMALLINT,
            'required' => Schema::TYPE_SMALLINT.'(1)',
            'sort_order' => Schema::TYPE_SMALLINT,
            'visible' => Schema::TYPE_SMALLINT.'(1)',
            'parent_id' => Schema::TYPE_INTEGER,
            'dynamic' => Schema::TYPE_SMALLINT.'(1)',
            'dynamic_owner' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
        $this->createTable('{{%profile_field_lang}}', [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'language' => Schema::TYPE_STRING.'(6)',
            'title' => Schema::TYPE_STRING,
            'range_list' => Schema::TYPE_TEXT,
            'error_message' => Schema::TYPE_TEXT,
            'defvalue' => Schema::TYPE_TEXT,
            'help_text' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%profile_field_lang}}');
        $this->dropTable('{{%profile_field}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
