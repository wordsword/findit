<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_174239_add_filter_column_to_profile_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile_field}}', 'filter', Schema::TYPE_SMALLINT.'(1)');
    }

    public function down()
    {
        $this->dropColumn('{{%profile_field}}', 'filter');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
