<?php

use yii\db\Schema;
use yii\db\Migration;

class m160111_012725_create_field_role_tbl extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%profile_field_roles}}', [
            'id' => Schema::TYPE_PK,
            'field_id'=> Schema::TYPE_INTEGER,
            'role'=>Schema::TYPE_STRING.'(64)',
        ], $tableOptions);

        $this->addForeignKey('profile_field_roles', '{{%profile_field_roles}}', 'field_id', '{{%profile_field}}', 'id', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('profile_field_roles', '{{%profile_field_roles}}');
        $this->dropTable('{{%profile_field_roles}}');
    }
}
