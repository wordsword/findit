<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_012126_update_form_constructor extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%profile_kit_blocks}}', 'dynamic', Schema::TYPE_SMALLINT.' DEFAULT 0');
        $this->addColumn('{{%profile_kit_blocks}}', 'dynamic_owner', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%profile_kit_blocks}}', 'dynamic');
        $this->dropColumn('{{%profile_kit_blocks}}', 'dynamic_owner');
    }

}
