<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_095213_create_form_constructor extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%profile_kit_forms}}', [
            'id' => Schema::TYPE_PK,
            'role' => Schema::TYPE_STRING.'(64)',
            'form_type' => Schema::TYPE_SMALLINT.'(1)',
        ], $tableOptions);

        $this->createTable('{{%profile_kit_forms_lang}}', [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'language' => Schema::TYPE_STRING.'(6)',
            'title' => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->createTable('{{%profile_kit_blocks}}', [
            'id' => Schema::TYPE_BIGPK,
            'form_id' => Schema::TYPE_INTEGER,
            'block_type' => Schema::TYPE_STRING.'(50)',
            'block_class' => Schema::TYPE_STRING.'(50)',
            'field_id' => Schema::TYPE_INTEGER,
            'parent_id' => Schema::TYPE_INTEGER,
            'sort_order' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->createTable('{{%profile_kit_blocks_lang}}', [
            'id' => Schema::TYPE_BIGPK,
            'owner_id' => Schema::TYPE_BIGINT,
            'language' => Schema::TYPE_STRING.'(6)',
            'text' => Schema::TYPE_TEXT,
        ], $tableOptions);
        $this->createTable('{{%profile_kit_system_fields}}', [
            'id' => Schema::TYPE_BIGPK,
            'form_id' => Schema::TYPE_INTEGER,
            'field_id' => Schema::TYPE_INTEGER,
            'value' => Schema::TYPE_TEXT,
        ], $tableOptions);

        $this->addForeignKey('profile_kit_forms_lang', '{{%profile_kit_forms_lang}}', 'owner_id', '{{%profile_kit_forms}}', 'id', 'cascade');
        $this->addForeignKey('profile_kit_blocks', '{{%profile_kit_blocks}}', 'form_id', '{{%profile_kit_forms}}', 'id', 'cascade');
        $this->addForeignKey('profile_kit_blocks_lang', '{{%profile_kit_blocks_lang}}', 'owner_id', '{{%profile_kit_blocks}}', 'id', 'cascade');
        $this->addForeignKey('profile_kit_system_fields', '{{%profile_kit_system_fields}}', 'form_id', '{{%profile_kit_forms}}', 'id', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('profile_kit_forms_lang', '{{%profile_kit_forms_lang}}');
        $this->dropForeignKey('profile_kit_blocks', '{{%profile_kit_blocks}}');
        $this->dropForeignKey('profile_kit_blocks_lang', '{{%profile_kit_blocks_lang}}');
        $this->dropForeignKey('profile_kit_system_fields', '{{%profile_kit_system_fields}}');

        $this->dropTable('{{%profile_kit_forms_lang}}');
        $this->dropTable('{{%profile_kit_forms}}');
        $this->dropTable('{{%profile_kit_blocks}}');
        $this->dropTable('{{%profile_kit_blocks_lang}}');
        $this->dropTable('{{%profile_kit_system_fields}}');
    }
}
