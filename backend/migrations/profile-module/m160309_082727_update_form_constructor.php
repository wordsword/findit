<?php

use yii\db\Schema;
use yii\db\Migration;

class m160309_082727_update_form_constructor extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile_kit_forms}}', 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_kit_forms}}', 'updated_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_kit_blocks}}', 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_kit_blocks}}', 'updated_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_kit_system_fields}}', 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_kit_system_fields}}', 'updated_at', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('{{%profile_kit_forms}}', 'created_at');
        $this->dropColumn('{{%profile_kit_forms}}', 'updated_at');
        $this->dropColumn('{{%profile_kit_blocks}}', 'created_at');
        $this->dropColumn('{{%profile_kit_blocks}}', 'updated_at');
        $this->dropColumn('{{%profile_kit_system_fields}}', 'created_at');
        $this->dropColumn('{{%profile_kit_system_fields}}', 'updated_at');
    }
}
