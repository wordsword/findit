<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_210011_profile_field_group_table_add_sort_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile_field_group}}', 'sort_order', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('{{%profile_field_group}}', 'sort_order');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
