<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_002238_profile_field_add_time_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%profile_field}}', 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn('{{%profile_field}}', 'updated_at', Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%profile_field}}', 'created_at');
        $this->dropColumn('{{%profile_field}}', 'updated_at');
    }

}
