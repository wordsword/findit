<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_203127_create_profile_field_groups_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%profile_field_group}}', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER,
            'visible' => Schema::TYPE_SMALLINT.'(1)',
            'width' => Schema::TYPE_STRING.'(15)',
        ], $tableOptions);
        
        $this->createTable('{{%profile_field_group_lang}}', [
            'id' => Schema::TYPE_PK,
            'owner_id' => Schema::TYPE_INTEGER,
            'language' => Schema::TYPE_STRING.'(6)',
            'title' => Schema::TYPE_STRING,
        ], $tableOptions);
        
        $this->addForeignKey('profile_field_group_lang', '{{%profile_field_group_lang}}', 'owner_id', '{{%profile_field_group}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('profile_field_group_lang', '{{%profile_field_group_lang}}');
        $this->dropTable('{{%profile_field_group_lang}}');
        $this->dropTable('{{%profile_field_group}}');
        
    }
}
