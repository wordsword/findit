<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use \backend\components\Nav;
use backend\assets\AppAsset;
use common\modules\user\Module;
//use yii\widgets\Breadcrumbs;
use rmrevin\yii\fontawesome\FA;
use backend\components\SkNavBar;
use common\components\SkLangHelper;
use frontend\components\Breadcrumbs;
use app\modules\main\models\Document;
use app\modules\main\models\AdminMenu;
use backend\widgets\langSwitcher\LangSwitcher;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
rmrevin\yii\fontawesome\AssetBundle::register($this);

$http_errors = json_encode([
    'Not connect. Verify Network.' => Yii::t('app', "Not connect. Verify Network."),
    'Requested page not found.' => Yii::t('app', "Requested page not found."),
    'Forbidden' => Yii::t('app', "Forbidden"),
    'Internal Server Error.' => Yii::t('app', "Internal Server Error."),
    'Bad Request.' => Yii::t('app', "Bad Request."),
    'Requested JSON parse failed.' => Yii::t('app', "Requested JSON parse failed."),
    'Time out error.' => Yii::t('app', "Time out error."),
    'Ajax request aborted.' => Yii::t('app', "Ajax request aborted.")
]);

$js = <<<JS
var http_errors = JSON.parse('$http_errors');
JS;

$this->registerJs($js, View::POS_HEAD);

$brandWrapClass = 'navbar-brand-wrap';
$contentWrapClass = (isset($this->params['contentWrap'])) ? $this->params['contentWrap'] . ' table-custom' : 'wrap table-custom';
$breadcrumbsWrapClass = 'breadcrumbs-wrap';
if (isset($_COOKIE['toggle-sidebar'])) {
    $sidebarClass = $_COOKIE['toggle-sidebar'] === 'true' ? 'open' : 'closed';
    if ($_COOKIE['toggle-sidebar'] === 'true') {
        $brandWrapClass .= ' open';
    } else {
        $contentWrapClass .= ' wide';
        $breadcrumbsWrapClass .= ' wide';
    }
} else {
    $sidebarClass = 'open';
    $brandWrapClass = 'open';
}
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="page">
    <? /* SkNavBar::begin([
        'brandLabel' => '',
        'brandWrapOptions' => [
            'class' => $brandWrapClass
        ],
        'afterBrand' => FA::icon('bars', ['id' => 'toggle-sidebar']),
        'brandUrl' => Url::toRoute(['/']),
        'options' => [
            'class' => 'navbar-fixed-top',
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid'
        ]
    ]); ?>
    <!--    --><?php //echo LangSwitcher::widget(); ?>
    <!--    --><? //= Nav::widget([
    //        'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'user-menu'],
    //        'items' => [
    //            ['label' => FA::icon('home', ['style' => 'margin-right: 5px;']) . Yii::t('app', 'Website'), 'url' => '/', 'linkOptions' => ['target' => '_blank']],
    //            ['label' => FA::icon('sign-out', ['style' => 'margin-right: 5px;']) . Yii::t('app', 'Logout'), 'url' => ['/user-management/auth/logout']]
    //        ],
    //        'encodeLabels' => false,
    //    ]); ?>
    <?php SkNavBar::end();
    $sidebarClass .= ' nano';
    echo Html::beginTag('aside', [
        'id' => 'sidebar-left',
        'class' => $sidebarClass
    ]);
    ?>
    <div class="sidebar-left-content nano-content">
        <?= Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => AdminMenu::getMenuTest(),
            'encodeLabels' => false,
        ]); ?>

    </div>
    <?= Html::endTag('aside') */ ?>

    <section class="<?= $contentWrapClass ?>">
        <? SkNavBar::begin([
            'brandLabel' => '',
            'brandWrapOptions' => [
                'class' => $brandWrapClass
            ],
//            'afterBrand' => FA::icon('bars', ['id' => 'toggle-sidebar']),
            'brandUrl' => Url::toRoute(['/']),
            'options' => [
                'class' => 'navbar-fixed-top',
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid'
            ]
        ]); ?>
        <?php echo LangSwitcher::widget(); ?>
        <?= Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'user-menu'],
            'items' => [
                ['label' => FA::icon('home', ['style' => 'margin-right: 5px; display: inline;']) . Yii::t('app', 'Website'), 'url' => '/', 'linkOptions' => ['target' => '_blank'], 'visible' => Yii::$app->settings->shop_status],
                ['label' => FA::icon('sign-out', ['style' => 'margin-right: 5px; display: inline;']) . Yii::t('app', 'Logout'), 'url' => ['/user-management/auth/logout']]
            ],
            'encodeLabels' => false,
        ]); ?>
        <?php SkNavBar::end();
        $sidebarClass .= ' nano';
        echo Html::beginTag('aside', [
            'id' => 'sidebar-left',
            'class' => $sidebarClass
        ]);
        ?>
        <div class="sidebar-left-content nano-content">
            <i id="toggle-sidebar" class="fa fa-bars"></i>
            <?= Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => AdminMenu::getMenuTest(),
                'encodeLabels' => false,
            ]); ?>

        </div>
        <?= Html::endTag('aside') ?>
        <?php if (isset($this->params['breadcrumbs'])) : ?>
            <div class="<?= $breadcrumbsWrapClass ?>">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false
                ]) ?>
            </div>
        <? endif; ?>
        <div class="container-fluid">
            <?= $content ?>
        </div>
        <footer class="main-footer"></footer>
    </section>
    <?= $this->render('@backend/modules/main/views/default/_confirmForm') ?>
    <?= $this->render('@backend/modules/main/views/default/_alertForm') ?>
    <?= $this->render('@backend/modules/main/views/default/_actionForm') ?>
    <?= $this->render('@backend/modules/main/views/default/_calculator') ?>
    <?= $this->render('@backend/modules/main/views/default/_detailForm') ?>
</div>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
