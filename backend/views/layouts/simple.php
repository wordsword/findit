<?php
use backend\assets\AppAsset;
use common\modules\user\Module;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use backend\components\SkNavBar;
use yii\widgets\Breadcrumbs;
use backend\widgets\langSwitcher\LangSwitcher;
use common\components\SkLangHelper;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FA;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

//if (Yii::$app->user->isGuest) {
//    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//} else {
//    $menuItems[] = [
//        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
//        'url' => ['/site/logout'],
//        'linkOptions' => ['data-method' => 'post']
//    ];
//}
$brandWrapClass = 'navbar-brand-wrap';
$contentWrapClass = 'wrap';
$breadcrumbsWrapClass = 'breadcrumbs-wrap';
if (isset($_COOKIE['toggle-sidebar'])) {
    $sidebarClass = $_COOKIE['toggle-sidebar'] === 'true' ? 'open' : 'closed';
    if ($_COOKIE['toggle-sidebar'] === 'true') {
        $brandWrapClass .= ' open';
    } else {
        $contentWrapClass .= ' wide';
        $breadcrumbsWrapClass .= ' wide';
    }
} else {
    $sidebarClass = 'open';
    $brandWrapClass = 'open';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <div id="page">
        <?php $this->beginBody() ?>
        <section class="<?= $contentWrapClass ?>">
            <div class="container-fluid">
            <?= $content ?>
            </div>
        </section>
        <?= $this->render('@backend/modules/main/views/default/_confirmForm')?>
        <?= $this->render('@backend/modules/main/views/default/_alertForm')?>
        <?= $this->render('@backend/modules/main/views/default/_actionForm')?>
        <?php $this->endBody() ?>
    </div>
</body>
</html>
<?php $this->endPage() ?>
