<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Find-it',
    'basePath' => dirname(__DIR__),
    // 'bootstrap' => ['log', 'assetsAutoCompress'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'main/default/index',
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'user-management' => [
            'class' => 'common\modules\user\Module',
            'confirmationTokenExpire' => 86400,
            // Here you can set your handler to change layout for any controller or action
            // Tip: you can use this event in any module
            'on beforeAction' => function (yii\base\ActionEvent $event) {
//                    if ( $event->action->uniqueId == 'user-management/auth/login' )
//                    {
//                        $event->action->controller->layout = 'loginLayout.php';
//                    };
            },
        ],
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
        // 'uagent' => [
        //     'class' => 'common\modules\uagent\Module',
        // ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            // Кеширования схемы
            'enableSchemaCache' => true,

            // Продолжительность кеширования схемы.
            'schemaCacheDuration' => 3600,

            // Название компонента кеша, используемого для хранения информации о схеме
            'schemaCache' => 'cache',
        ],

        'assetsAutoCompress' => [
            'class' => 'app\components\SkAssetsAutoCompress',
            'enabled' => false,

            'readFileTimeout' => 3,           //Time in seconds for reading each asset file

            'jsCompress' => true,        //Enable minification js in html code
            'jsCompressFlaggedComments' => true,        //Cut comments during processing js

            'cssCompress' => true,        //Enable minification css in html code

            'cssFileCompile' => true,        //Turning association css files
            'cssFileRemouteCompile' => false,       //Trying to get css files to which the specified path as the remote file, skchat him to her.
            'cssFileCompress' => true,        //Enable compression and processsing before being stored in the css file
            'cssFileBottom' => false,       //Moving down the page css files
            'cssFileBottomLoadOnJs' => false,       //Transfer css file down the page and uploading them using js

            'jsFileCompile' => false,        //Turning association js files
            'jsFileRemouteCompile' => false,       //Trying to get a js files to which the specified path as the remote file, skchat him to her.
            'jsFileCompress' => true,        //Enable compression and processing js before saving a file
            'jsFileCompressFlaggedComments' => true,        //Cut comments during processing js

            'noIncludeJsFilesOnPjax' => true,        //Do not connect the js files when all pjax requests

            // 'htmlFormatter' => [
            //     'class' => 'skeeks\yii2\assetsAuto\formatters\html\MrclayHtmlCompressor',
            // ],
        ],
        'urlManager' => [
            'class' => 'common\components\SkUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'main/default/index',
                '/login' => 'user-management/auth/login',
                '/sign-up' => 'user-management/auth/registration',

                '/main/<controller>/<action>' => '/main/<controller>/<action>',

                '<_a:(login|logout|captcha)>' => 'user-management/auth/<_a>',
                '<_a:(confirm-registration-phone|confirm-phone|password-recovery)>' => 'user-management/auth/<_a>',
                '<_a:(profile|change-own-password|profile-confirm-phone|confirm-email)>' => 'user-management/auth/<_a>',
                '<_a:(confirm-registration-email|password-recovery-receive|confirm-email-receive)>/<token:[\w\-]+>' => 'user-management/auth/<_a>',


                '<_a:error>' => 'main/default/<_a>',


                /*
                 * uAgent API methods BEGIN
                 */
                // [
                //     'class' => 'yii\rest\UrlRule',
                //     'controller' => 'uagent/product',
                //     'pluralize' => false,
                //     'extraPatterns' => [
                //         'DELETE' => 'delete'
                //     ]
                // ],
                // '<path:.*>/page=<page:\d+>' => 'main/router/index',
                // '<path:.*>' => 'main/router/index',
            ]
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
//            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'class' => 'common\modules\user\components\UserConfig',
            'identityClass' => 'common\modules\user\models\User'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'settings' => [
            'class' => 'common\components\Settings',
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\YiiAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => '@web',
                    'js' => ['js/yii.min.js']
                ],
                'yii\widgets\PjaxAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => '@web',
                    'js' => [YII_ENV_TEST ? 'js/jquery.pjax.js' : 'js/jquery.pjax.min.js']
                ],
                // 'yii\bootstrap\BootstrapAsset' => [
                //     'css' => [
                //         YII_ENV_TEST ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                //     ]
                // ],
                // 'yii\bootstrap\BootstrapPluginAsset' => [
                //     'js' => [
                //         YII_ENV_TEST ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                //     ]
                // ],
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_TEST ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\jui\JuiAsset' => [
                    'css' => [
                        YII_ENV_TEST ? 'themes/smoothness/jquery-ui.css' : 'themes/smoothness/jquery-ui.min.css',
                    ],
                    'js' => [
                        YII_ENV_TEST ? 'js/jquery-ui.js' : 'jquery-ui.min.js',
                    ],
                ],
                'bigpaulie\social\share\ShareAsset' => [
                    'depends' => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapPluginAsset'
                    ],
                ],
                'kartik\datetime\DateTimePickerAsset' => [
                    'css' => [
                        YII_ENV_TEST ? 'css/bootstrap-datetimepicker.css' : 'css/bootstrap-datetimepicker.min.css',
                        YII_ENV_TEST ? 'css/datetimepicker-kv.css' : 'css/datetimepicker-kv.min.css',
                    ],
                    'js' => [
                        YII_ENV_TEST ? 'js/bootstrap-datetimepicker.js' : 'js/bootstrap-datetimepicker.min.js',
                    ],
                    'depends' => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapPluginAsset'
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
