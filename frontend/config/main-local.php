<?php

$config = [
    'components' => [
        'request' => [
            'class' => 'common\components\SkRequest',
            'cookieValidationKey' => 'Ux5HzimrmscfCmcnqc8JELuw1yEmQdhJ',
            'baseUrl' => '',
        ],
        'assetManager' => [
            'linkAssets' => false,
        ],
    ],
];

// if (!YII_ENV_TEST) {
// configuration adjustments for 'dev' environment
$config['bootstrap'][] = 'debug';
$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '::1', '78.152.170.144']
];

$config['bootstrap'][] = 'gii';
$config['modules']['gii'] = 'yii\gii\Module';
// }

return $config;
