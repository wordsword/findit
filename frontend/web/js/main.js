$(document).ready(function () {


    $(document).on('click', '.fancy-link', function (e) {
        e.preventDefault();
        if ($('#pjax-container').length > 0) {
            return true;
        }

        var link = $(this),
            container = $('<div id="pjax-container" class="body-container hidden auth-form-wrap"/>');

        $('body').remove('#pjax-container');
        $('body').append(container);
        $.pjax({
            target: link,
            container: '#pjax-container',
            url: link.attr('href'),
            scrollTo: false,
            push: false,
            timeout: false
        });
    });


    $(document).on('pjax:success', function (event, data, status, xhr, options) {

        if ($(options.target).hasClass('fancy-link')) {
            var content = $('#pjax-container');
            var container = $('#pjax-container');
            $.fancybox.open({
                src: container.removeClass('hidden body-container'),
                type: 'html',
                touch: false,
                smallBtn: false,
                buttons: false,
                afterClose: function () {
                    container.remove();
                }
            });
        }

    });


    $(document).on("click", '.ui-common-form a', function (e) {
        var link = $(this);
        if (link.closest('#pjax-container').length) {
            e.preventDefault();
            $.pjax({
                target: link,
                container: '#pjax-container',
                url: link.attr('href'),
                scrollTo: false,
                push: false,
            });
        }
    });



    $(document).on('submit', '#registration-form', function (e) {
        if ($(this).closest('.popup-block').length || $(this).closest('.fancybox-container').length) {
            $.pjax.submit(e, '#pjax-container', {
                scrollTo: false,
                push: false,
                timeout: 50000,
            });
        }
    });

    // $(document).on('click', '.link_sign_in', function (e) {
    //     e.preventDefault();
    //     modal = $('.login-form').clone();
    //     modal.css('display', 'inline-block');

    //     modal.on('click', '.confirm-btn', function (e) {
    //         e.preventDefault();

    //         email = modal.find('.email')
    //         pass = modal.find('.pass')

    //         if (email.val().length == 0) {
    //             email.css('border', '1px solid red')
    //             email.on('focus', function () {
    //                 $(this).css('border', 'none')
    //             })
    //         } else if (pass.val().length == 0) {
    //             pass.css('border', '1px solid red')
    //             pass.on('focus', function () {
    //                 $(this).css('border', 'none')
    //             })
    //         } else {
    //             $.fancybox.close();
    //         }
    //     })

    //     $.fancybox.open(modal);
    // })
    // $(document).on('click', '.link_sign_up', function (e) {
    //     e.preventDefault();
    //     modal = $('.register-form').clone();
    //     modal.css('display', 'inline-block');


    //     modal.on('click', '.confirm-btn', function (e) {
    //         e.preventDefault();

    //         email = modal.find('.email')
    //         pass = modal.find('.pass')
    //         re_pass = modal.find('.re_pass')

    //         if (email.val().length == 0) {
    //             email.css('border', '1px solid red')
    //             email.on('focus', function () {
    //                 $(this).css('border', 'none')
    //             })
    //         } else if (pass.val().length < 8) {
    //             pass.css('border', '1px solid red')
    //             pass.parent().find('.error-block').text("Довжина паролю повинна бути не менше 8 символів")
    //             pass.on('focus', function () {
    //                 $(this).css('border', 'none')
    //                 $(this).parent().find('.error-block').empty()
    //             })
    //         } else if (re_pass.val().length == 0) {
    //             re_pass.css('border', '1px solid red')
    //             re_pass.on('focus', function () {
    //                 $(this).css('border', 'none')
    //             })
    //         } else if (re_pass.val().length == 0 || re_pass.val() != pass.val()) {
    //             re_pass.css('border', '1px solid red')
    //             re_pass.parent().find('.error-block').text("Паролі повинні співпадати")
    //             re_pass.on('focus', function () {
    //                 $(this).css('border', 'none')
    //                 $(this).parent().find('.error-block').empty()
    //             })
    //         } else {
    //             $.fancybox.close();
    //         }
    //     })

    //     $.fancybox.open(modal);
    // })


    $(document).on('click', '.set_like', function (e) {
        (new mojs.Burst({
            count: 20,
            left: $(this).offset().left + 10,
            top: $(this).offset().top + 10,
            children: {
                shape: ['circle', 'polygon', 'rect'],
                fill: ['#6886c5', '#ffe0ac', '#ffacb7'],
                degreeShift: 'rand(-360, 360)',
                delay: 'stagger(0, 30)',
            },
            duration: 400
        })).replay();

        console.log($(this).offset().left);
        console.log($(this).offset().top);

        this.classList.toggle('svg-filled');

        counter = $(this).parent().find('.num-post');

        count = parseInt(counter.text());

        if ($(this).hasClass('svg-filled'))
            count++;
        else
            count--;

        counter.text(count)
    })

    
})



function checkCurr(input) {
    if (window.event) {
        if (event.keyCode == 37 || event.keyCode == 39) return;
    }
    input.value = input.value.replace(/[^\d]/g, '');
};

function myFunctionUser() {
    document.getElementById("myDropdownUser").classList.toggle("show-user");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn-user')) {

        var dropdowns = document.getElementsByClassName("dropdown-content-user");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show-user')) {
                openDropdown.classList.contains('show-user');
            }
        }
    }
};


$(function() {
    var availableTags = [
        "Claire",
        "Jackson",
        "Michael Hodges",
        "Starbight",
    ];
    $( ".tags" ).autocomplete({
        source: availableTags
    });
} );