<?php

namespace app\widgets\slider;

use yii\web\AssetBundle;

class SliderAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/slider/assets';
    public $css = [
        'style.css',
    ];
    public $js = [
        'jquery.transit.min.js',
        'skslider.js',
    ];
    public $depends = [
        'yii\jui\JuiAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}