<?php

namespace app\widgets\slider;

use yii\helpers\Html;

class Slider extends \yii\bootstrap\Widget
{
    public $slides = [];
    public $animationLength = 5000;
    public $delay = 1000;
    public $js;


    public function init(){}

    public function run() {
        if (!is_array($this->slides)) {
            return;
        }
        $this->js = "$('#slideshow').skslider({"
                . "slideDelay: ".$this->animationLength.","
                . "effectDuration: ".$this->delay."});";
        return $this->render('slider', [
            'slides' => $this->slides,
        ]);
    }
}