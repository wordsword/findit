(function($, window, undefined){
    
    var Slider = $.skslider = function () {
            Slider.init.apply(this,arguments);
    };
    
    $.extend(Slider, {
        defaults: {
            slideDelay     : 6000,
            effectDuration : 1000,
            autoPlay       : true,
            titleTag       : 'h2',
            descriptionTag : 'p'
        },
        current      : null,
        timer        : null,
        titleTimer   : null,
        wrap         : null,
        descrWrap    : null,
        controls     : null,
        controlsWrap : null,
        bullets      : null,
        slides       : [],
        btns : {
            play: null,
            pause: null,
            prev: null,
            next: null,
        },
        options   : {},
        init: function(slides, options) {
            Slider.options = $.extend(true, {}, Slider.defaults, options);
            Slider.wrap = $('<div class="skslider-wrap"></div>').prependTo('body');
            Slider.wrap.append(slides);
            slides.addClass('skslider-slides');
            Slider.controlsWrap = $('<div class="skslider-controls-wrap"></div>').appendTo(Slider.wrap);
            Slider.controls = $('<div class="skslider-controls"></div>').appendTo(Slider.controlsWrap);
            Slider.btns.prev = $('<span><i class="prev icon-left-open"></i></span>').appendTo(Slider.controls).click(Slider.prev);
            Slider.btns.play = $('<span><i class="play icon-play"></i></span>').appendTo(Slider.controls);
            Slider.btns.pause = $('<span><i class="pause icon-pause"></i></span>').appendTo(Slider.controls).click(Slider.pause);
            Slider.btns.next = $('<span><i class="next icon-right-open"></i></span>').appendTo(Slider.controls).click(Slider.next);
            Slider.btns.play.click(function(){
                Slider.play();
                Slider.btns.play.hide();
                Slider.btns.pause.show();
            });
            Slider.btns.pause.click(function(){
                Slider.pause();
                Slider.btns.play.show();
                Slider.btns.pause.hide();
            });
            if (Slider.options.autoPlay) {
                Slider.btns.play.hide();
                Slider.btns.pause.show();
            } else {
                Slider.btns.play.show();
                Slider.btns.pause.hide();
            }
            Slider.descrWrap = $('<div class="skslider-description-wrap"></div>').appendTo(Slider.wrap);
            Slider.bullets = $('<ul class="skslider-bullets"></div>').appendTo(Slider.wrap);
            $.each(slides.children(),function(i, element){
                var title,
                    titleColor,
                    titleBg,
                    description,
                    descrColor,
                    descrBg,
                    wrapSelector,
                    titleSelector,
                    descrSelector,
                    bullet = [];
                title       = $(this).attr('data-title');
                titleColor  = $(this).attr('data-title-color');
                titleBg     = $(this).attr('data-title-bg');
                description = $(this).attr('data-description');
                descrColor  = $(this).attr('data-description-color');
                descrBg     = $(this).attr('data-description-bg');
                bullet[i] = $('<li class="bullet icon-circle"><span><img src="'+ $(this).attr('data-img') +'" /></span></li>').appendTo(Slider.bullets);
                bullet[i].click(function(){
                    $(this).addClass('active').siblings('li').removeClass('active');
                    Slider.jumpto(i);
                });
                $(this).removeAttr('data-img')
                       .removeAttr('data-title')
                       .removeAttr('data-title-color')
                       .removeAttr('data-title-bg')
                       .removeAttr('data-description')
                       .removeAttr('data-description-color')
                       .removeAttr('data-description-bg');
                wrapSelector = $('<div class="skslider-description-item"></div>').appendTo(Slider.descrWrap);
                titleSelector = $('<' + Slider.options.titleTag + '>' + title + '</' + Slider.options.titleTag + '>').appendTo(wrapSelector);
                titleSelector.addClass('skslider-title');
                if (titleColor !== undefined && titleColor.length > 0) {
                    titleSelector.css('color',titleColor);
                }
                if (titleBg !== undefined && titleBg.length > 0) {
                    titleSelector.css('background-color',titleBg);
                }
                descrSelector = $('<' + Slider.options.descriptionTag + '>' + description + '</' + Slider.options.descriptionTag + '>').appendTo(wrapSelector);
                descrSelector.addClass('skslider-description');
                if (descrColor !== undefined && descrColor.length > 0) {
                    descrSelector.css('color',descrColor);
                }
                if (descrBg !== undefined && descrBg.length > 0) {
                    descrSelector.css('background-color',descrBg);
                }
                Slider.slides[i] = {
                    slide       : $(this),
                    title       : titleSelector,
                    description : descrSelector
                };
            });
            Slider.current = -1;
            Slider.next();
        },
        play: function() {
            Slider.options.autoPlay = true;
            Slider.timer = setTimeout(Slider.next, Slider.options.slideDelay);
        },
        pause: function() {
            clearTimeout(Slider.timer);
            Slider.options.autoPlay = false;
        },
        next: function() {
            Slider.jumpto(Slider.current+1);
        },
        prev: function() {
            Slider.jumpto(Slider.current-1);
        },
        jumpto: function(index) {
            var next = index,
                current,
                scale = [['1','1.1'],['1.1','1'],['1.1','1'],['1','1.1'],['1.1','1']],
                transformOriginList = ['100% 0','0 0','100% 100%', '0 100%'];
            clearTimeout(Slider.timer);
            clearTimeout(Slider.titleTimer);
            if (next < 0) {
                next = Slider.slides.length - 1;
            }
            if (next > (Slider.slides.length - 1)) {
                next = 0;
            }
            current = Slider.current;
            if (current < 0) {
                current = Slider.slides.length - 1;
            }
            var curScale = scale[getRandomInt(0,4)];
            var curTransform = transformOriginList[getRandomInt(0,3)];
            Slider.slides[current].title.css({x: '0px'});
            Slider.slides[current].description.css({x: '0px'});
            Slider.slides[next].slide.css({scale: curScale[0]});
            Slider.slides[next].title.css({x: '-3000px',opacity: 1});
            Slider.slides[next].description.css({x: '3000px',opacity: 1});
            
            Slider.slides[current].slide.animate({opacity: 0},Slider.options.effectDuration); 
            Slider.slides[next].slide.animate({opacity: 1},Slider.options.effectDuration);
            Slider.slides[next].slide.css({transformOrigin: curTransform}).transition({scale: curScale[1]}, {
                duration: Slider.options.slideDelay+2*Slider.options.effectDuration, 
                easing: 'linear',
                queue: false
            });
            Slider.slides[current].title.transition({x: '3000px'}, 1500, 'linear');
            Slider.slides[current].description.transition({x: '-3000px'}, 1500, 'linear');
            Slider.titleTimer = setTimeout(function(){
                Slider.slides[next].title.transition({x: '0px'}, 1000,'cubic-bezier(0.215, 0.610, 0.355, 1.000)');
                Slider.slides[next].description.transition({x: '0px'}, 1000, 'cubic-bezier(0.215, 0.610, 0.355, 1.000)');
            }, 500);
            Slider.current = next;
            $('.skslider-bullets .bullet').removeClass('active');
            $('.skslider-bullets .bullet:nth-child('+(next+1)+')').addClass('active');
            if (Slider.options.autoPlay) {
                Slider.play();
            }
        }
    });
    
    $.fn.skslider = function(options) {
        Slider.init(this,options);
        return this;
    }
})(jQuery, window);

function getRandomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


