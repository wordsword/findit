<?php  

use yii\helpers\Html;
use app\widgets\slider\SliderAsset;
SliderAsset::register($this);
$this->registerJs($this->context->js, yii\web\View::POS_READY, __CLASS__.'slider');

echo Html::beginTag('ul',['id'=>'slideshow','class'=>'skslider-slides']);
    foreach ($slides as $key => $slide) : 
        echo Html::tag('li',false,[
            'style'=>'background: url('.Html::encode($slide->image).');',
            'data-img'=>Html::encode($slide->image),
            'data-title'=>Html::encode($slide->title),
            'data-title-color'=>Html::encode($slide->title_color),
            'data-title-bg'=>Html::encode($slide->title_bg),
            'data-description'=>Html::encode($slide->description),
            'data-description-color'=>Html::encode($slide->descr_color),
            'data-description-bg'=>Html::encode($slide->descr_bg),
        ]);
    endforeach;
echo Html::endTag('ul');
//echo Html::endTag('div');
