<?php

use yii\bootstrap\Nav;

echo Nav::widget([
    'options' => ['class' => '','id'=>'lang-switcher'],
    'items' => $links,
    'encodeLabels'=>false
]);

