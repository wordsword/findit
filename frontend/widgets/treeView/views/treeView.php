<?php

use yii\helpers\Html;


echo Html::beginTag('div',['class'=>$containerOptions]);
echo $data;
echo Html::endTag('div');