//$(document).ready(function(){
//    $(document).on('click','.treeview .glyphicon-folder-close, .treeview .glyphicon-folder-open', function(e){
//        $(this).parent().find('.dropdown:first').slideToggle('fast');
//        $(this).toggleClass('glyphicon-folder-close').toggleClass('glyphicon-folder-open');
//    });
//    $(document).on('click','.treeview a', function(e){
//        e.preventDefault();
//        $(this).find('span').toggleClass('glyphicon-unchecked').toggleClass('glyphicon-check');
//    });
//});

(function($, window, undefined){
    
    var TreeView = $.skTreeView = function () {
            TreeView.init.apply(this,arguments);
    };
    
    $.extend(TreeView, {
        defaults: {
            
            expandedClass: 'mjs-nestedSortable-expanded',
            collapsedClass: 'mjs-nestedSortable-collapsed',
            leafClass: "mjs-nestedSortable-leaf",
            branchClass: "mjs-nestedSortable-branch",
            checkedIcon: 'glyphicon-check',
            uncheckedIcon: 'glyphicon-unchecked',
            checkboxType: 'checkbox',
            checkedRadioIcon: 'glyphicon-record',
            uncheckedRadioIcon: 'glyphicon-remove-circle',
            disableSorting: false,
            startCollapsed: true,
            
            // Event handlers
            onNodeChecked: undefined,
            onNodeUnchecked: undefined,
            onNodeCollapsed: undefined,
            onNodeExpanded: undefined,
            openSelectedBranch: false,
            nestedOptions: {}
        },
        node      : null,
        options   : {},
        data      : null,
        init: function($data, options) {
            TreeView.data = $data;
            TreeView.options = $.extend(true, {}, TreeView.defaults, options);
            if (typeof (TreeView.options.onNodeChecked) === 'function') {
                $data.on('nodeChecked', $.debounce(500, TreeView.options.onNodeChecked));
            }
            if (typeof (TreeView.options.onNodeUnchecked) === 'function') {
                $data.on('nodeUnchecked', $.debounce(500, TreeView.options.onNodeUnchecked));
            }
            if (typeof (TreeView.options.onNodeCollapsed) === 'function') {
                $data.on('nodeCollapsed', $.debounce(500, TreeView.options.onNodeCollapsed));
            }
            if (typeof (TreeView.options.onNodeExpanded) === 'function') {
                $data.on('nodeExpanded', $.debounce(500, TreeView.options.onNodeExpanded));
            }
            $.each($data.find('li'),function(i, element){
                $(this).find('a:first').on('click',function(e) {
                    TreeView.node = $(this).parent().parent();
                    if (TreeView.options.checkboxType == 'checkbox') {
                        TreeView.toggleCheck(e);
                    }
                    if (TreeView.options.checkboxType == 'radio') {
                        TreeView.toggleRadio(e);
                    }
                });
                $(this).find('.extrigger:first').on('click',function(e) {
                    TreeView.node = $(this).parent().parent();
                    TreeView.toggleCollapse(e);
                });

                var $li = $(this),
                    hasCollapsedClass = $li.hasClass(TreeView.options.collapsedClass),
                    hasExpandedClass = $li.hasClass(TreeView.options.expandedClass);

                if ($li.children('ol').length) {
                    $li.addClass(TreeView.options.branchClass);
                    // expand/collapse class only if they have children

                    if ( !hasCollapsedClass && !hasExpandedClass ) {
                        if (TreeView.options.startCollapsed) {
                            if (TreeView.options.openSelectedBranch) {
                               if ($li.find('li.checked').length) {
                                   $li.addClass(TreeView.options.expandedClass);
                               } else {
                                   $li.addClass(TreeView.options.collapsedClass);
                               }
                            } else {
                                $li.addClass(TreeView.options.collapsedClass);
                            }
                        } else {
                            $li.addClass(TreeView.options.expandedClass);
                        }
                    }
                } else {
                    $li.addClass(TreeView.options.leafClass);
                }
            });
            
        },
        toggleCheck: function(e) {
            e.preventDefault();
            var recID = [];
            recID[0] = TreeView.node.attr('recID');
            $.each(TreeView.node.find('li'),function(i, element){
                recID[i+1] = $(this).attr('recID');
            });
            TreeView.node.find('.chtrigger:first')
                    .toggleClass(TreeView.options.uncheckedIcon)
                    .toggleClass('checked')
                    .toggleClass(TreeView.options.checkedIcon);
            TreeView.node.find('input:first').toggleClass('checked');
            if (!TreeView.node.find('input:first').hasClass('checked')) {
                TreeView.node.find('.chtrigger:not(:first)')
                    .removeClass('checked')
                    .removeClass(TreeView.options.checkedIcon)
                    .addClass(TreeView.options.uncheckedIcon);
                TreeView.node.find('input:not(:first)').removeClass('checked');
                TreeView.data.trigger('nodeUnchecked',[recID]);
            } else {
                TreeView.node.find('.chtrigger:not(:first)')
                    .removeClass(TreeView.options.uncheckedIcon)
                    .addClass(TreeView.options.checkedIcon)
                    .addClass('checked');
                TreeView.node.find('input:not(:first)').addClass('checked');
                TreeView.data.trigger('nodeChecked',[recID]);
            }
        },
        toggleRadio: function(e) {
            e.preventDefault();
            var recID = [];
            recID[0] = TreeView.node.attr('recID');
            TreeView.data.find('.chtrigger')
                    .removeClass(TreeView.options.checkedRadioIcon)
                    .addClass(TreeView.options.uncheckedRadioIcon);
            TreeView.data.find('.checked').removeClass('checked');
            TreeView.node.find('.chtrigger:first')
                    .addClass('checked')
                    .removeClass(TreeView.options.uncheckedRadioIcon)
                    .addClass(TreeView.options.checkedRadioIcon);
            TreeView.node.find('input:first').addClass('checked');
            TreeView.node.addClass('checked');
            TreeView.data.trigger('nodeChecked',[recID, TreeView.node]);
        },
        toggleCollapse: function(e) {
            e.preventDefault();
            TreeView.node.toggleClass(TreeView.options.collapsedClass).toggleClass(TreeView.options.expandedClass);
        },
        toArray: function(e) {
            return TreeView._toArrayRecursive(TreeView.data.find('li'));
        },
        _toArrayRecursive: function(items) {
            var list = [];
            var parentID;
            items.each(function(i,elem){
                if ($(this).parents('li').length > 0) {
                    parentID = Number($(this).parents('li').attr('recid'));
                } else {
                    parentID = 0;
                }
                list[i] = {recid: Number($(this).attr('recid')), parent:parentID}
            });
            return list;
        },
        refresh: function() {
            TreeView.init(this,TreeView.options);
            TreeView._refreshSorting();
        },
        _refreshSorting: function() {
            jQuery('.treeview-sortable').nestedSortable(TreeView.options.nestedOptions);
            jQuery('.treeview-sortable').nestedSortable('disable');
        }
    });
    
    $.fn.skTreeView = function(options) {
        if (typeof options === 'object' || !options) {
            TreeView.init(this,options);
            return this;
        } else {
            TreeView[options].apply(this,Array.prototype.slice.call(arguments, 1));
            return this;
        }
    };
})(jQuery, window);

