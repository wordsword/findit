<?php

namespace app\widgets\treeView;

use yii\helpers\Html;
use Yii;
use rmrevin\yii\fontawesome\FA;

class TreeView extends \yii\bootstrap\Widget
{
    public $data;
    public $header;
    public $headerOptions = [];
    public $containerOptions = [];
    public $options = [];

    private function _getDataRecursive($data, $options = [], $includeAll = false)
    {

        $params = Yii::$app->request->get('Filter');
        $tree = Html::beginTag('ul', $options);
        if ($includeAll) {
            $tree .= Html::beginTag('li', [
                'class' => (!isset($params['parent_id']) || $params['parent_id'] == 0) ? 'active' : 0
            ]);
            $tree .= Html::beginTag('a', [
                'data-id' => NULL,
                'class' => 'category-item',
                'data-pjax' => 0,
                'href' => '/catalogue'
            ]);
            $tree .= Html::tag('span', mb_strtoupper(Yii::t('app', 'All products')), ['class' => 'link-text']);
//                $tree .= Html::tag('span', $item['itemCount'], ['class' => 'category-item-quantity']);

            $tree .= Html::hiddenInput('Filter[parent_id]', 0, ['class' => 'filter-input']);
            $tree .= Html::endTag('a');
            if ($item['childs']) {
                $tree .= $this->_getDataRecursive($item['childs'], ['class' => 'dropdown-menu']);
            }
            $tree .= Html::endTag('li');
        }
        foreach ($data as $key => $item) {
            $itemOptions = is_array($item['options']) ? $item['options'] : [];
            $childs = false;
            $linkOptions = [
                'class' => 'category-item',
                'data-id' => $item['recID'],
                'data-pjax' => 0,
                'href' => $item['url']
            ];
            if ($item['open']) {
                $itemOptions['class'] .= ' active';
            }

            if (isset($params['parent_id']) && $params['parent_id'] == $item['recID']) {
                $itemOptions['class'] .= ' active';
            }
            if ($item['childs']) {
                $childs = true;
                $itemOptions['class'] .= ' dropdown';
                $linkOptions =
                    [
                        'data-id' => $item['recID'],
                        'class' => 'category-item dropdown-toggle',
                        'data-pjax' => 0,
                        'href' => $item['url']
                    ];
                if ($item['open']) {
                    $itemOptions['class'] .= ' open';
                }
            }
            $tree .= Html::beginTag('li', $itemOptions);
            $tree .= Html::beginTag('a', $linkOptions);
            if (!$childs) {
                $tree .= Html::tag('span', $item['text'], ['class' => 'link-text']);
//                $tree .= Html::tag('span', $item['itemCount'], ['class' => 'category-item-quantity']);
            } else {
                $tree .= Html::tag('span', FA::icon('chevron-right') . ' ' . $item['text'], ['class' => 'link-text']);
            }
            $tree .= Html::hiddenInput('Filter[parent_id]', $item['recID'], ['class' => 'filter-input']);
            $tree .= Html::endTag('a');
            if ($item['childs']) {
                $tree .= $this->_getDataRecursive($item['childs'], ['class' => 'dropdown-menu']);
            }
            $tree .= Html::endTag('li');
        }
        $tree .= Html::endTag('ul');
        return $tree;
    }

    public function run()
    {
        // echo '<pre>';
        
        // print_r($this->data);
        // echo '</pre>';
        return $this->render('treeView', [
            'data' => $this->_getDataRecursive($this->data, $this->options, true),
            'header' => $this->header,
            'headerOptions' => $this->headerOptions,
            'containerOptions' => $this->containerOptions,
        ]);
    }
}
