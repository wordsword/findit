<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/animate.min.css',
        'input-master/css/intlTelInput.min.css',
        'materialize/icons.css',
        // 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        'css/main.css',
        'css/auth.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\widgets\PjaxAsset',
        // 'yii\bootstrap\BootstrapPluginAsset',
        'frontend\components\FancyBoxAsset',
        'frontend\assets\InputmaskPhoneAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
    public $js = [
        // 'js/jquery.touchSwipe.min.js',
        'input-master/js/intlTelInput-jquery.min.js',
        'input-master/js/utils.min.js',
        // 'js/lazyload.min.js',
        'js/jquery.cookie.js',
        // 'js/jquery.ba-throttle-debounce.min.js',
        // 'js/jquery.bootstrap-growl.min.js',
        // 'js/messages.js',
        // 'materialize/materialize.min.js',
        // 'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
        'js/mo.min.js',
        'js/main.js',
    ];
}
