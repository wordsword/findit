<?php


$this->title = "Find-it";
?>


<div class="stetch">
    <section class="left-bar left_sidebar">
        <div class="left_sidebar_content">
            <div class="main main_home">
                <div class="main-page">
                    <a href="#"><img class="top" src="/images/main_img/home-outline.svg">Домой</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/flame-outline.svg">Популярное</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/shuffle-outline.svg">Случайное</a>
                </div>
            </div>

            <div class="main main_mark">
                <div class="main-page">
                    <a href="#"><img class="top" src="/images/main_img/heart-outline.svg">Мои лайки</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Мои теги</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/bookmarks-hand-drawn-outline-svgrepo-com.svg">Закладки</a>
                </div>
            </div>

            <div class="main main_hashtag">
                <div class="main-page">
                    <a href="#"><img class="top" src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Знакомства
                    </a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Отношения</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Помощь</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Учёба</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Экстрим</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Заведения</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Природа</a>
                </div>
                <div class="main-page">
                    <a href="#"><img src="/images/main_img/hashtag-svgrepo-com%2012.43.33.svg">Работа</a>
                </div>
            </div>
        </div>
    </section>


    <section class="navigation">
        <nav class="nav_menu">
            <!--<div class="dropdown-every">-->
            <!--<a class="every nav_link dropbtn-every" onclick="myFunctionEvery()" href="#">Везде-->
            <!--<img src="/images/main_img/chevron-down-outline.svg"-->
            <!--style="width: 10px"></a>-->
            <!--<div id="myDropdownEvery" class="dropdown-content-every">-->
            <!--<a href="#">Киев</a>-->
            <!--<a href="#">Львов</a>-->
            <!--</div>-->
            <!--</div>-->
            <a class="every nav_link" href="#">Везде<img src="/images/main_img/chevron-down-outline.svg" style="width: 10px"></a>
            <a class="nav_link" href="#">Новое<img src="/images/main_img/chevron-down-outline.svg" style="width: 10px"></a>
            <a class="nav_link" href="#">Теги</a>
            <a class="create" href="#">Создать</a>
            <!--<a href="#">-->
            <!--<svg class="svg-icon" viewBox="0 0 20 20">-->
            <!--<path fill="none" d="M16.803,18.615h-4.535c-1,0-1.814-0.812-1.814-1.812v-4.535c0-1.002,0.814-1.814,-->
            <!--1.814-1.814h4.535c1.001,0,1.813,0.812,1.813,1.814v4.535C18.616,17.803,17.804,18.615,16.803,-->
            <!--18.615zM17.71,12.268c0-0.502-0.405-0.906-0.907-0.906h-4.535c-0.501,0-0.906,0.404-0.906,-->
            <!--0.906v4.535c0,0.502,0.405,0.906,0.906,0.906h4.535c0.502,0,0.907-0.404,0.907-0.906V12.268z M16.803,-->
            <!--9.546h-4.535c-1,0-1.814-0.812-1.814-1.814V3.198c0-1.002,0.814-1.814,1.814-1.814h4.535c1.001,0,1.813,-->
            <!--0.812,1.813,1.814v4.534C18.616,8.734,17.804,9.546,16.803,9.546zM17.71,-->
            <!--3.198c0-0.501-0.405-0.907-0.907-0.907h-4.535c-0.501,0-0.906,0.406-0.906,0.907v4.534c0,0.501,0.405,-->
            <!--0.908,0.906,0.908h4.535c0.502,0,0.907-0.406,0.907-0.908V3.198z M7.733,18.615H3.198c-1.002,-->
            <!--0-1.814-0.812-1.814-1.812v-4.535c0-1.002,0.812-1.814,1.814-1.814h4.535c1.002,0,1.814,0.812,1.814,-->
            <!--1.814v4.535C9.547,17.803,8.735,18.615,7.733,18.615zM8.64,-->
            <!--12.268c0-0.502-0.406-0.906-0.907-0.906H3.198c-0.501,0-0.907,0.404-0.907,0.906v4.535c0,0.502,0.406,-->
            <!--0.906,0.907,0.906h4.535c0.501,0,0.907-0.404,0.907-0.906V12.268z M7.733,9.546H3.198c-1.002,-->
            <!--0-1.814-0.812-1.814-1.814V3.198c0-1.002,0.812-1.814,1.814-1.814h4.535c1.002,0,1.814,0.812,1.814,-->
            <!--1.814v4.534C9.547,8.734,8.735,9.546,7.733,9.546z M8.64,-->
            <!--3.198c0-0.501-0.406-0.907-0.907-0.907H3.198c-0.501,0-0.907,0.406-0.907,0.907v4.534c0,0.501,0.406,-->
            <!--0.908,0.907,0.908h4.535c0.501,0,0.907-0.406,0.907-0.908V3.198z"></path>-->
            <!--</svg>-->
            <!--</a>-->
        </nav>
        <div class="main_post">

            <div class="my_post">
                <div class="user">
                    <a href="#"><img class="user_icon-post" src="https://vignette.wikia.nocookie.net/avatar-the-last-airbender-discord/images/5/56/Other_bucky_thingy_idk_bruh_lmao_kekw.png/revision/latest/top-crop/width/360/height/450?cb=20200923035455"></a>
                    <nav class="inf_user">
                        <a class="inf name" href="#">Shade</a>
                        <a class="inf user_link" href="#">@shade89</a>
                        <a class="inf post_date" href="#">19 жов.</a>
                        <a class="nav_post" href="#"><img src="/images/main_img/ellipsis-horizontal-outline.svg" style="width: 20px; margin-top: -6px;"></a>
                    </nav>
                </div>
                <div class="inf_post">
                    <div class="post-badge">
                        <a class="item" href="#">
                            <span class="notify-badge">Помощь</span>
                        </a>
                    </div>
                    <p class="text_post">Загубив ключі в ботанічному саду. Хто знайде прошу написати мені.</p>
                    <img class="post_foto" src="https://ireland.apollo.olxcdn.com/v1/files/8q7lk41x14up3-UA/image" tabindex="0">

                </div>
                <nav class="under_post">
                    <div>
                        <svg class="set_like" fill="#ffffff" height="20" viewBox="0 0 48 48" width="20">

                            <path clip-rule="evenodd" d="M34.3 3.5C27.2 3.5 24 8.8 24 8.8s-3.2-5.3-10.3-5.3C6.4 3.5.5 9.9.5 17.8s6.1 12.4
                                  12.2 17.8c9.2 8.2 9.8 8.9 11.3 8.9s2.1-.7 11.3-8.9c6.2-5.5 12.2-10 12.2-17.8
                                  0-7.9-5.9-14.3-13.2-14.3zm-1 29.8c-5.4 4.8-8.3
                                  7.5-9.3 8.1-1-.7-4.6-3.9-9.3-8.1-5.5-4.9-11.2-9-11.2-15.6
                                  0-6.2 4.6-11.3 10.2-11.3 4.1 0 6.3 2 7.9 4.2 3.6 5.1 1.2 5.1 4.8 0 1.6-2.2 3.8-4.2
                                  7.9-4.2 5.6 0 10.2 5.1 10.2 11.3 0 6.7-5.7 10.8-11.2 15.6z" fill-rule="evenodd"></path>

                            <path clip-rule="evenodd" d="M35.3 35.6c-9.2 8.2-9.8 8.9-11.3 8.9s-2.1-.7-11.3-8.9C6.5 30.1.5 25.6.5 17.8.5 9.9
                                  6.4 3.5 13.7 3.5 20.8 3.5 24 8.8 24 8.8s3.2-5.3 10.3-5.3c7.3 0 13.2 6.4 13.2 14.3 0
                                  7.8-6.1 12.3-12.2 17.8z" fill-rule="evenodd">
                            </path>
                        </svg>
                        <span class="num-post">4</span>
                    </div>
                    <a href="#" style="text-decoration: none"><img src="/images/main_img/chatbubble-outline.svg" style="width: 20px"><span class="num-post">1</span></a>
                    <a href="#"><img src="/images/main_img/repeat-outline.svg" style="width: 20px"></a>
                    <a href="#"><img src="/images/main_img/push-outline.svg" style="width: 20px"></a>
                </nav>
            </div>



            <div class="my_post">
                <div class="user">
                    <a href="#"><img class="user_icon-post" src="/images/post_img/user-icon(1).jpg"></a>
                    <nav class="inf_user">
                        <a class="inf name" href="#">Claire</a>
                        <a class="inf user_link" href="#">@claire_1</a>
                        <a class="inf post_date" href="#">8 жов.</a>
                        <a class="nav_post" href="#"><img src="/images/main_img/ellipsis-horizontal-outline.svg" style="width: 20px; margin-top: -6px;"></a>
                    </nav>
                </div>
                <div class="inf_post">
                    <div class="post-badge">
                        <a class="item" href="#">
                            <span class="notify-badge">Помощь</span>
                        </a>
                    </div>
                    <p class="text_post">Знайшла студентський квиток. Хто загубив пишіть.</p>
                    <img class="post_foto" src="https://www.uzhnu.edu.ua/uploads/news/20200508_1362_photo_2019-12-28_11-08-40.jpg" tabindex="0">
                </div>
                <nav class="under_post">
                    <!--<a href="#"><img src="/images/main_img/heart-outline.svg" style="width: 20px"></a>-->
                    <div>
                        <svg class="set_like" fill="#ffffff" height="20" viewBox="0 0 48 48" width="20">

                            <path clip-rule="evenodd" d="M34.3 3.5C27.2 3.5 24 8.8 24 8.8s-3.2-5.3-10.3-5.3C6.4 3.5.5 9.9.5 17.8s6.1 12.4
                                  12.2 17.8c9.2 8.2 9.8 8.9 11.3 8.9s2.1-.7 11.3-8.9c6.2-5.5 12.2-10 12.2-17.8
                                  0-7.9-5.9-14.3-13.2-14.3zm-1 29.8c-5.4 4.8-8.3
                                  7.5-9.3 8.1-1-.7-4.6-3.9-9.3-8.1-5.5-4.9-11.2-9-11.2-15.6
                                  0-6.2 4.6-11.3 10.2-11.3 4.1 0 6.3 2 7.9 4.2 3.6 5.1 1.2 5.1 4.8 0 1.6-2.2 3.8-4.2
                                  7.9-4.2 5.6 0 10.2 5.1 10.2 11.3 0 6.7-5.7 10.8-11.2 15.6z" fill-rule="evenodd"></path>

                            <path clip-rule="evenodd" d="M35.3 35.6c-9.2 8.2-9.8 8.9-11.3 8.9s-2.1-.7-11.3-8.9C6.5 30.1.5 25.6.5 17.8.5 9.9
                                  6.4 3.5 13.7 3.5 20.8 3.5 24 8.8 24 8.8s3.2-5.3 10.3-5.3c7.3 0 13.2 6.4 13.2 14.3 0
                                  7.8-6.1 12.3-12.2 17.8z" fill-rule="evenodd">
                            </path>
                        </svg>
                        <span class="num-post">12</span>
                    </div>
                    <a href="#"><img src="/images/main_img/chatbubble-outline.svg" style="width: 20px"><span class="num-post">2</span></a>
                    <a href="#"><img src="/images/main_img/repeat-outline.svg" style="width: 20px"></a>
                    <a href="#"><img src="/images/main_img/push-outline.svg" style="width: 20px"></a>
                </nav>
            </div>


            <div class="my_post">
                <div class="user">
                    <a href="#"><img class="user_icon-post" src="/images/post_img/user-icon2.png"></a>
                    <nav class="inf_user">
                        <a class="inf name" href="#">Jackson</a>
                        <a class="inf user_link" href="#">@jackson_2</a>
                        <a class="inf post_date" href="#">8 жов.</a>
                        <a class="nav_post" href="#"><img src="/images/main_img/ellipsis-horizontal-outline.svg" style="width: 20px; margin-top: -6px;"></a>
                    </nav>
                </div>
                <div class="inf_post">
                    <div class="post-badge">
                        <a class="item" href="#">
                            <span class="notify-badge">Учёба</span>
                        </a>
                        <a class="item" href="#">
                            <span class="notify-badge">Работа</span>
                        </a>
                    </div>
                    <p class="text_post">📌До відома всіх
                        <br/>
                        - розвиваюча конференція двохденна пройде завтра і після завтра
                        <br/>
                        - долучитися до неї може кожен
                        <br/><br/>
                        MultiTool для кожного у світі IT
                        Готуйте улюблені снеки, напої та сідайте зручніше. Ми чекали цього цілий рік ... НІКСова мультиконференція повертається!
                        24-25 жовтня NIX MultiConf # 4 пройде у новому онлайн-форматі. Безкоштовно.
                        2 дні, 14 напрямів, більше 30 доповідей від українських і зарубіжних експертів світового рівня. Готуйте питання спікерам із NIX, Ask Applications, Data Art, BBС, AgileLAB, Elastic і Blue Yonder GmbH.
                        <br/><br/>
                        Корисності чекають новачків і senior’ов. Запускаємо чотири нових напрями: Python, Data Science, Sales Force. Студентам пропонуємо звернути увагу ще й на напрям HR, на якому можна поспілкуватися із експертами та дізнатися все про першу роботу в IT і NIX.
                        І не забуваємо про традиційні — .NET, PHP, QA, Java, Design, WordPress, Android, JavaScript, Business Analytics, Project Management.
                    </p>
                    <img class="post_foto" src="https://www.nixsolutions.com/ru/uploads//2018/11/%D0%903_%D0%B3%D0%BE%D1%8001-700x495.jpg" tabindex="0">
                </div>
                <nav class="under_post">
                    <div>
                        <svg class="set_like" fill="#ffffff" height="20" viewBox="0 0 48 48" width="20">

                            <path clip-rule="evenodd" d="M34.3 3.5C27.2 3.5 24 8.8 24 8.8s-3.2-5.3-10.3-5.3C6.4 3.5.5 9.9.5 17.8s6.1 12.4
                                  12.2 17.8c9.2 8.2 9.8 8.9 11.3 8.9s2.1-.7 11.3-8.9c6.2-5.5 12.2-10 12.2-17.8
                                  0-7.9-5.9-14.3-13.2-14.3zm-1 29.8c-5.4 4.8-8.3
                                  7.5-9.3 8.1-1-.7-4.6-3.9-9.3-8.1-5.5-4.9-11.2-9-11.2-15.6
                                  0-6.2 4.6-11.3 10.2-11.3 4.1 0 6.3 2 7.9 4.2 3.6 5.1 1.2 5.1 4.8 0 1.6-2.2 3.8-4.2
                                  7.9-4.2 5.6 0 10.2 5.1 10.2 11.3 0 6.7-5.7 10.8-11.2 15.6z" fill-rule="evenodd"></path>

                            <path clip-rule="evenodd" d="M35.3 35.6c-9.2 8.2-9.8 8.9-11.3 8.9s-2.1-.7-11.3-8.9C6.5 30.1.5 25.6.5 17.8.5 9.9
                                  6.4 3.5 13.7 3.5 20.8 3.5 24 8.8 24 8.8s3.2-5.3 10.3-5.3c7.3 0 13.2 6.4 13.2 14.3 0
                                  7.8-6.1 12.3-12.2 17.8z" fill-rule="evenodd">
                            </path>
                        </svg>
                        <span class="num-post">21</span>
                    </div>
                    <a href="#" style="text-decoration: none"><img src="/images/main_img/chatbubble-outline.svg" style="width: 20px"><span class="num-post">8</span></a>
                    <a href="#"><img src="/images/main_img/repeat-outline.svg" style="width: 20px"></a>
                    <a href="#"><img src="/images/main_img/push-outline.svg" style="width: 20px"></a>
                </nav>
            </div>

            <div class="my_post">
                <div class="user">
                    <a href="#"><img class="user_icon-post" src="/images/post_img/user-icon4.jpeg"></a>
                    <nav class="inf_user">
                        <a class="inf name" href="#">Michael Hodges</a>
                        <a class="inf user_link" href="#">@michael21</a>
                        <a class="inf post_date" href="#">10 жов.</a>
                        <a class="nav_post" href="#"><img src="/images/main_img/ellipsis-horizontal-outline.svg" style="width: 20px; margin-top: -6px;"></a>
                    </nav>
                </div>
                <div class="inf_post">
                    <div class="post-badge">
                        <a class="item" href="#">
                            <span class="notify-badge">Учёба</span>
                        </a>
                        <a class="item" href="#">
                            <span class="notify-badge">Работа</span>
                        </a>
                        <a class="item" href="#">
                            <span class="notify-badge">Помощь</span>
                        </a>
                    </div>
                    <p class="text_post">привіт👋
                        я шукаю якусь роботу для себе наприклад писати твори кому цікаво то ставте + за ціну
                        домовимось (не дорого)до 20 грн кому потрібно рада буду допомогти.<a href="https://loremipsum.io/ru/generator/?n=6&t=p">https://loremipsum.io/ru/generator/?n=6&t=p</a>
                    </p>
                    <img class="post_foto" src="/images/post_img/post-foto(3).jpg" tabindex="0">

                </div>
                <nav class="under_post">
                    <div>
                        <svg class="set_like" fill="#ffffff" height="20" viewBox="0 0 48 48" width="20">

                            <path clip-rule="evenodd" d="M34.3 3.5C27.2 3.5 24 8.8 24 8.8s-3.2-5.3-10.3-5.3C6.4 3.5.5 9.9.5 17.8s6.1 12.4
                                  12.2 17.8c9.2 8.2 9.8 8.9 11.3 8.9s2.1-.7 11.3-8.9c6.2-5.5 12.2-10 12.2-17.8
                                  0-7.9-5.9-14.3-13.2-14.3zm-1 29.8c-5.4 4.8-8.3
                                  7.5-9.3 8.1-1-.7-4.6-3.9-9.3-8.1-5.5-4.9-11.2-9-11.2-15.6
                                  0-6.2 4.6-11.3 10.2-11.3 4.1 0 6.3 2 7.9 4.2 3.6 5.1 1.2 5.1 4.8 0 1.6-2.2 3.8-4.2
                                  7.9-4.2 5.6 0 10.2 5.1 10.2 11.3 0 6.7-5.7 10.8-11.2 15.6z" fill-rule="evenodd"></path>

                            <path clip-rule="evenodd" d="M35.3 35.6c-9.2 8.2-9.8 8.9-11.3 8.9s-2.1-.7-11.3-8.9C6.5 30.1.5 25.6.5 17.8.5 9.9
                                  6.4 3.5 13.7 3.5 20.8 3.5 24 8.8 24 8.8s3.2-5.3 10.3-5.3c7.3 0 13.2 6.4 13.2 14.3 0
                                  7.8-6.1 12.3-12.2 17.8z" fill-rule="evenodd">
                            </path>
                        </svg>
                        <span class="num-post">21</span>
                    </div>
                    <a href="#" style="text-decoration: none"><img src="/images/main_img/chatbubble-outline.svg" style="width: 20px"><span class="num-post">6</span></a>
                    <a href="#"><img src="/images/main_img/repeat-outline.svg" style="width: 20px"></a>
                    <a href="#"><img src="/images/main_img/push-outline.svg" style="width: 20px"></a>
                </nav>
            </div>





    </section>



    <section class="right-bar right_sidebar">
        <div class="recom recom_home">
            <h3 class="title_recom">Рекомендации</h3>
            <hr>
            <div class="recom-page">
                <a class="user-link" href="#"><img class="top user-icon" src="/images/post_img/user-icon(1).jpg">Claire</a>
            </div>
            <hr>
            <div class="recom-page">
                <a class="user-link" href="#"><img class="user-icon" src="/images/post_img/user-icon2.png">Jacson</a>
            </div>
            <hr>
            <div class="recom-page">
                <a class="user-link" href="#"><img class="user-icon" src="/images/post_img/user-icon3.jpg">Starbight</a>
            </div>
            <hr>
        </div>
        <!--<p class="inc">© Find it, Inc., 2020</p>-->
    </section>
</div>