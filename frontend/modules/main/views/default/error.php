<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="main-views-error stetch">
        <div class="row navigation">
            <srction class="col-xs-12 col-sm-12">
                <p class="error-title"><?= Html::encode($this->title) ?></p>
                <div class="error-text">
                    <?= nl2br(Html::encode($message)) ?>
                </div>
                <a href="<?= Url::to(['/']) ?>" class='main-error-button'><i
                            class="fa fa-reply"></i><?= Yii::t('app', 'Home page') ?></a>
            </div>
        </div>
</section>
