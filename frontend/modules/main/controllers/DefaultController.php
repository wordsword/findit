<?php

namespace app\modules\main\controllers;

use common\modules\user\models\User;
use Yii;
use app\modules\main\models\Category;
use app\modules\main\models\ContactForm;
use app\modules\main\models\Document;
use app\modules\main\models\RequestForm;
use app\modules\main\models\Review;
use app\modules\main\models\Slide;
use frontend\modules\main\models\UserMessage;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
}
