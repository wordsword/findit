<?php

namespace app\components;

use rmrevin\yii\fontawesome\FA as FAv;
use skeeks\yii2\assetsAuto\AssetsAutoCompressComponent;
use Yii;
use yii\bootstrap\Html;
use kartik\base\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

class SkAssetsAutoCompress extends AssetsAutoCompressComponent
{

    protected function _processingJsFiles($files = [])
    {
        $fileName = md5(implode(array_keys($files)) . $this->getSettingsHash()) . '.js';
        $publicUrl = Yii::$app->urlManager->hostInfo . Yii::$app->assetManager->baseUrl . '/js-compress/' . $fileName;
        //$publicUrl  = \Yii::getAlias('@web/assets/js-compress/' . $fileName);

        $rootDir = \Yii::$app->assetManager->basePath . '/js-compress';
        //$rootDir    = \Yii::getAlias('@webroot/assets/js-compress');
        $rootUrl = $rootDir . '/' . $fileName;

        if (file_exists($rootUrl)) {
            $resultFiles = [];

            if (!$this->jsFileRemouteCompile) {
                foreach ($files as $fileCode => $fileTag) {
                    if (!Url::isRelative($fileCode)) {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }


            $publicUrl = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl] = Html::jsFile($publicUrl, $this->jsOptions);
            return $resultFiles;
        }

        //Reading the contents of the files
        try {
            $resultContent = [];
            $resultFiles = [];
            foreach ($files as $fileCode => $fileTag) {
                if (Url::isRelative($fileCode)) {
                    if ($pos = strpos($fileCode, "?")) {
                        $fileCode = substr($fileCode, 0, $pos);
                    }

                    $fileCode = $this->webroot . $fileCode;
                    $contentFile = $this->readLocalFile($fileCode);

                    /**\Yii::info("file: " . \Yii::getAlias(\Yii::$app->assetManager->basePath . $fileCode), self::class);*/
                    //$contentFile = $this->fileGetContents( Url::to(\Yii::getAlias($tmpFileCode), true) );
                    //$contentFile = $this->fileGetContents( \Yii::$app->assetManager->basePath . $fileCode );
                    $resultContent[] = trim($contentFile) . "\n;";;
                } else {
                    if ($this->jsFileRemouteCompile) {
                        //Try to download the deleted file
                        $contentFile = $this->fileGetContents($fileCode);
                        $resultContent[] = trim($contentFile);
                    } else {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }
        } catch (\Exception $e) {
            \Yii::error(__METHOD__ . ": " . $e->getMessage(), static::class);
            return $files;
        }

        if ($resultContent) {
            $content = implode($resultContent, ";\n");
            if (!is_dir($rootDir)) {
                if (!FileHelper::createDirectory($rootDir, 0777)) {
                    return $files;
                }
            }

            if ($this->jsFileCompress) {
                $content = \JShrink\Minifier::minify($content, ['flaggedComments' => $this->jsFileCompressFlaggedComments]);
            }

            $page = \Yii::$app->request->absoluteUrl;
            $useFunction = function_exists('curl_init') ? 'curl extension' : 'php file_get_contents';
            $filesString = implode(', ', array_keys($files));

            \Yii::info("Create js file: {$publicUrl} from files: {$filesString} to use {$useFunction} on page '{$page}'", static::className());

            $file = fopen($rootUrl, "w");
            fwrite($file, $content);
            fclose($file);
        }


        if (file_exists($rootUrl)) {
            $publicUrl = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl] = Html::jsFile($publicUrl, $this->jsOptions);
            return $resultFiles;
        } else {
            return $files;
        }
    }

    protected function _processingCssFiles($files = [])
    {
        $fileName = md5(implode(array_keys($files)) . $this->getSettingsHash()) . '.css';
        $publicUrl = Yii::$app->urlManager->hostInfo . Yii::$app->assetManager->baseUrl . '/css-compress/' . $fileName;
        //$publicUrl  = \Yii::getAlias('@web/assets/css-compress/' . $fileName);

        $rootDir = \Yii::$app->assetManager->basePath . '/css-compress';
        //$rootDir    = \Yii::getAlias('@webroot/assets/css-compress');
        $rootUrl = $rootDir . '/' . $fileName;

        if (file_exists($rootUrl)) {
            $resultFiles = [];

            if (!$this->cssFileRemouteCompile) {
                foreach ($files as $fileCode => $fileTag) {
                    if (!Url::isRelative($fileCode)) {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }

            $publicUrl = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl] = Html::cssFile($publicUrl, $this->cssOptions);
            return $resultFiles;
        }

        //Reading the contents of the files
        try {
            $resultContent = [];
            $resultFiles = [];
            foreach ($files as $fileCode => $fileTag) {
                if (Url::isRelative($fileCode)) {
                    $fileCodeLocal = $fileCode;
                    if ($pos = strpos($fileCode, "?")) {
                        $fileCodeLocal = substr($fileCodeLocal, 0, $pos);
                    }

                    $fileCodeLocal = $this->webroot . $fileCodeLocal;
                    $contentTmp = trim($this->readLocalFile($fileCodeLocal));

                    //$contentTmp         = trim($this->fileGetContents( Url::to(\Yii::getAlias($fileCode), true) ));

                    $fileCodeTmp = explode("/", $fileCode);
                    unset($fileCodeTmp[count($fileCodeTmp) - 1]);
                    $prependRelativePath = implode("/", $fileCodeTmp) . "/";

                    $contentTmp = \Minify_CSS::minify($contentTmp, [
                        "prependRelativePath" => $prependRelativePath,

                        'compress' => true,
                        'removeCharsets' => true,
                        'preserveComments' => true,
                    ]);

                    //$contentTmp = \CssMin::minify($contentTmp);

                    $resultContent[] = $contentTmp;
                } else {
                    if ($this->cssFileRemouteCompile) {
                        //Try to download the deleted file
                        $resultContent[] = trim($this->fileGetContents($fileCode));
                    } else {
                        $resultFiles[$fileCode] = $fileTag;
                    }
                }
            }
        } catch (\Exception $e) {
            \Yii::error(__METHOD__ . ": " . $e->getMessage(), static::class);
            return $files;
        }

        if ($resultContent) {
            $content = implode($resultContent, "\n");
            if (!is_dir($rootDir)) {
                if (!FileHelper::createDirectory($rootDir, 0777)) {
                    return $files;
                }
            }

            if ($this->cssFileCompress) {
                $content = \CssMin::minify($content);
            }

            $page = \Yii::$app->request->absoluteUrl;
            $useFunction = function_exists('curl_init') ? 'curl extension' : 'php file_get_contents';
            $filesString = implode(', ', array_keys($files));

            \Yii::info("Create css file: {$publicUrl} from files: {$filesString} to use {$useFunction} on page '{$page}'", static::className());


            $file = fopen($rootUrl, "w");
            fwrite($file, $content);
            fclose($file);
        }


        if (file_exists($rootUrl)) {
            $publicUrl = $publicUrl . "?v=" . filemtime($rootUrl);
            $resultFiles[$publicUrl] = Html::cssFile($publicUrl, $this->cssOptions);
            return $resultFiles;
        } else {
            return $files;
        }
    }

}