<?php

namespace app\components;


use Yii;
use yii\base\InvalidConfigException;
use yii\data\Sort;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\web\Request;

class SkSort extends Sort
{

    public function createSortParamWithDirection($attribute, $direction)
    {
        if (!isset($this->attributes[$attribute])) {
            throw new InvalidConfigException("Unknown attribute: $attribute");
        }
        $sorts[] = $direction === 'desc' ? '-' . $attribute : $attribute;

        return implode($this->separator, $sorts);
    }

    public function createUrlWithDirection($attribute, $absolute = false, $direction)
    {
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        $params[$this->sortParam] = $this->createSortParamWithDirection($attribute, $direction);
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        } else {
            return $urlManager->createUrl($params);
        }
    }

    public function link($attribute, $options = [])
    {
        $directions = ['asc','desc'];
        $links = [];
        foreach ($directions as $direction) {
            $tempOptions = $options;
            $class = $direction;
            Html::addCssClass($tempOptions,$class);
            if ($this->isCurrOrder($attribute,$direction)) {
                Html::addCssClass($tempOptions,'active');
            }
            if (isset($this->attributes[$attribute]['direction']) && $this->attributes[$attribute]['direction'] !== $direction) {
                continue;
            }
            $url = $this->createUrlWithDirection($attribute, false, $direction);
            $tempOptions['data-sort'] = $this->createSortParamWithDirection($attribute, $direction);

            if (isset($tempOptions['label'])) {
                $label = $tempOptions['label'];
                unset($tempOptions['label']);
            } else {
                if (isset($this->attributes[$attribute]['label_'.$direction])) {
                    $label = $this->attributes[$attribute]['label_'.$direction];
                } elseif (isset($this->attributes[$attribute]['label'])) {
                    $label = $this->attributes[$attribute]['label'];
                } else {
                    $label = Inflector::camel2words($attribute);
                }
            }
            $links[] = Html::a($label, $url, $tempOptions);

        }
        return $links;
    }

    public function getCurrentOrderLink($options = []) {
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if (isset($params[$this->sortParam])) {
            $attribute = $params[$this->sortParam];
        } else {
            $attribute = key($this->defaultOrder);
        }
        $direction = 'asc';
        if (strncmp($attribute, '-', 1) === 0) {
            $direction = 'desc';
            $attribute = substr($attribute, 1);
        }
        if (isset($tempOptions['label'])) {
            $label = $tempOptions['label'];
            unset($tempOptions['label']);
        } else {
            if (isset($this->attributes[$attribute]['label_'.$direction])) {
                $label = $this->attributes[$attribute]['label_'.$direction];
            } elseif (isset($this->attributes[$attribute]['label'])) {
                $label = $this->attributes[$attribute]['label'];
            } else {
                $label = Inflector::camel2words($attribute);
            }
        }
        Html::addCssClass($tempOptions,'current-sort-attr');
        return Html::tag('span',$label,$tempOptions);
    }

    public function isCurrOrder($attribute,$direction)
    {
        if ($direction == 'desc') {
            $attribute = '-'.$attribute;
        }
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if (isset($params[$this->sortParam])) {
            return $attribute == $params[$this->sortParam];
        } else {
            return $attribute == key($this->defaultOrder);
        }
    }

}