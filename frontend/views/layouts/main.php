<?php

use app\modules\main\models\Document;
use common\components\SkLangHelper;
use common\models\Script;
use common\modules\user\Module;
use frontend\assets\AppAsset;
use frontend\components\Breadcrumbs;
use frontend\widgets\langSwitcher\LangSwitcher;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
// use rmrevin\yii\fontawesome\AssetBundle as FAAsset;

/* @var $this \yii\web\View */
/* @var $content string */

// FAAsset::register($this);
AppAsset::register($this);
// \rmrevin\yii\fontawesome\AssetBundle::register($this);


// Script::registerEnbledScripts($this);


// $langs = SkLangHelper::suffixList();
// if (count($langs) > 1) {
//     foreach ($langs as $urlSuffix => $lang) {
//         $this->registerLinkTag([
//             'rel' => 'alternate',
//             'hreflang' => $lang['lang_id'],
//             'href' => Yii::$app->urlManager->getHostInfo() . $urlSuffix . Yii::$app->getRequest()->getUrl(),
//         ]);
//     }
// }
// $this->registerMetaTag([
//     'property' => 'og:site_name',
//     'content' => Yii::$app->settings->companyName,
// ]);
// $this->registerMetaTag([
//     'property' => "og:image",
//     'content' => Yii::$app->urlManager->getHostInfo() . '/images/logo.jpg',
// ]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html;">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>


<body>
    <?php $this->beginBody() ?>

    <div class="top-bar">
        <header class="container">
            <div class="header-left">
                <a href="#"><img class="logo_page" src="/images/header_img/Logo_Find-it.png" alt="logo"></a>
            </div>

            <div class="header-center">

                <form>
                    <label for="search">
                        <span class="icon-search"><img src="/images/header_img/search-solid.svg"></span>
                    </label>
                    <input class="input-form tags" type="text" id="search" placeholder="Искать здесь...">
                </form>

            </div>

            <div class="header-right">
                <? if(Yii::$app->user->isGuest) { ?>
                <a class="link_sign_in fancy-link" href="<?= Url::toRoute(['/login']) ?>">Sign in</a>
                <a class="link_sign_up fancy-link" href="<?= Url::toRoute(['/sign-up']) ?>">Sign up</a>
                <? } ?>
                <div class="dropdown-user">
                    <a href="#"><img onclick="myFunctionUser()" class="dropbtn-user user_icon" src="/images/header_img/iconmonstr-user-20.svg" /></a>
                    <div id="myDropdownUser" class="dropdown-content-user">
                        <p class="view">view options</p>
                        <a href="#">
                            <img src="/images/header_img/moon.svg">
                            Ночной режим
                            <input type="checkbox" name="checkbox1" id="checkbox1" class="ios-toggle" checked />
                            <label for="checkbox1" class="checkbox-label"></label>
                        </a>
                        <a href="#">
                            <img src="/images/header_img/question.svg">
                            Помощь
                        </a>
                        <? if(Yii::$app->user->isAdmin || Yii::$app->user->isSuperAdmin) { ?>
                            <a href="/admin" class="logout-btn">
                                <?= FA::icon('cog') ?>
                                <?= Yii::t('app', 'Admin panel') ?>
                            </a>
                        <? } ?>
                        <? if(!Yii::$app->user->isGuest) { ?>
                            <a href="/logout" class="logout-btn">
                                <?= FA::icon('sign-out') ?>
                                <?= Yii::t('app', 'Logout') ?>
                            </a>
                        <? } ?>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div id="page">
        <div class="container">
            <?= $content ?>
        </div>

        <footer>
        </footer>
    </div>
    <?//= $this->render('@backend/modules/main/views/default/_confirmForm') ?>
    <?//= $this->render('@backend/modules/main/views/default/_alertForm') ?>
    <?//= $this->render('@backend/modules/main/views/default/_actionForm') ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>