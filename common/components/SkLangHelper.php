<?php

namespace common\components;

use yii\helpers\Html;
use yii\caching\DbDependency;
use Yii;
use yii\helpers\Url;

/**
 * Description of SkLangHelper
 *
 * @author Snizhok <snizhok.lucky@gmail.com>
 */
class SkLangHelper {
    
    private static $_list; 

    public static function getLangs() {
        if (empty(self::$_list)) {
            $list = Yii::$app->cache->get(__CLASS__.Yii::$app->language);
            if (empty($list)) {
                $sql  = "SELECT * FROM languages "; 
                $sql .= "WHERE visible=1 ";
                $sql .= "ORDER BY sort_order ";
                $dependency = new DbDependency(['sql'=>"SELECT MAX(updated_at) FROM languages"]);
                $langs = Yii::$app->db->createCommand($sql)->cache(Yii::$app->db->queryCacheDuration, $dependency)->queryAll();
                $list = [];
                if (is_array($langs) && count($langs)) {
                    foreach ($langs as $lang) {
                        $list[Html::encode($lang['lang_id'])]= $lang;
                    }
                }
                Yii::$app->cache->set(__CLASS__.Yii::$app->language, $list, Yii::$app->db->queryCacheDuration, $dependency);
            }
            self::$_list = $list;
        }
        return self::$_list;
    }
    
    public static function getDefaultLanguage() {
        $langs = self::getLangs();
        foreach ($langs as $lang) {
            if ($lang['deflang']) {
                return Html::encode($lang['lang_id']);
            }
        }
    }
    
    public static function getLangsListImg()
    {
        $list = [];
        foreach (self::getLangs() as $lang) {
            $list[Html::encode($lang['lang_id'])] = Html::encode($lang['image']);
        }
        return $list;
    }

    public static function enabled()
    {
        return count(self::getLangs()) > 1;
    }
 
    public static function suffixList()
    {
        $list = [];
        foreach (self::getLangs() as $lang) {
            if ($lang['lang_id'] === self::getDefaultLanguage()) {
                $suffix = '';
                $list[$suffix] = $lang;
            } else {
                $suffix = '/'.Html::encode($lang['lang_id']);
                $list[$suffix] = $lang;
            }
        }
        return $list;
    }
    
    public static function getLangsList() {
        $list = [];
        foreach (self::getLangs() as $lang) {
            $list[Html::encode($lang['lang_id'])] = Html::encode($lang['lang_name']);
        }
        return $list;
    }

    public static function getLangFromUrl($url)
    {
        if (self::enabled()) {
            $urlParts = explode('/', ltrim($url, '/'));
            $langExists = in_array($urlParts[0], array_keys(self::getLangs()));
            $langDefault = $urlParts[0] == self::getDefaultLanguage();
            if ($langExists && !$langDefault) {
                $lang = array_shift($urlParts);
                Yii::$app->language = $lang;
            }
            $url = '/' . implode('/', $urlParts);
        }
 
        return $url;
    }
 
    public static function addLangToUrl($url)
    {
        if (self::enabled()) {
            $urlParts = explode('/', ltrim($url, '/'));

            $langExists = in_array($urlParts[0], array_keys(self::getLangs()));
            $langDefault = Yii::$app->language == self::getDefaultLanguage();
            if ($langExists && $langDefault) {
                array_shift($urlParts);
            }
            if (!$langExists && !$langDefault) {
                array_unshift($urlParts, Yii::$app->language);
            }
            $url = '/' . implode('/', $urlParts);
        }
        return $url;
    }

    public static function urlTo()
    {
        return self::addLangToUrl(Url::toRoute(''));
    }

}
