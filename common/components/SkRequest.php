<?php

namespace common\components;

use yii\web\Request;
use Yii;

/**
 * Description of SkRequest
 *
 * @author Snizhok <snizhok.lucky@gmail.com>
 */
class SkRequest extends Request {
    
    private $_originalUrl;
    
    protected function resolveRequestUri()
    {
        $requestUri = SkLangHelper::getLangFromUrl(parent::resolveRequestUri());
        return $requestUri;
    }    
    
    protected function resolvePathInfo()
    {
        // Для исключения задания некорректных языков в административной части сайта
        $pathInfo = $this->getUrl();
        if (Yii::$app->id == 'app-backend') {
            preg_match('|^([a-z]{2,4}/)?admin(/(.*))?$|', ltrim($pathInfo,'/'), $matches);
            if (is_array($matches) && !empty($matches[1]) && $matches[1] != '') {
                throw new \yii\web\NotFoundHttpException(Yii::t('app', 'Page not found'), 404);
            }
        }
        if (Yii::$app->id == 'app-foreign') {
            preg_match('|^([a-z]{2,4}/)?admin(/(.*))?$|', ltrim($pathInfo,'/'), $matches);
            if (is_array($matches) && !empty($matches[1]) && $matches[1] != '') {
                throw new \yii\web\NotFoundHttpException(Yii::t('app', 'Page not found'), 404);
            }
        }
        /*--------------------------------------------*/
        return parent::resolvePathInfo();
    }
    
    public function getOriginalUrl()
    {
        if ($this->_originalUrl === null) {
            $this->_originalUrl = parent::resolveRequestUri();
        }

        return $this->_originalUrl;
    }
}
