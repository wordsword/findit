<?php
namespace common\components\infinitescroll;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;


class InfiniteScrollPager extends \darkcs\infinitescroll\InfiniteScrollPager
{
    public function init()
    {
        $default = [
            'pagination' => $this->paginationSelector,
            'next' => $this->nextSelector,
            'item' => $this->itemSelector,
            'state' => [
                'isPaused' => !$this->autoStart,
            ],
            'pjax' => [
                'container' => $this->pjaxContainer,
            ],
            'bufferPx' => $this->bufferPx,
            'wrapper' => $this->wrapperSelector,
            'alwaysHidePagination' => $this->alwaysHidePagination,
        ];

        $this->pluginOptions = ArrayHelper::merge($default, $this->pluginOptions);

        InfiniteScrollAsset::register($this->view);
        $this->initInfiniteScroll();
        parent::init();
    }

    public function run()
    {
        parent::run();
    }

    public function initInfiniteScroll()
    {
        $options = Json::encode($this->pluginOptions);

        $js = "$('{$this->containerSelector}').infinitescroll({$options});";
        $this->view->registerJs($js);
    }
}
