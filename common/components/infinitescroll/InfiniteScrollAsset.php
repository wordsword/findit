<?php

namespace common\components\infinitescroll;

use Yii;
use yii\web\AssetBundle;

class InfiniteScrollAsset extends \darkcs\infinitescroll\InfiniteScrollAsset
{
    public $sourcePath = '@vendor/darkcs/yii2-infinite-scroll/assets';
    public $js = [
  //      'jquery.infinitescroll.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}