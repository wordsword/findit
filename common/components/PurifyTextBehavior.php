<?php

namespace common\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
use common\components\SkLangHelper;

class PurifyTextBehavior extends Behavior
{
    /**
     * @var string source attribute name
     */
    public $sourceAttribute = 'text';

    /**
     * @var string destination attribute name for result
     */
    public $destinationAttribute = 'purified_text';

    /**
     * @var bool use or not use the Markdown parser
     */
    public $enableMarkdown = false;

    /**
     * @var bool use CHtml::encode instead of HTML Purifier for <pre> and <code> contents
     */
    public $encodePreContent = false;

    /**
     * @var bool use or not use HTML Purifier
     */
    public $enablePurifier = true;

    /**
     * @var array options for the HTML Purifier
     */
    public $purifierOptions = [];

    /**
     * @var bool
     */
    public $processOnBeforeSave = true;

    /**
     * @var bool allow process if destination attribute is empty
     */
    public $processOnAfterFind = true;

    /**
     * @var bool save or don't save result to DB
     */
    public $updateOnAfterFind = true;

    private $_contentHash = '';
    
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_AFTER_FIND    => 'afterFind',
        ];
    }

    /**
     * @param ModelEvent $event event parameter
     */
    public function beforeSave($event)
    {
        $model = $event->sender;

        if ($this->processOnBeforeSave)
        {
            if (is_array($this->destinationAttribute)) {
                $destinationAttributes = $this->destinationAttribute;
                $sourceAttribute = $this->sourceAttribute;
                foreach ($destinationAttributes as $key => $destinationAttribute) {
                    if (
                        $sourceAttribute[$key] &&
                        $destinationAttribute &&
                        $this->calculateHash($model->{$sourceAttribute[$key]}) !== $this->_contentHash
                    )
                    {
                        $model->{$destinationAttribute} = $this->processContent($model->{$sourceAttribute[$key]});
                    }
                }
            } else {
                if (
                    $this->sourceAttribute &&
                    $this->destinationAttribute &&
                    $this->calculateHash($model->{$this->sourceAttribute}) !== $this->_contentHash
                )
                {
                    $model->{$this->destinationAttribute} = $this->processContent($model->{$this->sourceAttribute});
                }
            }
        }
    }

    /**
     * @param ModelEvent $event event parameter
     */
    public function afterFind($event)
    {
        $model = $event->sender;
        
        if ($this->processOnAfterFind)
        {
            if (is_array($this->destinationAttribute)) {
                $destinationAttributes = $this->destinationAttribute;
                $sourceAttribute = $this->sourceAttribute;
                foreach ($destinationAttributes as $key => $destinationAttribute) {
                    if (
                        $sourceAttribute[$key] &&
                        $destinationAttribute &&
                        $model->{$sourceAttribute[$key]} &&
                        !$model->{$destinationAttribute}
                    )
                    {
                        $model->{$destinationAttribute} = $this->processContent($model->{$sourceAttribute[$key]});
                        if ($this->updateOnAfterFind)
                            $this->updateModel($event);
                    }
                }
            } else {
                $this->_contentHash = $this->calculateHash($model->{$this->sourceAttribute});
                if (
                    $this->sourceAttribute &&
                    $this->destinationAttribute &&
                    $model->{$this->sourceAttribute} &&
                    !$model->{$this->destinationAttribute}
                )
                {
                    $model->{$this->destinationAttribute} = $this->processContent($model->{$this->sourceAttribute});
                    if ($this->updateOnAfterFind)
                        $this->updateModel($event);
                }
            }
        }
    }

    protected function processContent($text)
    {
        if ($this->enableMarkdown)
            $text = $this->markdownText($text);

        if ($this->enablePurifier)
            $text = $this->purifyText($text);

        return $text;
    }

    /**
     * @param string $text
     * @return string
     */
    public function markdownText($text)
    {
        $md = new \yii\helpers\Markdown;
        return $md->process($text);
    }

    /**
     * @param string $text
     * @return string
     */
    public function purifyText($text)
    {
        $p = new \yii\helpers\HtmlPurifier;
        if ($this->encodePreContent)
        {
            $text = preg_replace_callback('|<pre([^>]*)>(.*)</pre>|ismU', [$this, 'storePreContent'], $text);
            $text = preg_replace_callback('|<code([^>]*)>(.*)</code>|ismU', [$this, 'storeCodeContent'], $text);
        }

        $text = $p->process(trim($text),$this->purifierOptions);

        if ($this->encodePreContent)
        {
            $text = preg_replace_callback('|<pre([^>]*)>(.*)</pre>|ismU', [$this, 'resumePreContent'], $text);
            $text = preg_replace_callback('|<code([^>]*)>(.*)</code>|ismU', [$this, 'resumeCodeContent'], $text);
        }

        return $text;
    }
    
    /**
     * @param ModelEvent $event event parameter
     */
    protected function updateModel($event)
    {
        $model = $event->sender;
        $attributes = [];
        if (is_array($this->destinationAttribute)) {
            $destinationAttributes = $this->destinationAttribute;
            foreach ($destinationAttributes as $destinationAttribute) {
                $attributes[$destinationAttribute] = $model->{$destinationAttribute};
            }
        } else {
            $attributes[$this->destinationAttribute] = $model->{$this->destinationAttribute};
        }
        $model->updateAttributes($attributes);
    }

    private $_preContents = array();

    private function storePreContent($matches)
    {
        return '<pre' . $matches[1] . '>' . $this->storeContent($matches[2]) . '</pre>';
    }

    private function resumePreContent($matches)
    {
        return '<pre' . $matches[1] . '>' . $this->resumeContent($matches[2]) . '</pre>';
    }

    private function storeCodeContent($matches)
    {
        return '<code' . $matches[1] . '>' . $this->storeContent($matches[2]) . '</code>';
    }

    private function resumeCodeContent($matches)
    {
        return '<code' . $matches[1] . '>' . $this->resumeContent($matches[2]) . '</code>';
    }

    private function storeContent($content)
    {
        do {
            $id = md5(rand(0, 100000));
        } while (isset($this->_preContents[$id]));
        $this->_preContents[$id] = $content;
        return $id;
    }

    private function resumeContent($id)
    {
        return isset($this->_preContents[$id]) ? CHtml::encode($this->_preContents[$id]) : '';
    }

    private function calculateHash($content) {
        return md5($content);
    }
}