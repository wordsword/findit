<?php
/**
 * Created by PhpStorm.
 * User: Snizhok
 * Date: 03.02.2016
 * Time: 6:04
 */

namespace common\components;

use yii\base\Behavior;
use yii\base\Application;
use Yii;
use yii\web\NotFoundHttpException;


class SkBeforeRequestBehavior extends Behavior
{

    public function events()
    {
        return [
            Application::EVENT_BEFORE_REQUEST => 'beforeRequest',
        ];
    }

    public function beforeRequest() {
        Yii::$app->language = SkLangHelper::getDefaultLanguage();
        if (Yii::$app->id !== 'app-console') {
//            Yii::$app->mailer->transport = [
//                'class' => 'Swift_SmtpTransport',
//                'host' => Yii::$app->settings->getValue('emailHost'),
//                'username' => Yii::$app->settings->getValue('emailUsername'),
//                'password' => Yii::$app->settings->getValue('emailPass'),
//                'port' => Yii::$app->settings->getValue('emailPort'),
//            ];
//            if (Yii::$app->settings->getValue('emailEncryption') == 'tls' || Yii::$app->settings->getValue('emailEncryption') == 'ssl') {
//                Yii::$app->mailer->transport->encryption = Yii::$app->settings->getValue('emailEncryption');
//            }
        }

    }

}