<?php

namespace common\components;

use yii\web\UrlManager;
use Yii;

/**
 * Description of SkUrlManager
 *
 * @author Snizhok <snizhok.lucky@gmail.com>
 */
class SkUrlManager extends UrlManager {
    
    public function createUrl($params)
    {
        $url = $this->fixPathSlashes(parent::createUrl($params));
        return SkLangHelper::addLangToUrl($url);
    }
    
    protected  function fixPathSlashes($url)
    {
        return preg_replace('|\%2F|i', '/', $url);
    }
    
    public function createAbsoluteUrl($params, $scheme = null)
    {
        $params = (array) $params;
        $url = $this->createUrl($params);
        if (strpos($url, '://') === false) {
            $url = $this->getHostInfo() . $url;
        }
//        if (($pos = strpos($url, '://')) !== false) {
//            $url = substr($url, $pos+1);
//        }
        return $url;
    }
    
}
