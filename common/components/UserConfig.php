<?php
namespace common\components;

//use backend\modules\shop\models\Price;
use common\modules\user\models\Settings;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class UserSettings extends Component {

    private $_attributes;

    public function init()
    {
        $this->_attributes = ArrayHelper::map(Settings::find()->all(),'field_key','value');
    }

    public function __get($name)
    {
        if (isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        } else {
            return null;
        }
    }

}