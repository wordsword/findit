<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class Settings extends Component {

    private $model;

    public function init()
    {
        $this->model = \common\models\Settings::getModel();
    }

    public function __get($name)
    {
        if (isset($this->model[$name])) {
            return $this->model[$name];
        } else {
            return null;
        }
    }

}