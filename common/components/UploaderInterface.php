<?php

namespace common\components;


interface UploaderInterface
{
    /**
     * @param $params array
     * @param $ownerObject
     * @return object
     */
    public static function load($params);
}