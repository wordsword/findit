<?php

namespace common\components;

class MessageTranslate
{
    public static function getTranslations()
    {
        return [
            "api key is required" => [
                "en" => "API Key is required", 
                "uk" => "Потрібен API Key", 
                "ru" => "Нужен API Key"
            ],
            "api key is invalid" => [
                "en" => "API Key is invalid", 
                "uk" => "API Key недійсний", 
                "ru" => "API Key недействителен"
            ],
            "api key is not active" => [
                "en" => "API Key is not active", 
                "uk" => "API Key не активний", 
                "ru" => "API Key не активен"
            ],
            "license is required" => [
                "en" => "License is required", 
                "uk" => "Потрібна ліцензія", 
                "ru" => "Нужна лицензия"
            ],
            "license not found" => [
                "en" => "License not found", 
                "uk" => "Ліцензія не знайдена", 
                "ru" => "Лицензия не найдена"
            ],
            "license is not active" => [
                "en" => "License is not active", 
                "uk" => "Ліцензія не активна", 
                "ru" => "Лицензия не активна"
            ],
            "object was deleted" => [
                "en" => "Object was deleted", 
                "uk" => "Об’єкт вже був видалений", 
                "ru" => "Объект уже был удален"
            ],
            "object not allowed" => [
                "en" => "Object not allowed", 
                "uk" => "Немає дозволу на зміну об'єкту", 
                "ru" => "Нет разрешения на изменение объекта"
            ],
            "object not found" => [
                "en" => "Object not found", 
                "uk" => "Об'єкт не знайдено", 
                "ru" => "Объект не найден"
            ],
            "deleted object" => [
                "en" => "Deleted object", 
                "uk" => "Об’єкт видалено", 
                "ru" => "Объект удален"
            ],
            "cannot be deleted" => [
                "en" => "Cannot be deleted", 
                "uk" => "Видалити неможливо", 
                "ru" => "Удалить невозможно"
            ],
            "body is not array" => [
                "en" => "Body is not array", 
                "uk" => "Тіло запиту має бути масивом", 
                "ru" => "Тело запроса должно быть массивом"
            ],
            "parametr \"guid\" is required in request" => [
                "en" => "Parametr \"guid\" is required in request", 
                "uk" => "У запиті потрібен параметр \"guide\"", 
                "ru" => "В запросе необходимо параметр \"guide\""
            ],
            "guid is invalid" => [
                "en" => "GUID is invalid", 
                "uk" => "GUID недійсний", 
                "ru" => "GUID недействителен"
            ],
            "guid is required" => [
                "en" => "GUID is required", 
                "uk" => "GUID обов'язковий", 
                "ru" => "GUID обязательный"
            ],
            "price type not found" => [
                "en" => "Price type not found", 
                "uk" => "Тип ціни не знайдено", 
                "ru" => "Тип цены не найдено"
            ],
            "price type is required" => [
                "en" => "Price type is required", 
                "uk" => "Тип ціни обов'язковий", 
                "ru" => "Тип цены обязательный"
            ],
            "currency not found" => [
                "en" => "Currency not found", 
                "uk" => "Валюта не знайдена", 
                "ru" => "Валюта не найдена"
            ],
            "partner not found" => [
                "en" => "Partner not found", 
                "uk" => "Партнер не знайдено", 
                "ru" => "Партнер не найдено"
            ],
            "partner is required" => [
                "en" => "Partner is required", 
                "uk" => "Поле партнер обов'язкове", 
                "ru" => "Поле партнер обязательное"
            ],
            "store not found" => [
                "en" => "Store not found", 
                "uk" => "Склад не знайдено", 
                "ru" => "Склад не найдено"
            ],
            "store from not found" => [
                "en" => "Store from not found", 
                "uk" => "Склад відпуску не знайдено", 
                "ru" => "Склад отпуска не найдено"
            ],
            "store to not found" => [
                "en" => "Store to not found", 
                "uk" => "Склад приходу не знайдено", 
                "ru" => "Склад прихода не найдено"
            ],
            "product not found" => [
                "en" => "Product not found", 
                "uk" => "Товар не знайдено", 
                "ru" => "Товар не найден"
            ],
            "product is required" => [
                "en" => "Product is required", 
                "uk" => "Поле товар обов'язкове", 
                "ru" => "Поле товар обязательное"
            ],
            "is_post is required" => [
                "en" => "іs_post is required", 
                "uk" => "Параметр іs_post обов’язковий", 
                "ru" => "Параметр іs_post обязательный"
            ],
            "number is required" => [
                "en" => "Number is required", 
                "uk" => "Номер не вказано", 
                "ru" => "Номер не указан"
            ],
            "date is required" => [
                "en" => "Date is required", 
                "uk" => "Дата не вказана", 
                "ru" => "Дата не указана"
            ],
            "sum is required" => [
                "en" => "Sum is required", 
                "uk" => "Сума не вказана", 
                "ru" => "Сумма не указана"
            ],
            "sum is invalid" => [
                "en" => "Sum is invalid", 
                "uk" => "Сума введена некоректно", 
                "ru" => "Сумма введена некорректно"
            ],
            "quantity is required" => [
                "en" => "Quantity is required", 
                "uk" => "Кількість не вказана", 
                "ru" => "Количество не указано"
            ],
            "quantity is invalid" => [
                "en" => "Quantity is invalid", 
                "uk" => "Кількість введена некоректно", 
                "ru" => "Количество введено некорректно"
            ],
            "count is required" => [
                "en" => "Count is required", 
                "uk" => "Кількість не вказана", 
                "ru" => "Количество не указано"
            ],
            "count is invalid" => [
                "en" => "Count is invalid", 
                "uk" => "Кількість введена некоректно", 
                "ru" => "Количество введено некорректно"
            ],
            "count before is required" => [
                "en" => "Count before is required", 
                "uk" => "Кількість До не вказана", 
                "ru" => "Количество До не указано"
            ],
            "count before is invalid" => [
                "en" => "Count before is invalid", 
                "uk" => "Кількість До введена некоректно", 
                "ru" => "Количество До введено некорректно"
            ],
            "count after is required" => [
                "en" => "Count after is required", 
                "uk" => "Кількість Після не вказана", 
                "ru" => "Количество После не указано"
            ],
            "count after is invalid" => [
                "en" => "Count after is invalid", 
                "uk" => "Кількість Після введена некоректно", 
                "ru" => "Количество После введена некорректно"
            ],
            "value is required" => [
                "en" => "Value is required", 
                "uk" => "Значення не вказана", 
                "ru" => "Значения не указано"
            ],
            "value is invalid" => [
                "en" => "Value is invalid", 
                "uk" => "Значення введена некоректно", 
                "ru" => "Значение введено некорректно"
            ],
            "price is required" => [
                "en" => "Price is required", 
                "uk" => "Ціна не вказана", 
                "ru" => "Цена не указана"
            ],
            "price is invalid" => [
                "en" => "Price is invalid", 
                "uk" => "Ціна введена некоректно", 
                "ru" => "Цена введена некорректно"
            ],
            "basis document not found" => [
                "en" => "Basis document not found", 
                "uk" => "Базовий документ не знайдено", 
                "ru" => "Базовый документ не найдено"
            ],
            "type is required" => [
                "en" => "Type is required", 
                "uk" => "Тип платежу не вказано", 
                "ru" => "Тип платежа не указан"
            ],
            "type is invalid" => [
                "en" => "Type is invalid", 
                "uk" => "Тип платежу введено некоректно", 
                "ru" => "Тип платежа введен некорректно"
            ],
            "method is required" => [
                "en" => "Method is required", 
                "uk" => "Метод платежу введено некоректно", 
                "ru" => "Метод платежа введен некорректно"
            ],
            "items are required" => [
                "en" => "Items are required", 
                "uk" => "Таблична частина обовязкова", 
                "ru" => "Табличная часть обязательна"
            ],
            "category not found" => [
                "en" => "Category not found", 
                "uk" => "Категорія не знайдена", 
                "ru" => "Категория не найдена"
            ],
            "unit not found" => [
                "en" => "Unit not found", 
                "uk" => "Од. виміру не знайдено", 
                "ru" => "Ед. измерения не найдено"
            ],
            "article is required" => [
                "en" => "Article is required", 
                "uk" => "Артикул обов'язковий", 
                "ru" => "Артикул обязательный"
            ],
            "name is required" => [
                "en" => "Name is required", 
                "uk" => "Назва обов'язкова", 
                "ru" => "Название обязательное"
            ],
            "barcode is required" => [
                "en" => "Barcode is required", 
                "uk" => "Штрих-код обов'язковий", 
                "ru" => "Штрих-код обязателен"
            ],
            "barcode is invalid" => [
                "en" => "Barcode is invalid", 
                "uk" => "Штрих-код недійсний", 
                "ru" => "Штрих-код недействителен"
            ],
            "barcode is already taken" => [
                "en" => "Barcode is already taken", 
                "uk" => "Штрих-код вже використовується", 
                "ru" => "Штрих-код уже используется"
            ],
            "not file in request" => [
                "en" => "Not file in request", 
                "uk" => "У запиті немає файлу", 
                "ru" => "В запросе нет файла"
            ],
            "an internal barcode is already in use" => [
                "en" => "An internal barcode is already in use",
                "uk" => "Внутрішній штрих-код уже використовується",
                "ru" => "Внутренний штрих-код уже используется"
            ],


            "sum sales is required" => [
                "en" => "Sum sales is required",
                "uk" => "Сума продаж обов'язкова",
                "ru" => "Сума продаж обязательна"
            ],
            "sum returns is required" => [
                "en" => "Sum returns is required",
                "uk" => "Сума повернень обов'язкова",
                "ru" => "Сума возвратов обязательна"
            ],
            "sum pay cash is required" => [
                "en" => "Sum pay cash is required",
                "uk" => "Сума оплат готівкою обов'язкова",
                "ru" => "Сума оплат наличными обязательна"
            ],
            "sum pay cards is required" => [
                "en" => "Sum pay cards is required",
                "uk" => "Сума оплат карткою обов'язкова",
                "ru" => "Сума оплат карточкой обязательна"
            ],
            "sum sales is invalid" => [
                "en" => "Sum sales is invalid",
                "uk" => "Сума продаж вказана невірно",
                "ru" => "Сума продаж указана некорректно"
            ],
            "sum returns is invalid" => [
                "en" => "Sum returns is invalid",
                "uk" => "Сума повернень вказана невірно",
                "ru" => "Сума возвратов указана некорректно"
            ],
            "sum pay cash is invalid" => [
                "en" => "Sum pay cash is invalid",
                "uk" => "Сума оплат готівкою вказана невірно",
                "ru" => "Сума оплат наличными указана некорректно"
            ],
            "sum pay cards is invalid" => [
                "en" => "Sum pay cards is invalid",
                "uk" => "Сума оплат карткою вказана невірно",
                "ru" => "Сума оплат карточкой указана некорректно"
            ],
            "shift contains unfixed documents" => [
                "en" => "Shift contains unfixed documents",
                "uk" => "Зміна містить непроведені документи",
                "ru" => "Смена содержит непроведённые документы"
            ],
            "document is in fixed shift" => [
                "en" => "Document is in fixed shift",
                "uk" => "Документ прив'язано до проведеної зміни",
                "ru" => "Документ привязан к проведённой смене"
            ],
            "specified shift is fixed" => [
                "en" => "Specified shift is fixed",
                "uk" => "Указанная смена уже проведена",
                "ru" => "Вказана зміна вже проведена"
            ],
            "end date must be greater than the start date." => [
                "en" => "End date must be greater than the start date.",
                "uk" => "Дата завершення повинна бути більша за дату початку.",
                "ru" => "Дата завершения должна быть больше даты начала.",
            ],
            "priority cannot be less than 1" => [
                "en" => "Priority cannot be less than 1",
                "uk" => "Приорітет не може бути менше за 1",
                "ru" => "Приоритет не может быть меньше 1",
            ],
            "an action with this priority already exists." => [
                "en" => "An action with this priority already exists.",
                "uk" => "Акція з таким пріоритетом вже існує.",
                "ru" => "Акция с таким приоритетом уже существует.",
            ],
            "a promotion from the amount with current price type already exists" => [
                "en" => "A promotion from the amount with current price type already exists",
                "uk" => "Акція від суми з поточним типом ціни вже існує",
                "ru" => "Акция от суммы с текущим типом цены уже существует",
            ],
            "discount cannot be more than 100%." => [
                "en" => "Discount cannot be more than 100%.",
                "uk" => "Знижка не може перевищувати 100%.",
                "ru" => "Скидка не может быть более 100%.",
            ],
            "price cannot be less than 0" => [
                "en" => "Price cannot be less than 0",
                "uk" => "Ціна не може бути меншою за 0",
                "ru" => "Цена не может быть меньше 0",
            ],
            "document type not specified" => [
                "en" => "Document type not specified",
                "uk" => "Не вказано тип документа",
                "ru" => "Не указано тип документа",
            ],
        ];
    }

    public static function translate($message, $language = "en") {
        if(!in_array($language, ["en", "uk", "ru"]))
            $language = "en";

        $lower_message = strtolower($message);

        $translates = self::getTranslations();

        $translated = $translates[$lower_message][$language];

        return empty($translated) ? $message : $translated;
    }
}
