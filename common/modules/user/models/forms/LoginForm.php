<?php

namespace common\modules\user\models\forms;

use common\modules\user\helpers\LittleBigHelper;
use common\modules\user\models\User;
use common\modules\user\Module;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\base\Model;
use Yii;

class LoginForm extends Model
{

    public $password;
    public $rememberMe = false;
    private $_user = false;
    /**
     * @var string
     */
    public $user_email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rulesArray = [
            [['password', 'user_email'], 'required'],
            [['password', 'user_email'], 'trim'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            ['user_email', 'email'],
            ['password', 'string', 'min' => 6, 'max' => 255],
        ];

        return $rulesArray;
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me'),
            'user_email' => 'E-mail',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {

        /* @var $module Module */
        $module = Yii::$app->getModule('user-management');

        /* How much attempts user can made to login or recover password in $attemptsTimeout seconds interval */
        if (!$module->checkAttempts()) {
            $this->addError('password', Yii::t('app', 'Too many attempts'));
            return false;
        }

        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', Yii::t('app', 'Incorrect e-mail or password.'));
                return false;
            } else {
                if ($user->status == User::STATUS_INACTIVE) {
                    $this->addError('password', Yii::t('app', 'Your account is not active yet.'));
                } else if ($user->status == User::STATUS_BANNED) {
                    $this->addError('password', Yii::t('app', 'Your account is blocked. Please contact the administrator.'));
                } else if ($module->emailConfirmationRequired && !$user->email_confirmed) {
                    $this->addError('email', Yii::t('app', 'You need to confirm your email address.'));
                } else {
                    return true;
                }
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login($validate = true)
    {
        if ($validate) {
            if ($this->validate()) {
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->user->cookieLifetime : 0);
            } else {
                return false;
            }
        } else {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->user->cookieLifetime : 0);
        }
    }

    /**
     * Finds user by [[username]]
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $u = new \Yii::$app->user->identityClass;
            $this->_user = ($u instanceof User ? $u->findByUsername($this->user_email) : User::findByUsername($this->user_email));
        }

        return $this->_user;
    }
}
