<?php

namespace common\modules\user\models\forms;

use common\modules\user\models\User;
use common\modules\user\Module;
use yii\base\Model;
use Yii;

class ChangeOwnPasswordForm extends Model
{
	/**
	 * @var User
	 */
	public $user;

	/**
	 * @var string
	 */
	public $current_password;
	/**
	 * @var string
	 */
	public $password;
	/**
	 * @var string
	 */
	public $repeat_password;


	// public $scenario;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['password', 'repeat_password'], 'required'],
			[['password', 'repeat_password', 'current_password'], 'string', 'max' => 255],
			[['password', 'repeat_password', 'current_password'], 'trim'],

			['repeat_password', 'compare', 'compareAttribute' => 'password'],

			['current_password', 'required'],
			['current_password', 'validateCurrentPassword'],
			['scenario', 'safe']
		];
	}

	public function attributeLabels()
	{
		return [
			'current_password' => Yii::t('app', 'Current password'),
			'password'         => Yii::t('app', 'Password'),
			'repeat_password'  => Yii::t('app', 'Repeat password'),
		];
	}


	/**
	 * Validates current password
	 */
	public function validateCurrentPassword()
	{
		if (!Yii::$app->getModule('user-management')->checkAttempts()) {
			$this->addError('current_password', Yii::t('app', 'Too many attempts'));

			return false;
		}

		if (!Yii::$app->security->validatePassword($this->current_password, $this->user->password_hash)) {
			$this->addError('current_password', Yii::t('app', "Wrong password"));
		}
	}

	/**
	 * @param bool $performValidation
	 *
	 * @return bool
	 */
	public function changePassword($performValidation = true)
	{
		if ($performValidation and !$this->validate()) {
			return false;
		}

		$this->user->email_confirmed = 1;
		$this->user->save(false);


		$this->user->password = $this->password;
		$this->user->removeConfirmationToken();
		return $this->user->save();
	}
}
