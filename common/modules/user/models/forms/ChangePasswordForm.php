<?php
namespace common\modules\user\models\forms;

use common\modules\user\components\ReCaptcha;
use common\modules\user\models\Profile;
use common\modules\user\models\User;
use common\modules\user\Module;
use yii\base\Model;
use Yii;
use yii\helpers\Html;

class ChangePasswordForm extends Model
{
	public $password;
	public $repeat_password;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['password'], 'required'],
            ['password', 'string', 'min' => 6, 'max'=>255],
            ['repeat_password', 'compare','compareAttribute' => 'password']
		];
	}

	
}
