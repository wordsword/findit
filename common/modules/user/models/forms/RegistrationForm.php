<?php

namespace common\modules\user\models\forms;

use Yii;
use app\modules\main\models\Price;
use app\modules\main\models\Store;
use common\modules\user\Module;
use common\modules\user\components\ReCaptcha;
use common\modules\user\models\ContactInfo;
use common\modules\user\models\Partner;
use common\modules\user\models\Profile;
use common\modules\user\models\User;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\base\Model;
use yii\helpers\Html;

class RegistrationForm extends Model
{
    public $username;
    public $phone;
    public $name;
    public $email;
    public $password;
    public $repeat_password;
    public $captcha;
    public $displayFormat = PhoneNumberFormat::E164;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rulesArray = [
            [['username', 'name', 'email', 'password'], 'required'],
            [['username', 'password', 'phone', 'repeat_password'], 'trim'],
            [['phone'], 'phoneValidation'],
            [['email'], 'email'],
            [['username', 'phone', 'email'], 'purgeXSS'],
            [['password', 'repeat_password'], 'string', 'min' => 6, 'max' => 255],
            [['phone', 'username', 'name'], 'string', 'max' => 255],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],
        ];


        return $rulesArray;
    }

    public function phoneValidation()
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneProto = $phoneUtil->parse($this->phone, 'UA');
        if (!$phoneUtil->isValidNumber($phoneProto)) {
            $this->addError('phone', Yii::t('app', 'Phone number is incorrect.'));
        }
        $this->phone = PhoneNumberUtil::getInstance()->format($phoneProto, $this->displayFormat);
    }


    /**
     * Remove possible XSS stuff
     *
     * @param $attribute
     */
    public function purgeXSS($attribute)
    {
        $this->$attribute = Html::encode($this->$attribute);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Nickname'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'E-mail'),
            'password' => Yii::t('app', 'Password'),
            'repeat_password' => Yii::t('app', 'Repeat password'),
            'captcha' => Yii::t('app', 'Captcha'),
            'name' => Yii::t('app', 'Name')
        ];
    }

    /**
     * @param bool $performValidation
     *
     * @return bool|User
     */
    public function registerUser($performValidation = true)
    {
        if ($performValidation && !$this->validate()) {
            return false;
        }

        /* @var $module Module */
        $module = Yii::$app->getModule('user-management');

        $user = new User();
        $user->password = $this->password;
        $user->username = $this->username;
        $user->phone = $this->phone;
        $user->email = $this->email;

        $user->name = $this->name;

        $user->email_confirmed = 0;
        $user->phone_confirmed = 1;

        if (!$module->emailConfirmationRequired) {
            if ($user->email) {
                $user->email_confirmed = 1;
            }
            // if ($user->phone) {
            //     $user->phone_confirmed = 1;
            // }
        }

        /**
         * Статус користувача після реєстрації
         */
        // if ($module->activateAfterRegistration) {
        $user->status = User::STATUS_ACTIVE;
        // } else {
        //     $user->status = User::STATUS_INACTIVE;
        // }

        // If phone confirmation required then we save user with "inactive" status
        if ($module->emailConfirmationRequired) {

            /**
             * Генерація кодів підтвердження коритувача
             */

            /** Токен підтвердження коритувача за допомогою email*/
            $user->generateConfirmationToken();

            /** Код підтвердження коритувача за допомогою телефона*/
            // $user->generateActivationCode();

            $user->save(false);


            if (!$this->sendConfirmationEmail($user)) {
                $this->addError('email', Yii::t('app', 'Could not send confirmation email.'));
            }

            if (!$this->hasErrors()) {
                return $user;
            }
        }

        if ($user->save()) {

            if (!empty($user->phone)) {
                User::sendSMSLP($user, Yii::t('app', 'Thanks for registration'));
            }

            return $user;
        } else {
            foreach($user->getFirstErrors() as $attribute => $error) {
                if(property_exists($this, $attribute))
                    $this->addError($attribute, $error);
            }
            // $this->addError('username', Yii::t('app', 'Login has been taken'));
        }
    }


    /**
     * @param User $user
     *
     * @return bool
     */
    protected function sendConfirmationEmail($user)
    {
        /* @var $module Module */
        $module = Yii::$app->getModule('user-management');

        return Yii::$app->mailer
            ->compose($module->mailerOptions['registrationFormViewFile'], ['user' => $user])
            ->setFrom($module->mailerOptions['from'])
            ->setTo($user->email)
            ->setSubject(Yii::t('app', 'E-mail confirmation for') . ' ' . Yii::$app->name)
            ->send();
    }

    /**
     * Check received confirmation token and if user found - activate it, set username, roles and log him in
     *
     * @param string $token
     *
     * @return bool|User
     */
    public function checkConfirmationToken($token)
    {
        $user = User::findInactiveByConfirmationToken($token);

        /* @var $module Module */
        $module = Yii::$app->getModule('user-management');

        if ($user) {
            $user->email_confirmed = 1;
            $user->removeConfirmationToken();
            $user->save(false);


            if ($module->loginAfterRegistration) {
                Yii::$app->user->login($user);
            }

            return $user;
        }

        return false;
    }
}
