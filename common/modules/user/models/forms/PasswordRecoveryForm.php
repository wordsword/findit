<?php

namespace common\modules\user\models\forms;

use common\modules\user\components\ReCaptcha;
use common\modules\user\models\User;
use common\modules\user\Module;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\base\Model;
use Yii;
use yii\validators\EmailValidator;

class PasswordRecoveryForm extends Model
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $captcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // ['captcha', ReCaptcha::className(),'skipOnEmpty' => false],
            [['email'], 'required'],
            ['email', 'trim'],
            ['email', 'validateEmail','skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @return bool
     */
    public function validateEmail($attr, $params, $validator)
    {
        if (!Yii::$app->getModule('user-management')->checkAttempts()) {
            $this->addError('email', Yii::t('app', 'Too many attempts'));
            return false;
        }

        $e_validator = new EmailValidator();
        if (!$e_validator->validate($this->email)) {
            $this->addError('email', Yii::t('app', 'E-mail is invalid.'));
            return false;
        }

        $user = User::findOne([
            'email' => $this->email,
        ]);


        if (!$user) {
            $this->addError('email', Yii::t('app', 'E-mail not found!'));
            return false;
        }

        $this->user = $user;
        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'E-mail'),
            'captcha' => Yii::t('app', 'Captcha'),
        ];
    }

    /**
     * @param bool $performValidation
     *
     * @return bool
     */
    /*public function sendEmail($performValidation = true)
    {
        if ( $performValidation && !$this->validate() )
        {
            return false;
        }

        // $this->user->generateConfirmationToken();

        $pass = Yii::$app->security->generateRandomString(8);


        $this->user->password = $pass;
        $this->user->save(false);

        return Yii::$app->mailer->compose(
                Yii::$app->getModule('user-management')->mailerOptions['passwordRecoveryFormViewFile'],
                [
                    'user' => $this->user,
                    'newPass' => $pass
                ]
            )
            ->setFrom('hasko2200@gmail.com')
            ->setTo($this->email)
            ->setSubject(Yii::t('app', 'Password reset for') . ' ' . Yii::$app->name)
            ->send();
    }

    public function sendMessage($performValidation = true)
    {
        if ( $performValidation && !$this->validate() )
        {
            return false;
        }

        $pass = Yii::$app->security->generateRandomString(8);


        $this->user->generateConfirmationToken();
        $this->user->generateActivationCode();
        $this->user->save(false);
        return User::sendConfirmationMessage($this->user);

        return User::sendNewPassword($this->user, $pass);
    }

    public function getRecoveryByPhone() {
        return (strpos($this->email,"@") === false);
    }*/


    public function sendEmail($performValidation = true)
    {
        if ($performValidation && !$this->validate()) {
            return false;
        }

        $this->user->generateConfirmationToken();
        $this->user->save(false);

        return Yii::$app->mailer->compose(Yii::$app->getModule('user-management')->mailerOptions['passwordRecoveryFormViewFile'], ['user' => $this->user])
            ->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
            ->setTo($this->email)
            ->setSubject(Yii::t('app', 'Password reset for') . ' ' . Yii::$app->name)
            ->send();
    }

    // public function sendMessage($performValidation = true)
    // {
    //     if ($performValidation && !$this->validate()) {
    //         return false;
    //     }
    //     $this->user->generateConfirmationToken();
    //     $this->user->generateActivationCode();
    //     $this->user->save(false);
    //     return User::sendConfirmationMessage($this->user);
    // }

    // public function getRecoveryByPhone()
    // {
    //     return (strpos($this->email, "@") === false);
    // }


}
