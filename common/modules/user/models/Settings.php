<?php

namespace common\modules\user\models;

use app\components\SkDynamicModel;
use common\modules\user\Module;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for user settings table
 *
 * @property integer $id
 * @property string $field_key
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['field_key', 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['field_key', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field_key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getModel()
    {
        $attributes = [
            'activateAfterRegistration',
//            'phoneConfirmationRequired',
//            'emailConfirmationRequired',
            'loginAfterRegistration',
            'enableRegistration',
            'accountConfirmationRequired'
        ];
        $model = new SkDynamicModel($attributes);
        $model->setFormName('UserSettings');
        $model->addRule($attributes, 'required', ['message' => Yii::t('app', 'Option value cannot be blank.')]);
        $model->setAttributeLabels([
            'enableRegistration' => Yii::t('app', 'Enable registration'),
            'activateAfterRegistration' => Yii::t('app', 'Activate user after rigistration'),
//            'phoneConfirmationRequired' => Yii::t('app', 'Phone confirmaition'),
//            'emailConfirmationRequired' => Yii::t('app', 'Email confirmation'),
            'loginAfterRegistration' => Yii::t('app', 'Login after registration'),
            'accountConfirmationRequired' => Yii::t('app', 'User contacts verification'),
        ]);

        foreach ($attributes as $attribute) {
            $model->$attribute = self::findOne(['field_key' => $attribute])->value;
        }
        return $model;
    }

    public static function saveModel(SkDynamicModel $model)
    {
        $attributes = [
            'activateAfterRegistration',
//            'phoneConfirmationRequired',
//            'emailConfirmationRequired',
            'loginAfterRegistration',
            'enableRegistration',
            'accountConfirmationRequired'
        ];
        foreach ($attributes as $attribute) {
            $settings = self::findOne(['field_key' => $attribute]);
            $settings->value = $model->$attribute;
            $settings->save(false);
        }
    }
}
