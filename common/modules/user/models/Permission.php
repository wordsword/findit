<?php

namespace common\modules\user\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\AdminMenu;

/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 20],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Route'),
        ];
    }

    public static function isLocked($route, $role)
    {

        if($route !== "undefined")
        {

            $menu_id = AdminMenu::find()->select('id')->where(['like', 'url',$route])->scalar();
            // return $role;

            $role = Role::findOne(intval($role));

            if(!empty($role) && !empty($menu_id))
                return !$role->canSee(intval($menu_id));
        }

        return false;
    }



    public static function checkPermission($title)
    {
        $permission = self::find()->where(['title' => $title])->one();
        if(!$permission)
        {
            $route = explode('/', Yii::$app->request->pathInfo);

            $route = implode('/', ["", $route[0], $route[1]]);

            $permission = new self([
                'title' => $title,
                'url' => $route    
            ]);
            $permission->save();

            $roles = Role::find()->select('id')->column();

            foreach ($roles as $role) {
                (new RolePermission(['role_id' => $role, 'permission_id' => $permission->id]))->save();
            }
        }

        return $permission->id;
    }

    public static function checkRoute($url)
    {
        $permission = self::find()->where(['url' => $url])->one();
        if(empty($permission))
        {
            $permission = new self([
                'url' => $url
            ]);
            $permission->save();

            $roles = Role::find()->select('id')->column();

            foreach ($roles as $role) {
                (new RolePermission(['role_id' => $role, 'permission_id' => $permission->id]))->save();
            }
        }
        
        return $permission->id;
    }



    public static function getControllers($permissions = null)
    {
        if(is_null($permissions))
            $permissions = self::find()->asArray()->all();

        $controllers = ["undefined" => []];

        foreach($permissions as $permission)
        {
            $parsed = explode('/', $permission['url']);

            foreach ($parsed as $key => $value) {
                if(empty($value))
                    unset($parsed[$key]);
            }
            // print_r($parsed);
            $parsed = array_values($parsed);

            if(count($parsed) > 1)
            {
                $cont = implode("/",["",$parsed[0], $parsed[1]]);
                $name = str_replace('-',' ', ucfirst($parsed[count($parsed)-1])); 
                $permission['name'] = Yii::t('app', $name);
                if(is_null($controllers[$cont]))
                    $controllers[$cont] = [];
                $controllers[$cont][] = $permission;
            }
            else
                $controllers['undefined'][] = $permission;
        }

        $tmp = [];

        foreach ($controllers as $controller => $actions) {
            if(!empty($actions))
            {
                $tmp[] = [
                    'id' => $actions[0]['id'],
                    'sort_order' => $actions[0]['sort_order'],  
                    'route' => $controller,
                    'actions' => $actions
                ];
            }
        }

        usort($tmp, function($a, $b){
            return $a['sort_order'] <=> $b['sort_order'];
        });

        return $tmp;
    }

    public static function getList()
    {
        $list = self::getControllers();

        $roles = Role::find()->all();

        $columns = [];

        foreach($roles as $role)
        {
            $columns[$role['id']] = $role['title'];
        }


        foreach ($list as $controller => &$items) {
            if(empty($items))
                unset($list[$controller]);
            else
                foreach ($items as &$item) {
                    foreach($roles as $role)
                    {
                        $item[$role['id']] = $role->hasPermission($item['id']);
                    }
                }
        }

        return [
            'items' => $list,
            'columns' => $columns
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
        {
            $parsed = explode('/', $this->url);

            foreach ($parsed as $key => $value) {
                if(empty($value))
                    unset($parsed[$key]);
            }
            // print_r($parsed);
            $parsed = array_values($parsed);

            $route = "undefined";
            $found = false;

            if(count($parsed) > 1)
            {
                $route = implode("/",["",$parsed[0], $parsed[1]]);
            }

            $unc_id = false;

            $controllers = self::getControllers();

            foreach ($controllers as $controller)
            {
                if($route == $controller['route'])
                {
                    $found = $controller['id'];
                }

                if($controller['route'] == "undefined")
                {
                    $unc_id = $controller['id'];
                }
            }

            if($found !== false)
            {
                $params = self::find()->select('group_id, sort_order')->where(['id' => intval($found)])->asArray()->one();

                $this->group_id = $params['group_id'];
                $this->sort_order = $params['sort_order'];
            }
            else
            {
                $this->group_id = 0;
                $this->sort_order = intval(Permission::find()->select('sort_order')->where(['id' => $unc_id])->scalar());
            }
        }
        return parent::beforeSave($insert);
    }

    public static function getListByGroup($group_id)
    {
        $permissions = self::find()->where(['group_id' => $group_id])->asArray()->all();

        $list = self::getControllers($permissions);

        $roles = Role::find()->all();

        $columns = [];

        foreach($roles as $role)
        {
            $columns[$role['id']] = $role['title'];
        }


        foreach ($list as &$controller) {
            foreach ($controller['actions'] as &$item) {
                foreach($roles as $role)
                {
                    $item[$role['id']] = $role->hasPermission($item['id']);
                }
                $controller['columns'] = $columns;
            }
        }

        return [
            'items' => $list,
        ];
    }
}
