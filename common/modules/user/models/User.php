<?php

namespace common\modules\user\models;

use app\modules\main\models\Price;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;
use common\modules\user\Module;
use common\modules\user\components\AuthHelper;
use common\modules\user\components\UserIdentity;
use common\modules\user\helpers\LittleBigHelper;
use common\modules\user\helpers\Singleton;
use common\modules\user\models\DiscountCard;
use thamtech\uuid\helpers\UuidHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $email_confirmed
 * @property string $phone
 * @property string $phone_token
 * @property integer $phone_confirmed
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property integer $status
 * @property integer $superadmin
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends UserIdentity
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_BANNED = -1;

    /**
     * @var string
     */
    public $country = "UA";
    public $displayFormat = PhoneNumberFormat::E164;

    /**
     * @var string
     */
    public $gridRoleSearch;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $repeat_password;

    // public $sms_registration;

    /**
     * Store result in singleton to prevent multiple db requests with multiple calls
     *
     * @param bool $fromSingleton
     *
     * @return static
     */
    public static function getCurrentUser($fromSingleton = true)
    {
        if (!$fromSingleton) {
            return static::findOne(Yii::$app->user->id);
        }

        $user = Singleton::getData('__currentUser');

        if (!$user) {
            $user = static::findOne(Yii::$app->user->id);

            Singleton::setData('__currentUser', $user);
        }

        return $user;
    }

    /**
     * Assign role to user
     *
     * @param int $userId
     * @param string $roleName
     *
     * @return bool
     */
    public static function assignRole($userId, $roleName)
    {
        try {
            Yii::$app->db->createCommand()
                ->insert(Yii::$app->getModule('user-management')->auth_assignment_table, [
                    'user_id' => $userId,
                    'item_name' => $roleName,
                    'created_at' => time(),
                ])->execute();

            AuthHelper::invalidatePermissions();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Revoke role from user
     *
     * @param int $userId
     * @param string $roleName
     *
     * @return bool
     */
    public static function revokeRole($userId, $roleName)
    {
        $result = Yii::$app->db->createCommand()
            ->delete(Yii::$app->getModule('user-management')->auth_assignment_table, ['user_id' => $userId, 'item_name' => $roleName])
            ->execute() > 0;

        if ($result) {
            AuthHelper::invalidatePermissions();
        }

        return $result;
    }

    /**
     * @param string|array $roles
     * @param bool $superAdminAllowed
     *
     * @return bool
     */
    public static function hasRole($roles, $superAdminAllowed = true)
    {
        if ($superAdminAllowed and Yii::$app->user->isSuperadmin) {
            return true;
        }
        $roles = (array) $roles;

        AuthHelper::ensurePermissionsUpToDate();

        return array_intersect($roles, Yii::$app->session->get(AuthHelper::SESSION_PREFIX_ROLES, [])) !== [];
    }

    /**
     * @param string $permission
     * @param bool $superAdminAllowed
     *
     * @return bool
     */
    public static function hasPermission($permission, $superAdminAllowed = true)
    {
        $permission_id = Permission::checkPermission($permission);

        if ($superAdminAllowed and Yii::$app->user->isSuperadmin) {
            return true;
        }


        return (new Query())->from('role_permission rp')
            ->innerJoin('permission p', 'p.id = rp.permission_id')
            ->where('rp.role_id = (SELECT role_id FROM user WHERE id = :user) AND rp.permission_id = :permission')
            ->addParams([
                ':user' => Yii::$app->user->identity->id,
                ':permission' => $permission_id
            ])
            ->count() > 0;
    }

    /**
     * Useful for Menu widget
     *
     * <example>
     *    ...
     *        [ 'label'=>'Some label', 'url'=>['/site/index'], 'visible'=>User::canRoute(['/site/index']) ]
     *    ...
     * </example>
     *
     * @param string|array $route
     * @param bool $superAdminAllowed
     *
     * @return bool
     */
    public static function canRoute($route, $superAdminAllowed = true)
    {
        $permission_id = Permission::checkRoute($route);

        if ($superAdminAllowed and Yii::$app->user->isSuperadmin) {
            return true;
        }


        return (new yii\db\Query())->from('role_permission rp')
            ->innerJoin('permission p', 'p.id = rp.permission_id')
            ->where('rp.role_id = (SELECT role_id FROM user WHERE id = :user) AND rp.permission_id = :permission')
            ->addParams([
                ':user' => Yii::$app->user->identity->id,
                ':permission' => $permission_id
            ])
            ->count() > 0;
    }


    public static function CanSee($menu_id, $superAdminAllowed = true)
    {
        if ($superAdminAllowed and Yii::$app->user->isSuperadmin) {
            return true;
        }

        return (new Query())->from('admin_menu_role rp')
            ->where('rp.role_id = (SELECT role_id FROM user WHERE id = :user) AND rp.admin_menu_id = :menu')
            ->addParams([
                ':user' => Yii::$app->user->identity->id,
                ':menu' => $menu_id
            ])
            ->count() > 0;
    }

    /**
     * getStatusList
     * @return array
     */
    public static function getStatusList()
    {
        return array(
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_BANNED => Yii::t('app', 'Banned'),
        );
    }


    public static function getUserType()
    {
        return array(
            1 => Yii::t('app', 'Retail'),
            2 => Yii::t('app', 'Wholesale'),
            3 => Yii::t('app', 'Dropshipping'),
        );
    }


    /**
     * getStatusValue
     *
     * @param string $val
     *
     * @return string
     */
    public static function getStatusValue($val)
    {
        $ar = self::getStatusList();

        return isset($ar[$val]) ? $ar[$val] : $val;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rulesArray = [
            [['username', 'name', 'email'], 'required'],
            [['username', 'email', 'country'], 'trim'],

            [['status', 'email_confirmed', 'phone_confirmed', 'admin', 'role_id'], 'integer'],

            ['email', 'email'],
            [['phone', 'country', 'avatar', 'name'], 'string'],

            ['email', 'email'],
            [['phone'], 'phoneValidation'],


            ['password', 'required', 'on' => ['newUser', 'changePassword']],
            ['password', 'string', 'min' => 6, 'max' => 255, 'on' => ['newUser', 'changePassword']],
            ['password', 'trim', 'on' => ['newUser', 'changePassword']],

            ['repeat_password', 'required', 'on' => ['newUser', 'changePassword']],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],

            [['email', 'phone', 'username'], 'unique'],
            
            ['licenses', 'safe']
        ];

        return $rulesArray;
    }

    public function phoneValidation()
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneProto = $phoneUtil->parse($this->phone, empty($this->country) ? "UA" : strtoupper($this->country));
        if (!$phoneUtil->isValidNumber($phoneProto)) {
            $this->addError('phone', Yii::t('app', 'Phone number is incorrect.'));
        }
        $this->phone = PhoneNumberUtil::getInstance()->format($phoneProto, $this->displayFormat);
    }

    /**
     * Check that there is no such confirmed E-mail in the system
     */
    public function validateEmailConfirmedUnique()
    {
        if ($this->email) {
            $exists = User::findOne([
                'email' => $this->email,
                'email_confirmed' => 1,
            ]);

            if ($exists and $exists->id != $this->id) {
                $this->addError('email', Yii::t('app', 'This E-mail already exists'));
            }
        }
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('app', 'Username'),
            'admin' => Yii::t('app', 'Administrator'),
            'confirmation_token' => 'Confirmation Token',
            'status' => Yii::t('app', 'Status'),
            'gridRoleSearch' => Yii::t('app', 'Roles'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
            'password' => Yii::t('app', 'Password'),
            'repeat_password' => Yii::t('app', 'Repeat password'),
            'email_confirmed' => Yii::t('app', 'E-mail confirmed'),
            'email' => 'E-mail',
            'phone' => Yii::t('app', 'Phone'),
            'phone_confirmed' => Yii::t('app', 'Phone confirmed'),
            'price_type' => Yii::t('app', 'Price type')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function getRole()
    {
        return Role::findOne($this->role_id);
    }

    /**
     * Make sure user will not deactivate himself and superadmin could not demote himself
     * Also don't let non-superadmin edit superadmin
     *
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->generateAuthKey();
        } else {
            // Console doesn't have Yii::$app->user, so we skip it for console
            if (php_sapi_name() != 'cli') {
                if (Yii::$app->user->id == $this->id) {
                    // Make sure user will not deactivate himself
                    $this->status = static::STATUS_ACTIVE;

                    // Superadmin could not demote himself
                    if (Yii::$app->user->isSuperadmin and $this->superadmin != 1) {
                        $this->superadmin = 1;
                    }
                }

                // Don't let non-superadmin edit superadmin
                if (isset($this->oldAttributes['superadmin']) && !Yii::$app->user->isSuperadmin && $this->oldAttributes['superadmin'] == 1) {
                    return false;
                }
            }
        }

        // If password has been set, than create password hash
        if ($this->password) {
            $this->setPassword($this->password);
        }

        // Cleaning the phone number
        /*$this->phone = preg_replace('~\D+~', '', $this->phone);
        if (strlen($this->phone) < 12) {
            if (strlen($this->phone) < 11) {
                $this->phone = '8' . $this->phone;
            }
            $this->phone = '3' . $this->phone;
        }
        $this->phone = '+' . $this->phone;*/

        return parent::beforeSave($insert);
    }

    /**
     * Don't let delete yourself and don't let non-superadmin delete superadmin
     *
     * @inheritdoc
     */
    public function beforeDelete()
    {
        // Console doesn't have Yii::$app->user, so we skip it for console
        if (php_sapi_name() != 'cli') {
            // Don't let delete yourself
            if (Yii::$app->user->id == $this->id) {
                return false;
            }
            // Don't let non-superadmin delete superadmin
            if (!Yii::$app->user->isSuperadmin and $this->superadmin == 1) {
                return false;
            }
        }

        return parent::beforeDelete();
    }

    public static function sendConfirmationMessage(User $user)
    {
        $text = Yii::t('app', 'Activation code:') . " " . $user->phone_token;
        return Yii::$app->smsClient->sendSMS($user->phone, $text);
    }


    public static function sendSMSLP(User $user, $message = "")
    {
        $text = $message . "\n" . Yii::t('app', 'Login') . ":" . $user->phone . "\n" . Yii::t('app', 'Password') . ":" . $user->password;
        return Yii::$app->smsClient->sendSMS($user->phone, $text);
    }

    public static function sendEmailLP(User $user, $message = "")
    {
        //        $text = $message . "\n" . Yii::t('app', 'Login') . ":" . $user->phone . "\n" . Yii::t('app', 'Password') . ":" . $user->password;
        //        return Yii::$app->smsClient->sendSMS($user->phone, $text);

        return Yii::$app->mailer->compose(Yii::$app->getModule('user-management')->mailerOptions['passwordChangeFormViewFile'], ['user' => $user])
            ->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
            ->setTo($user->email)
            ->setSubject($message)
            ->send();
    }

    public static function sendSMSCreateAcount(User $user)
    {
        $text = Yii::t('app', 'Cabinet created') . PHP_EOL . parse_url(Yii::$app->urlManager->hostInfo, PHP_URL_HOST) . '/login' . PHP_EOL . Yii::t('app', 'Login') . ':' . $user->phone . PHP_EOL . Yii::t('app', 'Password') . ':' . $user->password;
        return Yii::$app->smsClient->sendSMS($user->phone, $text);
    }

    public static function sendSMSMessage(User $user = null, $text, $phone = null)
    {
        if (empty($phone)) {
            $phone = $user->phone;
        }
        return Yii::$app->smsClient->sendSMS($phone, $text);
    }

    public static function sendEmailMessage(User $user = null, $subject, $viewFile, $params)
    {
        if ($params['email']) {
            $email = $params['email'];
        } else {
            if (!empty($user)) {
                $email = $user->email;
            }
        }
        if (!empty($email)) {
            return Yii::$app->mailer->compose($viewFile, ['params' => $params])
                ->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
        }
        return true;
    }

    public function getTokenTimeLeft($inMinutes = false)
    {
        if ($this->confirmation_token) {
            $expire = Yii::$app->getModule('user-management')->confirmationTokenExpire;
            $parts = explode('_', $this->confirmation_token);
            $timestamp = (int) end($parts);

            $timeLeft = $timestamp + $expire - time();

            if ($timeLeft < 0) {
                return 0;
            }

            return $inMinutes ? round($timeLeft / 60) : $timeLeft;
        }

        return 0;
    }


    public static function getListForMessages()
    {
        $query = self::find();
        $query->select(['id', 'CONCAT_WS(" ",username,phone) as username']);
        $query->andWhere('phone  IS NOT NULL');
        return ArrayHelper::map($query->asArray()->all(), 'id', 'username');
    }

    public function afterFind()
    {
        return parent::afterFind();
    }

    public function afterDelete()
    {
        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

}
