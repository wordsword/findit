<?php
namespace common\modules\user\models\forms;

use common\modules\user\components\ReCaptcha;
use common\modules\user\models\Profile;
use common\modules\user\models\User;
use common\modules\user\Module;
use yii\base\Model;
use Yii;
use yii\helpers\Html;

class RegistrationPhoneForm extends Model
{
	public $username;
	public $phone;
	public $email;
	public $password;
	public $repeat_password;
	public $captcha;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['captcha', ReCaptcha::className(),'skipOnEmpty' => false],
			[['username', 'password', 'phone', 'email'], 'required'],
			[['username', 'password', 'phone', 'email'], 'trim'],
			['email','email'],
			[['phone'], 'phoneValidation'],
			[['phone'], 'unique',
				'targetClass'     => 'common\modules\user\models\User',
				'targetAttribute' => 'phone',
			],

			[['email'], 'unique',
				'targetClass'     => 'common\modules\user\models\User',
				'targetAttribute' => 'email',
			],
			[['username','phone', 'email'], 'purgeXSS'],
            ['password', 'string', 'min' => 6, 'max'=>255],
			[['phone','username'], 'string', 'max' => 255],
		];
	}

	public function phoneValidation()
	{
		$this->phone = preg_replace('~\D+~', '', $this->phone);
		if (strlen($this->phone) < 12) {
			if (strlen($this->phone) < 11) {
				$this->phone = '8' . $this->phone;
			}
			$this->phone = '3' . $this->phone;
		}
		$this->phone = '+' . $this->phone;
		if (strlen($this->phone) < 13) {
			$this->addError('phone',Yii::t('app','Phone number is incorrect.'));
		}
	}

	/**
	 * Remove possible XSS stuff
	 *
	 * @param $attribute
	 */
	public function purgeXSS($attribute)
	{
		$this->$attribute = Html::encode($this->$attribute);
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'username' => Yii::t('app', 'Name'),
			'phone'	   => Yii::t('app', 'Phone'),
			'email'	   => Yii::t('app', 'E-mail'),
			'password' => Yii::t('app', 'Password'),
			'captcha'  => Yii::t('app', 'Captcha'),
		];
	}

	/**
	 * @param bool $performValidation
	 *
	 * @return bool|User
	 */
	public function registerUser($performValidation = true)
	{
		if ( $performValidation AND !$this->validate() )
		{
			return false;
		}

		/* @var $module Module */
		$module = Yii::$app->getModule('user-management');

		$user = new User();
		$user->password = $this->password;
		$user->username = $this->username;
		$user->phone = $this->phone;
		$user->email = $this->email;
        if ($module->activateAfterRegistration) {
            $user->status = User::STATUS_ACTIVE;
        } else {
            $user->status = User::STATUS_INACTIVE;
        }

		// If phone confirmation required then we save user with "inactive" status
		if ( $module->phoneConfirmationRequired  || $module->emailConfirmationRequired) {

			$user->generateConfirmationToken();
			if ($module->phoneConfirmationRequired)
				$user->generateActivationCode();
			$user->save(false);

			$this->saveProfile($user);

			if ($module->phoneConfirmationRequired) {
				if (!User::sendConfirmationMessage($user)) {
					$this->addError('phone', Yii::t('app', 'Could not send confirmation message.'));
				}
			}
			if ($module->emailConfirmationRequired) {
				if (!$this->sendConfirmationEmail($user)) {
					$this->addError('email', Yii::t('app', 'Could not send confirmation email.'));
				}
			}

			if (!$this->hasErrors()) {
				return $user;
			}
		}

		if ( $user->save() )
		{
			$this->saveProfile($user);
			return $user;
		}
		else
		{
			$this->addError('username', Yii::t('app', 'Login has been taken'));
		}
	}

	/**
	 * Implement your own logic if you have user profile and save some there after registration
	 *
	 * @param User $user
	 */
	protected function saveProfile($user)
	{
        $profile = new Profile([
            'price_type' => Yii::$app->shopSettings->def_price_type,
            'discount' => Yii::$app->shopSettings->def_discount,
			'only_packages' => Yii::$app->shopSettings->use_only_packs,
            'user_id' => $user->primaryKey,
        ]);

        $profile->save(false);
	}

	/**
	 * @param User $user
	 *
	 * @return bool
	 */
	protected function sendConfirmationEmail($user)
	{
		return Yii::$app->mailer->compose(Yii::$app->getModule('user-management')->mailerOptions['registrationFormViewFile'], ['user' => $user])
			->setFrom(Yii::$app->getModule('user-management')->mailerOptions['from'])
			->setTo($user->email)
			->setSubject(Yii::t('app', 'E-mail confirmation for') . ' ' . Yii::$app->name)
			->send();
	}

	/**
	 * Check received confirmation token and if user found - activate it, set username, roles and log him in
	 *
	 * @param string $token
	 *
	 * @return bool|User
	 */
	public function checkConfirmationToken($token)
	{
		$user = User::findInactiveByConfirmationToken($token);

		if ( $user )
		{
			$user->email_confirmed = 1;
			$user->removeConfirmationToken();
			$user->save(false);

            if (Yii::$app->getModule('user-management')->loginAfterRegistration) {
                Yii::$app->user->login($user);
            }

			return $user;
		}

		return false;
	}
}
