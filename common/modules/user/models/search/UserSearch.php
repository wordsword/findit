<?php

namespace common\modules\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\user\models\User;

/**
 * UserSearch represents the model behind the search form about `common\modules\user\models\User`.
 */
class UserSearch extends User
{
	public $price_type;
	public $created_at;

	public function rules()
	{
		return [
			[['id', 'superadmin', 'status', 'created_at', 'updated_at', 'email_confirmed','phone_confirmed','admin','price_type', 'role_id'], 'integer'],
			[['username', 'email','phone', 'created_at'], 'string'],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = User::find();


		if ( !Yii::$app->user->isSuperadmin )
		{
			$query->where(['superadmin'=>0]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
			],
			'sort'=>[
				'defaultOrder'=>[
					'id'=>SORT_DESC,
				],
			],
		]);

		$this->load($params);


		if (!empty($this->created_at)) {
            $date = explode('-',$this->created_at);
            $query->andWhere('created_at BETWEEN :from AND :to',['from'=>strtotime($date[0]),'to'=>strtotime($date[1].' +1day')]);
        }

		$query->andFilterWhere([
			'id' => $this->id,
			'superadmin' => $this->superadmin,
			'status' => $this->status,
			// 'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'email_confirmed' => $this->email_confirmed,
			'phone_confirmed' => $this->phone_confirmed,
			'admin' => $this->admin
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
		->andFilterWhere(['like', 'email', $this->email])
		->andFilterWhere(['like', 'phone', $this->phone]);

		$query->andFilterWhere(['role_id'=>$this->role_id]);

		return $dataProvider;
	}
}
