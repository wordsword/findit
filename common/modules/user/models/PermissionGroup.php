<?php

namespace common\modules\user\models;

use Yii;

use common\components\SkLangHelper;
use common\components\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;

/**
 * This is the model class for table "permission_group".
 *
 * @property int $id
 */
class PermissionGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission_group';
    }

    public static function langTableName()
    {
        return 'permission_group_lang';
    }

    public function behaviors()
    {
        return [            
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => SkLangHelper::getLangsList(),
                'defaultLanguage' => SkLangHelper::getDefaultLanguage(),
                'langForeignKey' => 'owner_id',
                'tableName' => self::langTableName(),
                'attributes' => [
                    'title',
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['title', 'string'],
            ['title', 'required'],
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    public function beforeDelete()
    {
        Yii::$app->db->createCommand("UPDATE permission set group_id = 0 WHERE group_id = :id", [':id' => $this->id])->execute();
        return parent::beforeDelete();
    }


    public static function getList()
    {
        $groups = self::find()->orderBy('sort_order', SORT_ASC)->multilingual()->all();
        $tmp = [
            [
                'id' => 0,
                'title' => Yii::t('app', 'Uncategorized'),
                'controllers' => Permission::getListByGroup(0)
            ]
        ];

        foreach ($groups as $group) {
            $tmp[] = [
                'id' => $group->id,
                'title' => $group->title,
                'controllers' => Permission::getListByGroup($group->id)
            ];
        }

        return $tmp;
    }
}
