<?php

namespace common\modules\user\models;

use Yii;

/**
 * This is the model class for table "admin_menu_role".
 *
 * @property int $id
 * @property int $admin_menu_id
 * @property int $role_id
 */
class AdminMenuRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_menu_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_menu_id', 'role_id'], 'required'],
            [['admin_menu_id', 'role_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'admin_menu_id' => Yii::t('app', 'Admin Menu ID'),
            'role_id' => Yii::t('app', 'Role ID'),
        ];
    }
}
