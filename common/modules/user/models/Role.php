<?php

namespace common\modules\user\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $title
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'default_route'], 'required'],
            ['title', 'unique'],
            ['permissions', 'safe'],
            [['title', 'default_route'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'default_route' => Yii::t('app', 'Default route'),
        ];
    }

    public function hasPermission($permission_id)
    {
        return RolePermission::find()->where(['role_id' => $this->id, 'permission_id' => $permission_id])->count() > 0;
    }


    public function canSee($menu_id)
    {
        return AdminMenuRole::find()->where(['admin_menu_id' => $menu_id, 'role_id' => $this->id])->count() > 0;
    }

    public function afterSave($insert, $changeAttributes)
    {
        if ($insert) {
            $permissions = Permission::find()->select('id')->column();
            $menu_items = \common\models\AdminMenu::find()->select('id')->column();

            foreach ($permissions as $permission) {
                (new RolePermission(['permission_id' => $permission, 'role_id' => $this->id]))->insert();
            }

            foreach ($menu_items as $item) {
                (new AdminMenuRole(['admin_menu_id' => $item, 'role_id' => $this->id]))->insert();
            }
        }
        parent::afterSave($insert, $changeAttributes);
    }

    public function beforeDelete()
    {
        if ($this->predefined == 1)
            return false;

        RolePermission::deleteAll(['role_id' => $this->id]);
        AdminMenuRole::deleteAll(['role_id' => $this->id]);
        return parent::beforeDelete();
    }


    private static $_roles;

    public static function getRole($role_id = null)
    {

        if (!isset(self::$_roles)) {
            $arr = self::find()->select(['title', 'id'])->asArray()->all();
            self::$_roles = ArrayHelper::map($arr, 'id', 'title');
            unset($arr);
        }
        if ($role_id) {
            return self::$_roles[$role_id];
        } else {
            return self::$_roles;
        }
    }
}
