<?php

namespace common\modules\user;

use common\modules\user\models\Settings;
use Yii;
use yii\helpers\ArrayHelper;

class Module extends \yii\base\Module
{
    const SESSION_LAST_ATTEMPT = '_um_last_attempt';
    const SESSION_ATTEMPT_COUNT = '_um_attempt_count';

    private $_attributes = [];

    public $accountConfirmationRequired = false;

    /**
     *
     * If set true, then on after registration user status will be "active"
     *
     * @var bool
     */
    public $activateAfterRegistration = true;

    /**
     *
     * If set true, then on after registration message with activation code will be sent
     * to user phone and after confirmation user status will be "active"
     *
     * @var bool
     */
    public $phoneConfirmationRequired = false;

    /**
     *
     * If set true, then on after registration message with activation code will be sent
     * to user email and after confirmation user status will be "active"
     *
     * @var bool
     */
    public $emailConfirmationRequired = true;

    /**
     * Works only if $activateAfterRegistration = true
     *
     * If set true, then on after registration, user will be logged in
     *
     * @var bool
     */
    public $loginAfterRegistration = true;

    /**
     * Registration can be enabled either by this option or by adding '/user-management/auth/registration' route
     * to guest permissions
     * @var bool
     */
    public $enableRegistration = true;


    /**
     * Params for mailer
     * They will be merged with $_defaultMailerOptions
     *
     * @var array
     * @see $_defaultMailerOptions
     */
    public $mailerOptions = [];

    /**
     * Default options for mailer
     *
     * @var array
     */
    protected $_defaultMailerOptions = [
        'from' => '', // If empty it will be - [Yii::$app->params['adminEmail'] => Yii::$app->name . ' robot']
        'registrationFormViewFile' => '@common/modules/user/views/mail/registrationEmailConfirmation',
        'passwordRecoveryFormViewFile' => '@common/modules/user/views/mail/passwordRecoveryMail',
        'passwordChangeFormViewFile' => '@common/modules/user/views/mail/passwordChangeMail',
        'confirmEmailFormViewFile' => '@common/modules/user/views/mail/emailConfirmationMail',
    ];


    /**
     * You can define your own registration form class here.
     * Together with theming registration view file you can ultimately customize registration process.
     *
     * See AuthController::actionRegistration() and RegistrationForm how they work together
     *
     * @var string
     */
    public $registrationFormClass = 'common\modules\user\models\forms\RegistrationForm';

    /**
     * After how many seconds confirmation token will be invalid
     *
     * @var int
     */
    public $confirmationTokenExpire = 3600; // 1 hour



    /**
     * How much attempts user can made to login or recover password in $attemptsTimeout seconds interval
     *
     * @var int
     */
    public $maxAttempts = 5;

    /**
     * Number of seconds after attempt counter to login or recover password will reset
     *
     * @var int
     */
    public $attemptsTimeout = 60;

    /**
     * Options for registration and password recovery captcha
     *
     * @var array
     */
    public $captchaOptions = [
        'class' => 'yii\captcha\CaptchaAction',
        'minLength' => 3,
        'maxLength' => 4,
        'offset' => 5
    ];

    /**
     * Table aliases
     *
     * @var string
     */
    public $user_table = '{{%user}}';
    public $user_visit_log_table = '{{%user_visit_log}}';
    public $controllerNamespace = 'common\modules\user\controllers';


    public function init()
    {
        parent::init();

        $this->prepareMailerOptions();

        if (Yii::$app->id !== 'app-console') {
            $this->_attributes = ArrayHelper::map(Settings::find()->all(), 'field_key', 'value');
        }

        // $this->enableRegistration = Yii::$app->userSettings->enableRegistration;
        // $this->activateAfterRegistration = Yii::$app->userSettings->activateAfterRegistration;
        // $this->loginAfterRegistration = Yii::$app->userSettings->loginAfterRegistration;
        // $this->emailConfirmationRequired = Yii::$app->userSettings->emailConfirmationRequired;
        // $this->phoneConfirmationRequired = Yii::$app->userSettings->phoneConfirmationRequired;
        // $this->accountConfirmationRequired = Yii::$app->userSettings->accountConfirmationRequired;
    }


    /**
     * Check how much attempts user has been made in X seconds
     *
     * @return bool
     */
    public function checkAttempts()
    {
        $lastAttempt = Yii::$app->session->get(static::SESSION_LAST_ATTEMPT);

        if ($lastAttempt) {
            $attemptsCount = Yii::$app->session->get(static::SESSION_ATTEMPT_COUNT, 0);

            Yii::$app->session->set(static::SESSION_ATTEMPT_COUNT, ++$attemptsCount);

            // If last attempt was made more than X seconds ago then reset counters
            if (($lastAttempt + $this->attemptsTimeout) < time()) {
                Yii::$app->session->set(static::SESSION_LAST_ATTEMPT, time());
                Yii::$app->session->set(static::SESSION_ATTEMPT_COUNT, 1);

                return true;
            } elseif ($attemptsCount > $this->maxAttempts) {
                return false;
            }

            return true;
        }

        Yii::$app->session->set(static::SESSION_LAST_ATTEMPT, time());
        Yii::$app->session->set(static::SESSION_ATTEMPT_COUNT, 1);

        return true;
    }

    /**
     * Merge given mailer options with default
     */
    protected function prepareMailerOptions()
    {
        if (!isset($this->mailerOptions['from'])) {
            $this->mailerOptions['from'] = [Yii::$app->params['adminEmail'] => Yii::$app->name];
        }

        $this->mailerOptions = ArrayHelper::merge($this->_defaultMailerOptions, $this->mailerOptions);
    }

    public function __get($name)
    {
        if (isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        } else {
            return parent::__get($name);
        }
    }
}
