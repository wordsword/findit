$(document).ready(function () {

    $(document).on('pjax:success', function (event, data, status, xhr, options) {
        if ($(options.target).hasClass('phone-confirmation')) {
            var content = $('#pjax-container');
            $.fancybox.open({
                src: content.removeClass('hidden body-container'),
                type: 'html',
                touch: false,
                buttons: false,
                smallBtn: false,
                afterClose: function (instance, current) {
                    content.remove();
                }
            });
        }
    });

    $(document).on('pjax:end', function () {
        if ($('[data-phone-confirmation]').length > 0) {
            var link = $('[data-phone-confirmation]');
            link.removeAttr('data-phone-confirmation');
            $('body').remove('#pjax-container');
            var container = $('<div id="pjax-container" class="hidden body-container"/>');
            $('body').append(container);
            $.pjax({
                target: link,
                container: '#pjax-container',
                url: link.attr('data-url'),
                scrollTo: false,
                push: false,
                timeout: 10000
            });
        }
    });
});