<?php

namespace common\modules\user\helpers;

use common\modules\user\models\User;
use Yii;
use yii\helpers\Html;
use yii\web\View;

class Messenger
{
    const ACTION_REGISTRATION = 'registration';
    const ACTION_ORDER = 'order';
    const ACTION_USER_MESSAGE = 'message';
    const ACTION_REVIEW = 'review';

    public static function sendAdminMessages($type, $params)
    {
        $view = new View();
        $users = User::find()->where(['admin' => 1, 'status' => User::STATUS_ACTIVE])->all();
        foreach ($users as $user) {
            switch ($type) {
                case self::ACTION_REGISTRATION :
                    if ($user->sms_registration) {
                        User::sendSMSMessage($user, $view->render('@common/modules/user/views/messages/sms-registration', $params));
                    }
                    if ($user->profile->email_registration) {
                        User::sendEmailMessage($user, Yii::t('app', 'New registration'), '@common/modules/user/views/messages/email-registration', $params);
                    }
                    break;
                case self::ACTION_ORDER :
                    if ($user->profile->sms_order) {
                        User::sendSMSMessage($user, $view->render('@common/modules/user/views/messages/sms-order', $params));
                    }
                    if ($user->profile->email_order) {
                        User::sendEmailMessage($user, Yii::t('app', 'New order'), '@common/modules/user/views/messages/email-order', $params);
                    }
                    break;
                case self::ACTION_USER_MESSAGE :
                    if ($user->profile->sms_user_message) {
                        User::sendSMSMessage($user, $view->render('@common/modules/user/views/messages/sms-user-message', $params));
                    }
                    if ($user->profile->email_user_message) {
                        User::sendEmailMessage($user, Html::encode($params['model']->subject), '@common/modules/user/views/messages/email-user-message', $params);
                    }
                    break;
                case self::ACTION_REVIEW :
                    if ($user->profile->sms_review) {
                        User::sendSMSMessage($user, $view->render('@common/modules/user/views/messages/sms-review', $params));
                    }
                    if ($user->profile->email_review) {
                        User::sendEmailMessage($user, Yii::t('app', 'New review'), '@common/modules/user/views/messages/email-review', $params);
                    }
                    break;
            }
        }
    }

}