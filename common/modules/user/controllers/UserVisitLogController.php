<?php

namespace common\modules\user\controllers;

use Yii;
use common\modules\user\models\UserVisitLog;
use common\modules\user\models\search\UserVisitLogSearch;
use common\modules\user\components\AdminDefaultController;

/**
 * UserVisitLogController implements the CRUD actions for UserVisitLog model.
 */
class UserVisitLogController extends AdminDefaultController
{
	/**
	 * @var UserVisitLog
	 */
	public $modelClass = 'common\modules\user\models\UserVisitLog';

	/**
	 * @var UserVisitLogSearch
	 */
	public $modelSearchClass = 'common\modules\user\models\search\UserVisitLogSearch';

	public $enableOnlyActions = ['index', 'view', 'grid-page-size'];
}
