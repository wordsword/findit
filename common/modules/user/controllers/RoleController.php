<?php

namespace common\modules\user\controllers;

use Yii;
use yii\db\Query;
use yii\rbac\DbManager;
use common\models\AdminMenu;
use yii\helpers\ArrayHelper;
use common\modules\user\Module;
use common\modules\user\models\Role;
use common\modules\user\models\Permission;
use common\modules\user\components\AuthHelper;
use common\modules\user\models\rbacDB\search\RoleSearch;
use common\modules\user\components\AdminDefaultController;

class RoleController extends \common\modules\user\components\BaseController
{

	public function actionIndex()
	{
		Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => Role::find()
		]);

		return $this->render('index', compact('dataProvider'));
	}

	public function actionUpdate($id = null)
	{
		if(isset($id))
			$model = $this->findModel($id);
		else
			$model = new Role();

		if($model->load(Yii::$app->request->post()) && $model->save())
		{
			return $this->redirect(['index']);
		}

		$temp = (new Query)
			->from(AdminMenu::tableName() . ' main')
			->select([
				'main.url',
				'lang.title'
			])
			->innerJoin(AdminMenu::langTableName() . ' lang', 'lang.owner_id = main.id')
			->where('main.id NOT IN (SELECT DISTINCT parent_id FROM admin_menu)')
			->andWhere([
				'lang.language' => Yii::$app->language
			])
			->having(['!=', 'url', ''])
			->all();

		// echo '<pre>';
		// print_r($temp);
		// echo '</pre>';
		// die;

		$routes = ArrayHelper::map($temp, 'url', 'title');

		// $routes = [];

		// foreach ($temp as $route) {
		// 	$routes[$route] = $route;
		// }

		return $this->render('update', compact('model', 'routes'));
	}

	public function actionDelete($id)
	{
		if(!Yii::$app->request->isAjax)
			throw new \yii\web\BadRequestHttpException();

		$this->findModel($id)->delete();

		return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
	}


	private function findModel($id)
	{
		if($mod = Role::findOne($id))
			return $mod;
		else
			throw new \yii\web\NotFoundHttpException("Permission not found");
	}
}