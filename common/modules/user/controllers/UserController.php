<?php

namespace common\modules\user\controllers;

use Yii;
use yii\db\Query;
use kartik\mpdf\Pdf;
use app\modules\main\models\Price;
use app\modules\main\models\Store;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use common\models\PartnerSettlement;
use common\modules\user\models\Role;
use common\modules\user\models\User;
use yii\web\BadRequestHttpException;
use app\modules\uagent\models\License;
use common\modules\user\models\Partner;
use common\modules\user\models\Profile;
use common\modules\user\models\TradeSpot;
use common\modules\user\models\ContactFace;
use common\modules\user\models\ContactInfo;
use common\modules\user\models\DiscountCard;
use common\modules\user\models\ProfileField;
use common\modules\user\models\search\UserSearch;
use common\modules\user\components\AdminDefaultController;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminDefaultController
{
    /**
     * @var User
     */
    public $modelClass = 'common\modules\user\models\User';

    /**
     * @var UserSearch
     */
    public $modelSearchClass = 'common\modules\user\models\search\UserSearch';

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'newUser']);

        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->post('save') == 1) {

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->renderIsAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionChangeGroup()
    {
        if (!Yii::$app->request->isAjax)
            throw new BadRequestHttpException(Yii::t('app', 'Bad Request.'));
        $recid = $_POST['group'];
        $keys = $_POST['keys'];
        if (count($keys) > 0) {
            return Yii::$app->db->createCommand()->update(User::tableName(), [
                'parent_id' => $recid,
            ], ['in', 'id', $keys])->execute();
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->scenarioOnUpdate) {
            $model->scenario = $this->scenarioOnUpdate;
        }


        $newRecord = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->post('save') == 1) {
            
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        //      if (!empty($model->path)) {
        //          $files = scandir(Yii::getAlias('@frontend/web' . $model->path));
        //      }

        $roles = \yii\helpers\ArrayHelper::map(Role::find()->asArray()->all(), 'id', 'title');

        return $this->renderIsAjax('update', [
            'model' => $model,
            'roles' => $roles,
            //          'files'=>$files
        ]);
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        $redirect = $this->getRedirectPage('delete', $model);

        return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
        return $redirect === false ? '' : $this->redirect($redirect);
    }



    /**
     * @param int $id User ID
     *
     * @throws \yii\web\NotFoundHttpException
     * @return string
     */
    public function actionChangePassword($id)
    {
        $model = User::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('User not found');
        }

        $model->scenario = 'changePassword';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->primaryKey]);
        }

        return $this->renderIsAjax('changePassword', compact('model'));
    }
}
