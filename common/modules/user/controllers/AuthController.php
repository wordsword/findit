<?php

namespace common\modules\user\controllers;

use app\modules\main\models\Document;
use common\modules\user\components\BaseController;
use common\modules\user\components\UserAuthEvent;
use common\modules\user\helpers\Messenger;
use common\modules\user\models\forms\ChangeOwnPasswordForm;
use common\modules\user\models\forms\ConfirmEmailForm;
use common\modules\user\models\forms\ConfirmPhoneForm;
use common\modules\user\models\forms\LoginForm;
use common\modules\user\models\forms\LoginPhoneForm;
use common\modules\user\models\forms\PasswordRecoveryForm;
use common\modules\user\models\forms\RegistrationForm;
use common\modules\user\models\forms\RegistrationPhoneForm;
use common\modules\user\models\User;
use common\modules\user\Module;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\di\NotInstantiableException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AuthController extends \yii\web\Controller
{
    /**
     * @var array
     */
    public $freeAccessActions = [
        'login',
        'login-phone',
        'logout',
        'confirm-phone',
        'confirm-email',
        'confirm-registration-phone',
        'confirm-registration-email',
        'confirm-email-receive',
        'change-own-password',
        'registration',
        'registration-phone',
        'password-recovery',
        'password-recovery-receive',
        'profile',
        'profile-confirm-phone',
        'order'
    ];

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => $this->module->captchaOptions,
        ];
    }

    /**
     * Login form
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        Yii::$app->user->setReturnUrl(Yii::$app->request->referrer);

        $model = new LoginForm(['rememberMe' => 1]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $user = $model->getUser();

            /** Перевірка підтвердження контактних даних користувача */
            if ($this->module->accountConfirmationRequired && !$user->email_confirmed) {
                return $this->render('registrationWaitForEmailConfirmation', compact('user'));
            } else {

                $model->login(false);

                if (Yii::$app->user->identity->admin || Yii::$app->user->identity->superadmin) {
                    \common\modules\user\models\UserVisitLog::newVisitor(Yii::$app->user->identity->id);
                    return $this->redirect('/admin');
                } else
                    return $this->redirect('/');
            }
        }
        return $this->render('login', compact('model'));
    }


    /**
     * Logout and redirect to home page
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('/');
    }

    /**
     * Change your own password
     *
     * @throws \yii\web\ForbiddenHttpException
     * @return string|\yii\web\Response
     */
    public function actionChangeOwnPassword()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::getCurrentUser();

        if ($user->status != User::STATUS_ACTIVE) {
            if (!Yii::$app->request->isPjax) {
                throw new ForbiddenHttpException();
            } else {
                return $this->render('error', [
                    'name' => Yii::t('app', 'Access is denied') . ' (#403):',
                    'message' => Yii::t('app', 'You are not allowed to perform this action.')
                ]);
            }
        }

        if ($user->password_hash == '') {
            $model = new ChangeOwnPasswordForm([
                'user' => $user
            ]);
        } else {
            $model = new ChangeOwnPasswordForm(['user' => $user]);
        }


        if ($model->load(Yii::$app->request->post()) and $model->changePassword()) {
            User::sendSMSLP($user, Yii::t('app', 'Password has been changed!'));
            return $this->render('changeOwnPasswordSuccess');
        }

        return $this->render('changeOwnPassword', [
            'model' => $model,
            'action' => ['/user-management/auth/change-own-password']
        ]);
    }


    public function actionRegistration()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        /* @var $model Model */
        $model = new $this->module->registrationFormClass();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            // Trigger event "before registration" and checks if it's valid
            if ($this->triggerModuleEvent(UserAuthEvent::BEFORE_REGISTRATION, ['model' => $model])) {
                $user = $model->registerUser(false);
                // Trigger event "after registration" and checks if it's valid
                if ($this->triggerModuleEvent(UserAuthEvent::AFTER_REGISTRATION, ['model' => $model, 'user' => $user])) {

                    /**
                     * Якщо користувач успішно зареєструвався
                     */
                    if ($user) {

                        /**
                         * Відправлення повідомлення адміну про нову реєстрацію на сайті
                         */
                        // Messenger::sendAdminMessages(Messenger::ACTION_REGISTRATION, ['user' => $user]);

                        /** Перевірка підтвердження контактних даних користувача */
                        if ($this->module->emailConfirmationRequired) {

                            return $this->render('registrationWaitForEmailConfirmation', compact('user'));
                        } else {
                            /**
                             * Перевірка статуса активації користувача після реєстрації
                             */

                            /** Перевірка права автризації користувача після реєстрації */
                            if ($this->module->activateAfterRegistration && $this->module->loginAfterRegistration) {

                                Yii::$app->user->login($user);
                            }

                            /**
                             * Повідомлення про успішну реєстацію на сайті
                             */
                            return $this->render('registrationSuccess', compact('user'));
                        }
                    }
                }
            }
        }
        return $this->render('registration', compact('model', 'wishlist_image'));
    }


    public function actionPasswordRecovery()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordRecoveryForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($this->triggerModuleEvent(UserAuthEvent::BEFORE_PASSWORD_RECOVERY_REQUEST, ['model' => $model])) {
                if ($model->sendEmail(false)) {
                    if ($this->triggerModuleEvent(UserAuthEvent::AFTER_PASSWORD_RECOVERY_REQUEST, ['model' => $model])) {
                        return $this->render('passwordRecoverySuccess');
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', "Unable to send message for email provided"));
                }
            }
        }

        return $this->render('passwordRecovery', compact('model'));
    }

    /**
     * Receive token, find user by it and show form to change password
     *
     * @param string $token
     *
     * @throws \yii\web\BadRequestHttpException
     * @return string|\yii\web\Response
     */
    public function actionPasswordRecoveryReceive($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::findByConfirmationToken($token);

        if (!$user) {
            if (!Yii::$app->request->isPjax) {
                throw new BadRequestHttpException(Yii::t('app', 'Token not found. It may be expired. Try reset password once more.'));
            } else {
                return $this->render('error', [
                    'name' => Yii::t('app', 'Bad Request') . ' (#400):',
                    'message' => Yii::t('app', 'Token not found. It may be expired. Try reset password once more.')
                ]);
            }
        }

        $model = new ChangeOwnPasswordForm([
            'scenario' => 'restoreViaEmail',
            'user' => $user,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($this->triggerModuleEvent(UserAuthEvent::BEFORE_PASSWORD_RECOVERY_COMPLETE, ['model' => $model])) {
                $model->changePassword(false);

                if ($this->triggerModuleEvent(UserAuthEvent::AFTER_PASSWORD_RECOVERY_COMPLETE, ['model' => $model])) {
                    User::sendEmailLP($user, Yii::t('app', 'Password has been changed!'));

                    return $this->render('changeOwnPasswordSuccess');
                }
            }
        }

        return $this->render('changeOwnPassword', [
            'model' => $model,
            'action' => ['/user-management/auth/password-recovery-receive', 'token' => $user->confirmation_token]
        ]);
    }

    public function actionConfirmRegistrationEmail($token)
    {
        $model = new $this->module->registrationFormClass;

        $user = $model->checkConfirmationToken($token);

        if ($user) {
            return $this->render('registrationConfirmEmailSuccess', compact('user'));
        }
        if (!Yii::$app->request->isPjax) {
            throw new BadRequestHttpException(Yii::t('app', 'Token not found. It may be expired.') . "\n" . Yii::t('app', 'Please contact the administrator.'));
        } else {
            return $this->render('error', [
                'name' => Yii::t('app', 'Bad Request') . ' (#400):',
                'message' => Yii::t('app', 'Token not found. It may be expired.') . '<br>' . Yii::t('app', 'Please contact the administrator.')
            ]);
        }
    }


    /**
     * Universal method for triggering events like "before registration", "after registration" and so on
     *
     * @param string $eventName
     * @param array $data
     *
     * @return bool
     */


    /**
     * @return string|\yii\web\Response
     */
    public function actionConfirmEmail()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::getCurrentUser();

        if ($user->email_confirmed == 1) {
            return $this->redirect(['/user-management/auth/profile']);
        }

        $user->generateConfirmationToken();
        $user->save(false);

        $model = new ConfirmEmailForm([
            'email' => $user->email,
            'user' => $user,
        ]);

        if ($model->validate()) {
            if ($this->triggerModuleEvent(UserAuthEvent::BEFORE_EMAIL_CONFIRMATION_REQUEST, ['model' => $model])) {
                if ($model->sendEmail(false)) {
                    if ($this->triggerModuleEvent(UserAuthEvent::AFTER_EMAIL_CONFIRMATION_REQUEST, ['model' => $model])) {
                        return $this->render(Yii::$app->request->isPjax ? 'confirmEmailPjax' : 'confirmEmail', compact('model'));
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', "Unable to send message for email provided"));
                }
            }
        }

        return $this->render(Yii::$app->request->isPjax ? 'confirmEmailPjax' : 'confirmEmail', compact('model'));
    }


    /**
     * Receive token, find user by it and confirm email
     *
     * @param string $token
     *
     * @throws \yii\web\BadRequestHttpException
     * @return string|\yii\web\Response
     */
    public function actionConfirmEmailReceive($token)
    {
        $user = User::findByConfirmationToken($token);

        if (!$user) {
            if (!Yii::$app->request->isPjax) {
                throw new BadRequestHttpException(Yii::t('app', 'Token not found. It may be expired.'));
            } else {
                return $this->render('error', [
                    'name' => Yii::t('app', 'Bad Request') . ' (#400):',
                    'message' => Yii::t('app', 'Token not found. It may be expired.')
                ]);
            }
        }

        $user->email_confirmed = 1;
        $user->removeConfirmationToken();

        $user->save(false);

        return $this->render('confirmEmailSuccess', compact('user'));
    }

    protected function triggerModuleEvent($eventName, $data = [])
    {
        $event = new UserAuthEvent($data);

        $this->module->trigger($eventName, $event);

        return $event->isValid;
    }

    public function actionProfile()
    {
        if (Yii::$app->user->isGuest || !Yii::$app->settings->shop_status) {
            return $this->goHome();
        }

        $user = User::getCurrentUser();

        $attributes = ['username', 'phone', 'email', 'subscribe'];
        $profileAttr = ['address', 'birthday', 'user_type'];
        $changedAttr = [];
        $message = '';
        $phoneConfirmation = false;

        $profile = $user->profile;

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            foreach ($attributes as $attribute) {
                if ($user->oldAttributes[$attribute] != $user->$attribute) {
                    $changedAttr[] = $attribute;
                }
            }

            if ($this->module->emailConfirmationRequired && in_array('email', $changedAttr)) {
                if (!empty($user->email)) {
                    $user->email_confirmed = 1;
                } else {
                    $user->email_confirmed = 0;
                }
            }

            if (count($changedAttr) && $user->save(false) && $profile->save(false)) {
                $message = Yii::t('app', 'Saved') . $message;
                Yii::$app->session->setFlash('savedSuccess', $message);
            }
        }


        return $this->render('user-profile', [
            'model' => $user,
            'profile' => $profile,
        ]);
    }
}
