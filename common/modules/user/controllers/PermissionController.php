<?php

namespace common\modules\user\controllers;


use common\modules\user\models\Permission;
use common\modules\user\models\PermissionGroup;
use common\modules\user\models\RolePermission;
use common\modules\user\models\Role;
use common\modules\user\Module;

use yii\helpers\Inflector;
use Yii;

class PermissionController extends \common\modules\user\components\BaseController
{

    public function actionIndex()
    {
        Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
        $groups = PermissionGroup::getList();

        // echo "<pre>";
        // print_r($groups);


        return $this->render('index', compact('groups'));
    }



    private function getRouteRecrusive($module, &$result)
    {
        foreach ($module->getModules() as $id => $child) {
            if (($child = $module->getModule($id)) !== null) {
                $this->getRouteRecrusive($child, $result);
            }
        }
        foreach ($module->controllerMap as $id => $type) {
            $this->getControllerActions($type, $id, $module, $result);
        }
        $namespace = trim($module->controllerNamespace, '\\') . '\\';
        $this->getControllerFiles($module, $namespace, '', $result);
        $result[] = ($module->uniqueId === '' ? '' : '/' . $module->uniqueId) . '/*';
    }
    private function getControllerFiles($module, $namespace, $prefix, &$result)
    {
        $path = @Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file)) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $id = Inflector::camel2id(substr(basename($file), 0, -14));
                    $className = $namespace . Inflector::id2camel($id) . 'Controller';
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }



    private function getIdStr($acts)
    {
        return "(" . implode(",", array_column($acts, 'id')) . ")";
    }

    private function moveElement(&$array, $a, $b)
    {
        $out = array_splice($array, $a, 1);
        array_splice($array, $b, 0, $out);
    }

    public function actionSort()
    {
        if (!Yii::$app->request->isPost)
            throw new \yii\web\BadRequestHttpException();

        $curr = $_POST['sortable']['curr'];
        $next = $_POST['sortable']['next'];

        $controllers = Permission::getControllers();

        $curr_key = 0;
        $next_key = 0;

        foreach ($controllers as $key => $controller) {
            if ($controller['id'] == $curr)
                $curr_key = $key;

            if ($controller['id'] == $next)
                $next_key = $key;
        }

        if (is_null($next))
            $next_key = count($controllers);

        if ($next_key < $curr_key)
            $this->moveElement($controllers, $curr_key, $next_key);
        else
            $this->moveElement($controllers, $curr_key, $next_key - 1);

        foreach ($controllers as $key => $controller) {
            Yii::$app->db->createCommand("UPDATE permission SET sort_order = " . $key . " WHERE id IN " . $this->getIdStr($controller['actions']))->execute();
        }

        return json_encode(array_column($controllers, 'id'));
    }


    public function actionChangeGroup($id, $group)
    {
        if (!Yii::$app->request->isAjax)
            throw new \yii\web\BadRequestHttpException();

        $controllers = Permission::getControllers();

        foreach ($controllers as $controller) {
            if ($controller['id'] == $id) {
                Yii::$app->db->createCommand("UPDATE permission SET group_id = " . $group . " WHERE id IN " . $this->getIdStr($controller['actions']))->execute();

                return json_encode([
                    'ids' => $this->getIdStr($controller['actions']),
                    'success' => true
                ]);
                break;
            }
        }
    }

    public function actionCreateGroup()
    {
        $model = new PermissionGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('_group_form', compact('model'));
    }

    public function actionUpdateGroup($id)
    {
        $model = PermissionGroup::find()->where(['id' => $id])->multilingual()->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('_group_form', compact('model'));
    }

    public function actionDeleteGroup($id)
    {
        $model = PermissionGroup::findOne($id)->delete();
        Yii::$app->db->createCommand("UPDATE permission SET group_id = 0 WHERE group_id = " . $id)->execute();
        // return $this->redirect(['index']);

        return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
    }

    public function actionSortGroups($curr, $next = null)
    {
        if (!Yii::$app->request->isAjax)
            throw new \yii\web\BadRequestHttpException();

        $groups = PermissionGroup::find()->select('id')->orderBy('sort_order', SORT_ASC)->column();

        $curr_key = 0;
        $next_key = 0;

        foreach ($groups as $key => $group) {
            if (intval($group) == intval($curr))
                $curr_key = $key;

            if (intval($group) == intval($next))
                $next_key = $key;
        }

        if (is_null($next))
            $next_key = count($groups) - 1;

        $this->moveElement($groups, $curr_key, $next_key);

        foreach ($groups as $key => $group) {
            Yii::$app->db->createCommand("UPDATE permission_group SET sort_order = :key WHERE id = :id", [':key' => $key, ':id' => $group])->execute();
        }

        return json_encode([$groups]);
    }

    /**
     * Get list action of controller
     *
     * @param mixed $type
     * @param string $id
     * @param \yii\base\Module $module
     * @param string $result
     */
    private function getControllerActions($type, $id, $module, &$result)
    {
        try {
            /* @var $controller \yii\base\Controller */
            $controller = Yii::createObject($type, [$id, $module]);
            $this->getActionRoutes($controller, $result);
            $result[] = '/' . $controller->uniqueId . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    /**
     * Get route of action
     *
     * @param \yii\base\Controller $controller
     * @param array $result all controller action.
     */
    private function getActionRoutes($controller, &$result)
    {
        try {
            $prefix = '/' . $controller->uniqueId . '/';
            foreach ($controller->actions() as $id => $value) {
                $result[] = $prefix . $id;
            }
            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $method) {
                $name = $method->getName();
                if ($method->isPublic() && !$method->isStatic() && strpos($name, 'action') === 0 && $name !== 'actions') {
                    $result[] = $prefix . Inflector::camel2id(substr($name, 6));
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    private function clearRoutes(&$routes)
    {
        foreach ($routes as $key => $route) {
            if (strpos($route, '*') !== false)
                unset($routes[$key]);
        }
    }

    public function checkRoutes($routes)
    {
        foreach ($routes as $route) {
            Permission::checkRoute($route);
        }
    }

    public function deleteUnused($routes)
    {
        foreach (Permission::find()->all() as $permission) {
            if (empty($permission['title']) && !in_array($permission['url'], $routes))
                $permission->delete();
        }
    }

    public function actionRefreshRoutes()
    {
        $modules = [
            'user-management',
            'main',
            'uagent'
        ];

        $all = [];

        foreach ($modules as $module) {
            $res = [];

            $this->getRouteRecrusive(Yii::$app->getModule($module), $res);


            $this->clearRoutes($res);

            $this->checkRoutes($res);

            $all = array_merge($res, $all);
        }

        // $this->deleteUnused($all);

        return $this->redirect('index');
    }

    public function actionUpdate($id = null, $controller = "")
    {
        if (isset($id))
            $model = $this->findModel($id);
        else
            $model = new Permission(['url' => $controller]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model'));
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->request->isAjax)
            throw new \yii\web\BadRequestHttpException();

        $this->findModel($id)->delete();

        // return $this->redirect(['index']);

        return $this->redirect(Yii::$app->user->getReturnUrl(['index']));
    }

    public function actionAsignPermission(array $permission_ids, $role_id, $value = null)
    {
        if (!Yii::$app->request->isAjax)
            throw new \yii\web\BadRequestHttpException();

        $value = $value === "true";

        $rps = RolePermission::find()->where(['permission_id' => $permission_ids, 'role_id' => $role_id])->indexBy('permission_id')->all();

        $return = [];
        foreach ($permission_ids as $permission_id) {
            $rp = $rps[$permission_id];
            if (!empty($rp) && $value === false) {
                $rp->delete();
                $return[$permission_id] = 'unasigned';
            } elseif (empty($rp) && $value === true) {
                (new RolePermission(['permission_id' => $permission_id, 'role_id' => $role_id]))->save();
                $return[$permission_id] = 'asigned';
            } else {
                $return[$permission_id] = 'nothing';
            }
        }

        return json_encode($return);
    }


    private function findModel($id)
    {
        if ($mod = Permission::findOne($id))
            return $mod;
        else
            throw new \yii\web\NotFoundHttpException("Permission not found");
    }
}
