<?php

namespace common\modules\user\components;

use linslin\yii2\curl\Curl;
use Yii;
use yii\helpers\Json;
use yii\validators\Validator;

class ReCaptcha extends Validator
{

    private $_secretKey = '6LcJfSUTAAAAACqyM0HUtVVjOK9oASed02CmUBWZ';
    
    public static $sitekey = '6LcJfSUTAAAAAO-egABXfD0cYYRHINIVDjVXPZrq';

    public function validateAttribute($model, $attribute)
    {
        $curl = new Curl();
        $response = Json::decode($curl->setOption(CURLOPT_POSTFIELDS, http_build_query([
            'secret' => $this->_secretKey,
            'response' => Yii::$app->request->post('g-recaptcha-response'),
        ]))->post('https://www.google.com/recaptcha/api/siteverify'));
        if (!$response["success"]) {
            $this->addError($model, $attribute,Yii::t('app','Confirm that you are not a robot.'));
        }
    }
}