<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\modules\user;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/user/assets';
    public $js =[
        'profile.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
