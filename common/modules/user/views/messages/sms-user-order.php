<?php

/* @var $model \app\modules\main\models\Document */

echo Yii::$app->request->hostInfo.' '.Yii::t('app','Thank you for your order.')." \n";
echo Yii::t('app','Order').' №'.$model->id.", ".date('d.m.Y H:i',$model->created_at).", \n";
if(!empty($model->payment->description)){
    echo $model->payment->description.", \n";
}
echo Yii::t('app','Sum').': '.number_format($model->summ, 2).' '.Yii::t('app','uah');