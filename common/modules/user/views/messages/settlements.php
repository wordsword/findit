<?php

use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\main\models\Store;
use kartik\daterange\DateRangePicker;

?>

<div class="user-settlements">
	<p>
		<?= Yii::T('app', 'User') . ': ' . $user->name ?>
	</p>
	<p>
		<?= Yii::T('app', 'Period') . ': ' . $date ?>
	</p>
	<div class="user-settlements-list">
		<p>
			<?= Yii::t('app', 'Balance for begining') . ': ' . Yii::$app->formatter->asDecimal($start_debt, 2) ?>
		</p>
		<div class="table-wrap">
			<table border="1">
				<tr>
					<th style="padding: 5px;">#</th>
					<th style="padding: 5px;"><?= Yii::t('app', 'Date') ?></th>
					<th style="padding: 5px;"><?= Yii::t('app', 'Document ID') ?></th>
					<!-- <th><? //=Yii::t('app', 'Cause')
								?></th> -->
					<th style="padding: 5px;"><?= Yii::t('app', 'Debet') ?></th>
					<th style="padding: 5px;"><?= Yii::t('app', 'Credit') ?></th>
					<th style="padding: 5px;"><?= Yii::t('app', 'Leftover') ?></th>
				</tr>
				<?php foreach ($settlements as $settlement) : ?>
					<tr>
						<td style="padding: 5px;"><?= $settlement['index'] ?></td>
						<td style="padding: 5px;"><?= date('d.m.Y', $settlement['created_at']) ?></td>
						<td style="padding: 5px;">
							<? $doc_number = "" ?>
							<? switch ($settlement['document_type']) {
								case 'order':
									$doc_number = (new Query)->from('shop_document')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Order') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
								case 'realization':
									$doc_number = (new Query)->from('shop_document')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Realization') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
								case 'return':
									$doc_number = (new Query)->from('shop_document_return')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Return') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
								case 'bank_payment':
								case 'checkout_payment':
									$doc_number = (new Query)->from('shop_document_payment')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Payment') . ($settlement['document_type'] == 'bank_payment' ? '(' . Yii::T('app', 'By terminal') . ')' : '(' . Yii::T('app', 'By cash') . ')') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
								case 'supply':
									$doc_number = (new Query)->from('shop_document_product_supply')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Supply') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
								case 'initial_debt':
									$doc_number = (new Query)->from('shop_document_initial_debts')->where(['id' => $settlement['document_id']])->select('number')->scalar();
									echo Yii::t('app', 'Initial debts') . ' №' . ($doc_number ? $doc_number : $settlement['document_id']);
									break;
							} ?>
						</td>
						<td style="padding: 5px;">
							<?= ($settlement['summ'] > 0 ? Yii::$app->formatter->asDecimal(abs($settlement['summ'])) : '') ?>
						</td style="padding: 5px;">
						<td style="padding: 5px;">
							<?= ($settlement['summ'] < 0 ? Yii::$app->formatter->asDecimal(abs($settlement['summ'])) : '') ?>
						</td style="padding: 5px;">
						<td style="padding: 5px;">
							<?= Yii::$app->formatter->asDecimal($settlement['leftover']) ?>
						</td>
					</tr>
				<?php endforeach ?>
				<tr>
					<th class="summary" colspan="3" style="padding: 5px;">
						<?= Yii::t('app', 'Summ') ?>
					</th>
					<th class="summary" style="padding: 5px;">
						<?= Yii::$app->formatter->asDecimal($income) ?>
					</th>
					<th class="summary" style="padding: 5px;">
						<?= Yii::$app->formatter->asDecimal(abs($outgo)) ?>
					</th>
					<th class="summary"></th>
				</tr>
			</table>
		</div>
		<br>
		<p style="float: left;">
			<?= Yii::t('app', 'Balance for end') . ': ' . Yii::$app->formatter->asDecimal($end_debt, 2) ?>
		</p>
	</div>
</div>