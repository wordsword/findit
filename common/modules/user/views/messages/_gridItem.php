<?php

use app\modules\shop\models\Document;
use rmrevin\yii\fontawesome\FA;
use app\modules\shop\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;

$item = $model;
$item_price = number_format($item->price * (1 + ($item->discount / 100)), 2);

//if (!empty($item->product->image) && file_exists(Yii::getAlias('@frontend/web') . $item->product->image)) {
//    $image = Yii::$app->request->hostInfo . $item->product->image;
//} else {
//    $image = Yii::$app->request->hostInfo . '/images/no_image.jpg';
//}
?>
<tr class="product-checkout-item">
    <td class="td-product" style="width: 40%; border: 1px solid #e8e7f7; border-left: 0; padding: 8px;">
        <div class="checkout-product-image"
             style="float: left; width: 100px; margin-right: 10px; height: 100px; text-align: center; line-height: 100px;">
            <img src="<?= Yii::$app->request->hostInfo . Html::encode($item->product->coverImage) ?>"
                 alt="<?= Html::encode($item->product->title) ?>"
                 title="<?= Html::encode($item->product->title) ?>"
                 style="width: auto; max-width: 100%; height: auto; max-height: 100%; display: inline-block; vertical-align: middle;"/>
        </div>
        <div class="checkout-product-info">
            <div class="cart-product-name"
                 style="font-size: 14px;color: #303030; font-family: Arial,sans-serif; padding-bottom: 5px"><?= Html::encode($item->product->title) ?></div>
            <div class="cart-info checkout-article"
                 style="color: #a4a4a4; font-size: 13px;font-family: Arial,sans-serif;"><?= Yii::t('app', 'Article') . ': ' ?><?= Html::encode($item->product->article) ?>
            </div>
        </div>
    </td>
    <td align="center" style="width: 20%; border: 1px solid #e8e7f7; padding: 8px;">
        <div class="checkout-product-price"
             style="color: #cea985; font-size: 18px; font-weight: 700; padding-bottom: 10px;"><?= $item_price ?><?= ' ' . Yii::t('app', 'uah') ?></div>
        <div class="checkout-product-price-type"><?= $item->productPrice->title ?></div>
    </td>

    <td align="center" style="width: 20%; border: 1px solid #e8e7f7; padding: 8px;">
        <?
        echo Html::beginTag('div', ['class' => 'quantity-info first']);
        echo Html::tag('span', Html::encode($item->quantity), ['class' => 'value']);
        echo Html::tag('span', ' ' . Yii::t('app', 'pcs'));
        echo Html::endTag('div');
        ?>
    </td>
    <td align="center"
        style="width: 20%; color: #cea985; font-size: 18px; font-weight: 700;border: 1px solid #e8e7f7; border-right: 0; padding: 8px;"><?= $item_price * $item->quantity ?><?= ' ' . Yii::t('app', 'uah') ?></td>
</tr>