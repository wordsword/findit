<?php
/**
 * @var $this yii\web\View
 * @var $user common\modules\user\models\User
 */
use yii\helpers\Html;

?>
<?php

ob_start();

echo $this->render('@common/modules/user/views/messages/_order',$params);

$content = ob_get_contents();
ob_end_clean();

echo $this->render('@common/modules/user/views/mail/layout', [
    'content' => $content
]) ?>
