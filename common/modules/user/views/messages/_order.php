<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Document */

$this->title = Yii::t('app', 'Order') . ' №' . $model->primaryKey . ', ' . Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Account'), 'url' => ['/user-management/auth/profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" height="100%"
       style="border: 1px solid #ddd; font-family: Arial, sans-serif">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; width: 100%; height: 200px; background-image: url(<?= Yii::$app->request->hostInfo ?>/images/email-logo.jpg); background-size: cover; background-position: 50% 50%; border-bottom: 1px solid #ddd;">
                <tr></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color: #282a30; font-size: 2.5em; line-height: 1;  padding:0px  0px  .75em; letter-spacing: 1.38px; font-family: Arial,sans-serif;">
                        <?= Yii::t('app', 'Order') . ' №' . Html::encode($model->primaryKey) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="90%">
                            <tr>
                                <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'Date of ordering') ?></td>
                                <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm') ?></td>
                            </tr>
                            <tr>
                                <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'User') ?></td>
                                <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->name) ?></td>
                            </tr>
                            <tr>
                                <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'Phone number') ?></td>
                                <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->phone) ?></td>
                            </tr>
                            <? if ($model->email): ?>
                                <tr>
                                    <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'E-mail') ?></td>
                                    <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->phone) ?></td>
                                </tr>
                            <? endif; ?>
                            <?php if (!empty($model->info)) : ?>
                                <tr>
                                    <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'Info') ?></td>
                                    <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->info) ?></td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'Payment') ?></td>
                                <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->payment->title) . (($model->payment->description) ? '. <br>' . Html::encode($model->payment->description) : "") ?></td>
                            </tr>
                            <?php if (!empty($model->delivery_address)) : ?>
                                <tr>
                                    <td style="color: rgba(0,0,0,.54); font-size: 16px; font-family: Arial,sans-serif; padding: 8px 0; border-bottom: 1px solid #ddd;"><?= Yii::t('app', 'Delivery address') ?></td>
                                    <td style="padding: 8px 0; border-bottom: 1px solid #ddd; color: rgba(0,0,0,.87);"><?= Html::encode($model->delivery_address) ?></td>
                                </tr>
                            <? endif; ?>
                            <tr>
                                <td style="color: rgba(0,0,0,.54); font-size: 30px; font-family: Arial,sans-serif; padding: 8px 0;"><?= Yii::t('app', 'Sum') ?></td>
                                <td style="padding: 8px 0;font-size: 30px;  color: rgba(0,0,0,.87);"><?= number_format($model->summ, 2) . ' ' . Yii::t('app', 'uah') ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 0 20px">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; max-width: 800px; height: auto;">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="border-collapse: collapse;">
                            <thead>
                            <tr>
                                <td align="center"
                                    style="padding: 20px 5px; color: #303030; font: 16px/1 Arial,sans-serif; font-weight: 600;"><?= Yii::t('app', 'Product') ?></td>
                                <td align="center"
                                    style="padding: 20px 5px; color: #303030; font: 16px/1 Arial,sans-serif; font-weight: 600;"><?= Yii::t('app', 'Price') ?></td>
                                <td align="center"
                                    style="padding: 20px 5px; color: #303030; font: 16px/1 Arial,sans-serif; font-weight: 600;"><?= Yii::t('app', 'Quantity') ?></td>
                                <td align="center"
                                    style="padding: 20px 5px; color: #303030; font: 16px/1 Arial,sans-serif; font-weight: 600;"><?= Yii::t('app', 'Sum') ?></td>
                            </tr>
                            </thead>
                            <?= yii\widgets\ListView::widget([
                                'id' => 'cart-list',
                                'dataProvider' => $dataProvider,
                                'itemView' => '@common/modules/user/views/messages/_gridItem',
                                'layout' => '<tbody>{items}</tbody>',
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                            ]); ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    <tr>
        <td style="height: 50px; background: transparent;"></td>
    </tr>
    <tr>
        <td align="center"
            style="height: 80px; font-size: 20px; color: #303030; background-color: #F0F1F5;"><?= Yii::t('app', 'Online store') ?>
            <a href="http://parfumestyle.com.ua" target="_blank"
               style="color: #303030 !important; text-decoration: none !important;"><strong>parfumestyle.com.ua</strong></a>
        </td>
    </tr>
</table>