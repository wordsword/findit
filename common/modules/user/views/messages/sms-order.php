<?php

/* @var $model \app\modules\shop\models\Document */

echo Yii::t('app','New order').": \n";
echo Yii::t('app','User').": ".$model->name.", \n";
echo Yii::t('app','Order').' №'.$model->id.", ".date('d.m.Y H:i',$model->created_at).", \n";
echo Yii::t('app','Sum').': '.Yii::$app->formatter->asDecimal($model->sum,2).' '.Yii::t('app','uah');