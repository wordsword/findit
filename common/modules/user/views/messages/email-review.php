<?php
/**
 * @var $this yii\web\View
 * @var $user common\modules\user\models\User
 */

use yii\helpers\Html;

ob_start();
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" height="100%"
       style="border: 1px solid #ddd;">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; width: 100%; height: 200px; background-image: url(<?= Yii::$app->request->hostInfo ?>/images/email-logo.jpg); background-size: cover; background-position: 50% 50%; border-bottom: 1px solid #ddd;">
                <tr></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 60px 20px; text-align:center;">
            <p style="color: #282a30; font-size: 2.5em; line-height: 1;  padding:0px  0px  .75em; letter-spacing: 1.38px; font-family: Arial,sans-serif;"><?= Yii::t('app', 'New review') . ":" ?></p>
            <p>
                <?= Html::encode($params['review']->name . ", " . $params['review']->email) ?>
            </p>
            <p>
                <?= Html::encode($params['review']->text) ?>
            </p>
        </td>
    <tr>
        <td align="center"
            style="height: 80px; font-size: 20px; color: #303030; background-color: #F0F1F5;"><?= Yii::t('app', 'Online store') ?>
            <a href="http://parfumestyle.com.ua" target="_blank"
               style="color: #303030 !important; text-decoration: none !important;"><strong>parfumestyle.com.ua</strong></a>
        </td>
    </tr>
</table>

<?php
$content = ob_get_contents();
ob_end_clean();

echo $this->render('@common/modules/user/views/mail/layout', [
    'content' => $content
]) ?>
