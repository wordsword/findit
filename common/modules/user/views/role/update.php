<?php
/**
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\user\models\rbacDB\Permission $model
 */

use common\modules\user\Module;

if (!$model->isNewRecord)
    $this->title = Yii::t('app', 'Edit role: ') . ' ' . $model->title;
else
    $this->title = Yii::t('app', 'Create role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="custom-page-header">
    <div class="action-line"><?= (!$model->isNewRecord) ? Yii::t('app', 'Edit role: ') : Yii::t('app', 'Create role') ?></div>
    <div class="info-line">
        <div class="left-box"><?= $model->title ?></div>
        <div class="right-box">
            <i class="fa fa-pencil" aria-hidden="true"></i>
            <span class="action"><?= (!$model->isNewRecord) ? Yii::t('app', 'Editing') : Yii::t('app', 'Create') ?></span>
        </div>
    </div>
</div>
<div class="panel panel-default row">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $this->title ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>