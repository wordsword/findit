<?php
/**
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\user\models\rbacDB\Permission $model
 */

use common\modules\user\models\rbacDB\AuthItemGroup;
use common\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\select2\Select2;

?>
<div class="row">
    <div class="col-xs-12">
        <?php $form = ActiveForm::begin([
            'id' => 'role-form',
            'validateOnBlur' => false,
        ]) ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'autofocus' => $model->isNewRecord ? true : false]) ?>
        <?= $form->field($model, 'default_route')->widget(Select2::className(), [
            'data' => $routes,
            'theme' => Select2::THEME_BOOTSTRAP,
            'hideSearch' => false,
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ]) ?>
        <div class="profile-actions-buttons">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => 'simple-button accept-button']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'cancel-button hollow-button', 'data-pjax' => 0]) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>