<?php

use app\components\SkGridView;
use backend\modules\shop\models\Price;
use common\modules\user\components\GhostHtml;
use common\modules\user\models\rbacDB\Role;
use common\modules\user\models\User;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\modules\user\models\search\UserSearch $searchModel
 */

$this->title = Yii::t('app', 'Roles');
$this->params['breadcrumbs'][] = $this->title;

$gridID = 'role-grid';

$PHT = <<< HTML
    <h3 class="panel-title">
        {heading}
        <span class="summary">
            <span class="label label-success">{$dataProvider->getTotalCount()}</span>
        </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;

?>
<div class="role-index">
    <div class="row">
        <?= SkGridView::widget([
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'style' => 'text-align:left;'
                ];
            },
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterUrl' => $url,
            'filterModel' => $searchModel,
            'panelHeadingTemplate' => $PHT,
            'panelFooterTemplate' => $PFT,
            'panelBeforeTemplate' => $PBT,
            'toolbar' => [
                ['content' =>
                    Html::a(FA::icon('plus'), Url::toRoute('/user-management/role/update'), [
                        'title' => Yii::t('app', 'Create'),
                        'class' => 'btn btn-success',
                        'id' => 'add-language',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Create'),
                        'data-pjax' => 0,
                    ]) .
                    '{collapse}'
                ],
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container-roles',
                ]
            ],
            'pager' => [
                'class' => 'app\components\SkPager',
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => false,
            'responsiveWrap' => true,
            'panel' => [
                'type' => SkGridView::TYPE_DEFAULT,
                'heading' => $this->title,
                'footer' => true,
                'after' => false,
                'beforeOptions' => ['style' => 'padding: 0;'],
            ],
            'persistResize' => false,
            'filterPosition' => SkGridView::FILTER_POS_BODY,
            'columns' => [
                'title',
                [
                    'class' => '\app\components\SkActionColumn',
                    'contentOptions' => [
                        'class' => 'action-column kv-align-center kv-align-middle',
                    ],
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(
                                FA::icon('pencil') . ' ' . Yii::t('app', 'Edit'),
                                $url,
                                [
                                    'title' => Yii::t('app', 'Update'),
                                    'data-pjax' => 0
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $key) {
                            if ($model->predefined) return '';
                            return Html::a(
                                FA::icon('trash') . ' ' . Yii::t('app', 'Delete'),
                                $url,
                                [
                                    'class' => 'delete',
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-pjax' => 0,
                                    'data-confirm-msg' => Yii::t('app', 'Are you sure you want to delete this role?'),
                                    'data-msg' => $model->title,
                                ]
                            );
                        },
                    ]
                ],
            ]
        ]); ?>
    </div>
</div>
