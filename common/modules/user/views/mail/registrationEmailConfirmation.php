<?php
/**
 * @var $this yii\web\View
 * @var $user common\modules\user\models\User
 */

use yii\helpers\Html;

?>
<?php
$returnUrl = Yii::$app->user->returnUrl == Yii::$app->homeUrl ? null : rtrim(Yii::$app->homeUrl, '/') . Yii::$app->user->returnUrl;

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['/user-management/auth/confirm-registration-email', 'token' => $user->confirmation_token, 'returnUrl' => $returnUrl]);

ob_start();
?>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" height="100%"
           style="border: 1px solid #ddd; margin: 0 auto;">
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                       style="border-collapse: collapse; width: 100%; height: 200px; background-image: url(<?= Yii::$app->request->hostInfo ?>/images/email-logo.jpg); background-size: cover; background-position: 50% 50%; border-bottom: 1px solid #ddd;">
                    <tr></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 60px 20px; text-align:center;">
                <p style='display: block; color: #282a30; margin-bottom: 1em; font: 20px "Arial",sans-serif;'><?= Yii::t('app', 'Hello, you have been registered on') ?> <?= Yii::$app->urlManager->hostInfo ?></p>
                <p style="margin: 0 0 30px; font: 16px 'Arial',sans-serif;"><?= Yii::t('app', 'Follow this link to confirm your E-mail and activate account') ?>
                    :</p>
                <p><?= Html::a(Yii::t('app', 'Confirm E-mail'), $confirmLink, ['style' => 'height: 45px; width: 245px; background-color: #cea985; font-size: 16px; border-color: #cea985; color: #ffff; text-align: center; line-height: 45px; display: inline-block; text-transform: uppercase; text-decoration: none; color: #fff;']) ?></p>
            </td>
        <tr>
            <td align="center"
                style="height: 80px; font-size: 20px; color: #303030; background-color: #F0F1F5;">
                <?/*<?= Yii::t('app', 'Online store') ?>
                <a href="http://parfumestyle.com.ua" target="_blank"
                   style="color: #303030 !important; text-decoration: none !important;"><strong>litemarket.com.ua</strong></a>*/?>
            </td>
        </tr>
    </table>
<?php
$content = ob_get_contents();
ob_end_clean();

echo $this->render('layout', [
    'content' => $content
]) ?>