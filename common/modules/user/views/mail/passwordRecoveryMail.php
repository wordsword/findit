<?php
/**
 * @var $this yii\web\View
 * @var $user common\modules\user\models\User
 */

use yii\helpers\Html;

?>
<?php

ob_start();
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user-management/auth/password-recovery-receive', 'token' => $user->confirmation_token]);
?>

<? //= Html::tag('span', Yii::t('app','Hello') . ' ' . Html::encode($user->username), ['style'=>'display: block; color: #000; text-align: center; margin-bottom: 10px; font-size: 16px;'])?>
<!--<p style="margin: 0;">--><? //= Yii::t('app','Your new password')?><!--: --><? //=$newPass?><!-- </p>-->

<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" height="100%"
       style="border: 1px solid #ddd;">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; width: 100%; height: 200px; background-image: url(<?= Yii::$app->request->hostInfo ?>/images/email-logo.jpg); background-size: cover; background-position: 50% 50%; border-bottom: 1px solid #ddd;">
                <tr></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 60px 20px; text-align:center;">
            <?= Html::tag('span', Yii::t('app', 'Hello') . ' ' . Html::encode($user->username), ['style' => 'display: block; color: #000; margin-bottom: 1em; font: 24px "Arial";']) ?>
            <p style="margin: 0 0 30px; font: 18px 'Arial';"><?= Yii::t('app', 'follow this link to reset yout password') ?>
                :</p>
            <p><?= Html::a(Yii::t('app', 'Reset password'), $resetLink, ['style' => 'height: 45px; width: 245px; background-color: #cea985; font-size: 16px; border-color: #cea985; color: #ffff; text-align: center; line-height: 45px; display: inline-block; transition: all 0.3s; text-transform: uppercase; text-decoration: none; color: #fff;']) ?></p>
        </td>
    <tr>
        <td align="center"
            style="height: 80px; font-size: 20px; color: #303030; background-color: #F0F1F5;"><?= Yii::t('app', 'Online store') ?>
            <a href="http://parfumestyle.com.ua" target="_blank"
               style="color: #303030 !important; text-decoration: none !important;"><strong>parfumestyle.com.ua</strong></a>
        </td>
    </tr>
</table>

<?php
$content = ob_get_contents();
ob_end_clean();

echo $this->render('layout', [
    'content' => $content
]) ?>
