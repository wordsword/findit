<?php
/**
 * @var $this yii\web\View
 * @var $user common\modules\user\models\User
 */

use yii\helpers\Html;

?>
<?php
ob_start();
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" height="100%"
       style="border: 1px solid #ddd;">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; width: 100%; height: 200px; background-image: url(<?= Yii::$app->request->hostInfo ?>/images/email-logo.jpg); background-size: cover; background-position: 50% 50%; border-bottom: 1px solid #ddd;">
                <tr></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 60px 20px; text-align:center;">
            <?= Html::tag('span', Yii::t('app', 'Hello') . ' ' . Html::encode($user->username), ['style' => 'display: block; color: #000; margin-bottom: 1em; font: 24px "Arial";']) ?>
            <p style="margin: 0 0 30px; font: 18px 'Arial';"><?= Yii::t('app', 'Password has been changed!') ?></p>
            <p style="margin: 0 0 10px; font: 14px 'Arial';"><?= '<strong>' . Yii::t('app', 'Login') . ":</strong> " . $user->email ?></p>
            <p style="margin: 0 0 10px; font: 14px 'Arial';"><?= '<strong>' . Yii::t('app', 'Password') . ":</strong> " . $user->password ?></p>
        </td>
    <tr>
        <td align="center"
            style="height: 80px; font-size: 20px; color: #303030; background-color: #F0F1F5;"><?= Yii::t('app', 'Online store') ?>
            <a href="http://parfumestyle.com.ua" target="_blank"
               style="color: #303030 !important; text-decoration: none !important;"><strong>parfumestyle.com.ua</strong></a>
        </td>
    </tr>
</table>

<?php
$content = ob_get_contents();
ob_end_clean();

echo $this->render('layout', [
    'content' => $content
]) ?>
