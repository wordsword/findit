<?php


use backend\modules\shop\models\Settings;
use common\components\SkLangHelper;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model \app\components\SkDynamicModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-settings-form">
    <div class="row">
        <div class="col-md-8">
            <div class="form-block">
                <?= $form->field($model, 'enableRegistration')->checkbox() ?>
                <?= $form->field($model, 'activateAfterRegistration')->checkbox() ?>
                <?= $form->field($model, 'phoneConfirmationRequired')->checkbox() ?>
                <?= $form->field($model, 'emailConfirmationRequired')->checkbox() ?>
                <?= $form->field($model, 'loginAfterRegistration')->checkbox() ?>
            </div>
        </div>
    </div>
</div>
