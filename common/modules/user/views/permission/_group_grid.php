<?php

use app\components\SkGridView;
use backend\modules\shop\models\Price;
use common\modules\user\components\GhostHtml;
use common\modules\user\models\rbacDB\Role;
use common\modules\user\models\User;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
    'allModels' => $group['controllers']['items'],
    'pagination' => false,
    'key' => 'id'
]);


$PHT = <<< HTML
    <h3 class="panel-title">
        {heading}
        <span class="summary">
            <span class="label label-success">{$dataProvider->getTotalCount()}</span>
        </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;

$gridColumns = [];


$gridID = "group-".$gr_key;

?>
<div class="permission-index">
    <div class="row">
        <div class="col-lg-12">
            <?= SkGridView::widget([
                'id' => $gridID,
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return [
                        'style' => 'text-align:left;',
                    ];
                },
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterUrl' => $url,
                'filterModel' => $searchModel,
                'panelHeadingTemplate' => $PHT,
                'panelFooterTemplate' => $PFT,
                'panelBeforeTemplate' => $PBT,
                'toolbar' => [
                    ['content' => 
                        ($group['id'] == 0 ? '' : Html::a(FA::icon('pencil'), Url::toRoute(['update-group', 'id' => $group['id']]) ,[
                            'type' => 'button',
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Update'),
                        ]) .
                        Html::a(FA::icon('trash'), Url::toRoute(['delete-group', 'id' => $group['id']]), [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Delete'),
                        ]) ).
                        Html::button(FA::icon('sort'), [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Sort'),
                            'class' => 'btn sortable',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Sort'),
                        ]) .
                        Html::button(FA::icon('expand'), [
                            'class' => 'btn btn-danger grid-collapse',
                            'type' => 'button',
                            'title' => Yii::t('app', 'Collapse'),
                            'data-toggle' => 'tooltip',
                        ])
                    ],
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container-permissions',
                    ]
                ],
                'pager' => [
                    'class' => 'app\components\SkPager',
                ],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'sizer' => false,
                'responsive' => true,
                'responsiveWrap' => true,
                'panel' => [
                    'type' => SkGridView::TYPE_DEFAULT,
                    'heading' => $group['title'],
                    'footer' => false,
                    'after' => false,
                    'beforeOptions' => ['style' => 'padding: 0; margin: 0;'],
                ],
                'persistResize' => false,
                'filterPosition' => SkGridView::FILTER_POS_BODY,
                'columns' => [
                    [
                        'attribute' => 'actions',
                        'header' => " ",
                        'content' => function($model){
                            $columns = $model['columns'];
                            $item_title = $model['route'];

                            if($item_title != "uncategorized")
                                $group_title = Yii::t('app', str_replace('-', ' ', ucfirst(explode('/', $item_title)[2])));
                            else
                                $group_title = Yii::t('app', "Uncategorized");

                            $dataProvider = new ArrayDataProvider([
                                'allModels' => $model['actions']
                            ]);
                            return $this->render('_controller', compact(
                                'model'
                            ));
                        }
                    ]
               ]
            ]); ?>
        </div>
    </div>
</div>