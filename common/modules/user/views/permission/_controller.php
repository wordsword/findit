<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\components\SkGridView;
use rmrevin\yii\fontawesome\FA;
use common\modules\user\models\User;
use backend\modules\shop\models\Price;
use common\modules\user\models\Permission;
use common\modules\user\components\GhostHtml;


$title = "";

if ($model['route'] != "undefined")
    $title = Yii::t('app', str_replace('-', ' ', ucfirst(explode('/', $model['route'])[2])));
else
    $title = Yii::t('app', "Undefined");


$change = <<<JS
    $(document).on('click', '.rp_check', function(e){
        per = $(this).attr('per-id');
        role = $(this).attr('role-id');
        val = $(this).prop('checked');

        is_index = $(this).attr('data-index-page') == 'true';

        ids = [per];


        if(is_index) {
            if(val) {
                $(this).closest('table').find(':checkbox[role-id="' + role + '"]:not(:checked):not([per-id="' + per + '"])').each(function() {
                    ids.push($(this).attr('per-id'))
                }).prop('checked', val);
            } else {
                $(this).closest('table').find(':checkbox[role-id="' + role + '"]:checked:not([per-id="' + per + '"])').each(function() {
                    ids.push($(this).attr('per-id'))
                }).prop('checked', val);
            }
        } else {
            $(this).closest('table').find(':checkbox[role-id="' + role + '"]:not(:checked)[data-index-page="true"]').each(function() {
                ids.push($(this).attr('per-id'))
            }).prop('checked', val);
        }
        // console.log(per + " - " + val);



        // setTimeout(() => {
        //     if(!artificial) {
        //         if(is_index) {
        //             if(val)
        //                 $(this).closest('table').find(':checkbox[role-id="' + role + '"]:not(:checked):not([per-id="' + per + '"])').trigger('click', true);
        //             else
        //                 $(this).closest('table').find(':checkbox[role-id="' + role + '"]:checked:not([per-id="' + per + '"])').trigger('click', true);
        //         } else {
        //             if(val)
        //                 $(this).closest('table').find(':checkbox[role-id="' + role + '"]:not(:checked)[data-index-page="true"]').trigger('click', true);
        //         }
        //     }
        // }, 10);

        $.get({
            url: '/admin/user-management/permission/asign-permission',
            data: {
                permission_ids: ids,
                role_id: role,
                value: val 
            },
            success: function(re){
                console.log(per + " = " + re);
            }
        });
    });
JS;

$this->registerJs($change, View::POS_END);

?>




<div class="controller-table is-bs3" data-id="<?= $model['id'] ?>">
    <div class="panel panel-dafault grid-collapse" style="border: 1px solid grey; margin: 0;">
        <h3 class="panel-title">
            <?= FA::icon("caret-right") ?>
            <?= $title ?>
            <h6 style="display: inline-block; padding-top: 0.6em; margin-left: -.7em;">
                <?= $model['route'] ?>
            </h6>
        </h3>
        <div class="kv-panel-before" style="padding: 0; margin: 0;">
            <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                <div class="btn-group">
                    <span>
                        <?/*=Html::button(FA::icon('expand'), [
                            'class' => 'btn btn-danger grid-collapse',
                            'type' => 'button',
                            'title' => Yii::t('app', 'Collapse'),
                            'data-toggle' => 'tooltip',
                        ])*/ ?>
                        <?/*=Html::a(FA::icon('plus'), Url::toRoute('/user-management/permission/update?controller='.$item_title), [
                            'title' => Yii::t('app', 'Create'),
                            'class' => 'btn btn-danger',
                            'id' => 'add-language',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Create'),
                        ])*/ ?>
                        <?= ($model['route'] == 'undefined' ? '' :
                            Html::a(FA::icon('share'), Url::toRoute($model['route']), [
                                'title' => Yii::t('app', 'Go'),
                                'class' => 'btn btn-danger',
                                'id' => 'add-language',
                                'target' => '_blank',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => Yii::t('app', 'Go'),
                            ])) ?>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-12 kv-grid-container">

        <table class="col-lg-12" border=1>
            <thead>
                <tr>
                    <td><?= Yii::t('app', 'Caption') ?></td>
                    <td><?= Yii::t('app', 'Title') ?></td>
                    <td><?= Yii::t('app', 'Url') ?></td>
                    <?php foreach ($model['columns'] as $role) {
                    ?>
                        <td><?= $role ?></td>
                    <?php
                    } ?>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['actions'] as $item) {
                ?>
                    <tr>
                        <td>
                            <?php
                            if (strpos($item['url'], 'index') !== false)
                                echo
                                    Html::tag('span', '', [
                                        'class' => '    glyphicon glyphicon-lock'
                                    ]);
                            ?>
                            <?= $item['name'] ?>
                            <?php
                            if (strpos($item['url'], 'index') !== false)
                                echo
                                    Html::tag('span', '', [
                                        'class' => 'glyphicon glyphicon-question-sign',
                                        'style' => [
                                            'margin-top' => "-0.5em",
                                        ],
                                        'data-toggle' => 'tooltip',
                                        'data-title' => Yii::t('app', 'This permission can be edited through editing the admin panel menu')
                                    ]);
                            ?>
                        </td>
                        <td>
                            <?= $item['title'] ?>
                        </td>
                        <td>
                            <?= $item['url'] ?>
                        </td>
                        <?php
                        foreach ($model['columns'] as $role => $val) {
                        ?>
                            <td style="text-align: center;">
                                <?= Html::checkBox('val', $item[$role], [
                                    'per-id' => $item['id'],
                                    'role-id' => $role,
                                    'class' => 'rp_check',
                                    'data-index-page' => (Permission::isLocked($item['url'], $role) || strpos($item['url'], 'index') !== false) ? "true" : "false"
                                ]) ?>
                            </td>
                        <?php
                        }
                        ?>
                        <td style="text-align: center;">
                            <?= Html::a(
                                FA::icon('pencil'),
                                ['/user-management/permission/update/' . $model['id']],
                                [
                                    'title' => Yii::t('app', 'Update'),
                                    'data-pjax' => 0
                                ]
                            ); ?>
                        </td>
                    </tr>
                <?php
                } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="clearfix"></div>