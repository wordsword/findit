<?php
/**
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\user\models\rbacDB\Permission $model
 */

use common\modules\user\models\rbacDB\AuthItemGroup;
use common\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
	'id'      => 'role-form',
	// 'layout'=>'horizontal',
	'validateOnBlur' => false,
]) ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'autofocus'=>$model->isNewRecord ? true:false]) ?>

	<?= $form->field($model, 'url') ?>

	<div class="form-group">
			<?php if ( $model->isNewRecord ): ?>
				<?= Html::submitButton(
					'<span class="glyphicon glyphicon-plus-sign"></span> ' . Yii::t('app', 'Create'),
					['class' => 'btn btn-success']
				) ?>
			<?php else: ?>
				<?= Html::submitButton(
					'<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('app', 'Save'),
					['class' => 'btn btn-primary']
				) ?>
			<?php endif; ?>
	</div>
<?php ActiveForm::end() ?>