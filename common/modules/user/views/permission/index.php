<?php

use app\components\SkGridView;
use backend\modules\shop\models\Price;
use common\modules\user\components\GhostHtml;
use common\modules\user\models\rbacDB\Role;
use common\modules\user\models\User;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\modules\user\models\search\UserSearch $searchModel
 */
// \backend\modules\shop\ShopAsset::register($this);
\backend\assets\PermissionAsset::register($this);


$this->title = Yii::t('app', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel panel-default">
    <div class="panel-body" style="margin:0">
        <div class="kv-panel-before">
            <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                <div class="btn-group">
	                    <span>
	                        <?= Html::button(FA::icon('sort'), [
                                'class' => 'btn btn-danger sort-groups',
                                'type' => 'button',
                                'title' => Yii::t('app', 'Sort'),
                                'data-toggle' => 'tooltip',
                                'data-original-title' => Yii::t('app', 'Sort')
                            ]) ?>
                            <?= Html::a(FA::icon('plus'), Url::toRoute(['permission/create-group']), [
                                'type' => 'button',
                                'title' => Yii::t('app', 'Create group'),
                                'class' => 'btn btn-danger',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => Yii::t('app', 'Create group'),
                            ]) ?>
                            <?= Html::a(FA::icon('refresh'), Url::toRoute(['permission/refresh-routes']), [
                                'type' => 'button',
                                'title' => Yii::t('app', 'Refresh routes'),
                                'class' => 'btn btn-danger',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => Yii::t('app', 'Refresh routes'),
                            ]) ?>
	                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <ul class="group-container col-lg-12">
        <?php
        foreach ($groups as $gr_key => $group) {
            echo $this->render('_group', compact('group', 'gr_key'));
        }
        ?>
    </ul>
</div>
