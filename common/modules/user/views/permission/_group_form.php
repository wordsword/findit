<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use common\components\SkLangHelper;

$this->title = $model->isNewRecord ? Yii::t('app', 'Create group') : Yii::t('app', 'Update group').': '.$model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin();

$langs = SkLangHelper::getLangs();


?>

<div class="panel panel-dafault">
	<div class="panel-body">
		<?php 
			$langTabs = [];
            foreach (SkLangHelper::getLangs() as $lang) {
                unset($suffix);
                if (!$lang['deflang']) {
                    $suffix = '_' . Html::encode($lang['lang_id']);
                }
                // var_dump('title'.$suffix);

                $content = $form->field($model, 'title'.$suffix)->textInput()->label(Yii::t('app', 'Title'));


                $langTabs[] = [
                    'label' => Html::img(Html::encode($lang['image']), ['alt' => Html::encode($lang['lang_name']), 'data-toggle' => 'tooltip', 'title' => Html::encode($lang['lang_name'])]),
                    'content' => $content,
                    'options' => ['langid' => Html::encode($lang['lang_id'])],
                    'linkOptions' => ['class' => $hasErrors ? 'has-error' : '']
                ];
            }
            echo Tabs::widget([
                'items' => $langTabs,
                'encodeLabels' => false
            ]);
		 ?>
		 <div class="form-group">
			<?php if ( $model->isNewRecord ): ?>
				<?= Html::submitButton(
					'<span class="glyphicon glyphicon-plus-sign"></span> ' . Yii::t('app', 'Create'),
					['class' => 'btn btn-success']
				) ?>
			<?php else: ?>
				<?= Html::submitButton(
					'<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('app', 'Save'),
					['class' => 'btn btn-primary']
				) ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php ActiveForm::end(); ?>