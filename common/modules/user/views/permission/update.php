<?php
/**
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\user\models\rbacDB\Permission $model
 */

use common\modules\user\Module;

if(!$model->isNewRecord)
	$this->title = Yii::t('app', 'Edit permission: ') . ' ' . $model->title;
else
	$this->title = Yii::t('app', 'Create permission');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<h2 class="lte-hide-title"><?= $this->title ?></h2>

<div class="panel panel-default">
	<div class="panel-body">
		<?= $this->render('_form', [
			'model'=>$model,
		]) ?>
	</div>
</div>