<?php

use app\components\SkGridView;
use backend\modules\shop\models\Price;
use common\modules\user\components\GhostHtml;
use common\modules\user\models\rbacDB\Role;
use common\modules\user\models\User;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;



?>




<li class="group-table is-bs3 <?=($group['id'] == 0 ? 'unsorted':'')?>" style="margin: 10px 0;" data-group="<?=$group['id']?>">
    <div class="panel panel-dafault grid-collapse" style="border: 1px solid grey; margin: 0;">
        <h3 class="panel-title">
            <?=FA::icon("caret-right")?>
            <?=$group['title']?>
            <span class="summary">
                <span class="label label-success gr-quant"><?=count($group['controllers']['items'])?></span>
            </span>
        </h3>
        <div class="kv-panel-before" style="padding: 0; margin: 0;">    
            <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                <div class="btn-group">
                    <span>
                        <?=($group['id'] == 0 ? '' : Html::a(FA::icon('pencil'), Url::toRoute(['update-group', 'id' => $group['id']]) ,[
                            'type' => 'button',
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Update'),
                        ]) .
                        Html::a(FA::icon('trash'), Url::toRoute(['delete-group', 'id' => $group['id']]), [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Delete'),
                        ]) )?>
                        <?=Html::button(FA::icon('arrow-up'), [
                            'class' => 'btn btn-danger change-group',
                            'type' => 'button',
                            'title' => Yii::t('app', 'Change group'),
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Change group')
                        ])?>
                        <?=Html::button(FA::icon('sort'), [
                            'class' => 'btn btn-danger sort-group',
                            'type' => 'button',
                            'title' => Yii::t('app', 'Sort'),
                            'data-toggle' => 'tooltip',
                            'data-original-title' => Yii::t('app', 'Sort')
                        ])?>
                        <?/*=Html::button(FA::icon('expand'), [
                            'class' => 'btn btn-danger grid-collapse',
                            'type' => 'button',
                            'title' => Yii::t('app', 'Collapse'),
                            'data-toggle' => 'tooltip',
                        ])*/?>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-12 kv-grid-container">
        
        <table class="col-lg-12" border=1>
            <tbody class="sortable-table">
                <?php foreach ($group['controllers']['items'] as $model) {
                ?>
                    <tr class="dragg ui-state-default"  data-id="<?=$model['id']?>">
                        <td>
                            <?=$this->render('_controller', compact(
                                'model'
                                )
                            )?>
                            
                        </td>
                    </tr>
                <?php 
                } ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</li>




