<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\components\SkGridView;
use rmrevin\yii\fontawesome\FA;
use app\widgets\treeView\TreeView;
use app\components\SkCollapsePanel;
use common\modules\user\models\User;
use backend\modules\shop\models\Price;
use common\modules\user\models\UserGroup;
use common\modules\user\models\rbacDB\Role;
use common\modules\user\components\GhostHtml;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\modules\user\models\search\UserSearch $searchModel
 */

\kartik\daterange\DateRangePickerAsset::register($this);

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$url = Url::toRoute(['/user-management/user/index']);
$gridID = 'user-grid';

$reflection = new ReflectionClass($searchModel);
$class = $reflection->getShortName();
$onNodeChecked = new \yii\web\JsExpression(
    <<<JS
function (undefined, recID) {
    $('#{$gridID}').yiiGridView('applyFilter');
}
JS
);
$url2 = \yii\helpers\Url::toRoute(['/user-management/group/sort']);
$onRelocate = new \yii\web\JsExpression(
    <<<JS
function (undefined, object, list) {
    $.post('$url2',{list:list},function(data){
        refreshTree('#' + $('#treeview').parent().attr('id'),true);
    },'json');
}
JS
);


$PHT = <<< HTML
    <h3 class="panel-title">
        {$this->title}
        <span class="summary">
            <span class="label label-success">{$dataProvider->getTotalCount()}</span>
        </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;


?>
<div class="user-index">
    <div class="row">
        <?= SkGridView::widget([
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model, $key, $index, $grid) {
                $labels = [
                    User::STATUS_ACTIVE => [Yii::t('app', 'Active'), 'published'],
                    User::STATUS_INACTIVE => [Yii::t('app', 'Inactive'), 'not-published'],
                    User::STATUS_BANNED => [Yii::t('app', 'Banned'), 'banned']
                ];
                $options = $labels[$model->status];
                return [
                    'class' => $options,
                    'style' => 'text-align:left;'
                ];
            },
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterUrl' => $url,
            'filterModel' => $searchModel,
            'filterSelector' => '#treeview input.checked',
            'panelHeadingTemplate' => $PHT,
            'panelFooterTemplate' => $PFT,
            'panelBeforeTemplate' => $PBT,
            'toolbar' => [
                [
                    'content' =>
                    Html::a(FA::icon('plus'), Url::toRoute('/user-management/user/create'), [
                        'title' => Yii::t('app', 'Create'),
                        'class' => 'btn btn-success',
                        'id' => 'add-language',
                        'data-toggle' => 'tooltip',
                        'data-original-title' => Yii::t('app', 'Create'),
                    ]) .
                        '{collapse}'
                ],
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container-users',
                ]
            ],
            'pager' => [
                'class' => 'app\components\SkPager',
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => false,
            'responsiveWrap' => true,
            'panel' => [
                'type' => SkGridView::TYPE_DEFAULT,
                'heading' => $this->title,
                'footer' => true,
                'after' => false,
                'beforeOptions' => ['style' => 'padding: 0;'],
            ],
            'persistResize' => false,
            'filterPosition' => SkGridView::FILTER_POS_BODY,
            'columns' => [
                [
                    'filterType' => SkGridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'placeholder' => '',
                            'allowClear' => true
                        ],
                    ],
                    'filter' => [
                        1 => Yii::t('yii', 'Yes'),
                        0 => Yii::t('yii', 'No'),
                    ],
                    'filterOptions' => [
                        'class' => 'select2-cell'
                    ],
                    'contentOptions' => [
                        'class' => 'kv-align-center'
                    ],
                    'content' => function ($model, $key, $index, $column) {
                        $attribute = $column->attribute;
                        return Html::tag(
                            'span',
                            $model->$attribute ? Yii::t('yii', 'Yes') : Yii::t('yii', 'No'),
                            [
                                'class' => $model->$attribute ? 'label label-success' : 'label label-warning',
                                'style' => 'font-size: 85%;'
                            ]
                        );
                    },
                    'attribute' => 'admin',
                    'visible' => User::hasPermission('viewAdmin'),
                ],
                [
                    'attribute' => 'username',
                    'value' => function (User $model) {
                        return Html::a($model->username, ['update', 'id' => $model->id], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'visible' => User::hasPermission('viewUserPhone'),
                ],
                [
                    'filterType' => SkGridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'placeholder' => '',
                            'allowClear' => true
                        ],
                    ],
                    'filter' => [
                        User::STATUS_ACTIVE => Yii::t('app', 'Active'),
                        User::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                        User::STATUS_BANNED => Yii::t('app', 'Banned')
                    ],
                    'filterOptions' => [
                        'class' => 'select2-cell'
                    ],
                    'contentOptions' => [
                        'class' => 'kv-align-center'
                    ],
                    'attribute' => 'status',
                    'content' => function ($model, $key, $index, $column) {
                        $attribute = $column->attribute;
                        $labels = [
                            User::STATUS_ACTIVE => [Yii::t('app', 'Active'), 'success'],
                            User::STATUS_INACTIVE => [Yii::t('app', 'Inactive'), 'warning'],
                            User::STATUS_BANNED => [Yii::t('app', 'Banned'), 'danger']
                        ];
                        return Html::tag('span', $labels[$model->$attribute][0], [
                            'class' => 'label label-' . $labels[$model->$attribute][1],
                            'style' => 'font-size: 85%;'
                        ]);
                    },
                ],
                [
                    'attribute' => 'role_id',
                    'filterType' => SkGridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'placeholder' => '',
                            'allowClear' => true
                        ],
                    ],
                    'filter' => \common\modules\user\models\Role::getRole(),
                    'filterOptions' => [
                        'class' => 'select2-cell'
                    ],
                    'contentOptions' => [
                        'class' => 'kv-align-center'
                    ],
                    'header' => Yii::t('app', 'Role'),
                    'content' => function ($model, $key, $index, $column) {
                        if ($model->role_id) {
                            return \common\modules\user\models\Role::getRole($model->role_id);
                        } else {
                            return Yii::t('app', 'Not set');
                        }
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'filterType' => SkGridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                        'presetDropdown' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'DD.MM.YYYY'
                            ]
                        ],
                        'pluginEvents' => [
                            'apply.daterangepicker' => 'function(ev, picker) {
                                var val = picker.startDate.format(picker.locale.format) + picker.locale.separator + picker.endDate.format(picker.locale.format);
                                
                                console.log(picker.element[0].nextElementSibling);

                                picker.element[0].children[1].textContent = val;
                                $(picker.element[0].nextElementSibling).val(val).trigger("change");
                            }',
                        ],
                    ],

                    'headerOptions' => ['style' => 'min-width: 130px;'],
                    'pageSummary' => Yii::t('app', 'Total'),
                    'contentOptions' => [
                        'data-title' => Yii::t('app', 'Registration date'),
                        'class' => 'kv-align-center kv-align-middle'
                    ],
                    'header' => Yii::t('app', 'Registration date'),
                    'content' => function ($model) {
                        return Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm');
                    },
                    'filterOptions' => [
                        'class' => 'date-cell'
                    ],
                ],
                [
                    'class' => '\app\components\SkActionColumn',
                    'contentOptions' => [
                        'class' => 'action-column kv-align-center kv-align-middle',
                    ],
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(
                                FA::icon('pencil') . ' ' . Yii::t('app', 'Edit'),
                                $url,
                                [
                                    'title' => Yii::t('app', 'Update'),
                                    'data-pjax' => 0
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a(
                                FA::icon('trash') . ' ' . Yii::t('app', 'Delete'),
                                $url,
                                [
                                    'class' => 'delete',
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-pjax' => 0,
                                    'data-confirm-msg' => Yii::t('app', 'Are you sure you want to delete this user?'),
                                    'data-msg' => $model->username,
                                ]
                            );
                        },
                    ]
                ]
            ]
        ]); ?>
    </div>
</div>