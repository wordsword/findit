<?php

use common\modules\user\Module;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $model
 */

$this->title = Yii::t('app', 'User creation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="editing-form">
    <?php Pjax::begin([
        'id' => 'pjax-container',
        'formSelector' => '#user',
        'timeout' => 3000,
    ]);
    $form = ActiveForm::begin([
        'id' => 'user',
        'enableClientScript' => false,
        'fieldConfig' => ['options' => ['class' => 'form-group form-group-material']],
    ]); ?>
    <div class="custom-page-header">
        <div class="action-line"><?= Yii::t('app', 'User') ?></div>
        <div class="info-line">
            <div class="left-box"><?= Html::encode($model->username) ?></div>
            <div class="right-box">
                <i class="fa fa-pencil" aria-hidden="true"></i>
                <span class="action"><?= Yii::t('app', 'Created') ?></span>
            </div>
        </div>
    </div>
    <?= $this->render('form', [
        'model' => $model,
        'form' => $form,
    ]) ?>
    <?php
    ActiveForm::end();
    Pjax::end();
    ?>
</div>