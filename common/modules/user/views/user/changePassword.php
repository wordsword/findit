<?php

use common\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $model
 */

$this->title = Yii::t('app', 'Changing password for user: ') . ' ' . $user->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['update', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Changing password');
?>

<div class="user-update panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?= $this->title; ?></h3>
	</div>
	<div class="panel-body">
		<div class="user-form">
			<div class="col-sm-8">
				<?php $form = ActiveForm::begin([
					'id'=>'user',
				]); ?>
				<?= $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>
				<?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>
				<div class="form-group">
						<?= Html::submitButton(Yii::t('app', 'Save'),['class' => 'btn btn-sm btn-primary']
						) ?>
			<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
