<?php

use yii\db\Query;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\user\Module;
use app\modules\main\models\Store;
use common\modules\user\models\Role;
use common\modules\user\models\User;
use app\modules\uagent\models\License;
use common\modules\user\models\UserGroup;
use common\modules\user\components\GhostHtml;

\common\modules\user\ProfileAsset::register($this);

$roles = Role::find()->select('id, title')->asArray()->all();
$roles = ArrayHelper::map($roles, 'id', 'title');

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $model
 * @var yii\bootstrap\ActiveForm $form
 */

$js = new \yii\web\JsExpression(
    <<<JS
 $(document).on('change', 'input#user-admin', function(e) {
    $(this).closest('form').submit();
   });




$(document).on('click', '.btn-edit-user', function(e) {
    e.preventDefault()
    var data = $(this).closest("form").serialize() + "&save=1";
    
    console.log(data);
    $.pjax.reload({
        type: "POST",
        container: "#pjax-container",
        scrollTo: false,
        timeout: 20000,
        data: data
    });
});
JS
);
$this->registerJs($js, \yii\web\View::POS_READY);

?>
<style>
    .form-group .intl-tel-input .flag-container .selected-flag {
        -webkit-border-radius: 0 !important;
        -moz-border-radius: 0 !important;
        border-radius: 0 !important;
    }
</style>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="user-form">
            <?= $form->errorSummary([$model], ['class' => 'alert alert-danger']) ?>
            <div class="row">
                <div class="col-md-6">
                    <? if (Yii::$app->user->identity->superadmin) : ?>
                        <!--                        --><? //= $form->field($model, 'admin')->checkbox(['onChange' => "$(this).closest('form').submit();"]) 
                                                        ?>
                        <?= $form->field($model, 'admin', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
                            'onText' => '',
                            'offText' => '',
                            'checked' => false,
                            'uncheck' => 0
                        ])->label($model->getAttributeLabel('admin'), ['style' => 'display:inline-block;']); ?>
                    <? endif; ?>
                    <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'autocomplete' => 'off']) ?>

                    <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'autocomplete' => 'off']) ?>ч

                    <?php if ($model->isNewRecord) : ?>
                        <?= $form->field($model, 'password')->passwordInput(['minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off']) ?>
                        <?= $form->field($model, 'repeat_password')->passwordInput(['minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off']) ?>
                    <?php endif; ?>
                    <?php if (User::hasPermission('editUserPhone')) : ?>
                        <?= Html::hiddenInput($model->formName() . '[country]', $model->country, ['id' => 'country']); ?>
                        <?= $form->field($model, 'phone')->input("tel", ['autocomplete' => 'off', 'onkeyup' => "checkCurr(this)",]) ?>
                        <?/*= $form->field($model, 'phone_confirmed', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
                            'onText' => '',
                            'offText' => '',
                            'checked' => false,
                            'uncheck' => 0
                        ])->label($model->getAttributeLabel('phone_confirmed'), ['style' => 'display:inline-block;']); */?>
                    <?php endif; ?>

                    <?php if (User::hasPermission('editUserEmail')) : ?>
                        <?= $form->field($model, 'email')->input("email", ['maxlength' => 255]) ?>
                        <?= $form->field($model, 'email_confirmed', ['template' => "{input}\n{label}\n{hint}\n{error}"])->widget(\macgyer\yii2materializecss\widgets\form\SwitchButton::className(), [
                            'onText' => '',
                            'offText' => '',
                            'checked' => false,
                            'uncheck' => 0
                        ])->label($model->getAttributeLabel('email_confirmed'), ['style' => 'display:inline-block;']); ?>
                    <?php endif; ?>



                    <?php
                    if ($model->admin && !empty($roles))
                        echo $form->field($model, 'role_id')->dropdownList($roles)->label(Yii::t('app', 'Role'));
                    ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model->loadDefaultValues(), 'status')->widget(
                        Select2::classname(),
                        [
                            'data' => User::getStatusList(),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]
                    ) ?>

                </div>
                <div class="col-xs-12">
                    <div class="profile-actions-buttons">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'simple-button accept-button btn-edit-user']) ?>
                        <? if (!$model->isNewRecord) {
                            echo Html::a(
                                \rmrevin\yii\fontawesome\FA::icon('unlock-alt', ['style' => 'margin-right: 10px;']) .
                                    Yii::t('app', 'Change password'),
                                ['/user-management/user/change-password', 'id' => $model->id],
                                [
                                    'class' => 'simple-button accept-button', 'data-pjax' => 0
                                ]
                            );
                        } ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['/user-management/user'], ['class' => 'cancel-button hollow-button', 'data-pjax' => 0]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>