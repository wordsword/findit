<?php

use common\modules\user\components\GhostHtml;
use app\modules\shop\models\Promo;
use yii\helpers\Html;
use common\modules\user\models\User;
use rmrevin\yii\fontawesome\FA;

/* @var $this \yii\web\View */

$js = <<<JS
$(document).on('click','.btn-edit-user',function(){
    $('.view-form').addClass('hidden');
    $('.editing-form').removeClass('hidden');
});
JS;
$this->registerJs($js);

?>
<section class="admin-user-profile">
    <div class="form-block">
        <?php if ($model->admin) : ?>
            <div class="user-profile-row">
                <span class="user-profile-options"><?= Yii::t('app', 'Administrator') ?></span>
            </div>
        <?php endif; ?>
        <div class="user-profile-row">
            <span class="user-profile-options"><?= $model->getAttributeLabel('username') ?></span>
            <p><?= Html::encode($model->username) ?></p>
        </div>

        <div class="user-profile-row">
            <span class="user-profile-options"><?= $model->getAttributeLabel('phone') ?></span>
            <p><?= Html::encode($model->phone) ?> <?= $model->phone_confirmed ? FA::icon('check') : FA::icon('exclamation'); ?></p>
        </div>

        <div class="user-profile-row">
            <span class="user-profile-options"><?= $model->getAttributeLabel('email') ?></span>
            <p><?= Html::encode($model->email) ?></p>
        </div>
        <?php if ($model->admin) { ?>
            <div class="user-profile-row">
                <span class="user-profile-options"><?= Yii::t('app', 'Role') ?></span>
                <p><?= Html::encode($model->role->title) ?></p>
            </div>
        <?php } ?>


        <div class="user-profile-row">
            <span class="user-profile-options"><?= $model->getAttributeLabel('status') ?></span>
            <p>
                <?= User::getStatusValue($model->status) ?>
                <?php if ($model->status == 1) {
                    echo FA::icon('check');
                } else if ($model->status == 0) {
                    echo FA::icon('exclamation');
                } else if ($model->status == -1) {
                    echo FA::icon('ban');
                } ?>
            </p>
        </div>

        <div class="form-group" style="margin-top: 15px;">
            <div class="profile-actions-buttons">
                <?= Html::submitButton(FA::i('pencil') . ' ' . Yii::t('app', 'Edit'), ['class' => 'simple-button accept-button btn-edit-user']) ?>
                <?= Html::a(\rmrevin\yii\fontawesome\FA::icon('unlock-alt', ['style' => 'margin-right: 10px;']) .
                    Yii::t('app', 'Change password'),
                    ['/user-management/user/change-password', 'id' => $model->id],
                    [
                        'class' => 'simple-button accept-button', 'data-pjax' => 0
                    ]) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), ['/user-management/user'], ['class' => 'cancel-button hollow-button', 'data-pjax' => 0]) ?>
            </div>
        </div>
    </div>
</section>