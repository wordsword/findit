<?php 

use app\modules\main\models\Store;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;

 ?>

<style type="text/css">
	.user-settlements
	{
		background-color: rgba(0,0,0,0);
	}

	.daterangepicker 
	{
		z-index: 99999999;
	}

	.table-wrap	
	{
		width: 100%;
		max-height: 50vh;
		overflow-y: scroll;
		position: relative;
		border: 1px solid black;
	}

	.table-wrap table
	{
		width: 100%;
	}

	.table-wrap table tr th
	{
		position: sticky;
		top: 0px;
		background-color: white;
		padding: 5px;
		border: 1px solid grey;
	}

	.table-wrap table tr th.summary
	{
		position: sticky;
		bottom: 0px;
		background-color: white;
		padding: 5px;
	}


	.table-wrap table td,
	.table-wrap table th
	{
		border: 1px solid black;
		text-align: center;
		padding: 5px;
	}

	.fancybox-active
	{
		min-height: 100vh !important;
	}
</style>


<div class="user-settlements">
	 <p>
	 	<label>
	 		<?=Yii::t('app', 'Date range')?>
	 	</label><br>
	 	<?=Html::input('text', 'date-range', $date, [
	 		'readOnly' => true,
	 		'class' => 'date-range-filter'
	 	])?>
	 </p>
	 <div class="user-settlements-list">
	 	<p>
	 		<?=Yii::t('app', 'Balance for begining').': '.Yii::$app->formatter->asDecimal($start_debt, 2)?>
	 	</p>
		 <div class="table-wrap">
			 <table border="0" >
			 		<tr>
			 			<th><?=Yii::t('app', 'Date')?></th>
			 			<th><?=Yii::t('app', 'Document ID')?></th>
			 			<!-- <th><?//=Yii::t('app', 'Cause')?></th> -->
			 			<th><?=Yii::t('app', 'Debet')?></th>
			 			<th><?=Yii::t('app', 'Credit')?></th>
			 			<th><?=Yii::t('app', 'Leftover')?></th>
			 		</tr>
					 <?php foreach ($settlements as $settlement): ?>
					 	<tr>
					 		<td><?=date('d.m.Y H:i:s', $settlement['created_at'])?></td>
					 		<td>
					 			<?switch($settlement['document_type'])
					 			{
					 				case 'order': 
					 					echo Yii::t('app', 'Order');
					 					break;
					 				case 'realization':
					 					echo Yii::t('app', 'Realization');
					 					break;
					 				case 'bank_payment': 
					 				case 'checkout_payment':
					 					if($settlement['summ'] > 0) 
					 						echo Yii::t('app', 'Payment');
					 					else 
					 						echo Yii::t('app', 'Refund');

					 					if($settlement['document_type'] == 'bank_payment')
					 						echo '('.Yii::T('app', 'By terminal').')';
					 					else 
					 						echo '('.Yii::T('app', 'By cash').')';
					 					break;
					 			}?>
								<?=' №'.$settlement['document_id']?>
					 		</td>
					 		<td>
					 			<?=($settlement['summ'] > 0 ? Yii::$app->formatter->asDecimal(abs($settlement['summ'])) : '')?>
					 		</td>
					 		<td>
					 			<?=($settlement['summ'] < 0 ? Yii::$app->formatter->asDecimal(abs($settlement['summ'])) : '')?>
					 		</td>
					 		<td>
					 			<?=Yii::$app->formatter->asDecimal($settlement['leftover'])?>
					 		</td>
					 	</tr>
					 <?php endforeach ?>
			 		<tr>
			 			<th class="summary" colspan="2">
			 				<?=Yii::t('app', 'Summ')?>
			 			</th>
			 			<th class="summary">
			 				<?=Yii::$app->formatter->asDecimal($income)?>
		 				</th>
			 			<th class="summary">
			 				<?=Yii::$app->formatter->asDecimal(abs($outgo))?>
		 				</th>
		 				<th class="summary"></th>
			 		</tr>
			 </table>
		 </div>
		 <br>
		 <div class="row" style="width: 100%;">
		 	<p style="float: left;">
		 		<?=Yii::t('app', 'Balance for end').': '.Yii::$app->formatter->asDecimal($end_debt, 2)?>
		 	</p>
		 	<div style="float: right;">
				 <button class="btn-sm btn-primary send-email">
				 	<?=Yii::t('app', 'Send to user')?>
				 </button>
				 <span class="sent-info"></span>
		 	</div>
		 </div>
	 </div>
</div>
