<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $model
 */
// \app\modules\main\ShopAsset::register($this);

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-update view-form">
    <div class="custom-page-header">
        <div class="action-line"><?= Yii::t('app', 'User') ?></div>
        <div class="info-line">
            <div class="left-box"><?= Html::encode($model->username) ?></div>
            <div class="right-box">
                <i class="fa fa-pencil" aria-hidden="true"></i>
                <span class="action"><?= Yii::t('app', 'Editing') ?></span>
            </div>
        </div>
    </div>
    <div class="panel panel-default row">
        <div class="panel-body">
            <?= $this->render('_viewForm', [
                'model' => $model
            ]) ?>
        </div>
    </div>
</div>


<div class="editing-form hidden">
    <?php Pjax::begin([
        'id' => 'pjax-container',
        'formSelector' => '#user',
        'timeout' => 3000,
    ]);
    $form = ActiveForm::begin([
        'id' => 'user',
        'enableClientScript' => false,
        'fieldConfig' => ['options' => ['class' => 'form-group form-group-material']],
    ]); ?>
    <div class="custom-page-header">
        <div class="action-line"><?= Yii::t('app', 'User') ?></div>
        <div class="info-line">
            <div class="left-box"><?= Html::encode($model->username) ?></div>
            <div class="right-box">
                <i class="fa fa-pencil" aria-hidden="true"></i>
                <span class="action"><?= Yii::t('app', 'Editing') ?></span>
            </div>
        </div>
    </div>
    <div class="panel panel-default row">
        <div class="panel-body">
            <?= $this->render('form', [
                'model' => $model,
                'form' => $form,
            ]) ?>
        </div>
    </div>
    <?php
    ActiveForm::end();
    Pjax::end();
    ?>
</div>