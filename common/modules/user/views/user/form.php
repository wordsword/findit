<?php 

use kartik\alert\Alert;
use yii\bootstrap\Tabs;
	

if (isset($_COOKIE['active-user-tab'])) {
    $tab = $_COOKIE['active-user-tab'];
    $activeTab = strtr($tab, ['#user-tabs-tab' => '']);
}

if (Yii::$app->session->hasFlash('save-user')) {
    echo Alert::widget([
        'id' => 'success-alert',
        'type' => Alert::TYPE_SUCCESS,
        'body' => Yii::$app->session->getFlash('save-user'),
        'closeButton' => [],
        'delay' => 3000
    ]);
}
?>
<div class="row user-card">
	<?= $this->render('_form', [
        'model' => $model,
        'form' => $form
    ]) ?>
</div>