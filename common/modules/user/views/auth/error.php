<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
]);
?>
    <div class="ui-common-form register-form">
        <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
        <h2 class="header"><?= Html::encode($this->title) ?></h2>
        <h5 class="error-text text-center">
            <?= nl2br(Html::encode($message)) ?>
        </h5>

        <div class="form-buttons">
            <a href="<?= \yii\helpers\Url::to(['/']) ?>" class='confirm-btn' data-pjax="0">
                <?= Yii::t('app', 'Go back') ?>
            </a>
        </div>
    </div>
<?php
Pjax::end();