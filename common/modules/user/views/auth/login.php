<?php

/**
 * @var $this yii\web\View
 * @var $model common\modules\user\models\forms\LoginForm
 */


use common\components\SkLangHelper;
use common\modules\user\components\GhostHtml;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


$this->title = Yii::t('app', 'Authorization');
// $this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
    'formSelector' => '#registration-form',
]); ?>
<div class="user-login header-form ui-common-form login-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <h2 class="header"><?= Yii::t('app', 'Authorization') ?></h2>
    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'options' => ['autocomplete' => 'off'],
        'enableClientScript' => false,
        'action' => SkLangHelper::addLangToUrl(Url::toRoute('')),
    ]) ?>

    <?= $form->field($model, 'user_email')->input('email', ['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter email')]) ?>

    <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Enter password'), 'autocomplete' => 'off', 'minlength' => 6, 'maxlength' => 255]) ?>

    <div class="row ui-common-form-link-block">
        <div class="col-xs-7">
            <!-- Запомнити мене -->
            <div class="user-form-checkbox">
                <?= $form->field($model, 'rememberMe', [
                    'template' => '<input type="hidden" name="LoginForm[rememberMe]" value="0"><label class="control-label">{input}<span></span>' . $model->getAttributeLabel('rememberMe')
                ])->input('checkbox', ['value' => 1, 'class' => '', 'checked' => ($model->rememberMe) ? true : false]); ?>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="password-block" style="float: right;">
                <?= Html::a(Yii::t('app', "Forgot password?"), ['/user-management/auth/password-recovery'], ['class' => 'fancy-link']) ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="form-buttons">
        <?= Html::submitButton(Yii::t('app', 'Sign in'), ['class' => 'confirm-btn']) ?>
        <?= Html::a(Yii::t('app', 'Sign up'), ['/user-management/auth/registration'], ['class' => 'confirm-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
Pjax::end();
