<?php

use common\modules\user\Module;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $user
 */

$this->title = Yii::t('app', 'Registration - confirm your e-mail');
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 3000,
    'clientOptions' => []
]);
?>
<div class="registration-wait-for-confirmation ui-common-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <h2 class="header"><?= Yii::t('app', 'Thank you for registering!') ?></h2>
    <div class="text-center">
        <p><?= Yii::t('app', 'Check your e-mail {email} for instructions to activate account', [
                'email' => '<strong>' . $user->email . '</strong>'
            ]) ?></p>
    </div>
</div>
<?php
Pjax::end();
