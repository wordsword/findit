<?php

use common\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\forms\ConfirmEmailForm $model
 */

$this->title = Yii::t('app', 'E-mail confirmation');

$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
]);
?>
<div class="confirm-email ui-common-form register-form">
    <h2 class="header"><?= $this->title ?></h2>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php if (Yii::$app->session->hasFlash('error')) : ?>
                <div class="alert alert-warning text-center">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>

            <?php if ($model->user->confirmation_token === null) : ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'user',
                    'layout' => 'horizontal',
                    'validateOnBlur' => false,
                ]); ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'autofocus' => true]) ?>

                <div class="form-buttons">
                    <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            <?php else : ?>
                <div class="text-center">
                    <?= Yii::t('app', 'E-mail with activation link has been sent to <strong>{email}</strong>.<br>This link will expire in {minutes} min.', [
                        'email' => $model->user->email,
                        'minutes' => $model->getTokenTimeLeft(true),
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>