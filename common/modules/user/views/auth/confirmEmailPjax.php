<?php

use common\modules\user\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\forms\ConfirmEmailForm $model
 */

$this->title = Yii::t('app', 'E-mail confirmation');

$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
]);
?>
    <div class="alert-form-wrap ui-common-form">
        <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
        <div class="text-center">
            <h2 class="header"><?= $this->title ?></h2>
            <div class="form-inner">
                <div class="message">
                    <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <?= Yii::$app->session->getFlash('error') ?>
                    <?php else : ?>
                        <?= Yii::t('app', 'E-mail with activation link has been sent to <strong>{email}</strong>.<br>This link will expire in {minutes} min.', [
                            'email' => $model->user->email,
                            'minutes' => $model->getTokenTimeLeft(true),
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php
Pjax::end();