<?php

use common\modules\user\Module;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $user
 */

$this->title = Yii::t('app', 'E-mail confirmed');
$this->params['breadcrumbs'][] = $this->title;

$url = \yii\helpers\Url::to(['/']);

/*if (Yii::$app->getModule('user-management')->activateAfterRegistration && !Yii::$app->getModule('user-management')->loginAfterRegistration) {
	$url =['/user-management/auth/login'];
}*/

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
]);

?>
    <div class="change-own-password-success ui-common-form register-form">
        <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
        <div class="text-center">
            <h2 class="header"><?= Yii::t('app', 'Thank you!') ?></h2>
            <?= Yii::t('app', 'E-mail confirmed') ?> - <b><?= $user->email ?></b>
            <?php if (!Yii::$app->getModule('user-management')->loginAfterRegistration && Yii::$app->user->isGuest) : ?>
                <p><?= Yii::t('app', 'Now you can log in using your e-mail and password.') ?></p>
            <?php endif; ?>
            <div class="form-buttons">
                <?= Html::a(Yii::t('app', 'Continue'), $url, ['class' => 'confirm-btn', 'style' => 'width: 100%;']); ?>
            </div>
        </div>

    </div>
<?php
Pjax::end();
