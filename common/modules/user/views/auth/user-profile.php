<?php

/* @var $this \yii\web\View */
/* @var $model \common\modules\user\models\User */
/* @var $profile \app\modules\profile\models\Profile */


$this->title = Yii::t('app', 'My Account');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="user-profile-sec">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="user-profile-tabs-controls">
                    <div class="profile-tabs-title"><?= Yii::t('app', 'Private office') ?></div>
                    <ul class="user-profile-tabs-controls-list">
                        <li class="active">
                            <a href="#user-profile-form" class="profile-tab-item"
                               data-toggle="tab"><span>•</span> <?= Yii::t('app', 'Profile') ?></a>
                        </li>
                        <li>
                            <a href="#user-profile-orders" class="profile-tab-item"
                               data-toggle="tab"><span>•</span> <?= Yii::t('app', 'Order list') ?></a>
                        </li>
                        <li>
                            <a class="profile-tab-item user-options-link logout"
                               href="/logout"><span>•</span> <?= Yii::t('app', 'Sign out') ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="user-profile-tabs-content tab-content">
                    <div class="tab-pane fade in active" id="user-profile-form">
                        <div class="section-title"><?= Yii::t('app', 'Personal data') ?></div>
                        <?= $this->render('user-profile/_form', [
                            'model' => $model,
                            'phoneConfirmation' => $phoneConfirmation,
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="user-profile-orders">
                        <div class="section-title"><?= Yii::t('app', 'Order list') ?></div>
                        <?= $this->render('user-profile/_orders', [
                            'dataProvider' => $dataProvider,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
