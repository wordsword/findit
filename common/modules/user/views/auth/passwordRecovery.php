<?php

use common\components\SkLangHelper;
use common\modules\user\components\ReCaptcha;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\forms\PasswordRecoveryForm $model
 */

$this->title = Yii::t('app', 'Password recovery');
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
    'formSelector' => '#registration-form',
]);
?>
<div class="password-recovery ui-common-form register-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <?php if (Yii::$app->session->hasFlash('error')) : ?>
        <div class="text-center">
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php else : ?>
        <h2 class="header"><?= $this->title ?></h2>

        <?php $form = ActiveForm::begin([
            'id' => 'registration-form',
            'enableClientScript' => false,
            'action' => SkLangHelper::addLangToUrl(Url::toRoute('')),
        ]); ?>

        <?= $form->field($model, 'email')->input('email', ['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter email'), 'autofocus' => true, 'maxlength' => 255,])->label(Yii::t('app', 'Email')) ?>

        <div class="form-buttons">
            <?= Html::submitButton(Yii::t('app', 'Recover'), ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <?php
    endif;
    ?>
</div>
<? Pjax::end();