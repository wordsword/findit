<?php

use common\modules\user\components\GhostHtml;
use common\modules\user\models\User;
use common\modules\user\Module;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $model
 * @var yii\bootstrap\ActiveForm $form
 */

\common\modules\user\ProfileAsset::register($this);

$phoneInputTmpl = '{input}';
if (!$model->phone_confirmed) {
    $phoneInputTmpl = '<div class="input-group">{input}<span class="input-group-addon">';
    $phoneInputTmpl .= Html::a(FA::icon('check'), Url::toRoute(['/user-management/auth/profile-confirm-phone']), [
        'title' => Yii::t('app', 'Confirm'),
        'class' => 'user-options-link fancy-link'
    ]);
    $phoneInputTmpl .= '</span></div>';
}
$emailInputTmpl = '{input}';
if (!$model->email_confirmed) {
    $emailInputTmpl = '<div class="input-group">{input}<span class="input-group-addon">';
    $emailInputTmpl .= Html::a(FA::icon('check'), Url::toRoute(['/user-management/auth/confirm-email']), [
        'title' => Yii::t('app', 'Confirm'),
        'class' => 'user-options-link fancy-link'
    ]);
    $emailInputTmpl .= '</span></div>';
}
?>
<div class="user-form">
    <?php Pjax::begin([
        'id' => 'pjax-profile-container',
        'formSelector' => '#user',
        'timeout' => 3000,
    ]);
    if (Yii::$app->session->hasFlash('savedSuccess')) {
        echo Html::tag('div', Yii::$app->session->getFlash('savedSuccess'), ['class' => 'alert alert-success']);
    }
    $form = ActiveForm::begin([
        'id' => 'user',
        'enableClientScript' => false
    ]); ?>
    <?= Html::hiddenInput($model->formName() . '[country]', $model->country, ['id' => 'country']); ?>

    <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app', 'Enter name and surname'), 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'phone', [
        'inputOptions' => [
            'data-phone-confirmation' => $phoneConfirmation,
            'data-url' => Url::toRoute(['/user-management/auth/profile-confirm-phone']),
            'class' => 'phone-confirmation form-control',
        ],
        'inputTemplate' => '<div class="active-block ' . ($model->phone_confirmed ? 'confirmed' : 'unconfirmed') . '" title="' . ($model->phone_confirmed ? Yii::t('app', 'Confirmed') : Yii::t('app', 'Unconfirmed')) . '">' . $phoneInputTmpl . '</div>'
    ])->input('tel', ['autocomplete' => 'off', 'onkeyup' => "checkCurr(this)", 'placeholder' => Yii::t('app', 'Enter phone')]) ?>
    <?= $form->field($model, 'email', [
        'inputTemplate' => '<div class="active-block ' . ($model->email_confirmed ? 'confirmed' : 'unconfirmed') . '" title="' . ($model->email_confirmed ? Yii::t('app', 'Confirmed') : Yii::t('app', 'Unconfirmed')) . '">' . $emailInputTmpl . '</div>'
    ])->input('email', ['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter email')]) ?>

    <div class="form-group user-profile-form-button">
        <?= Html::submitButton(
            Yii::t('app', 'Save'), [
                'class' => 'profile-btn-save ',
                'style' => 'margin-right: 10px;'
            ]
        ) ?><?= Html::a(
            \rmrevin\yii\fontawesome\FA::icon('unlock-alt ') .
            Yii::t('app', 'Change password'),
            ['/user-management/auth/change-own-password'],
            [
                'class' => 'profile-btn-change change-password', 'data-pjax' => 0
            ])
        ?>
    </div>
    <?php
    ActiveForm::end();
    Pjax::end();
    ?>
</div>
