<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\shop\models\Document */

$this->title = Yii::t('app', 'Order') . ' №' . $model->primaryKey . ', ' . Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Account'), 'url' => ['/user-management/auth/profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="profile-order-sec">

    <div class="container">

        <div class="order-info">
            <div class="row">
                <div class="col-md-12">
                    <p class="order-title"><span class="order-info-options main-options"><?= Yii::t('app', 'Order') ?>
                            №<?= Html::encode($model->primaryKey) ?></span>,
                        <?= Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm') ?>
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-block">
                        <p><span
                                class="order-info-options"><?= Yii::t('app', 'User') ?></span>: <?= Html::encode($model->name) ?>
                        </p>
                        <p><span
                                class="order-info-options"><?= Yii::t('app', 'Phone number') ?></span>: <?= Html::encode($model->phone) ?>
                        </p>
                        <p><span
                                class="order-info-options"><?= Yii::t('app', 'E-mail') ?></span>: <?= Html::encode($model->email) ?>
                        </p>
                        <?php if (!empty($model->info)) : ?>
                            <p><?= Yii::t('app', 'Info') ?>: <?= Html::encode($model->info) ?></p>
                        <?php endif; ?>
                        <tr>
                            <td><?= Yii::t('app', 'Payment') ?></td>
                            <td><?= Html::encode($model->payment->title) . (($model->payment->description) ? '. <br>' . Html::encode($model->payment->description) : "")?></td>
                        </tr>
                        <p><span
                                class="order-info-options"><?= Yii::t('app', 'Delivery address') ?></span>: <?= Html::encode($model->delivery_address) ?>
                        </p>
                    </div>
                    <div class="sublotal">
                        <p class="main-options"><?= Yii::t('app', 'Sum') ?>:
                            <span>
                                <?= Yii::$app->formatter->asDecimal($model->totalSum, 2) ?>
                                <?= Html::encode(Yii::$app->shopSettings->currency) ?>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="cart-table-body">

                <table class="responsive">
                    <thead>
                    <tr>
                        <td class="th-product"><?= Yii::t('app', 'Product') ?></td>
                        <td class="th-price"><?= Yii::t('app', 'Price') ?></td>
                        <td class="th-quantity"><?= Yii::t('app', 'Quantity') ?></td>
                        <td class="th-sum"><?= Yii::t('app', 'Sum') ?></td>
                    </tr>
                    </thead>
                    <?= yii\widgets\ListView::widget([
                        'id' => 'cart-list',
                        'dataProvider' => $dataProvider,
                        'itemView' => '@common/modules/user/views/auth/user-profile/_gridItem',
                        'layout' => '<tbody>{items}</tbody>',
                        'itemOptions' => [
                            'tag' => false,
                        ],
                    ]); ?>
                </table>

        </div>

    </div>

</section>
