<?php

use app\modules\shop\models\Document;
use rmrevin\yii\fontawesome\FA;
use app\modules\shop\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;


$product = Product::findOne($model->product_id);

?>
<tr class="product-item">
<td class="td-image" data-label="<?= Yii::t('app', 'Product') ?>">
    <div class="cart-td-content">
        <div class="cart-product-image">
            <img src="<?= $product->getImageByOptions($model->lineOptions, 'thumbs') ?>" alt="">
        </div>
        <div class="cart-product-options">
            <a href="<?= $product->url ?>" target="_blank">
                <span class="cart-product-name"><?= Html::encode($product->title) ?></span>
            </a>
            <p class="cart-info"><span><?= Yii::t('app', 'Article') ?>:</span> <?= Html::encode($product->article) ?>
            </p>
            <?php
            foreach ($model->lineOptions as $option) {
                echo Html::beginTag('p', ['class' => 'cart-info']);
                echo Html::tag('span', Html::encode($option->attrItem->title) . ': ');
                echo Html::encode($option->valueItem->title);
                echo Html::endTag('p');
            }
            ?>
        </div>
    </div>
</td>
<td data-label="<?= Yii::t('app', 'Price') ?>">
    <div class="cart-td-content cart-price">
        <p>
            <?= Yii::$app->formatter->asDecimal($model->price, 2) ?>
            <?= Html::encode(Yii::$app->shopSettings->currency) ?>
        </p>
    </div>
</td>

<td data-label="<?= Yii::t('app', 'Quantity') ?>">
    <div class="cart-td-content">
        <?php
            echo Html::beginTag('div', ['class' => 'quantity-info first']);
            echo Html::tag('span', $product->onlyPacks ? Html::encode($model->number_of_packs) . ' ' . Yii::t('app', 'pack') : Html::encode($model->quantity) . ' ' . Yii::t('app', 'pcs'));
            echo Html::endTag('div');
        if ($product->onlyPacks) :
            echo Html::beginTag('div', ['class' => 'quantity-info']);
            echo Html::tag('span', Html::encode($model->quantity)) . ' ' . Yii::t('app', 'pcs');
            echo Html::endTag('div');
        endif;?>
    </div>
</td>

<td data-label="<?= Yii::t('app', 'Sum') ?>">
    <div class="cart-td-content cart-price">
        <p>
            <?= Yii::$app->formatter->asDecimal($model->price * $model->quantity, 2) ?>
            <?= Html::encode(Yii::$app->shopSettings->currency) ?>
        </p>
    </div>
</td>
</tr>