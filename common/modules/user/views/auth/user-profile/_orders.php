<?php

use app\components\SkGridView;
use app\modules\main\models\Document;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;

\yii\jui\JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\main\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$gridID = 'document-grid';

$PHT = <<< HTML
    <h3 class="panel-title">
        {$this->title}
        <span class="summary">
            <span class="label label-success">{$dataProvider->getTotalCount()}</span>
        </span>
    </h3>
    <div class="clearfix"></div>
HTML;
$PFT = <<< HTML
    <div class="kv-panel-pager">
        <input type="hidden" value="{$dataProvider->getPagination()->pageSize}" name="per-page" />
        {pager}
    </div>
    <div class="clearfix"></div>
HTML;
$PBT = <<< HTML
    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
        {toolbar}
    </div>
    {before}
    <div class="clearfix"></div>
HTML;

$query = $dataProvider->query;

// echo "<pre>";
// print_r(clone($query));
// echo "</pre>";
// exit();

?>
<div class="user-profile-orders user-profile-tab-inner js-user-profile-tab-inner">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <?= SkGridView::widget([
                'id' => $gridID,
                'dataProvider' => $dataProvider,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return [
                        'style' => 'text-align:left;'
                    ];
                },
//                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterUrl' => $url,
//                'showHeader'=>false,
                'panelHeadingTemplate' => $PHT,
                'panelFooterTemplate' => $PFT,
                'panelBeforeTemplate' => $PBT,
                'showPageSummary' => true,
                'toolbar' => [],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container-documents',
                    ]
                ],
                'pager' => [
                    'class' => 'app\components\SkPager',
                ],

                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
//                'responsiveWrap'=>true,
                'responsiveWrap' => false,
                'panel' => [
                    'type' => SkGridView::TYPE_DEFAULT,
                    'heading' => false,
                    'footer' => true,
                    'after' => false,
                    'beforeOptions' => ['style' => 'padding: 0;'],
                ],
                'persistResize' => false,
                'filterPosition' => SkGridView::FILTER_POS_BODY,
                'columns' => [
                    [
                        'attribute' => 'number',
                        'headerOptions' => ['style' => 'width:6%; text-align:center;'],
                        'contentOptions' => [
                            'data-title' => Yii::t('app', 'ID'),
                            'class' => 'kv-align-center kv-align-middle'
                        ],
                        'header' => Yii::t('app', 'Number'),
                    ],
                    [
                        'attribute' => 'created_at',
                        'headerOptions' => ['style' => 'width:44%; text-align:center;'],
                        'contentOptions' => [
                            'data-title' => Yii::t('app', 'Date'),
                            'style' => 'text-align:center;'
                        ],
                        'header' => Yii::t('app', 'Date'),
                        'content' => function ($model) {
                            return Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.y HH:mm');
                        }
                    ],
                    [
                        'attribute' => 'sum',
                        'headerOptions' => ['style' => 'width:44%; text-align:center;'],
                        'pageSummary' => Html::tag('span', number_format(((clone($query))->select('SUM(summ)')->scalar()), 2) . ' ' . Yii::t('app', 'uah'), ['class' => 'total-quantity']),
                        'pageSummaryOptions' => ['style' => 'text-align: right'],
                        'contentOptions' => [
                            'data-title' => Yii::t('app', 'Sum'),
                            'style' => 'text-align: center;',

                        ],
                        'header' => Yii::t('app', 'Sum'),
                        'content' => function ($model) {
                            return number_format($model->summ, 2) . ' ' . Yii::t('app', 'uah');
                        }
                    ],
                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'contentOptions' => [
                            'class' => 'action-column',
                        ],
                        'headerOptions' => [
                            'rowspan' => 1,
                        ],
                        'width'=>'6%',
                        'template' => '{visible}',
                        'buttons' => [
                            'visible' => function ($url, $model, $key) {
                                return Html::a(
                                    FA::icon('file-text-o'),
                                    Url::toRoute(['/receipt/'. Html::encode($model->id)]),
                                    [
                                        'class' => 'btnview',
                                        'data-pjax' => 0,
                                        'title' => Yii::t('app', 'View'),
                                    ]
                                );
                            },
                        ]
                    ]
                ]
            ]); ?>
        </div>
    </div>
</div>
