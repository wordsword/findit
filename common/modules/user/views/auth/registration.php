<?php

use common\components\SkLangHelper;
use common\modules\user\components\ReCaptcha;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\forms\RegistrationForm $model
 */

$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>

<? Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
    'formSelector' => '#registration-form',
]);
?>
<div class="user-registration ui-common-form register-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <h2 class="header"><?= $this->title ?></h2>
    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'enableClientScript' => false,
        'action' => SkLangHelper::addLangToUrl(Url::to('')),
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Enter name and surname'), 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'username')->textInput(['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter nickname')]) ?>

    <?= $form->field($model, 'email')->input('email', ['autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter email')]) ?>

    <?= $form->field($model, 'phone')->input('tel', ['autocomplete' => 'off', 'onkeyup' => "checkCurr(this)", 'placeholder' => Yii::t('app', 'Enter phone')]) ?>

    <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Enter password'), 'minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off']) ?>
    
    <?= $form->field($model, 'repeat_password')->passwordInput(['placeholder' => Yii::t('app', 'Repeat password'), 'minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off']) ?>

    <div class="form-buttons">
        <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
Pjax::end(); ?>