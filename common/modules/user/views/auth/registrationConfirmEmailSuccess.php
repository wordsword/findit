<?php

use common\modules\user\Module;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $user
 */

$this->title = Yii::t('app', 'E-mail confirmed');
$this->params['breadcrumbs'][] = $this->title;

$url = \yii\helpers\Url::to(['/']);

/*if (Yii::$app->getModule('user-management')->activateAfterRegistration && !Yii::$app->getModule('user-management')->loginAfterRegistration) {
    $url = ['/user-management/auth/login'];
}*/

?>
<div class="change-own-password-success ui-common-form register-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <h2 class="header"><?= Yii::t('app', 'Thank you for registering!') ?></h2>
    <div class="text-center">
        <h4><?= Yii::t('app', 'E-mail confirmed') ?> - <b><?= $user->email ?></b></h4>
        <?php if (!Yii::$app->getModule('user-management')->loginAfterRegistration) : ?>
            <p><?= Yii::t('app', 'Now you can log in using your e-mail and password.') ?></p>
        <?php endif; ?>

    </div>
    <div class="form-buttons">
        <?= Html::a(Yii::t('app', 'Continue'), $url, ['class' => 'confirm-btn', 'style' => 'width: 100%;', 'data-pjax' => 0]); ?>
    </div>
</div>