<?php

use common\modules\user\Module;
use yii\helpers\Html;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('app', 'Change own password');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="pjax-block">
    <?php
    Pjax::begin([
        'id' => 'pjax-container',
        'timeout' => 5000,
    ]);
    ?>
    <div class="ui-common-form ui-common-form-password-recowery register-form">
        <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
        <h2 class="header"><?= Yii::t('app', 'Password has been changed!') ?></h2>
        <?php if (Yii::$app->user->isGuest) : ?>
            <div class="text-center">
                <p><?= Yii::t('app', 'Now you can log in using your phone number or e-mail and password.') ?></p>
            </div>

            <div class="form-buttons">
                <?= Html::a(Yii::t('app', 'Sign in'), ['/user-management/auth/login'], ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
            </div>
        <?php endif; ?>
    </div>
    <?php Pjax::end(); ?>
</div>