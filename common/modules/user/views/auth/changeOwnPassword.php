<?php

use common\components\SkLangHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\forms\ChangeOwnPasswordForm $model
 */

$this->title = Yii::t('app', 'Change own password');

$this->params['breadcrumbs'][] = $this->title;
?>
<?php
Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
    'formSelector' => '#registration-form',
]);
?>
<div class="change-own-password ui-common-form register-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <?php if (Yii::$app->session->hasFlash('success')) : ?>
        <div class="text-center">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php else : ?>
        <h2 class="header"><?= $this->title ?></h2>
        <div class="user-form">
            <?php $form = ActiveForm::begin([
                'id' => 'registration-form',
                'enableClientScript' => false,
                'action' => Url::toRoute($action),
            ]); ?>

            <?= $form->field($model, 'current_password')->passwordInput(['minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter current password')]) ?>

            <?= $form->field($model, 'password')->passwordInput(['minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Enter new password')]) ?>

            <?= $form->field($model, 'repeat_password')->passwordInput(['minlength' => 6, 'maxlength' => 255, 'autocomplete' => 'off', 'placeholder' => Yii::t('app', 'Repeat password')]) ?>

            <div class="form-buttons">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
</div>
<?php
    endif;
    Pjax::end(); ?>