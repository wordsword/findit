<?php

use common\modules\user\Module;
use yii\widgets\Pjax;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('app', 'Password recovery');
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 5000,
]);
?>
    <div class="password-recovery-success ui-common-form register-form">
        <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
        <h2 class="header"><?= $this->title ?></h2>
        <div class="text-center">
            <p>
                <?= Yii::t('app', 'Check your E-mail for further instructions.') ?>
            </p>
        </div>
        <div class="form-buttons">
            <?= Html::a(Yii::t('app', 'Authorization'), ['/user-management/auth/login'], ['class' => 'confirm-btn', 'style' => 'width: 100%;']) ?>
        </div>
    </div>
<?php
Pjax::end();
