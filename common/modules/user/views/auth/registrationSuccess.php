<?php

use common\modules\user\Module;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\user\models\User $user
 */

$this->title = Yii::t('app', 'E-mail confirmed');
$this->params['breadcrumbs'][] = $this->title;

$url = '/catalogue';

Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 3000,
]);
?>

<div class="registration-success header-form ui-common-form register-form">
    <a data-fancybox-close class='modal-form-close' data-pjax='0'></a>
    <h2 class="header"><?= Yii::t('app', 'Thank you for registering!') ?></h2>
    <div class="form-buttons">
        <?= Html::a(Yii::t('app', 'Continue'), $url, ['data-pjax' => 0,'class' => 'confirm-btn', 'style' => 'width: 100%;']); ?>
    </div>
</div>

<?php Pjax::end(); ?>