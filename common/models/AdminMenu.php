<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use Yii;
use common\components\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use common\components\SkLangHelper;
use common\components\CategoryBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $object
 * @property integer $object_id
 * @property string $color
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $visible
 * @property integer $position
 * @property string $image
 */
class AdminMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_menu';
    }

    public static function langTableName()
    {
        return 'admin_menu_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id',], 'integer'],
            [['sort_order', 'owner'],'default','value' => 0],
            [['sort_order', 'owner'],'integer'],
            [['title', 'icon'], 'required'],
            [['title','url','icon', 'counter'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent menu item'),
            'url' => Yii::t('app', 'Link'),
            'sort_order' => Yii::t('app', 'Sort order'),
            'title'=> Yii::t('app','Title'),
            'icon' => Yii::t('app', 'Icon')
        ];
    }


    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => SkLangHelper::getLangsList(),
                'defaultLanguage' => SkLangHelper::getDefaultLanguage(),
                'langForeignKey' => 'owner_id',
                'tableName' => self::langTableName(),
                'attributes' => [
                    'title',
                ]
            ],
        ];
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id'=>'parent_id']);
    }

    public function getChilds()
    {
        return $this->hasMany(self::className(), ['parent_id'=>'id']);
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

}
