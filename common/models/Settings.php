<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use common\models\SkDynamicModel;
use common\components\SkLangHelper;
use common\components\PurifyTextBehavior;
use omgdef\multilingual\MultilingualQuery;
use common\components\MultilingualBehavior;


/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property string $field_key
 * @property string $field_value
 */
class Settings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    public static function langTableName()
    {
        return '{{%settings_lang}}';
    }

    public static function valueTableName()
    {
        return '{{%settings_values}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['field_key', 'string', 'max' => 255],
            ['field_value', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field_key' => Yii::t('app', 'Key'),
            'field_value' => Yii::t('app', 'Value'),
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => SkLangHelper::getLangsList(),
                'defaultLanguage' => SkLangHelper::getDefaultLanguage(),
                'langForeignKey' => 'owner_id',
                'tableName' => self::langTableName(),
                'attributes' => [
                    'field_value',
                ]
            ],
            'PurifyText' => [
                'class' => PurifyTextBehavior::className(),
                'sourceAttribute' => ['field_value'],
                'destinationAttribute' => ['field_value'],
                'purifierOptions' => [
                    'Attr.AllowedRel' => ['nofollow'],
                    'HTML.SafeObject' => true,
                    'Output.FlashCompat' => true,
                ],
            ],
        ];
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    public static function getModel()
    {
        $fields = self::getLangAttributes();
        $attributes = self::getSettingsAttributes();
        $langAttributes = [];
        foreach (SkLangHelper::getLangs() as $lang) {
            unset($suffix);
            if (!$lang['deflang']) {
                $suffix = '_' . Html::encode($lang['lang_id']);
            }
            foreach ($fields as $field) {
                $langAttributes[] = $field . $suffix;
            }
        }
        $model = new SkDynamicModel(ArrayHelper::merge($attributes, $langAttributes));
        $model->setFormName('GeneralSettings');
        // $model->addRule($langAttributes, 'required', ['message' => Yii::t('app', 'You need to specify values for all languages.')]);
        // $model->addRule($langAttributes, 'string', ['message' => Yii::t('app', 'Attribute value must be a string.')]);
        // $model->addRule($attributes, 'string', ['message' => Yii::t('app', 'Attribute value must be a string.')]);
        // $model->addRule('email', 'email');

        // $model->addRule('max_return_days', 'required');
        $model->setAttributeLabels([
            // 'companyName' => Yii::t('app', 'Company name'),
        ]);

        foreach ($fields as $field) {
            $settings = self::find()->andWhere(['field_key' => $field])->multilingual()->one();
            foreach (SkLangHelper::getLangs() as $lang) {
                unset($suffix);
                if (!$lang['deflang']) {
                    $suffix = '_' . Html::encode($lang['lang_id']);
                }
                $attribute = $field . $suffix;
                $value = 'field_value' . $suffix;
                $model->$attribute = $settings->$value;
            }
        }

        foreach ($attributes as $attribute) {
            $settings = (new Query())->from(self::valueTableName() . ' val')->innerJoin(self::tableName() . ' main', 'val.owner_id = main.id')->select('main.field_key, val.value')->where(['main.field_key' => $attribute])->one();
            $model->$attribute = $settings['value'];
        }


        return $model;
    }

    public static function getLangAttributes()
    {
        return [
        ];
    }

    public static function getSettingsAttributes()
    {
        return [
        ];
    }

    public static function saveModel(SkDynamicModel $model)
    {
        $attributes = self::getSettingsAttributes();
        $langAttr = self::getLangAttributes();

        foreach ($langAttr as $attribute) {
            $settings = self::find()->andWhere(['field_key' => $attribute])->multilingual()->one();
            if ($settings) {
                foreach (SkLangHelper::getLangs() as $lang) {
                    unset($suffix);
                    if (!$lang['deflang']) {
                        $suffix = '_' . Html::encode($lang['lang_id']);
                    }
                    if (in_array($attribute, $langAttr)) {
                        $field_key = $attribute . $suffix;
                    } else {
                        $field_key = $attribute;
                    }
                    $field_value = 'field_value' . $suffix;
                    $settings->$field_value = $model->$field_key;
                }
                $settings->save(false);
            }
        }

        foreach ($attributes as $attribute) {
            $exists = (new Query())->from(self::valueTableName() . ' val')->innerJoin(self::tableName() . ' main', 'val.owner_id = main.id')->select('main.field_key, val.value')->where(['main.field_key' => $attribute])->count() > 0;

            $owner_id = self::find()->select('id')->where(['field_key' => $attribute])->scalar();
            if (intval($owner_id) !== 0) {
                if ($exists) {
                    Yii::$app->db->createCommand("UPDATE settings_values SET value=:value WHERE owner_id=:owner", [':owner' => $owner_id, ':value' => $model->$attribute])->execute();
                } else {
                    Yii::$app->db->createCommand("INSERT INTO `settings_values` (`id`, `owner_id`, `value`) VALUES (NULL, :owner, :value)", [':owner' => $owner_id, ':value' => $model->$attribute])->execute();
                }
            }
        }
    }
}
