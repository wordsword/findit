<?php

namespace common\models;

use Yii;
use yii\web\View;

/**
 * This is the model class for table "script".
 *
 * @property int $id
 * @property string $title
 * @property string $script
 * @property int $position
 */
class Script extends \yii\db\ActiveRecord
{
    public static $positions = [
        View::POS_HEAD => 'HEAD',
        View::POS_BEGIN => 'BEGIN',
        View::POS_END => 'END',
        View::POS_LOAD => 'LOAD',
        View::POS_READY => 'READY'
    ];


    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'script';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'script', 'position'], 'required'],
            [['script'], 'string'],
            [['position', 'enabled'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
            ['enabled', 'default', 'value' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'script' => Yii::t('app', 'Script'),
            'position' => Yii::t('app', 'Position'),
        ];
    }


    public static function registerScript($name, $view)
    {
        $script = self::findOne(['title' => $name]);
        $view->registerJs($script->script, $script->position, $script->title);
    }


    public static function registerEnbledScripts($view)
    {
        $scripts = self::findAll(['enabled' => 1]);
        foreach ($scripts as $script) {
            $view->registerJs($script->script, $script->position, $script->title);
        }
    }

}
