<?php
return [
    'language' => 'ru',
    'name' => 'LiteMarket',
    'timeZone' => 'Europe/Kiev',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 360 * 24 * 60 * 60],
            'timeout' => 360 * 24 * 60 * 60, //session expire
            'useCookies' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache'
        ],
        'dbCache' => [
            'class' => 'yii\caching\DbCache',
            'keyPrefix' => 'skapp',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'sourceMessageTable' => 'source_message',
                    'messageTable' => 'message',
                ],
                // 'kvdrp' => [
                //     'class' => 'yii\i18n\PhpMessageSource',
                //     'basePath' => '@vendor/kartik-v/yii2-date-range/messages',
                //     'forceTranslation' => true
                // ]
                'kvdrp' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@kvdrp/messages',
                    'forceTranslation' => true
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'hasko2200@gmail.com',//base64_decode('aGFza28yMjAwQGdtYWlsLmNvbQ=='),
                'password' => 'fivjigugescuosaz',//base64_decode('MjgwNzIwMDBxd2VydHk='),
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [ 
                    'ssl' => [ 
                        'allow_self_signed' => true, 
                        'verify_peer' => false, 
                        'verify_peer_name' => false, 
                    ], 
                ]
            ],
        ],
        'settings' => [
            'class' => 'common\components\Settings',
        ],
        'userSettings' => [
            'class' => 'common\components\UserSettings',
        ],
        'smsClient' => [
            'class' => 'common\components\AlphaSMS',
        ],
        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyBbyijDFB5AYbHCQyECxQjrwP48rp9ikNs',
                        'language' => 'ru',
                        'version' => '3.1.18'
                    ]
                ]
            ]
        ],
    ],
    'as beforeRequest' => [
        'class' => 'common\components\SkBeforeRequestBehavior'
    ]
];
