<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'cacheDuration' => 43200,
    'siteDomain' => 'parfumestyle.com.ua'
];
